<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id_User = $_SESSION['id'];
if($id_User==2099){
    $id_User=2087;
}
$today = date("Y-m-d");
$fecha1 = "";
if (isset($_GET['F1'])) {
    $fecha1 = $_GET['F1'];
    $res = explode("-", $fecha1);
    $fecha1 = $res[2] . "-" . $res[0] . "-" . $res[1];
}
$fecha2 = "";
if (isset($_GET['F2'])) {
    $fecha2 = $_GET['F2'];
    $res = explode("-", $fecha2);
    $fecha2 = $res[2] . "-" . $res[0] . "-" . $res[1];
}
$queryHours = "SELECT * FROM `registroclock` where iduser=$id_User and date BETWEEN  '$fecha1' and '$fecha2'";

$resultHours = mysql_query($queryHours);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="datatables/jquery.dataTables.min.js"></script>
        <script src="datatables/dataTables.bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>


        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }
        </style>


    </head>

    <body >
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();

            });

        </script>
        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>


            <div class="container">
                <div class="row" hidden>
                    <div class="panel panel-primary">
                        <div class="panel-heading">Hours Report 1</div>
                        <div class="panel-body">
                            <form action="CheckTime.php">
                                <div class="row">
                                
                                    <div class="col-lg-3">
                                        <input type="submit" href="#" class="btn btn-lg btn-success"> 
                                    </div>
                                </div>
                            </form>

                        </div>
                        
                  
                    </div>
                </div>

                <div class="row">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Hours Report</div>
                        <div class="panel-body">
                            <form action="CheckTime.php">
                                <div class="row">
                                    <div class="col-lg-2 col-lg-offset-2" style="padding: 10px 16px;">
                                        <div class="form-group">
                                            <div class='input-group date' >
                                                <input id='datepicker' type='text' name="F1" class="form-control" placeholder="Star Date"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2" style="padding: 10px 16px;">
                                        <div class="form-group">
                                            <div class='input-group date' >
                                                <input type='text' class="form-control" name="F2" id='datepicker2' placeholder="End Date"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="submit" href="#" class="btn btn-lg btn-success"> 
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="row">
                            <?
                            $totalHours = 0;
                            while ($row2 = mysql_fetch_assoc($resultHours)) {
                                $horaIn = $row2['hora'];
                                $horaOut = $row2['horaout'];
                                if ($horaOut != Null && $horaOut != "0000-00-00 00:00:00") {
                                    $minutos = (strtotime($horaIn) - strtotime($horaOut)) / 60;
                                    $minutos = abs($minutos);
                                    $minutos = floor($minutos);
                                    $hours = $minutos / 60;
                                    $hours = round($hours, 2);
                                    $totalHours += $hours;
                                } else {
                                    $hours = 0;
                                }
                                ?>

                                <div class="col-lg-3  container-fluid col-lg-offset-2">
                                    <a href="#" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-import"></span> <? echo $horaIn; ?></a>
                                </div>
                                <div class="col-lg-2 container-fluid ">
                                    <a href="#" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-import"></span>Hours: <? echo $hours; ?></a>
                                </div>
                                <div class="col-lg-3 container-fluid">
                                    <a href="#" class="btn btn-lg btn-danger"><span class="glyphicon glyphicon-import"></span> <? echo $horaOut; ?></a>
                                </div>


                            <? } ?>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 container-fluid ">
                                <a href="#" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-import"></span>Total Hours: <? echo $totalHours; ?></a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <script type="text/javascript">
            $("#datepicker").datepicker({dateFormat: 'mm-dd-yy'});
            $("#datepicker2").datepicker({dateFormat: 'mm-dd-yy'});

        </script>


        <script type="text/javascript">


            function SearchHours() {
                var f1 = $("#datepicker").datepicker('getDate').formatDate('yy-mm-dd');
                var f2 = $("#datepicker2").datepicker('getDate');
                var fecha1 = $.datepicker.formatDate('yy-mm-dd', f1);
                var fecha2 = $.datepicker.formatDate('yy-mm-dd', f2);

                $.post("CheckTime.php", {fecha1, fecha2}, function (data) {

                });
            }
            $('li').click(function () {
                $('.active').removeClass('active');
                $(this).addClass('active');
            });
            //class="active"
            function ChangeValue(number) {


                var value = $("#chekCashier" + number + ":checked").val();

                if (value == "on") {
                    value = 1;
                } else {
                    value = 0;
                }
                $.post("UpdateCashier.php", {number, value}, function (data) {
                    if (data.status == "success") {
                        alert("Complete");
                    }
                });
            }

            function ChangeValue2(number) {

                var value = $("#cheklog" + number + ":checked").val();

                if (value == "on") {
                    value = 1;
                } else {
                    value = 0;
                }
                $.post("UpdateCashier2.php", {number, value}, function (data) {
                    if (data.status == "success") {
                        alert("Complete");
                    }
                });


            }
            function borrar(id) {
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    console.log("#form-" + id);
                    $("#form-" + id).submit();
                });
            }


            function viewholder(id) {
                window.open("details.php?n=" + id);
            }
            function cargarT() {

                var agencia = $("#selag").val();
                $("#dt_searchbox").val("");
                $("#tabcont").empty();
                $.post("buscaT22.php", {ag: agencia},
                        function (data) {
                            $('#tabcont').html(data);
                        });
            }
        </script>
    </body>
</html>
<?

function resgresanombrecompania3($val) {
    $sql = "SELECT * FROM `companyes` where id=" . $val;

    $res = mysql_query($sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['nombre'];
    }
}

function regresauser($val) {


    $sql = "SELECT * FROM `users` where id=" . $val;

    $res = mysql_query($sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['user'];
    }
}

function resgresanombrecashier($val) {
    if (empty($val))
        return "-";


    $sql = "SELECT * FROM cashier where id=" . $val;

    $res = mysql_query($sql);
    $vr = "-";
    while ($re = mysql_fetch_assoc($res)) {
        $vr = $re['name'];
    }
    return $vr;
}
?>
