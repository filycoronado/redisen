<?php
        include("inc/dbconnection.php");
        require('./fpdf.php');
        $id_client=$_GET['id_client'];
        $id_ticket=$_GET['tik'];
       
        $sql_tiket = "SELECT cv.* ,comp.nombre as company_name ,us.user as agent FROM `cutvone` as cv join companyes as comp on comp.id=cv.company join users as us on cv.id_usuario=us.id where cv.id=$id_ticket";
        $result_tiket= mysql_query($sql_tiket);
        $tiket= mysql_fetch_assoc($result_tiket);
       
        $sql_client = "SELECT * FROM `UserManagement` where id=$id_client";
        $result_client= mysql_query($sql_client);
        $client= mysql_fetch_assoc($result_client);
        $id_agency=$tiket['agency'];
        
        $sql_agency = "SELECT * FROM `agency` where id=$id_agency";
        $result_agency= mysql_query($sql_agency);
        $agency= mysql_fetch_assoc($result_agency);
        $method = "";
        $transactionType = "Regular";
        if ($tiket['companytype'] == 1) {
            $method = "Cash";
        } else if ($tiket['companytype'] == 2) {
            $method = "Card";
        }
        if ($tiket['typeDO'] == 4) {
            $transactionType = "Endorse";
        }
        $pdf = new FPDF();

        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0);
        $pdf->SetDrawColor(190, 30, 45);
        $pdf->SetLineWidth(.9);
        
        $pdf->Image( './img/logo_blc.png', 10, 10, 25, 25, 'png', '');
        $pdf->Image( './img/website2.png', 50, 160, 100, 50, 'png', '');
        
        $pdf->SetXY(10, 10);
        $pdf->SetFont('Arial', 'B', 15);
        $pdf->Text(45, 24, "Buy Low Cost Insurance");
        $pdf->Text(45, 30, "You' ll Never Drive Alone");
        $pdf->Text(150, 24, "INVOICE #" .$id_ticket);
        $pdf->Line(35, 25, 190, 25);
        $pdf->Ln(65);

        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Text(10, 50, $agency['nombre']);
        $pdf->Text(10, 55, "Phone:" . $agency['tel']);
        $pdf->Text(10, 60, "Fax:" . $agency['fax']);
        
        $date=date("m-d-Y", strtotime($tiket['fecha']));
        $upername= strtoupper($tiket['agent']);
        $pdf->Text(150, 50, "Date ".$date." ".$tiket['horat']);
        $pdf->Text(150, 55, $tiket['company_name']);
        $pdf->Text(150, 60, "PN:".$tiket['policynumber']);
        $pdf->Text(150, 65,"User: ".$upername);
        
         $pdf->SetFont('Arial', 'B', 13);
        $pdf->Text(8, 150, "Effective July 1 2020 Arizona Mandates To Increase Policy Limits To 25/50/15");
        $pdf->Text(8, 160, "Efectivo  Julio 1 2020 Arizona Requiere Incremento En Su Poliza A Limites De  25/50/15");
        
        $pdf->Text(10, 290, "Thank You For You Business! / buylowins.com");

// $pdf->Cell(40,7,'Report'.$f1."/".$f2,0, 1 , ' R ');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetFillColor(190, 30, 45);
        $pdf->SetTextColor(0);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(.5);

        $pdf->Cell(20, 5, "Customer : ", 0, 0, 'L');
        $pdf->Cell(25, 5, $client['nombre'], 0, 1, 'L');
        $pdf->Cell(25, 5, $client['address'], 0, 1, 'L');
        $pdf->Cell(25, 5,$client['city'] . " " . $client['zipcode'], 0, 1, 'L');

        $pdf->Ln(25);
        $pdf->SetTextColor(255);
        $pdf->Cell(130, 5, "Description", 1, 0, 'C', true);
        $pdf->Cell(50, 5, "Amount", 1, 1, 'C', true);
        $pdf->SetTextColor(0);
        $pdf->Cell(130, 6, $transactionType . "/Payment" . "(" . $method . ")", 1, 0, 'C');
        $pdf->Cell(50, 6, "$" . $tiket['total'], 1, 1, 'C');
        $pdf->Cell(130, 6, "Sub Total:", 0, 0, 'R');
        $pdf->Cell(50, 6, "$" . $tiket['total'], 1, 1, 'C');
//        $pdf->Cell(130, 6, "Fee:", 0, 0, 'R');
//        $pdf->Cell(50, 6, "$0", 1, 0, 'C');
        $pdf->Cell(130, 6, "Total:", 0, 0, 'R');
        $pdf->Cell(50, 6, "$" . $tiket['total'], 1, 0, 'C');

        return $pdf->Output();
   