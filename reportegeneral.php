<?
session_start();
if(isset($_SESSION['nivel'])){

}else{
  
 header('Location: index.php');
}
include("inc/dbconnection.php");

   $iduser=$_SESSION['id'];
  $username=$_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    
  <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="sweetalert2/sweetalert2.min.js"></script>

  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
  <title> Report</title>

  <style type="text/css">
     html,body{
      height: 100%;
     }
     .navbar-default {
          background-color: black !important;
          border-color: white !important;
      }

      .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
          color: white !important;
          background-color: #555555 !important;
      }
      .fondo{
          background-image: url(img/logo.svg);
          background-size: contain;
          height: 50px;
          width: 133px;
          background-repeat: no-repeat;
      }

      .cabecera{
      text-align: center !important;
      }
  </style>
</head>

<body>
<script type="text/javascript">
  $( document ).ready(function() {
    $("#cont-input").remove();
    $("#btn-go").remove();
    $( "#datepicker" ).datepicker();
  });
</script>
<div style="height:100%">
<nav class="navbar navbar-default" role="navigation">
  <?php include("menu_mgtm_boostrap.php");?>
</nav>
<div class="container" style="max-height: calc(100% - 113px) !important; height:calc(100% - 113px); overflow:auto;">
  <div style="text-align: center;">
    <h3>General Report</h3>
  </div>
  <div style="height: 5rem;background-color: #1c335c;color: white; text-align: center; border-radius: 6px 6px 0px 0px;"><h3 style="padding: 11px;">Information</h3></div>     
   
  <div class="row">
    <div class="col-md-4">
      <label>Agency</label>
      <select  class="form-control"  type="date" id="selag" onchange="turnDownForWhat2();">
        <!--<option value="0">select</option>-->
        <?php
        $sql="";
        if($_SESSION['nivel']==1 || $_SESSION['username']=="ALBERTAGENT"){
          $sql="SELECT * FROM `agency`";
        }else{
         $sql="SELECT * FROM `agency` where id=".$_SESSION['agencia'];
        }

        $result=mysql_query($sql);
        while($row=mysql_fetch_assoc($result)){ ?>
          <option value="<?=$row['id']?>"> ADC<?=$row['id']?></option>
        <?}?>
      </select>
    </div>
    <div class="col-md-4">
      <label>Start Date</label>
      <input  id="datepicker" name="caja" class="form-control" type="text" onchange="turnDownForWhat2();" placeholder="MM/DD/YYYY" readonly>
    </div>
    <div class="col-md-4">
      <label>End Date</label>
      <input id="datefinal" name="caja2" class="form-control" type="text" onchange="turnDownForWhat2();" placeholder="MM/DD/YYYY" readonly>
    </div>
  </div>
  <div class="row">
    <div class="table-responsive" id="contenido-tabla">
      <table class="table table-striped" style="border-width: 1px; border-style: solid; background-color: #9c9c9c;">
        <tbody>
          <!-- Aplicadas en las filas -->
          <tr >
            <th class="cabecera"  COLSPAN=13>Transactions</th>
          </tr>
          <tr>
            
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="panel-footer">Panel Footer</div>
</div>
 
<script type="text/javascript">
  $( 'li' ).click(function() {
    $('.active').removeClass('active');
    $(this).addClass('active');
  });

  function turnDownForWhat2(){ 
    //despliega el calendario con los resultados
    var agencia=$("#selag").val();
    var dateTypefecha1 = $('#datepicker').datepicker('getDate');
    var fecha1= $.datepicker.formatDate('yy-mm-dd', dateTypefecha1);
    var dateTypefecha2 = $('#datefinal').datepicker('getDate');
    var fecha2= $.datepicker.formatDate('yy-mm-dd', dateTypefecha2);
    
    var page = $(this).attr('data');        
    var dataString = 'page='+page;
    if(fecha1!=""&&fecha2!=""){
      $("#contenido-tabla").empty();
      // $('#contenido-tabla').html('<div><img src="img/loading.gif"/></div>');
      $.post("ConsultaReportesGeneral.php",{caja:fecha1, caja2:fecha2, ag:agencia }, 
        function(data){
          $('#contenido-tabla').html(data);
        }
      );
    }
  }
</script>
</body>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
  $("#datepicker").datepicker();
  $("#datefinal").datepicker();
</script>
</html>