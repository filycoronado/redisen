

<!DOCTYPE html>
<html lang="en" ng-app="myApp">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BuyLowCost | Login </title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <!-- Custom Theme Style -->
        <link href="App/css/main.css" rel="stylesheet">

        <script src="App/js/angular1.7.2.js"></script>
        <script data-require="angular-translate@*" data-semver="2.5.0"
        src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.5.0/angular-translate.js"></script>
        <script src="https://code.angularjs.org/1.6.0/angular-route.min.js"></script>

        <script src="https://code.angularjs.org/1.6.9/angular-cookies.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.6.3/mask.js"></script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4XIWXEWQeTRZC3wunJMzELHYVb83D4VI">
        </script>
        <title>BuylowCost | Login</title>
    </script>
</head>

<body class="login" ng-controller="LoginController"> 
    <div class="">
        <div class="login_wrapper">
            <div class="animate form login_form">

                <form ng-submit="Login()">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="App/img/logo_blc_vartical.png" style="width: 370px;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-group">
                            <div><i class="fa fa-home icon_style"></i></div>
                            <select  class="form-control" ng-model="loging.agency"  >
                                <option ng-repeat="a in agencies" value="{{a.number}}">{{a.number}}</option>
                            </select>
                        </div>
                        <br>
                        <div class="input-group">
                            <i class="fa fa-user-circle icon_style"></i>
                            <input type="text" class="form-control" placeholder="Username" ng-model="loging.username" >
                        </div>
                        <br>
                        <div class="input-group">
                            <i class="fa fa-align-justify icon_style"></i>
                            <input type="password" class="form-control" placeholder="Password"  ng-model="loging.password"  >
                        </div>
                    </div>
                    <div class="row">
                        <button class="btn btn-danger mx-auto " type="submit" >Log in</button>

                    </div>




                </form>
            </div>


        </div>
    </div>
</body>
<script src="App/js/App.js"></script>
<script src="App/js/Controllers/LoginController.js"></script>
</html>
