<?php
session_start();
if (isset($_SESSION['nivel'])) {
    if ($_SESSION['nivel'] > 2) {
        header('Location: principal22.php');
    }
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id = $_SESSION['id'];
$sql = "SELECT id,user FROM `users` WHERE (agencia=101 or agencia=102 or agencia=103 or agencia=104 or agencia=105) and (nivel=1 or nivel=2 or nivel=5) and habilitado=1  and id!=1";
$result = mysql_query($sql);

function _data_last_month_day() {
    $month = date('m');
    $year = date('Y');
    $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));

    return date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
}

/** Actual month first day * */
function _data_first_month_day() {
    $month = date('m');
    $year = date('Y');
    return date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
}

$date1 = _data_first_month_day();
$date2 = _data_last_month_day();
$array_final = array();
while ($row = mysql_fetch_assoc($result)) {


    $username = $row['user'];

    $id_user = $row['id'];

    $getADC = "SELECT count(*) as adcs FROM `tickets` JOIN clients on clients.id=tickets.id_client where agente='$username' and date BETWEEN '$date1' and '$date2' and clients.plan=2";
    $resultadc = mysql_query($getADC);
    $dataadc = mysql_fetch_assoc($resultadc);
    $adcs = $dataadc['adcs'];


    $getNBP = "SELECT count(id) as bnpolicies FROM `cutvone`  as cv where ( cv.Dpayment=1 or cv.Dpayment=3  or cv.Dpayment=3 or cv.Dpayment=5 or cv.Dpayment=10 ) and id_usuario=$id_user and fecha BETWEEN '$date1' and '$date2' and cv.erase=0";
    $resultNBP = mysql_query($getNBP);
    $dataNBP = mysql_fetch_assoc($resultNBP);
    $bnpolicies = $dataNBP['bnpolicies'];


    $get_notes = "SELECT count(*) as total FROM `notas_mgtm` where id_usuario=$id_user and fecha BETWEEN '$date1' and '$date2'";
    $resultnotas = mysql_query($get_notes);
    $dataNotas = mysql_fetch_assoc($resultnotas);
    $notas = $dataNotas['total'];

    $get_transactions = "SELECT count(*) as total  FROM `cutvone`  where  erase=0 and fecha BETWEEN  '$date1' and '$date2' and id_usuario=$id_user";
    $resultTransactions = mysql_query($get_transactions);
    $dataTransactions = mysql_fetch_assoc($resultTransactions);
    $tickets = $dataTransactions['total'];

    $get_fee = "SELECT SUM(feeoffice)as total  FROM `cutvone` where id_usuario=$id_user  and fecha BETWEEN '$date1' and '$date2' and erase=0";
    $resultFee = mysql_query($get_fee);
    $dataFee = mysql_fetch_assoc($resultFee);
    $fee = $dataFee['total'];

    $get_leads = "SELECT count(*) as total FROM `leads` where id_usuario=$id_user and fechaalta BETWEEN '$date1' and '$date2' ";
    $resultLeads = mysql_query($get_leads);
    $dataLeads = mysql_fetch_assoc($resultLeads);
    $leads = $dataLeads['total'];
    
    
    $get_task="SELECT count(*)  as total FROM `notas_mgtm` where user_asigned=$id_user and status_aplication=0";
    $result_tasks= mysql_query($get_task);
    $data_task= mysql_fetch_assoc($result_tasks);
    $taks_pending=$data_task['total'];
    
    $get_marks="SELECT count(*) as total FROM `notas_mgtm` where user_asigned=$id_user and status_aplication!=0";
     $result_marks= mysql_query($get_marks);
    $data_marks= mysql_fetch_assoc($result_marks);
    $marks=$data_marks['total'];
    
    $response = [
        'user' => $username,
        'adc' => $adcs,
        'nbpolicies' => $bnpolicies,
        'notes' => $notas,
        'transactions' => $tickets,
        'fee' => $fee,
        'leads' => $leads,
        'pendingTaks'=>$taks_pending,
        'marks'=>$marks,
    ];
    array_push($array_final, $response);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>
        <script src="js/jquery-validation/jquery.validate.js"></script>
        <script src="js/tooltipster/js/tooltipster.bundle.min.js"></script>
        <script src="js/kartik-v-bootstrap-fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
        <script src="js/kartik-v-bootstrap-fileinput/js/fileinput.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="js/tooltipster/css/tooltipster.bundle.min.css" />
        <link href="js/kartik-v-bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }

            .cabecera{
                text-align: center !important;
            }

            form .show-on-dealer{
                display: none;
            }
            form.dealer .show-on-dealer{
                display: block;
            }
            .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
                margin: 0;
                padding: 0;
                border: none;
                box-shadow: none;
                text-align: center;
            }
            .kv-avatar {
                display: inline-block;
            }
            .kv-avatar .file-input {
                display: table-cell;
                width: 213px;
            }
            .kv-reqd {
                color: red;
                font-family: monospace;
                font-weight: normal;
            }
        </style>
    </head>
    <body >
        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-primary" onclick="download_xls()" style="padding-left: 30px;">
                                Export To CSV
                            </button>
                        </div>
                        <table class="table table-striped Production">
                            <thead>
                                <tr>
                                    <th scope="col">Employee</th>
                                    <th scope="col">NB</th>
                                    <th scope="col">ADC</th>
                                    <th scope="col">Fee</th>
                                    <th scope="col">Transactions</th>
                                    <th scope="col">Notes</th>
                                    <th>Leads</th>
                                    <th scope="col">Pending Task</th>
                                    <th scope="col">Completed Tasks</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <? foreach ($array_final as &$employee) { ?>
                                    <tr>
                                        <td><?= $employee['user'] ?></td>
                                        <td><?= $employee['nbpolicies'] ?></td>
                                        <td><?= $employee['adc'] ?></td>
                                        <td><?= $employee['fee'] ?></td>
                                        <td><?= $employee['transactions'] ?></td>
                                        <td><?= $employee['notes'] ?></td>
                                        <td><?= $employee['leads'] ?></td>
                                        <td><?= $employee['pendingTaks'] ?></td>
                                        <td><?= $employee['marks'] ?></td>
                                    </tr>
                                <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function download_xls() {
                $(".Production").table2excel({
                    filename: "Production.xls"
                });
            }
        </script>
    </body>
</html>