 <?
include("inc/dbconnection.php");
session_start();
require('fpdf.php');
$fecha1=$_POST['caja'];
$fecha2=$_POST['caja2'];
$agency=$_POST['ag'];

//covnertir fechas
//$fecha1=preg_replace("/", "-", $fecha1);
//$fecha2=preg_replace("/", "-", $fecha2);
// seleccionar usuarios con actividad en el periodo seleccionado
$periodTotal = 0;
$nb=0;
$pay=0;
$payeft=0;
$dpayeft=0;
$cancel=0;
$reins=0;
$renew=0;
$nbt=0;
$pif=0;
$aorc=0;
$sql="SELECT `cv`.`id_usuario`, `us`.`user`, COUNT(`cv`.`id`) as `total` FROM `cutvone` AS `cv` LEFT JOIN `users` AS `us` ON `cv`.`id_usuario`=`us`.`id` WHERE `cv`.`agency`=$agency AND (`cv`.`fecha` BETWEEN '$fecha1' AND '$fecha2') AND `cv`.`erase`!=1 GROUP BY `cv`.`id_usuario` ORDER BY `cv`.`id_usuario` ASC";
$resultU=mysql_query($sql);

$pdf=new FPDF('L','mm','A4');
  $pdf->AddPage();
  $pdf->SetFont('Arial','B',9);
  $pdf->SetFillColor(28,51,92);//Fondo rgb
  $pdf->SetTextColor(240, 255, 240); //Letra color blanco
  $pdf->Image('logo.png' , 10 ,1, 35 , 38,'png','http://azdriversclub.com/azaz/maketa/');
  $pdf->SetXY(50, 15);
  $pdf->SetFont('Arial','B',9);
  $pdf->SetTextColor(10, 10, 10);

  $pdf->Cell(40,7,'Agency#'.$agency,0, 1 , ' R ');
  $pdf->SetXY(50, 10);
  $pdf->Cell(40,7,'Report:  '.date("m/d/Y", strtotime($fecha1))." - ".date("m/d/Y", strtotime($fecha2)),0, 1 , ' R ');

  $pdf->SetXY(125, 20);
  $pdf->SetFont('Arial','B',14);
  $pdf->Cell(100,7, "General Report", 0, 1 , ' C ');

  $pdf->Ln(10);
?>  
<table class="table" style="border-width: 1px; border-style: solid; background-color: #9c9c9c;">
  <tbody>
  <tr>
    <th colspan="11" style="background-color: #FFF;" >
      <div class="btn-group" id="exportCont">
        <a class="btn btn-primary" href="ReporteGeneral.PDF" target="_blank">Report</a>
        <!--<button type="button" class="btn btn-primary" onclick="$('#xlsfile2').submit();" style="padding-left: 30px;display: none;">
          <img class="xls" src="img/csv.svg" style="width: 25px;position: absolute;top: 4px;left: 5px;"> Export To CSV
        </button>-->
      </div>
      <style type="text/css">
          #exportCont .btn{
            background-color: #1c335c;
          }
      </style>
    </th>
  </tr>
  <?php while($user=mysql_fetch_assoc($resultU)){ ?>
    <tr><th colspan="11" style="text-align: center;color: #FFF;background-color: #1c335c;"><?= strtoupper($user['user']) ?></th></tr>
    <?php
      $pdf->SetFont('Arial','B',9);
      $pdf->SetFillColor(28,51,92);//Fondo rgb
      $pdf->SetTextColor(240, 255, 240); //Letra color blanco
      $pdf->Ln();
      $pdf->Cell(0,5,strtoupper($user['user']),1,0,'C',true); 
    $periodTotal += $user['total'];
    $sql="SELECT `cv`.`company`, `cm`.`nombre`, COUNT(`cv`.`id`) as `total` FROM `cutvone` AS `cv` LEFT JOIN `companyes` AS `cm` ON `cv`.`company`=`cm`.`id` WHERE `cv`.`agency`=$agency AND (`cv`.`fecha` BETWEEN '$fecha1' AND '$fecha2') AND `cv`.`erase`!=1 AND `cv`.`id_usuario`='$user[id_usuario]' GROUP BY `cv`.`company` ORDER BY `cv`.`company` ASC";
    $resultC=mysql_query($sql);
    $userTotals=array('nb'=>0,'pay'=>0,'payeft'=>0,'dpayeft'=>0,'cancel'=>0,'reins'=>0,'renew'=>0,'nbt'=>0,'pif'=>0,'aorc'=>0);
    ?>
    <?php while($company=mysql_fetch_assoc($resultC)){?>
      <tr><th colspan="11" style="text-align: center;"><?= $company['nombre'] ?></th></tr>
      <tr style="background-color: #dedede;font-size: 12px;"><th>NEW BUSINESS</th><th>PAYMENT</th><!--<th>ENDORSE</th>--><th>PAYMENT EFT</th><th>DOWNPAYMENT EFT</th><th>CANCELLATION</th><th>REINSTATEMENT</th><th>RENEWAL</th><th>NEW BUSINESS TRANSFER</th><th>PAY IN FULL</th><th>AORC</th><th>TOTAL</th></tr>
      <?php
        $pdf->Ln();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(156,156,156);
        $pdf->SetTextColor(10, 10, 10);
        $pdf->Cell(0,5,$company['nombre'],1,0,'C',true);
        $pdf->Ln();
        $pdf->SetFont('Arial','B',7);
        $pdf->SetFillColor(222,222,222);
        $pdf->SetTextColor(10, 10, 10);
        $pdf->Cell(30,5,"NEW BUSINESS",1,0,'C',true);
        $pdf->Cell(20,5,"PAYMENT",1,0,'C',true); 
        $pdf->Cell(25,5,"PAYMENT EFT",1,0,'C',true); 
        $pdf->Cell(30,5,"DOWNPAYMENT EFT",1,0,'C',true); 
        $pdf->Cell(25,5,"CANCELLATION",1,0,'C',true); 
        $pdf->Cell(30,5,"REINSTATEMENT",1,0,'C',true); 
        $pdf->Cell(20,5,"RENEWAL",1,0,'C',true); 
        $pdf->Cell(40,5,"NEW BUSINESS TRANSFER",1,0,'C',true); 
        $pdf->Cell(20,5,"PAY IN FULL",1,0,'C',true); 
        $pdf->Cell(15,5,"AORC",1,0,'C',true); 
        $pdf->Cell(22,5,"TOTAL",1,0,'C',true); 
      ?>
      <tr style="background-color: #FFF;">
        <?php
        $sql="SELECT 
                SUM(CASE WHEN `Dpayment` = 1 THEN 1 ELSE 0 END) as `nb`, 
                SUM(CASE WHEN `Dpayment` = 2 THEN 1 ELSE 0 END) as `pay`, 
                SUM(CASE WHEN `typeDO` = 4 THEN 1 ELSE 0 END) as `end`, 
                SUM(CASE WHEN `Dpayment` = 3 THEN 1 ELSE 0 END) as `payeft`, 
                SUM(CASE WHEN `Dpayment` = 5 THEN 1 ELSE 0 END) as `dpayeft`, 
                SUM(CASE WHEN `Dpayment` = 6 THEN 1 ELSE 0 END) as `cancel`, 
                SUM(CASE WHEN `Dpayment` = 7 THEN 1 ELSE 0 END) as `reins`, 
                SUM(CASE WHEN `Dpayment` = 8 THEN 1 ELSE 0 END) as `renew`, 
                SUM(CASE WHEN `Dpayment` = 9 THEN 1 ELSE 0 END) as `nbt`, 
                SUM(CASE WHEN `Dpayment` = 10 THEN 1 ELSE 0 END) as `pif`, 
                SUM(CASE WHEN `Dpayment` = 11 THEN 1 ELSE 0 END) as `aorc` 
                FROM `cutvone` WHERE `agency`=$agency AND (`fecha` BETWEEN '$fecha1' AND '$fecha2') AND `erase`!=1 AND `id_usuario`='$user[id_usuario]' AND `company`='$company[company]'";
        $resultSUM=mysql_query($sql);
        $sumas=mysql_fetch_assoc($resultSUM);
        ?>
        <td><?= round($sumas['nb'],2) ?></td>
        <td><?= round($sumas['pay'],2) ?></td>
        <td><?= round($sumas['payeft'],2) ?></td>
        <td><?= round($sumas['dpayeft'],2) ?></td>
        <td><?= round($sumas['cancel'],2) ?></td>
        <td><?= round($sumas['reins'],2) ?></td>
        <td><?= round($sumas['renew'],2) ?></td>
        <td><?= round($sumas['nbt'],2) ?></td>
        <td><?= round($sumas['pif'],2) ?></td>
        <td><?= round($sumas['aorc'],2) ?></td>
        <td><?= round($company['total'],2) ?></td>
        <?php
          $pdf->Ln();
          $pdf->SetFont('Arial','B',7);
          $pdf->SetFillColor(255,255,255);
          $pdf->SetTextColor(10, 10, 10);
          $pdf->Cell(30,5,$sumas['nb'],1,0,'C',true);
          $pdf->Cell(20,5,$sumas['pay'],1,0,'C',true); 
          $pdf->Cell(25,5,$sumas['payeft'],1,0,'C',true); 
          $pdf->Cell(30,5,$sumas['dpayeft'],1,0,'C',true); 
          $pdf->Cell(25,5,$sumas['cancel'],1,0,'C',true); 
          $pdf->Cell(30,5,$sumas['reins'],1,0,'C',true); 
          $pdf->Cell(20,5,$sumas['renew'],1,0,'C',true); 
          $pdf->Cell(40,5,$sumas['nbt'],1,0,'C',true); 
          $pdf->Cell(20,5,$sumas['pif'],1,0,'C',true); 
          $pdf->Cell(15,5,$sumas['aorc'],1,0,'C',true); 
          $pdf->Cell(22,5,$company['total'],1,0,'C',true); 

          $userTotals['nb'] += $sumas['nb'];
          $userTotals['pay'] += $sumas['pay'];
          $userTotals['payeft'] += $sumas['payeft'];
          $userTotals['dpayeft'] += $sumas['dpayeft'];
          $userTotals['cancel'] += $sumas['cancel'];
          $userTotals['reins'] += $sumas['reins'];
          $userTotals['renew'] += $sumas['renew'];
          $userTotals['nbt'] += $sumas['nbt'];
          $userTotals['pif'] += $sumas['pif'];
          $userTotals['aorc'] += $sumas['aorc'];
      
         
 
        ?>
      </tr>
    <?php } 
            
            $nb+=$userTotals['nb'];
            $pay+=$userTotals['pay'];
            $payeft+=$userTotals['payeft'];
            $dpayeft+=$userTotals['dpayeft'];
            $cancel+=$userTotals['cancel'];
            $reins+=$userTotals['reins'];
            $renew+=$userTotals['renew'];
            $nbt+=$userTotals['nbt'];
            $pif+=$userTotals['pif'];
            $aorc+=$userTotals['aorc']; 
    ?> 
    <?php 
      $pdf->SetFont('Arial','B',7);
      $pdf->SetFillColor(222,222,222);
      $pdf->SetTextColor(10, 10, 10);
      foreach ($companyTotals as $comp => $tot) { ?>
    	
    	<?php
    	  
    	?>
    <?php } ?>
    <tr><th colspan="11" style="background-color: #dedede;text-align: center;">USER <?= strtoupper($user['user']) ?> TOTALS</th></tr>
    <tr style="background-color: #dedede;font-size: 12px;"><th>NEW BUSINESS</th><th>PAYMENT</th><!--<th>ENDORSE</th>--><th>PAYMENT EFT</th><th>DOWNPAYMENT EFT</th><th>CANCELLATION</th><th>REINSTATEMENT</th><th>RENEWAL</th><th>NEW BUSINESS TRANSFER</th><th>PAY IN FULL</th><th>AORC</th><th>TOTAL</th></tr>
    <tr style="background-color: #FFF;">
        <td><?= round($userTotals['nb'],2) ?></td>
        <td><?= round($userTotals['pay'],2) ?></td>
        <td><?= round($userTotals['payeft'],2) ?></td>
        <td><?= round($userTotals['dpayeft'],2) ?></td>
        <td><?= round($userTotals['cancel'],2) ?></td>
        <td><?= round($userTotals['reins'],2) ?></td>
        <td><?= round($userTotals['renew'],2) ?></td>
        <td><?= round($userTotals['nbt'],2) ?></td>
        <td><?= round($userTotals['pif'],2) ?></td>
        <td><?= round($userTotals['aorc'],2) ?></td>
        <td><?= round($user['total'],2) ?></td>
        <?php



          $pdf->Ln();
          $pdf->SetFont('Arial','B',7);
          $pdf->SetFillColor(222,222,222);
          $pdf->SetTextColor(10, 10, 10);
          $pdf->Cell(0,5,"USER ".strtoupper($user['user'])." TOTALS",1,0,'C',true);
          $pdf->Ln();
          $pdf->SetFont('Arial','B',7);
          $pdf->SetFillColor(222,222,222);
          $pdf->SetTextColor(10, 10, 10);
          $pdf->Cell(30,5,"NEW BUSINESS",1,0,'C',true);
          $pdf->Cell(20,5,"PAYMENT",1,0,'C',true); 
          $pdf->Cell(25,5,"PAYMENT EFT",1,0,'C',true); 
          $pdf->Cell(30,5,"DOWNPAYMENT EFT",1,0,'C',true); 
          $pdf->Cell(25,5,"CANCELLATION",1,0,'C',true); 
          $pdf->Cell(30,5,"REINSTATEMENT",1,0,'C',true); 
          $pdf->Cell(20,5,"RENEWAL",1,0,'C',true); 
          $pdf->Cell(40,5,"NEW BUSINESS TRANSFER",1,0,'C',true); 
          $pdf->Cell(20,5,"PAY IN FULL",1,0,'C',true); 
          $pdf->Cell(15,5,"AORC",1,0,'C',true); 
          $pdf->Cell(22,5,"TOTAL",1,0,'C',true); 
          $pdf->Ln();
          $pdf->SetFont('Arial','B',7);
          $pdf->SetFillColor(255,255,255);
          $pdf->SetTextColor(10, 10, 10);
          $pdf->Cell(30,5,$userTotals['nb'],1,0,'C',true);
          $pdf->Cell(20,5,$userTotals['pay'],1,0,'C',true); 
          $pdf->Cell(25,5,$userTotals['payeft'],1,0,'C',true); 
          $pdf->Cell(30,5,$userTotals['dpayeft'],1,0,'C',true); 
          $pdf->Cell(25,5,$userTotals['cancel'],1,0,'C',true); 
          $pdf->Cell(30,5,$userTotals['reins'],1,0,'C',true); 
          $pdf->Cell(20,5,$userTotals['renew'],1,0,'C',true); 
          $pdf->Cell(40,5,$userTotals['nbt'],1,0,'C',true); 
          $pdf->Cell(20,5,$userTotals['pif'],1,0,'C',true); 
          $pdf->Cell(15,5,$userTotals['aorc'],1,0,'C',true); 
          $pdf->Cell(22,5,$user['total'],1,0,'C',true); 
        ?>
      </tr>
  <?php } ?> 
  <tr style="background-color: #dedede;font-size: 12px;"><th>NEW BUSINESS</th><th>PAYMENT</th><!--<th>ENDORSE</th>--><th>PAYMENT EFT</th><th>DOWNPAYMENT EFT</th><th>CANCELLATION</th><th>REINSTATEMENT</th><th>RENEWAL</th><th>NEW BUSINESS TRANSFER</th><th>PAY IN FULL</th><th>AORC</th><th>TOTAL</th></tr>
  <tr style="background-color: #dedede;font-size: 12px;"><th><?=$nb?></th><th><?=$pay?></th><!--<th>ENDORSE</th>--><th><?=$payeft?></th><th><?=$dpayeft?></th><th><?=$cancel?></th><th><?=$reins?></th><th><?=$renew?></th><th><?$nbt?></th><th><?=$pif?></th><th><?=$aorc?></th></tr>
  <tr style="color: #FFF;background-color: #1c335c;"><th colspan="10" style="text-align: right;">PERIOD TOTAL</th><th><?= $periodTotal ?></th></tr>
  <?php
    $pdf->Ln();
    $pdf->SetFont('Arial','B',7);
    $pdf->SetFillColor(28,51,92);//Fondo rgb
    $pdf->SetTextColor(240, 255, 240); //Letra color blanco
    $pdf->Cell(255,5,"PERIOD TOTAL",1,0,'R',true);
    $pdf->Cell(22,5,$periodTotal,1,0,'C',true);
  ?>
  </tbody>
</table>
<?php
$name="ReporteGeneral.PDF";
$pdf->Output($name,'F');
?>