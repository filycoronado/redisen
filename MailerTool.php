<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <title> Report</title>

        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }
            #datepicker,#datefinal {
                height: 34px!important;
                width: 100%!important;
                border-radius: 4px!important;
                border-style: solid!important;
                padding: 6px 12px!important;

            }
        </style>


    </head>

    <body >
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();
                $("#datepicker").datepicker();
            });

        </script>


        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <?
                include("menu_mgtm_boostrap.php");
                ?>
            </nav>


            <div class="container" style="max-height: 86% !important; height: 86%; overflow:auto;">
                <div style="text-align: center;">

                    <h3>Mailers</h3>

                </div>
                <div style="height: 5rem;background-color: #1c335c;color: white; text-align: center;     border-radius: 6px 6px 0px 0px;"> <h3 style="padding: 11px;">  Information </h3></div>     


                <div class="row">
                    <div class="col-md-6">
                        <label>Message</label>
                        <textarea  class="form-control"  type="text" id="message">
                       
                        </textarea>
                    </div>

                    <div class="col-md-6">
                        <label>Mails</label>
                        <textarea   class="form-control"  type="text" id="mails">
                       
                        </textarea>
                    </div>
                </div>
                <button onclick="SendMail()" >Submit</button>

                <div class="row table-responsive" id="tablas" style="background-color: #FFFFFF;padding: 0;text-align: center;">
                    <div style="width: 100%; height: 150px;"></div>
                    <div style="clear: both;"></div>
                </div>

            </div>
            <div class="panel-footer">Panel Footer</div>
        </div>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script type="text/javascript">
                    //$("#datepicker").datepicker();
                    //$("#datefinal").datepicker();
        </script>
        <script type="text/javascript">

            function SendMail() {
                var message=$("#message").val();
                var mails=$("#mails").val();
                    $.post("SendMailsMas.php", {msj: message,mails:mails},function (data) {
                            $('#tablas').html(data);
                        });
            }
        </script>
    </body>

</html>