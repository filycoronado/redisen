<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">



        <title> Summary Report</title>

        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }

            .list-group {
                padding-left: 0;
                margin-bottom: 5px;
            }
            .imageGainzco{

                background-size: contain;
                background-repeat: no-repeat;
                background-position: center;
                margin-bottom: 5px;
                height: 11rem;
            }
            .item-Sel{
                z-index: 2;
                color: #fff;
                background-color: #337ab7;
                border-color: #337ab7;
            }
        </style>
    </head>

    <body>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();
                $("#datepicker").datepicker();
            });
        </script>
        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <?php include("menu_mgtm_boostrap.php"); ?>
            </nav>
            <div class="container" style="max-height: calc(100% - 113px) !important; height:calc(100% - 113px); overflow:auto;">
                <div style="text-align: center;">
                    <h3>Summary Report</h3>
                </div>
                <div style="height: 5rem;background-color: #1c335c;color: white; text-align: center; border-radius: 6px 6px 0px 0px;"><h3 style="padding: 11px;">Information</h3></div>     

                <div class="row">
                    <div class="col-md-4">
                        <label>Agency</label>
                        <select  class="form-control"  type="date" id="selag" onchange="turnDownForWhat2();">
                            <!--<option value="0">select</option>-->
                            <?php
                            $sql = "";
                            if ($_SESSION['nivel'] == 1) {
                                $sql = "SELECT * FROM `agency` ";
                            } else {
                                $sql = "SELECT * FROM `agency` where id=" . $_SESSION['agencia'];
                            }

                            $result = mysql_query($sql);
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?= $row['id'] ?>"> ADC<?= $row['id'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Start Date</label>
                        <input  id="datepicker" name="caja" class="form-control" type="text" onchange="turnDownForWhat2();" placeholder="MM/DD/YYYY" readonly>
                    </div>
                    <div class="col-md-4">
                        <label>End Date</label>
                        <input id="datefinal" name="caja2" class="form-control" type="text" onchange="turnDownForWhat2();" placeholder="MM/DD/YYYY" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive" id="contenido-tabla">
                        <table class="table table-striped" style="border-width: 1px; border-style: solid; background-color: #9c9c9c;">
                            <tbody>
                                <!-- Aplicadas en las filas -->
                                <tr >
                                    <th class="cabecera"  COLSPAN=13>Transactions</th>
                                </tr>
                                <tr>

                                </tr>
                            </tbody>
                        </table>

                        <!--card-->



                        <div class=".col-xs-12 .col-md-8">

                            <?php
                            $sql = "SELECT * FROM `companyes` where agency=101";
                            $result = mysql_query($sql);
                            $lastDay = _data_last_month_day();
                            $firstDay = _data_first_month_day();
                            $pendingsDAte = strtotime('-30 day', strtotime($firstDay));
                            $pendingsDAte = date('Y-m-d', $pendingsDAte);


                            $totalsPending = 0;
                            $totalExpired = 0;
                            $totalCancel = 0;
                            while ($r = mysql_fetch_assoc($result)) {
                                $name = "";
                                $totalCancelled = 0;
                                $image = "http://thetechtemple.com/wp-content/themes/TechNews/images/img_not_available.jpg";
                                $idCompany = $r['id'];
                                $name = $r['nombre'];
                                if ($r['ImageRoute'] != "") {
                                    $image = $r['ImageRoute'];
                                }



                                $sqlStatus = "SELECT  id  from  `UserManagement` where fecha_mod_satus BETWEEN '$pendingsDAte' AND '$lastDay'   and  status_client=1 and compania=$idCompany";
                                $res = mysql_query($sqlStatus);
                                $countPending = mysql_num_rows($res);
                                $sqlPendins = "SELECT * FROM `PoliciesForOldMGTM`  where fech_mod_status BETWEEN '$pendingsDAte' AND '$lastDay'   and  status_policy=1 and company_id=$idCompany";
                                $respending = mysql_query($sqlPendins);
                                $countPending += mysql_num_rows($respending);
                                $totalsPending += $countPending;

                                $sqlStatus = "SELECT  id  from  `UserManagement` where fecha_mod_satus BETWEEN '$firstDay' AND '$lastDay'   and  status_client=7 and compania=$idCompany";
                                $res = mysql_query($sqlStatus);
                                $countExpired = mysql_num_rows($res);

                                $sqlexpired = "SELECT * FROM `PoliciesForOldMGTM`  where fech_mod_status BETWEEN '$firstDay' AND '$lastDay'   and  status_policy=7 and company_id=$idCompany";
                                $resExpired = mysql_query($sqlexpired);
                                $countExpired += mysql_num_rows($resExpired);
                                $totalExpired += $countExpired;

                                $sqlStatus = "SELECT  id  from  `UserManagement` where fecha_mod_satus BETWEEN '$firstDay' AND '$lastDay'   and  status_client=2 and compania=$idCompany";
                                $res = mysql_query($sqlStatus);
                                $cnp = mysql_num_rows($res);

                                $sql_non_payment = "SELECT * FROM `PoliciesForOldMGTM`  where fech_mod_status BETWEEN '$firstDay' AND '$lastDay'   and  status_policy=2 and company_id=$idCompany";
                                $res_non_payment = mysql_query($sql_non_payment);
                                $cnp += mysql_num_rows($res_non_payment);


                                $sqlStatus = "SELECT  id  from  `UserManagement` where fecha_mod_satus BETWEEN '$firstDay' AND '$lastDay'   and  status_client=3 and compania=$idCompany";
                                $res = mysql_query($sqlStatus);
                                $cuw = mysql_num_rows($res);

                                $sql_under_writeen = "SELECT * FROM `PoliciesForOldMGTM`  where fech_mod_status BETWEEN '$firstDay' AND '$lastDay'   and  status_policy=3 and company_id=$idCompany";
                                $res_under_wrrtiten = mysql_query($sql_under_writeen);
                                $cuw += mysql_num_rows($res_under_wrrtiten);

                                $sqlStatus = "SELECT  id  from  `UserManagement` where fecha_mod_satus BETWEEN '$firstDay' AND '$lastDay'   and  status_client=11 and compania=$idCompany";
                                $res = mysql_query($sqlStatus);
                                $cnlc = mysql_num_rows($res);

                                $sql_no_longer_client = "SELECT * FROM `PoliciesForOldMGTM`  where fech_mod_status BETWEEN '$firstDay' AND '$lastDay'   and  status_policy=11 and company_id=$idCompany";
                                $res_non_longer_client = mysql_query($sql_no_longer_client);
                                $cnlc += mysql_num_rows($res_non_longer_client);


                                $totalCancelled = $cnlc + $cuw + $cnp;

                                $totalCancel += $totalCancelled;
                                ?>
                                <!--Gainzco-->  
                                <div class="col-sm-3" 
                                     style="background-color: white;
                                     border-radius: 6px;
                                     border-style: solid;
                                     margin: 2px auto;
                                     border-width: 1px;" >
                                    <div class="imageGainzco" style="background-image:url('<?= $image ?>')"> </div>
                                    <ul class="list-group" > 
                                        <li class="list-group-item" style=" text-align: center; background-color: #337ab7; border-color: #337ab7;color:white;">
                                            <?= $name ?>
                                        </li> 
                                    </ul>
                                    <ul class="list-group">
                                        <li class="list-group-item" >
                                            <span class="badge"><?= $countPending ?></span>
                                            Pending Cancellation
                                        </li> 
                                    </ul>

                                    <ul class="list-group" style="cursor: pointer">
                                        <li class="list-group-item" data-toggle="collapse" data-target="#<?= $idCompany ?>" aria-expanded="false" aria-controls="<?= $idCompany ?>">
                                            <span class="badge"><?= $totalCancelled ?></span>
                                            Total Cancelled
                                        </li>
                                    </ul>
                                    <div class="collapse" id="<?= $idCompany ?>">
                                        <ul class="list-group">
                                            <li class="list-group-item" >
                                                <span class="badge"><?= $cnp ?></span>
                                                Cancelled  Non-Payment
                                            </li> 
                                        </ul>
                                        <ul class="list-group">
                                            <li class="list-group-item" >
                                                <span class="badge"><?= $cuw ?></span>
                                                Cancelled UW
                                            </li> 
                                        </ul>
                                        <ul class="list-group">
                                            <li class="list-group-item" >
                                                <span class="badge"><?= $cnlc ?></span>
                                                No Longer Client
                                            </li> 
                                        </ul>
                                    </div>

                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <span class="badge"><?= $countExpired ?></span>
                                            Expired
                                        </li>
                                    </ul>
                                </div>


                            <? } ?>



                        </div>

                        <!---->
                    </div>
                </div>
                <div class="container">
                    <h2>Totals </h2>
                    <ul class="list-group">
                        <li class="list-group-item">Pending Cancellation :<?= $totalsPending ?></li>
                        <li class="list-group-item">Cancelled: <?= $totalCancel ?></li>
                        <li class="list-group-item">Expired :<?= $totalExpired ?></li>
                    </ul>
                </div>
            </div>
            <div class="panel-footer">Panel Footer</div>
        </div>

        <script type="text/javascript">
            $('li').click(function () {
                $('.active').removeClass('active');
                $(this).addClass('active');
            });

            function turnDownForWhat2() {
                //despliega el calendario con los resultados
                var agencia = $("#selag").val();
                var dateTypefecha1 = $('#datepicker').datepicker('getDate');
                var fecha1 = $.datepicker.formatDate('yy-mm-dd', dateTypefecha1);
                var dateTypefecha2 = $('#datefinal').datepicker('getDate');
                var fecha2 = $.datepicker.formatDate('yy-mm-dd', dateTypefecha2);

                var page = $(this).attr('data');
                var dataString = 'page=' + page;
                if (fecha1 != "" && fecha2 != "") {
                    $("#contenido-tabla").empty();
                    // $('#contenido-tabla').html('<div><img src="img/loading.gif"/></div>');
                    $.post("ConsultaReportesGeneral.php", {caja: fecha1, caja2: fecha2, ag: agencia},
                            function (data) {
                                $('#contenido-tabla').html(data);
                            }
                    );
                }
            }
        </script>
    </body>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


    <script type="text/javascript">
            $("#datepicker").datepicker();
            $("#datefinal").datepicker();
    </script>

    <?php

    function _data_last_month_day() {
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));

        return date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
    }

    ;

    /** Actual month first day * */
    function _data_first_month_day() {
        $month = date('m');
        $year = date('Y');
        return date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
    }
    ?>
</html>