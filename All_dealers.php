<?php
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id_User = $_SESSION['id'];

$txtButton = "SAVE";

$nombreDealer = "";
$latitud = "";
$longuitud = "";
$idDealerSelected = 0;
$habilitado = 0;
if (isset($_GET['idDealer'])) {
    $id = $_GET['idDealer'];
    $idDealerSelected = $_GET['idDealer'];
    $txtButton = "UPDATE";
    $sqlDealer = "SELECT * FROM `dealers` where  id=$id";
    $result = mysql_query($sqlDealer);
    $row = mysql_fetch_assoc($result);
    $nombreDealer = $row['nombre'];
    $latitud = $row['latitud'];
    $longuitud = $row['long'];
    $habilitado = $row['habilitado'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="datatables/jquery.dataTables.min.js"></script>
        <script src="datatables/dataTables.bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>


        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }
        </style>


    </head>

    <body >

        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>


            <div>
                <? ?>
                <div class="col-md-6">
                    <div class="panel panel-primary" >
                        <div class="panel-heading"></div>
                        <div class="row">
                            <?
                            $sql = "SELECT * FROM `dealers` where agency=101";
                            $result = mysql_query($sql);
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>

                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body" style="padding: 20px; border-style: solid;border-width: 1px;border-color: #337ab7;">
                                            <h5 class="card-title"><?= $row['nombre'] ?></h5>

                                            <a href="All_dealers.php?idDealer=<?= $row['id']; ?>" class="btn btn-primary">Edit</a>
                                        </div>
                                    </div>
                                </div>


                            <? } ?>
                        </div>
                        <!--                        <form action="#" class="credit-card-div col-lg-10 col-md-12">
                                                    <div class="panel panel-default" >
                                                        <div class="panel-heading">
                        
                        
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                    <span class="help-block text-muted small-font" >ADC Number</span>
                                                                    <input id="ADCNUMBERADD"  type="text" class="form-control" placeholder="ADC Number" value="" />
                                                                </div>
                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                    <span class="help-block text-muted small-font" >Amount:</span>
                                                                    <input id="AmountADCADD"  type="text" class="form-control" placeholder="0.00" value="0" />
                                                                </div>
                                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                                    <span class="help-block text-muted small-font" >Name Client</span>
                                                                    <input id="clientNameaDC"  type="text" class="form-control" placeholder="Jhon Smith" value=""/>
                                                                </div>
                        
                                                            </div>
                        
                        
                                                        </div>
                                                        <div class="row ">
                                                            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                                                <input  id="Bnt-Save-ADC" type="button" onclick="SaveADC();"  class="btn btn-success" value="Save" />
                                                            </div>
                        
                                                        </div>
                        
                                                    </div>
                        
                                                </form>-->


                    </div>



                </div>


                <div class="col-md-6">
                    <div class="panel panel-primary" >
                        <div class="panel-heading"></div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="help-block text-muted small-font" >Name</span>
                                    <input id="name_dealer"  type="text" class="form-control" placeholder="Name DEaler" value="<?= $nombreDealer ?>" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="help-block text-muted small-font" >Lat</span>
                                    <input id="Latitude"  type="number" class="form-control" placeholder="325511" value="<?= $latitud ?>" />
                                </div>
                                <div class="col-md-4">
                                    <span class="help-block text-muted small-font" >Lon</span>
                                    <input id="longuitude"  type="number" class="form-control" placeholder="-110256" value="<?= $longuitud ?>" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <span class="help-block text-muted small-font">Select</span>
                                    <select id="habilitado" class="form-control">
                                        <option value="1" <? if ($habilitado == 1) { ?>selected<? } ?>>Enalbed</option>
                                        <option value="0" <? if ($habilitado == 0) { ?>selected<? } ?>>Disabled</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                        <div class="row ">
                            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                <input  id="BntAction" type="button" onclick="SubmitForm();"  class="btn btn-success" value="<?= $txtButton ?>" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script>
                                    function SubmitForm() {
                                        var id_dealer =<?= $idDealerSelected ?>;
                                        var name_deler = $("#name_dealer").val();
                                        var latitude = $("#Latitude").val();
                                        var longuitude = $("#longuitude").val();
                                        var BntAction = $("#BntAction").val();
                                        var habilitado = $("#habilitado").val();
                                        $.post("./Views/Controllers/SaveOrUpdateDealer.php", {id_dealer, name_deler, latitude, longuitude, BntAction, habilitado}, function (data) {
                                            alert(data.message);

                                        });
                                    }
        </script>




    </body>
</html>

