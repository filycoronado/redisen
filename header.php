<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>America Drivers Club</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
    	
        <link href="https://code.jquery.com/ui/1.10.4/themes/trontastic/jquery-ui.css" rel="stylesheet" type="text/css"/>
		
       
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/main.css">

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

		<script src="jquery/jquery.min.js" type="text/javascript"></script>
		<script src="jquery/jquery.nicescroll.min.js" type="text/javascript"></script>
		<script src="jquery/jquery.bxslider.min.js"></script>
		<script src="jquery/jquery-ui.min.js" type="text/javascript"></script>
		<script src="jquery/jquery.wallform.js" type="text/javascript"></script>

	   <style type="text/css">
	   	img:hover, a:hover, button:hover, h1:hover {
          transform: initial !important;

}
@media only screen and (max-width: 750px) and (min-width: 5px)  { 
	.navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
    #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
   background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}
.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}
#contss{
        width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;	
}
#contus{
       margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;	
}

#cell{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
}
}
/* Para 320px */  
@media only screen and (max-width: 320px) and (min-width: 5px)  { 
	.navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
    #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
   background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}
.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}
#contss{
        width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;	
}
#contus{
       margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;	
}

#cell{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
}
}
	   	/* Para 480px */  
@media only screen and (max-width: 500px) and (min-width: 341px) {  
 .navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
  #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
    background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}

.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}

#contss{
        width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;
}
#contus{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;	
}

#cell{
	margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.6rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
} 


/* Para 320px */  
@media only screen and (max-width: 340px) and (min-width: 5px)  { 
	.navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
    #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
   background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}
.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}
#contss{
        width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;	
}
#contus{
       margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;	
}

#cell{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.6rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
}
} 
@media only screen and (max-width: 586px) and (min-width: 320px)  {  
.navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
#contss{
         width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;
}
#contus{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;		
}

#cell{
margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.6rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
}
    #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
   background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}
.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}
}

@media only screen and (max-width: 780px) and (min-width: 600px)  {  
.navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
    #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
   background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}
.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}

#contss{
        width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;
}
#contus{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;	
}

#cell{
margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.6rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
}
} 

@media only screen and (max-width: 640px) and (min-width: 360px)  {  
.navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
    #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
   background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}
.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}

#contss{
        width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;
}
#contus{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;	
}

#cell{
margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.6rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
}
}


@media only screen and  (max-width: 800px) and (min-width: 600px) {  
.navbar img {
    /* margin: 28%; */
    /* width: 20%; */
    margin-left: 37% !important;
    margin-top: 1% !important;
}
    #navbar{

  	margin-left: 20%;
    height: 20rem;
  }
  .nav-pills>li {
    float: none;
}

#resd{
 margin-left: 0 !important;
}
.slide span {

    font-size: 6vh !important;

}

.cont-slider {
    position: relative !important;
    height: 63vh !important;
    max-height: 100vh !important;
    width: auto !important;
    min-width: 81vw !important;
    margin: 0 auto !important;
    overflow: hidden !importants;
}

.slide {
    display: inline-block;
    position: relative;
    width: 20%;
    height: 100%;
   background-attachment: initial !important; 
    background-position: center center;
    background-size: cover;
}
.footer-contacto{
	font-size: 1rem;
}
.col-md-9{
    font-size: 2rem !important;
}

#contss{
        width: 62% !important;
    position: relative !important;
    /* right: 0% !important; */
    /* top: 57% !important; */
    margin-left: 20% !important;
    margin-top: 5% !important;
}
#contus{
    margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.3rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;	
}

#cell{
margin: -6px 0 10px !important;
    color: #1c335c !important;
    font-weight: bold !important;
    font-size: 1.6rem !important;
    font-family: 'Museo', sans-serif !important;
    text-align: center;
}
} 
	   </style>	
	</head>

	<!--<script type="text/javascript">
		$("·pre1").click(function(){
			$("·pre1").addClass("active");
			$("·pre2").removeClass("active");
			$("·pre3").removeClass("active");
			$("·pre4").removeClass("active");
		});

		$("·pre2").click(function(){
			$("·pre1").removeClass("active");
			$("·pre2").addClass("active");
			$("·pre3").removeClass("active");
			$("·pre4").removeClass("active");
		});

		$("·pre3").click(function(){
			$("·pre1").removeClass("active");
			$("·pre2").removeClass("active");
			$("·pre3").addClass("active");
			$("·pre4").removeClass("active");
		});

		$("·pre4").click(function(){
			$("·pre1").removeClass("active");
			$("·pre2").removeClass("active");
			$("·pre3").removeClass("active");
			$("·pre4").addClass("active");
		});
	</script>-->
		<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		<!-- menu -->
	    

		<nav class="navbar" role="navigation">
                   
			<div class="container">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>

					</button>
					<a class="navbar-brand" href="index.php"> <img src="img/logo.png" style="width: 25%;"/>
					 </a>
					 <div>
                        <div id="contss" class="col-md-9" style="width: 30% ; position: absolute  ;right: 0% ; top: 0% ;">
						
						<p id="contus" style="margin: -6px 0 10px ; color: #1c335c ;  font-weight: bold ; font-size: 2rem; font-family: 'Museo', sans-serif ;">
							Contact Us
						</p>
						<p id="cell" style=" margin: -6px 0 10px ; color: #1c335c ;  font-weight: bold ; font-size: 3rem; font-family: 'Museo', sans-serif ;">
							1-800-699-1103
						</p>
					</div> 	
					</div>
				</div>
                  
				<div style="margin-left: 20%;" id="navbar" class="navbar-collapse collapse">
					<ul class="nav nav-pills" style="    position: absolute !important;">
						<li id="pre1" role="presentation" class="active">
							<a href="index.php">Home</a>
						</li>
						<li id="pre2" role="presentation">
							<a href="aboutus.php">About Us</a>
						</li>
						<li id="pre3" role="presentation">
							<a href="services.php">Service</a>
						</li>
						<!--<li id="pre3" role="presentation">
							<a href="producer.php">Become An ADC Agent</a>
						</li>-->

						
						<!--<li role="presentation">
							<a href="#">FAQ</a>
						</li>-->
						<li id="pre4" role="presentation">
							<a id="enla" href="contact.php">Contact</a>
						<div id="desple" style="display: none; position: absolute;background-color:#5F9F4E;border-radius: 5px;">
								
                                    <!--
									<div style="width: 309px;">
										<div style=" background-color:#5F9F4E; border-radius: 5px;padding-left: 3%;"><a style=" text-decoration:none;    color: white !important;" href="Mailto:CONTACT@AMERICADRIVERSCLUB.COM">Becoming AZD Approved Towing Provider</a> </div>
									</div>
									<!--<div style="width: 309px;">
										<div style=" background-color:#5F9F4E; border-radius: 5px;padding-left: 3%;"><a style=" text-decoration:none;    color: white !important;" href="Mailto:CONTACT@AMERICADRIVERSCLUB.COM">Summit Comments And Suggestions</a> </div>
									</div>-->
							</div>
							
						</li>


							<li id="pre5" role="presentation">
							<a id="enla" href="#">Agents</a>
							<div id="desple2" style="display: none; position: absolute;background-color:#5F9F4E;border-radius: 5px;">
									<div style="width: 180px; ">
										<div style=" background-color:#5F9F4E;border-radius: 5px;padding-left: 3%; "><a style=" text-decoration:none;    color: white !important;" href="Agentslogin.php">Agents Login</a></div>
										<div style="width: 180px; ">
										<div style=" background-color:#5F9F4E;border-radius: 5px;padding-left: 3%; "><a style=" text-decoration:none;    color: white !important;" href="producer.php">Become An ADC Agent</a></div>
									</div>
									</div>

									
								
							</div>
							
						</li>

					</ul>
                    
					
				</div>
			
				<!--/.navbar-collapse
	<ul id="desple" style="display: none;margin-left:-24px !important;">
								<li>
									<label id="fdr" style="font-size: 13px; color:white;"><a <a href="Mailto:CONTACT@AZDRIVERSCLUB.COM">Becoming AZD Roadside Assistence Provider</a></label>
									<ul id="desple2" style="display: none;">
										<li>
											<label id="fdr4" style="font-size: 13px;">Please contact us CONTACT@AZDRIVERSCLUB.COM</label>
										</li>
									</ul>
								</li>
								<li>
									<label style="font-size: 13px; color:white;" id="fdr2"><a <a href="Mailto:CONTACT@AZDRIVERSCLUB.COM">Becoming AZD Approved Towing Provider</a></label>
									<ul id="desple3" style="display: none;">
										<li>
											<label id="fdr5" style="font-size: 13px;">Please contact us CONTACT@AZDRIVERSCLUB.COM</label>
										</li>
									</ul>
								</li>
								<li>
									<label style="font-size: 13px; color:white;" id="fdr3"><a href="Mailto:CONTACT@AZDRIVERSCLUB.COM">Summit Comments And Suggestions</a></label>
									<ul id="desple4" style="display: none;">
										<li>
											<label id="fdr6" style="font-size: 13px;">Please contact us CONTACT@AZDRIVERSCLUB.COM</label>
										</li>
									</ul>
								</li>
							</ul>
				 -->
			</div>

		</nav>

		<script type="text/javascript">

			$("#pre4").hover(function(){
				$("#desple").fadeIn(800);
				$("li").css("list-style","none");
				$("#enla").css("width","80px");
			},function(){
				$("#desple").fadeOut(800);
			});
            $("#pre5").hover(function(){
				$("#desple2").fadeIn(800);
				$("li").css("list-style","none");
				$("#enla").css("width","80px");
			},function(){
				$("#desple2").fadeOut(800);
			});
			$("#fdr").hover(function(){
				$("#fdr").addClass("btn-xs btn btn-success");
				$("#fdr2").removeClass("btn-xs btn btn-success");
				$("#fdr3").removeClass("btn-xs btn btn-success");
				$("#desple2").css("display","block");
				$("#desple2").css("float","right");
				$("li").css("list-style","none");
			},function(){
				$("#desple2").fadeOut(800);
				$("#fdr").removeClass("btn-xs btn btn-success");
			});

			$("#fdr2").hover(function(){
				$("#fdr").removeClass("btn-xs btn btn-success");
				$("#fdr2").addClass("btn-xs btn btn-success");
				$("#fdr3").removeClass("btn-xs btn btn-success");
				$("#desple3").css("display","block");
				$("#desple3").css("float","right");
				$("li").css("list-style","none");
			},function(){
				$("#desple3").fadeOut(800);
				$("#fdr2").removeClass("btn-xs btn btn-success");
			});

			$("#fdr3").hover(function(){
				$("#fdr").removeClass("btn-xs btn btn-success");
				$("#fdr2").removeClass("btn-xs btn btn-success");
				$("#fdr3").addClass("btn-xs btn btn-success");
				$("#desple4").css("display","block");
				$("#desple4").css("float","right");
				$("li").css("list-style","none");
			},function(){
				$("#desple4").fadeOut(800);
				$("#fdr3").removeClass("btn-xs btn btn-success");
			});

			/*$("#pre4").click(function(){
				$("#desple").css("display","block");
				$("li").css("list-style","none");
				$("#enla").css("width","80px");
			});

			$("#fdr").click(function(){
				$("#desple2").css("display","block");
				$("#desple2").css("float","right");
				$("li").css("list-style","none");
			});

			$("#fdr2").click(function(){
				$("#desple3").css("display","block");
				$("#desple3").css("float","right");
				$("li").css("list-style","none");
			});

			$("#fdr3").click(function(){
				$("#desple4").css("display","block");
				$("#desple4").css("float","right");
				$("li").css("list-style","none");
			});*/



			

		</script>