<?
session_start();
if (isset($_SESSION['nivel'])) {
    if ($_SESSION['nivel'] > 2) {
        header('Location: principal22.php');
    }
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id = $_GET['id'];
$sql = "SELECT * FROM `users` WHERE `id` = $id AND `deleted` = 0";
$result = mysql_query($sql);
$employee = mysql_fetch_assoc($result);
if ($employee) {
    $user = $employee['user'];
    $name = $employee['name'];
    $apellido = $employee['apellido'];
    $email = $employee['email'];
    $nivel = $employee['nivel'];
    $dob = $employee['dob'];
}
if (isset($_POST['adc_nb'])) {
    $nbADC = $_POST['adc_nb'];
    $nbPolicy = $_POST['policy_nb'];
    $date1 = $_POST['date1'];
    $date2 = $_POST['date2'];
    $date1 = date("Y-m-d", strtotime($date1));
    $date2 = date("Y-m-d", strtotime($date2));
    $insert = "INSERT INTO `GoalsEmployee`(`user`, `NB_ADC`, `NB_Policy`, `StartDate`, `EndDate`) VALUES ('$user',$nbADC,$nbPolicy,'$date1','$date2')";
    mysql_query($insert);
    unset($_POST["adc_nb"] );
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>
        <script src="js/jquery-validation/jquery.validate.js"></script>
        <script src="js/tooltipster/js/tooltipster.bundle.min.js"></script>
        <script src="js/kartik-v-bootstrap-fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
        <script src="js/kartik-v-bootstrap-fileinput/js/fileinput.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="js/tooltipster/css/tooltipster.bundle.min.css" />
        <link href="js/kartik-v-bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }

            .cabecera{
                text-align: center !important;
            }

            form .show-on-dealer{
                display: none;
            }
            form.dealer .show-on-dealer{
                display: block;
            }
            .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
                margin: 0;
                padding: 0;
                border: none;
                box-shadow: none;
                text-align: center;
            }
            .kv-avatar {
                display: inline-block;
            }
            .kv-avatar .file-input {
                display: table-cell;
                width: 213px;
            }
            .kv-reqd {
                color: red;
                font-family: monospace;
                font-weight: normal;
            }
        </style>
    </head>
    <body >
        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>
            <div class="row">


                <form method="post" action="employees_goals.php?id=<?= $id ?>" id="formu1">
                    <div class="col-md-2 mx-auto" required>
                        <label>New ADC:</label>
                        <input type="number" name="adc_nb"  class="form-control" value="0" required/>
                    </div>
                    <div class="col-md-2 mx-auto">
                        <label>NB Policies:</label>
                        <input type="number" name="policy_nb" class="form-control"  value="0" required/>
                    </div>
                    <div class="col-md-2 mx-auto">
                        <label>Start Date</label>
                        <input type="date" name="date1" class="form-control" required/>
                    </div>
                    <div class="col-md-2 mx-auto">
                        <label>End Date</label>
                        <input type="date" name="date2" class="form-control" required />
                    </div>

                    <button style="margin-top: 1.4%;" type="submit" class="btn btn-primary"> Submit</button>

                </form>
            </div>
            <div classs="row">
                <div class="col-md-4">
                    <label><?= $user ?></label>
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Goal NB ADC</th>
                        <th scope="col">Goal NB Policies</th>
                        <th scope="col">Start Date</th>
                        <th scope="col">End Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    $cont = 1;
                    $slq = "SELECT * FROM `GoalsEmployee` where user='$user'";
                    $result = mysql_query($slq);
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>

                        <tr>
                            <th scope="row"><?= $cont++ ?></th>
                            <td><?= $row['user'] ?></td>
                            <td><?= $row['NB_ADC'] ?></td>
                            <td><?= $row['NB_Policy'] ?></td>
                            <td><?= $row['StartDate'] ?></td>
                            <td><?= $row['EndDate'] ?></td>
                        </tr>
                    <? } ?>
                </tbody>
            </table>
        </div>

    </body>
</html>