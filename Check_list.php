<style>
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css') 

    .funkyradio div {
        clear: both;
        overflow: hidden;
    }

    .funkyradio label {
        width: 100%;
        border-radius: 3px;
        border: 1px solid #D1D3D4;
        font-weight: normal;
    }

    .funkyradio input[type="radio"]:empty,
    .funkyradio input[type="checkbox"]:empty {
        display: none;
    }

    .funkyradio input[type="radio"]:empty ~ label,
    .funkyradio input[type="checkbox"]:empty ~ label {
        position: relative;
        line-height: 2.5em;
        text-indent: 3.25em;
        margin-top: 2em;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .funkyradio input[type="radio"]:empty ~ label:before,
    .funkyradio input[type="checkbox"]:empty ~ label:before {
        position: absolute;
        display: block;
        top: 0;
        bottom: 0;
        left: 0;
        content: '';
        width: 2.5em;
        background: #D1D3D4;
        border-radius: 3px 0 0 3px;
    }

    .funkyradio input[type="radio"]:hover:not(:checked) ~ label,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
        color: #888;
    }

    .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
        content: '\2714';
        text-indent: .9em;
        color: #C2C2C2;
    }

    .funkyradio input[type="radio"]:checked ~ label,
    .funkyradio input[type="checkbox"]:checked ~ label {
        color: #777;
    }

    .funkyradio input[type="radio"]:checked ~ label:before,
    .funkyradio input[type="checkbox"]:checked ~ label:before {
        content: '\2714';
        text-indent: .9em;
        color: #333;
        background-color: #ccc;
    }

    .funkyradio input[type="radio"]:focus ~ label:before,
    .funkyradio input[type="checkbox"]:focus ~ label:before {
        box-shadow: 0 0 0 3px #999;
    }

    .funkyradio-default input[type="radio"]:checked ~ label:before,
    .funkyradio-default input[type="checkbox"]:checked ~ label:before {
        color: #333;
        background-color: #ccc;
    }

    .funkyradio-primary input[type="radio"]:checked ~ label:before,
    .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #337ab7;
    }

    .funkyradio-success input[type="radio"]:checked ~ label:before,
    .funkyradio-success input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5cb85c;
    }

    .funkyradio-danger input[type="radio"]:checked ~ label:before,
    .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #d9534f;
    }

    .funkyradio-warning input[type="radio"]:checked ~ label:before,
    .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #f0ad4e;
    }

    .funkyradio-info input[type="radio"]:checked ~ label:before,
    .funkyradio-info input[type="checkbox"]:checked ~ label:before {
        color: #fff;
        background-color: #5bc0de;
    }
</style>
<div class="col-md-12">
    <h4>pending transactions assigned for you</h4>

    <div class="funkyradio">
        <!--        <div class="funkyradio-default">
                    <input type="checkbox" name="checkbox" id="checkbox1" checked/>
                    <label for="checkbox1">First Option default</label>
                </div>
                <div class="funkyradio-primary">
                    <input type="checkbox" name="checkbox" id="checkbox2" checked/>
                    <label for="checkbox2">Second Option primary</label>
                </div>-->
        <!--        <div class="funkyradio-danger">
                    <input type="checkbox" name="checkbox" id="checkbox4" checked/>
                    <label for="checkbox4">Fourth Option danger</label>
                </div>
                <div class="funkyradio-warning">
                    <input type="checkbox" name="checkbox" id="checkbox5" checked/>
                    <label for="checkbox5">Fifth Option warning</label>
                </div>
                <div class="funkyradio-info">
                    <input type="checkbox" name="checkbox" id="checkbox6" checked/>
                    <label for="checkbox6">Sixth Option info</label>
                </div>-->

        <?
        $id_User=$_SESSION['id'];
        $today = date("Y-m-d");
        $queryCashier = "and id_cashier=$id_User";
        if ($id_User == 1|| $id_User==364) {
            $queryCashier = "";
        }
        $sql = "SELECT * FROM `cutvone` where fecha='$today' and erase=0 and id_cashier=$id_User and (typeaz=1 or companytype=1) and recivedcashConfirmation is null";
       
        $result = mysql_query($sql);
        $totalCAsh = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $cashaz = 0;
            $cashins = 0;
            $bandera = 0;
            if ($row['typeaz'] == 1) {
                $cashaz = $row['feeforaz'];
                $totalCAsh += $cashaz;
                $bandera = 1;
            }
            if ($row['companytype'] == 1) {
                $cashins = $row['feecompany'];
                $totalCAsh += $cashins;
                $bandera = 1;
            }
            $user = regresauser($row['id_usuario']);
            $checked = " ";
            if ($row['recivedcashConfirmation'] != null && $row['recivedcashConfirmation'] > 0) {
                $checked = "checked";
            }
            if ($bandera == 1 && ($cashaz > 0 || $cashins > 0)) {
                ?>
                <div class="funkyradio-success">
                    <input type="checkbox" name="checkbox" id="checkbox<?= $row['id'] ?>" <? echo $checked ?>  onclick="SaveCheck('checkbox<?= $row['id'] ?>',<?= $row['id'] ?>)"/>
                    <label for="checkbox<?= $row['id'] ?>">Cash  <?= $cashins + $cashaz ?>  for Customer <?= $row['nameclient'] ?>  / Transaction created by <?= $user ?></label>
                </div>
                <?
            }
        }
        ?>
    </div>
</div>
<div class="col-md-12">
    <h4>Transaction Cash Pending To aproval</h4>

    <div class="funkyradio">
        <?
        $today = date("Y-m-d");
        $queryCashier = "and id_usuario=$id_User";
        if ($id_User == 1 || $id_User==364 ) {
            $queryCashier = "";
        }
        $sql = "SELECT * FROM `cutvone` where fecha='$today' and erase=0 and (id_usuario=$id_User ) and (typeaz=1 or companytype=1)  and recivedcashConfirmation is null";
        $result = mysql_query($sql);
        $totalCAsh = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $cashaz = 0;
            $cashins = 0;
            $bandera = 0;
            if ($row['typeaz'] == 1) {
                $cashaz = $row['feeforaz'];
                $totalCAsh += $cashaz;
                $bandera = 1;
            }
            if ($row['companytype'] == 1) {
                $cashins = $row['feecompany'];
                $totalCAsh += $cashins;
                $bandera = 1;
            }
            $user = regresauser($row['id_usuario']);
            $checked = " ";
            if ($row['recivedcashConfirmation'] != null && $row['recivedcashConfirmation'] > 0) {
                $checked = "checked";
            }
            if ($bandera == 1 && ($cashaz > 0 || $cashins > 0)) {
                ?>
                <div class="funkyradio-success">
                    <input disabled type="checkbox" name="checkbox" id="1checkbox<?= $row['id'] ?>" <? echo $checked ?>    />
                    <label for="1checkbox<?= $row['id'] ?>">Cash  <?= $cashins + $cashaz ?>  for Customer <?= $row['nameclient'] ?>  / Transaction created by <?= $user ?></label>
                </div>
                <?
            }
        }
        ?>
    </div>
</div>    
<script>
    function SaveCheck(id, id_transacction) {
        var val = $('#' + id).is(":checked");
        var param = "";
        if (val == true) {
            param = 1;
        }else {
            param = 0;
        }

        console.log(val);
        $.post("./Views/Controllers/CheckCash.php", {id_transacction, param}, function (data) {
            alert(data.message);
        });
    }
</script>
<?

function regresauser($val) {


    $sql = "SELECT * FROM `users` where id=" . $val;

    $res = mysql_query($sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['user'];
    }
}
?>