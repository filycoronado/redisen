<?php
session_start();
$nivelLoge = 0;
$iduserloig = 0;
if (isset($_SESSION['nivel'])) {
    $nivelLoge = $_SESSION['nivel'];
    $iduserloig = $_SESSION['id'];
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");
$id = 0;
$id = $_GET['id'];
$nameCustomer = nameCustomer($mysqli, $id);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> -->

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">


        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>


        <script src="../jquery.maskedinput.js"></script>
        <script src="../js/mask/jquery/src/jquery.mask.js"></script>

        <link rel="stylesheet" href="../libs/kartik-v-bootstrap-fileinput/css/fileinput.css">
        <link rel="stylesheet" href="../libs/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.css">

      <!--  <script src="../libs/ckeditor/ckeditor.js"></script>-->
        <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
        <meta http-equiv="x-ua-compatible" content="IE=11">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <title>Cusatomer Details</title>



    </head>

    <body >
        <div>

            <?php
            include("./BarMenu.php");
            ?>

        </div>

        <!---->


        <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 container" >
            <div class="row" style="margin-top: 5%;">
                <?
                //include './CInformationForm.php'; 
                include ("./WizardCustomerDetails.php");
                ?>

            </div>


            <div>

                <?
                $FlagForADC = 0;
                $FlagForADC2 = 0;
                $membeershipId = "";
                $dataINS = array();

                $sql = "SELECT * FROM `UserManagement`  where  id=$id  and compania!=0 and compania is not null";
                $res = mysqli_query($mysqli, $sql);


                while ($row = mysqli_fetch_assoc($res)) {

                    $fechaAlta = date('m-d-Y', strtotime($row['fechaalta']));
                    $status_client = $row['status_client']; //is a sstatus profile really
                    if ($status_client == 1) {
                        $statusText = "PENDING CANCELLATION";
                    } else if ($status_client == 2 || $status_client == 3 || $status_client == 6 || $status_client == 13) {
                        $statusText = "CANCELED";
                    } else if ($status_client == 7) {
                        $statusText = "EXPIRED";
                    } else if ($status_client != 9 && $status_client != 12 && $status_client != 11) { //add if or satatis is new number make active
                        $statusText = "ACTIVE";
                    }
                    if ($status_client == -5) {
                        $statusText = "INACTIVE";
                    }
                    $nameUser = getName($mysqli, $row['id_usurio']);
                    $companyName = getCompany($mysqli, $row['compania']);
                    $img = getimage($mysqli, $row['compania']);
                    $idCompanyE = $row['compania'];
                    $stautsADc = "Inactive";
                    $adcNumber = "";
                    if ($row['ADC'] > 0 && $row['pagomensualdrivers'] > 0) {
                        $FlagForADC = 1;
                        $stautsADc = "Current Active";
                        $adcNumber = $row['ADC'];
                    }
                    if ($status_client == 14) {
                        $statusText = "REWRITE";
                    }
                    array_push($dataINS, array("id" => $row['id'], "idCompany" => $idCompanyE, "status" => $statusText, "user" => $nameUser, "company" => $companyName, "img" => $img, "StatusADC" => $stautsADc, "adc_number" => $adcNumber, "id_customer" => $row['id'], "date" => $fechaAlta));
                }




                $sql2 = "SELECT * FROM `PoliciesForOldMGTM` where  id_cliente=$id";
                $res2 = mysqli_query($mysqli, $sql2);
                while ($r = mysqli_fetch_assoc($res2)) {

                    $fechaAlta = date('m-d-Y', strtotime($r['date_alta']));
                    $status_client = $r['status_policy']; //is a sstatus profile really
                    if ($status_client == 1) {
                        $statusText = "PENDING CANCELLATION";
                    } else if ($status_client == 2 || $status_client == 3 || $status_client == 6 || $status_client == 13) {
                        $statusText = "CANCELED";
                    } else if ($status_client == 7) {
                        $statusText = "EXPIRED";
                    } else if ($status_client != 9 && $status_client != 12 && $status_client != 11) { //add if or satatis is new number make active
                        $statusText = "ACTIVE";
                    }

                    if ($status_client == -5) {
                        $statusText = "INACTIVE";
                    }
                    if ($status_client == 14) {
                        $statusText = "REWRITE";
                    }
                    $nameUser = getName($mysqli, $r['id_user']);
                    $companyName = getCompany($mysqli, $r['company_id']);
                    $img = getimage($mysqli, $r['company_id']);
                    $idCompanyD = $r['company_id'];
                    $stautsADc = "";
                    $adcNumber = "";
                    if ($r['adc_number'] > 0 && $r['pago_menusal_drviers'] > 0) {
                        $FlagForADC2 = 1;
                        $stautsADc = "Current Active";
                        $adcNumber = $r['adc_number'];
                    }
                    array_push($dataINS, array("id" => $r['Id'], "idCompany" => $idCompanyD, "status" => $statusText, "user" => $nameUser, "company" => $companyName, "img" => $img, "StatusADC" => $stautsADc, "adc_number" => $adcNumber, "id_customer" => $r['id_cliente'], "date" => $fechaAlta));
                }


                $aux = array();
                foreach ($dataINS as $key => $row) {
                    $aux[$key] = $row['status'];
                }
                array_multisort($aux, SORT_ASC, $dataINS);

//                echo "<PRE>";
//                print_r($dataINS);
//                echo "</PRE>";
                ?>
                <div class="container">
                    <div class="row col-md-12 col-lg-12 custyle">
                        <table class="table table-striped custab">
                            <thead>
                            <a href="./AddPolicy.php?id=<?= $id ?>&name=<?= $nameCustomer ?>" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new Policy</a>
                            <tr>
                                <th>#</th>
                                <th>Company</th>
                                <th>Status</th>
                                <th>RSA</th>

                            </tr>
                            </thead>
                            <? for ($i = 0; $i < count($dataINS); $i++) { ?>
                                <tr>
                                    <td>1</td>
                                    <td>
                                        <a href="#" class="pull-left">
                                            <img src="<?= $dataINS[$i]['img'] ?>" style="width: 95px !important;" class="media-photo">
                                        </a>
                                    </td>
                                    <td><?= $dataINS[$i]['status'] ?></td>
                                    <th><?= $dataINS[$i]['StatusADC'] ?></th>

                                </tr>
                                <tr>
                                    <td colspan="4">

                                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                            <a href="./MakeAPaymentS.php?id_client=<?= $dataINS[$i]['id_customer'] ?>&id_policy=<?= $dataINS[$i]['id'] ?>&Customer=<?= $nameCustomer ?>&Company=<?= $dataINS[$i]['idCompany'] ?>" class="btn btn-success btn-sm" ><span class="fa fa-credit-card fa-lg"></span>Make A Payment</a> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                            <a class='btn btn-info btn-sm' href="./AddPolicy.php?id_policy=<?= $dataINS[$i]['id'] ?>&id_client=<?= $dataINS[$i]['id_customer'] ?>&update=1"><span class="fa  fa-pencil-square-o fa-lg"></span>Edit Policy</a> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                            <button class="btn btn-info btn-sm" onclick="ShowFiles(<?= $dataINS[$i]['idCompany'] ?>,<?= $dataINS[$i]['id_customer'] ?>);"><span class="fa fa-files-o fa-lg"></span>Files</button>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                            <button class="btn btn-info btn-sm" onclick="ShowData(<?= $dataINS[$i]['id'] ?>,<?= $dataINS[$i]['id_customer'] ?>);"><span class="fa fa-money fa-lg"></span>Payments</button>
                                        </div>
                                    </td>

                                </tr>
                            <? } ?>
                        </table>
                    </div>

                </div>

                <div id="cont_gen">

                </div>
            </div>
        </div>

        <script>
            function ShowDataPayment(id_policy, id_client) {

            }

            function ShowData(id_policy, id_client) {
                $("#cont_gen").empty();
                $.post("./Payments.php", {id_policy, id_client}, function (data) {
                    $("#cont_gen").html(data);
                });
            }

            function ShowFiles(companyID, id_cliente) {
                $("#cont_gen").empty();

                var iduserloig =<?= $iduserloig ?>;
                var Company_id = companyID;
                var id_user = iduserloig;
                var id_customer = id_cliente;
                $.get("./File_history.php", {companyID, id_cliente, iduserloig}, function (data) {
                    $("#cont_gen").html(data);
//$ip = "../../../../Filesmgtm/file.PDF"; 
                    console.log("readey");
                    $("#input-700").fileinput({
                        uploadUrl: "./Controllers/UploadFile2.php",
                        uploadAsync: false,
                        maxFileCount: 1,
                        uploadExtraData: function () {
                            return {
                                customerID: id_customer,
                                UserID: id_user,
                                Company_ID: Company_id

                            };
                        }
                    });
// CATCH RESPONSE
                    $('#input-700').on('filebatchuploaderror', function (event, data, previewId, index) {
                        response = data.response, reader = data.reader;

                        alertify.error(response.message);
                    });
                    $('#input-700').on('filebatchuploadsuccess', function (event, data, previewId, index) {
                        response = data.response, reader = data.reader;

                        alertify.success(response.message);
                    });
                });
            }
        </script>

        <?php

        function getName($mysqli, $id) {
            $sql = "SELECT user FROM `users` where id=$id";
            $res = mysqli_query($mysqli, $sql);
            $r = mysqli_fetch_assoc($res);
            return $r['user'];
        }

        function getCompany($mysqli, $id) {
            $sql = "SELECT nombre FROM `companyes` where id=$id";
            $res = mysqli_query($mysqli, $sql);
            $r = mysqli_fetch_assoc($res);
            return $r['nombre'];
        }

        function getimage($mysqli, $id) {
            $sql = "SELECT ImageRoute FROM `companyes` where id=$id";

            $res = mysqli_query($mysqli, $sql);
            $r = mysqli_fetch_assoc($res);
            return $r['ImageRoute'];
        }

        function nameCustomer($mysqli, $id) {
            $sql = "SELECT nombre, apellido FROM `UserManagement` where id=$id";

            $result = mysqli_query($mysqli, $sql);
            $row = mysqli_fetch_assoc($result);
            $name = $row['nombre'] . " " . $row['apellido'];
            return $name;
        }
        ?>


        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>

        <link rel="stylesheet" href="../libs/kartik-v-bootstrap-fileinput/css/fileinput.css">
        <link rel="stylesheet" href="../libs/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.css">
<!--        <script src="../js/maks.js"></script>-->

        <script src="../libs/jquery-ui/jquery-ui.js"></script>
        <script src="../libs/kartik-v-bootstrap-fileinput/js/fileinput.min.js"></script>
        <script src="../libs/kartik-v-bootstrap-fileinput/js/fileinput.min.js"></script>

        <script src="../libs/kartik-v-bootstrap-fileinput/js/fileinput.js"></script>
        <script src="../libs/kartik-v-bootstrap-fileinput/themes/fa/theme.min.js"></script>
        <script src="../libs/kartik-v-bootstrap-fileinput/js/plugins/piexif.js"></script>
        <script>
            $(document).ready(function () {

                $(".phoneMask").mask('(000)000-0000');
                $(".CardMASK").mask('0000 0000 0000 0000');
                $(".monthmask").mask("00");
                $(".yearMAsk").mask("00");
                $(".ccvmask").mask("000");
            });
        </script>

    </body>
</html>


