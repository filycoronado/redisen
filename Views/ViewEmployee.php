<?
session_start();
$idUser = 0;
$niveluser = 0;
if (isset($_SESSION['nivel'])) {
    $niveluser = $_SESSION['nivel'];
    $idUser = $_SESSION['id'];
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");

$enabled = "disabled";
if ($idUser == 1 || $idUser == 364 || $idUser == 2117 || $idUser == 2155 || $idUser == 2149 || $idUser == 2127) {
    $enabled = "";
}
$agency = $_SESSION['agencia'];
if (isset($_GET['agency'])) {
    $agency = $_GET['agency'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>
        <script src="../jquery.maskedinput.js"></script>
        <script src="../js/mask/jquery/src/jquery.mask.js"></script>
        <title> Transactions</title>



    </head>

    <body >

        <div>

            <?
            include("./BarMenu.php");
            ?>

        </div>


        <div class="col-md-10 col-lg-10 col-md-offset-4 col-lg-offset-2" >

           <? include ("./WizardEmployee.php");?>
          
   
        </div>
   




    <script type="text/javascript">


        function EraseRow(id) {
            var id_transaction = id;
            var action = "delete";
            alertify.confirm('Please Confim This Ation', 'Delete Transaction?', function () {


                $.post("./Controllers/UpdateTransaction.php", {id_transaction, action}, function (data) {
                    if (data.status == "success") {
                        $("#row_t2" + id).remove();
                        $("#row_t1" + id).remove();
                        alertify.success(data.message);
                    } else {
                        alertify.error(data.message);
                    }
                });

            }
            , function () {
                alertify.error('Cancel');
            });


        }



       
    </script>
</body>
</html>


