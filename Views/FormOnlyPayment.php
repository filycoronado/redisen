<?php
//include("../inc/dbconnection.php");
session_start();

$idPolicy = "";
$sql = "";

$iduser = $_SESSION['id'];


$QueryPolicy = "SELECT * FROM `policy_type`";
$resultTypes = mysqli_query($mysqli, $QueryPolicy);

$queryCompany = "SELECT * FROM `companyes` where agency=101";
$resultCompany = mysqli_query($mysqli, $queryCompany);
$id_agency = $_SESSION['agencia'];
$company = $_GET['Company'];
$Customer = $_GET['Customer'];
$policyNumber = "";
if ($_GET['id_client'] == $_GET['id_policy']) {
    $id_client = $_GET['id_client'];
    $sql = "SELECT policynumber FROM `UserManagement` where id=$id_client";
    $result = mysqli_query($mysqli, $sql);
    $r = mysqli_fetch_assoc($result);
    $PolicyNumber = $r['policynumber'];
} else {
    $id = $_GET['id_policy'];
    $sql = "SELECT policynumber FROM `PoliciesForOldMGTM` where id=$id";
    $result = mysqli_query($mysqli, $sql);
    $r = mysqli_fetch_assoc($result);
    $PolicyNumber = $r['policynumber'];
}
?>
<form  class="credit-card-div  col-lg-12 col-md-12 col-xs-12">
    <div class="panel panel-default" >
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-3">
                    <span class="help-block text-muted small-font" >  Delivery?</span>
                    <select  id="DeliveryCon" class="form-control selectpicker"  onchange="showOption();">
                        <option value="No" selected>No</option>
                        <option value="Yes">Yes</option>
                    </select>
                </div>
                <div class="col-md-4 deliveryShow">
                    <span class="help-block text-muted small-font" > Company Dealer:</span>
                    <select  id="DeliveryCompany" class="form-control selectpicker" >
                        <?php
                        $sql = "SELECT * FROM `dealers`";
                        $res = mysqli_query($mysqli, $sql);
                        while ($r = mysqli_fetch_assoc($res)) {
                            ?>
                            <option value="<?= $r['id'] ?>"><?= $r['nombre'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 deliveryShow">
                    <span class="help-block text-muted small-font" > Seller Name :</span>
                    <input id="NameSeller" type="text" class="form-control" placeholder="Seller Name" value=""/>
                </div>
                <div class="col-md-4 deliveryShow">
                    <span class="help-block text-muted small-font" > Delivery Name :</span>
                    <input id="NameDelivery" type="text" class="form-control" placeholder="Delivery Name" value=""/>
                </div>
                <div class="col-md-3 deliveryShow">
                    <span class="help-block text-muted small-font" > Amount For Dealer :</span>
                    <input id="AmonDealer" type="number" class="form-control" placeholder="$" value="0"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <span class="help-block text-muted small-font" >  Endorsement?</span>
                    <select  id="IsAendrose" class="form-control selectpicker"  onchange="showOption2();">
                        <option value="No" selected>No</option>
                        <option value="Yes">Yes</option>
                    </select>
                </div>
                <div class="col-md-6 Concept"> 
                    <span class="help-block text-muted small-font" > Concept :</span>
                    <input id="CocneptText" type="text" class="form-control" placeholder="Conncept" value=""/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <span class="help-block text-muted small-font" > Insurance $:</span>
                    <input id="InsAmount" type="number" class="form-control" placeholder="Insurance" value="0"/>
                </div>
                <div class="col-md-3">
                    <span class="help-block text-muted small-font" > ADC $:</span>
                    <input id="AdcAmount" type="number" class="form-control" placeholder="Membership Number" value="0"/>
                </div>
                <div class="col-md-3">
                    <span class="help-block text-muted small-font" > Office $:</span>
                    <input id="Fee" type="number" class="form-control" placeholder="Membership Number" value="0"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <span class="help-block text-muted small-font" > 
                        Type Transaction:

                    </span>
                    <select  id="TypeTransaction" class="form-control selectpicker" >
                        <option value="1">Downpayment</option>
                        <option value="2" selected="">Payment</option>
                        <option value="3">Payment EFT</option>
                        <option value="5">Downpayment EFT</option>
                        <option value="6">CANCELLATION</option>
                        <option value="7">REINSTATEMENT</option>
                        <option value="8">RENEWAL </option>
                        <option value="9">NEW BUSINESS TRANSFER</option>
                        <option value="12">NEW BUSINESS TRANSFER EFT</option>
                        <option value="10">PAY IN FULL</option>
                        <option value="11">AGENT OF RECORD CHANGE</option>

                    </select>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12"> 
                    <span class="help-block text-muted small-font" >
                        Payment Method Ins:        
                    </span>

                    <select  id="InsMp" class="form-control selectpicker" >

                        <option value="1">Payment With CASH</option>
                        <option value="2"> Payment With Debit CC</option>
                    </select>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12"> 
                    <span class="help-block text-muted small-font" >
                        Payment Method ADC:        
                    </span>

                    <select  id="ADCMp" class="form-control selectpicker"  >
                        <option value="1">Payment With CASH</option>
                        <option value="2"> Payment With CARD Chase</option>
                        <option value="3"> Payment With CARD-ADC</option>

                    </select>
                </div>

                <div class="col-md-3 col-sm-12 col-xs-12"> 
                    <span class="help-block text-muted small-font" >
                        Cashier :        
                    </span>

                    <select  id="CashierSel" class="form-control selectpicker">
                        <option value="-1" >None</option>
                        <?php
                        $sqlCashiers = "SELECT * FROM `users` where isCashier=1 and isloggedClock=1 and (agencia=101 or agencia=103) and habilitado=1";
                        $resultCashiers = mysqli_query($mysqli, $sqlCashiers);
                        while ($rowc = mysqli_fetch_assoc($resultCashiers)) {
                            ?>
                            <option value="<?= $rowc['id'] ?>"><?= $rowc['name'] ?></option>
                        <?php } ?>


                    </select>
                </div>
            </div>


            <!--            <div class="row" id="cardInput" hidden>
                            <div class="col-md-3" >
                                <span class="help-block text-muted small-font" > Card Number </span>
                                <input  id="CCNumber" type="text" class="form-control" placeholder="" value=""/>
                            </div>
                            <div class="col-md-2" >
                                <span class="help-block text-muted small-font" > Month </span>
                                <input onkeydown="checkMonth()" id="expdate" type="number" maxlength="2" class="form-control expdate2"  placeholder="05"/>
                            </div>
                            <div class="col-md-2" >
                                <span class="help-block text-muted small-font" > Year </span>
                                <input  onkeydown="checkYear()" id="expyear" type="number" maxlength="2" class="form-control expdate2"  placeholder="18"/>
                            </div>
                            <div class="col-md-2" >
                                <span class="help-block text-muted small-font" > CVV </span>
                                <input  onkeydown="checkcvv()" id="cvvCode" type="number" maxlength="2" class="form-control expdate2"  placeholder="045"/>
                            </div>
                            <div class="col-md-2" >
                                <span class="help-block text-muted small-font" > Confirmation </span>
                                <input id="btnConfirm" type="button"  onclick="CheckCArd();" class="btn btn-success" value="Confrim" />
                            </div>
                        </div>-->




        </div>
        <div class="row ">
            <label id="messageText" hidden></label>
            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">

                <input id="CPayment" type="button"  onclick="MakeApayment();" class="btn btn-success btn-lg" value="Save" />

            </div>

        </div>

    </div>
</form>

<script>
    $(document).ready(function () {

        var value = $("#DeliveryCon").val();
        var value2 = $("#IsAendrose").val();
        if (value == "No") {
            $(".deliveryShow").hide();
        }
        if (value2 == "No") {
            $(".Concept").hide();
        }
    });
    function IsCard() {
        var value = $("#ADCMp").val();
        var value2 = $("#InsMp").val();
        var eft = $("#TypeTransaction").val();
        if ((value != 1 || value2 == 2) && eft == 5) {
            $("#CPayment").hide();
            $("#cardInput").show();
            $("#messageText").text("Add Card For Continue");
            $("#messageText").show();
        } else {
            $("#CCNumber").val("");
            $("#CPayment").show();
            $("#cardInput").hide();
            $("#messageText").hide();
        }
    }

    function checkMonth() {
        var str1 = $("#expdate").val();
        if (str1.length > 1) {
            var slice2 = str1.slice(0, -1);
            $("#expdate").val(slice2);
        }
    }

    function checkYear() {
        var str1 = $("#expyear").val();
        if (str1.length > 1) {
            var slice2 = str1.slice(0, -1);
            $("#expyear").val(slice2);
        }
    }

    function checkcvv() {
        var str1 = $("#cvvCode").val();
        if (str1.length > 2) {
            var slice2 = str1.slice(0, -1);
            $("#cvvCode").val(slice2);
        }
    }
    function CheckCArd() {

        var card = $("#CCNumber").val();
        var cvv = $("#cvvCode").val();
        var year = $("#expyear").val();
        var month = $("#expdate").val();
        var expfulldate = month + "/" + year;


        if (card.length > 9 && cvv.length > 2 && expfulldate.length > 4) {
            $("#CPayment").show();
            $("#btnConfirm").hide();
            $("#CPayment").val("Finish Payment");
        }
    }
    function EnableCashiers() {
        var adcMetpd = $("#ADCMp").val();
        var InsMP = $("#InsMp").val();
        if (adcMetpd == 1 || InsMP == 1) {
            $("#CashierSel").show();
        } else {
            $("#CashierSel").hide();
        }
    }

    function showOption() {
        var value = $("#DeliveryCon").val();
        if (value == "No") {
            $(".deliveryShow").hide();
        } else {
            $(".deliveryShow").show();
        }

    }
    function showOption2() {
        var value = $("#IsAendrose").val();
        if (value == "No") {
            $(".Concept").hide();
        } else {
            $(".Concept").show();
        }

    }
    function MakeApayment() {
        var con = confirm("Complete Transaction?");
        if (con) {
            var DeliveryOptions = $("#DeliveryCon").val();


            var id_client = Gid_customer;
            var id_user =<?= $iduser ?>;
            var id_policy = policy_created;
            var Fee = $("#Fee").val();
            var id_agency =<?= $id_agency ?>;
            var TypeTransaction = $("#TypeTransaction").val();
            var InsMp = $("#InsMp").val();
            var ADCMp = $("#ADCMp").val();
            var CashierSel = $("#CashierSel").val();
            var InsAmount = $("#InsAmount").val();
            var AdcAmount = $("#AdcAmount").val();


            var company =<?= $company ?>;
            var nameCustomer = "<?= $Customer ?>";
            var LastNameCustomer = "";
            var nameClient = nameCustomer + " " + LastNameCustomer;
            var policyN = "<?= $PolicyNumber ?>";

            var DeliveryCompany = $('#DeliveryCompany').val();
            var NameSeller = $('#NameSeller').val();
            var NameDelivery = $('#NameDelivery').val();
            var AmonDealer = $('#AmonDealer').val();

            var IsAendrose = $("#IsAendrose").val();
            var CocneptText = $("#CocneptText").val();


            /*var card = $("#CCNumber").val();
             var cvv = $("#cvvCode").val();
             var year = $("#expyear").val();
             var month = $("#expdate").val();
             var expfulldate = month + "/" + year;*/


            $.post("/MGTM/Views/Controllers/CompletePayment.php", {id_client, id_user, id_policy, Fee, TypeTransaction, InsMp, ADCMp, CashierSel, InsAmount, AdcAmount, company, nameClient, policyN, DeliveryOptions, DeliveryCompany, NameSeller, NameDelivery, AmonDealer, IsAendrose, CocneptText, id_agency}, function (data) {
                if (data.message != "Error") {
                    if (data.status == "success") {

                        alertify.success(data.message);
                        completePayment = true;
                        $(".request_field").removeClass("border-error-field");
                        var url = "../../recibos/" + data.filepdf;
                        $("#final_ticket").attr("src", url);
                        $("#tagdone").click();
                        // location.href = data.url;
                    } else {
                        alertify.error(data.message);
                    }

                }
            });
        } else {

        }
    }
</script>