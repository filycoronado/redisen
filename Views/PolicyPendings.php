<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");
//include ("./../Views/Controllers/Utils.php");
$iduser = $_SESSION['id'];
$username = $_SESSION['username'];

$todayDate = date("m-d-Y");
$agency_user = $_SESSION['agencia'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>





        <title>Pending Cancellations</title>

    </head>
    <style>
        .ui-widget-header {
            border: 1px solid #2d1c1c !important;
            color: #ffffff !important;
            font-weight: bold;
            background: #ed502e !important;
        }
        .ui-widget-content {
            color: #ed502e !important;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {

            font-weight: bold !important;;
            color: #f3f1f1 !important;;
            background: #ed502e !important;;
            border-collapse: collapse !important;
        }
        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            border: 1px solid #f7360c  !important;;

            color: #f7360c  !important;
            background: white  !important;
        }
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            border: 1px solid #aaaaaa;
            font-weight: bold  !important;
            color: #000000 !important;
            background: #ed502e7a !important;
        }

        .btn-round {
            color: #666;
            padding: 3px 9px;
            font-size: 13px;
            line-height: 1.5;
            background-color: #fff;
            border-color: #ccc;
            border-radius: 50px;
        }

        .btn.active {                
            display: none;		
        }

        .btn span:nth-of-type(1)  {            	
            display: none;
        }
        .btn span:last-child  {            	
            display: block;		
        }

        .btn.active  span:nth-of-type(1)  {            	
            display: block;		
        }
        .btn.active span:last-child  {            	
            display: none;			
        }
    </style>
    <body >




        <div>

            <?php
            include("./BarMenu.php");
            ?>

        </div>
        <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 container" style="margin-top: 2%;" >
            <?
            $date = date("Y-m-d");


            $f2 = strtotime('+1 day', strtotime($date));
            $f2 = date('Y-m-d', $f2);

            $f3 = strtotime('+2 day', strtotime($date));
            $f3 = date('Y-m-d', $f3);

            $f4 = strtotime('+3 day', strtotime($date));
            $f4 = date('Y-m-d', $f4);

            $f5 = strtotime('+4 day', strtotime($date));
            $f5 = date('Y-m-d', $f5);

            $f6 = strtotime('+5 day', strtotime($date));
            $f6 = date('Y-m-d', $f6);

            $f7 = strtotime('+6 day', strtotime($date));
            $f7 = date('Y-m-d', $f7);


            $f8 = strtotime('+7 day', strtotime($date));
            $f8 = date('Y-m-d', $f8);

            $sql1 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$date' and habilitado=1";
            $result1 = mysqli_query($mysqli, $sql1);


            $sql2 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$f2' and habilitado=1";
            $result2 = mysqli_query($mysqli, $sql2);

            $sql3 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$f3' and habilitado=1";
            $result3 = mysqli_query($mysqli, $sql3);

            $sql4 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$f4' and habilitado=1";
            $result4 = mysqli_query($mysqli, $sql4);

            $sql5 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$f5' and habilitado=1";
            $result5 = mysqli_query($mysqli, $sql5);

            $sql6 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$f6' and habilitado=1";
            $result6 = mysqli_query($mysqli, $sql6);

            $sql7 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$f7' and habilitado=1";
            $result7 = mysqli_query($mysqli, $sql7);


            $sql8 = "SELECT * FROM `Cancellations` where status_policy=1 and canceldate='$f8' and habilitado=1";
            $result8 = mysqli_query($mysqli, $sql8);

            $date = split(" ", $date);
            $date = date("m-d-Y", strtotime($date[0]));

            $f2 = split(" ", $f2);
            $f2 = date("m-d-Y", strtotime($f2[0]));

            $f3 = split(" ", $f3);
            $f3 = date("m-d-Y", strtotime($f3[0]));

            $f4 = split(" ", $f4);
            $f4 = date("m-d-Y", strtotime($f4[0]));

            $f5 = split(" ", $f5);
            $f5 = date("m-d-Y", strtotime($f5[0]));

            $f6 = split(" ", $f6);
            $f6 = date("m-d-Y", strtotime($f6[0]));


            $f7 = split(" ", $f7);
            $f7 = date("m-d-Y", strtotime($f7[0]));

            $f8 = split(" ", $f8);
            $f8 = date("m-d-Y", strtotime($f8[0]));
            ?>

            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $date ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result1)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>

                        <div class="col-md-4">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>

                            </div>
                        </div>

                    <? } ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-warning" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $f2 ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result2)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>
                        <div class="col-md-4">
                            <div class="alert alert-warning" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                            </div>
                        </div>
                    <? } ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $f3 ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result3)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>
                        <div class="col-md-4">
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                            </div>
                        </div >
                    <? } ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $f4 ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result4)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>
                        <div class="col-md-4">
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                            </div>
                        </div >
                    <? } ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $f5 ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result5)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>
                        <div class="col-md-4">
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                            </div>
                        </div >
                    <? } ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $f6 ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result6)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>
                        <div class="col-md-4">
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                            </div>
                        </div >
                    <? } ?>
                </div>
            </div>


            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $f7 ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result7)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>
                        <div class="col-md-4">
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                            </div>
                        </div >
                    <? } ?>
                </div>
            </div>

            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?= $f8 ?>
                </div>
                <div  style="    overflow-y: auto; height: 600px;">
                    <?
                    while ($r = mysqli_fetch_assoc($result8)) {
                        $fecha = split(" ", $r['canceldate']);
                        $fecha = date("m-d-Y", strtotime($fecha[0]));

                        $name = $r['customer_name'];
                        $idCLient = $r['customer_id'];
                        ?>
                        <div class="col-md-4">
                            <div class="alert alert-success" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span> <?= $fecha ?>

                                <p> <strong><?= $name ?></strong></p><br>
                                <p> <strong><? ?></strong></p>
                                <hr class="message-inner-separator">

                                <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                            </div>
                        </div >
                    <? } ?>
                </div>
            </div>

        </div>


    </body>






    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

</html>

<script>
    function CheckTime(value) {
        var id_user =<?= $iduser ?>;
        var agency =<?= $agency_user ?>;
        $.post("./Controllers/CheckMoves.php", {value, id_user, agency}, function (data) {
            if (data.status == "success") {
                alertify.success(data.message);
                setTimeout('document.location.reload()', 2000);
            } else {
                alertify.error(data.message);
            }
        });
    }
</script>