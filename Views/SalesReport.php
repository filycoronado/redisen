<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];
$todayDate = date("m-d-Y");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>


        <title>Sales Report </title>

    </head>
    <style>
        .ui-widget-header {
            border: 1px solid #2d1c1c !important;
            color: #ffffff !important;
            font-weight: bold;
            background: #ed502e !important;
        }
        .ui-widget-content {
            color: #ed502e !important;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {

            font-weight: bold !important;;
            color: #f3f1f1 !important;;
            background: #ed502e !important;;
            border-collapse: collapse !important;
        }
        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            border: 1px solid #f7360c  !important;;

            color: #f7360c  !important;
            background: white  !important;
        }
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            border: 1px solid #aaaaaa;
            font-weight: bold  !important;
            color: #000000 !important;
            background: #ed502e7a !important;
        }
    </style>
    <body >




        <div>

            <?php
            include("./BarMenu.php");
            ?>

        </div>
        <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 container" >
            <div class="col-lg-12 col-md-12" style="margin-top: 3%;">
                <div class="col-md-2 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                        <select id="agencies" class="form-control" >   
                           
                            <?
                            $agencia = $_SESSION['agencia'];
                            if ($agencyP == 1) {
                                echo '<option value="-1">All Agencies</option>';
                                $sql = "SELECT id, nombre FROM `agency` where id_owner=1";
                            } else {
                                $sql = "SELECT id, nombre FROM `agency` where id=$agencia";
                            }
                            $result = mysqli_query($mysqli, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                ?>
                                <option value="<?= $row['id'] ?>"><?= $row['id'] . "" . $row['nombre'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-2 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <select id="users" class="form-control" >   
                            <option value="-1">All Users</option>
                            <?
                            $sql = "SELECT * FROM `users` where habilitado=1 and (nivel=2 or nivel=1 or nivel=5) and (agencia=101 or agencia=103 or agencia=104 or agencia=105)";
                            $result = mysqli_query($mysqli, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                ?>
                                <option value="<?= $row['id'] ?>"><?= $row['user'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
                        <select id="companies" class="form-control" >   
                            <option value="-1">All Companies</option>
                            <?
                            $sql = "SELECT id,nombre FROM `companyes` where agency=101 and active=1";
                            $result = mysqli_query($mysqli, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                ?>
                                <option value="<?= $row['id'] ?>"><?= $row['nombre'] ?></option>
                            <? } ?>
                        </select>                                    
                    </div>
                </div>
                <div class="col-md-3 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input id="date1"  class="form-control datePickerSel"  value="<?= $todayDate ?>" placeholder="MM-DD-YYYY">                                        
                    </div>
                </div>
                <div class="col-md-2 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        <input id="date2"  class="form-control datePickerSel"  value="<?= $todayDate ?>" placeholder="MM-DD-YYYY">                                        
                    </div>
                </div>


            </div>
            <div class="col-lg-12 col-md-12" style="margin-top:1%;">
                <div class="col-md-2 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                        <select id="TypePolicy"  class="form-control"  >  
                            <option value="-1">All Types</option>
                            <?
                            $sql = "SELECT id,name FROM `policy_type` ORDER BY `policy_type`.`id` ASC";
                            $result = mysqli_query($mysqli, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                ?>
                                <option value="<?= $row['id'] ?>"> <?= $row['name'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-credit-card"></i></span>
                        <select id="MethodTransaction"  class="form-control" > 
                            <option value="-1">ALL Methods</option>
                            <option value="1">Cash</option>
                            <option value="2">Card</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
                        <select id="TypeSell"  class="form-control" > 
                            <option value="-1">ALL</option>
                            <option value="0">Erase</option>
                            <option value="1">NEW BUSINESS</option>
                            <option value="2">PAYMENT</option> 
                            <option value="4">ENDORSE</option>  
                            <option value="3">PAYMENT EFT</option> 
                            <option value="5">DOWNPAYMENT EFT</option>  
                            <option value="6">CANCELLATION </option>
                            <option value="7">REINSTATEMENT </option>
                            <option value="8">RENEWAL </option>
                            <option value="9">NEW BUSINESS TRANSFER</option>
                            <option value="12">NEW BUSINESS TRANSFER EFT</option>
                            <option value="10">PAY IN FULL</option>  
                            <option value="11">AGENT OF RECORD CHANGE</option>   
                        </select>
                    </div>
                </div>
                <button id="Btn_sub" type="submit" class="btn btn-primary" onclick="GetReport();">Submit</button>
            </div>

            <div id="ContReport" >

            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#Btn_sub').trigger('click');
            });
            function  GetReport() {
                $("#ContReport").empty();
                var agencies = $("#agencies").val();
                var usres = $("#users").val();
                var companies = $("#companies").val();
                var date1 = $("#date1").val();
                var date2 = $("#date2").val();
                var TypePolicy = $("#TypePolicy").val();
                var MethodTransaction = $("#MethodTransaction").val();
                var TypeSell = $("#TypeSell").val();
                var caseReport = 1;
                $.post("./Controllers/CreateReport.php", {agencies, usres, companies, date1, date2, TypePolicy, MethodTransaction, TypeSell, caseReport}, function (data) {
                    $("#ContReport").html(data);
                });
            }

        </script>
    </body>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>



    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
            $("#datepicker").datepicker();
            $("#datefinal").datepicker();
            $(".datePickerSel").datepicker({dateFormat: 'mm-dd-yy'});


            function EraseRow(id) {
                var id_transaction = id;
                var action = "delete";
                alertify.confirm('Please Confim This Ation', 'Delete Transaction?', function () {


                    $.post("./Controllers/UpdateTransaction.php", {id_transaction, action}, function (data) {
                        if (data.status == "success") {
                            $("#row_t2" + id).remove();
                            $("#row_t1" + id).remove();
                            alertify.success(data.message);
                        } else {
                            alertify.error(data.message);
                        }
                    });

                }
                , function () {
                    alertify.error('Cancel');
                });


            }
    </script>
</html>