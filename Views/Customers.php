<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");
$bsearch = true;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>

        <title>Customers</title>

        <style>
            .glyphicon-lg
            {
                font-size:4em
            }
            .info-block
            {
                border-right:5px solid #E6E6E6;
                border-bottom: 5px solid;
                margin-bottom: 20px;
                border-radius: 8px;
            }
            .info-block .square-box
            {
                width:100px;min-height:110px;margin-right:22px;text-align:center!important;background-color:#676767;padding:20px 0
            }
            .info-block.block-info
            {
                border-color:#20819e
            }
            .info-block.block-info .square-box
            {
                background-color:#20819e;color:#FFF
            }
            .info-block.block-info .square-box:hover{
                background-color: #ed502e;
                cursor: pointer;
            }
        </style>

    </head>

    <body >
        <?
        $TAMANO_PAGINA = 250;

        //examino la página a mostrar y el inicio del registro a mostrar
        $pagina = $_GET["pagina"];
        if (!$pagina) {
            $inicio = 0;
            $pagina = 1;
        } else {
            $inicio = ($pagina - 1) * $TAMANO_PAGINA;
        }

        $sql = "SELECT * FROM `UserManagement` where (agencia=101 or agencia=102 or agencia=103 or agencia=104 or agencia=105)";
        $result = mysqli_query($mysqli, $sql);
        $num_total_registros = mysqli_num_rows($result);
        $total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);
        ?>
        <div style="height:100%">

            <div>
                <? include("./BarMenu.php"); ?>
            </div>
            <div class="col-md-8 col-lg-8 col-md-offset-4 col-lg-offset-3" >


                <div class="row">
                    <h2>Filter Name</h2>
                    <div class="col-lg-4 col-md-4">
                        <input id="SearchName" type="search" class="form-control" id="input-search" placeholder="Search..." >
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <butto class="btn btn-lg btn-success" onclick="Filter()">Submit</butto>
                    </div>
                </div>
                <div class="row" id="Contenido">
                    <div class="searchable-container">
                        <?
                        $sql = "SELECT * FROM `UserManagement` where (agencia=101 or agencia=102 or agencia=103 or agencia=104 or agencia=105) LIMIT " . $inicio . "," . $TAMANO_PAGINA . "";
                        $result = mysqli_query($mysqli, $sql);
                        while ($row = mysqli_fetch_assoc($result)) {
                            ?>
                            <div class="items col-xs-12 col-sm-6 col-md-6 col-lg-6 clearfix " id="cont_client<?= $row['id'] ?>">
                                <div class="info-block block-info clearfix">
                                    <a href="CustomerDetails.php?id=<?= $row['id'] ?>">
                                        <div class="square-box pull-left">
                                            <span class="glyphicon glyphicon-user glyphicon-lg"></span>
                                        </div>
                                    </a>
                                    <h5>Location:<?= $row['agencia'] ?></h5>
                                    <h4>Name: <?= strtoupper($row['nombre']) ?></h4>
                                    <p><span>Phone:<?= $row['tel'] ?></span></p>
                                    <p><span>Email: <?= $row['mail'] ?></span></p>

                                    <? if ($eraseP == 1) { ?> <button  class="btn btn-danger "  onclick="EraseUser(<?= $row['id'] ?>), 'cont_client<?= $row['id'] ?>'">Erase</button><? } ?>
                                </div>


                            </div>
                        <? } ?>
                    </div>
                </div>
                <div class="container">
                    <ul class="pagination">
                        <li class="disabled"><a href="#">«</a></li>
                        <?
                        $class = "";
                        for ($i = 1; $i < $total_paginas; $i++) {
                            if ($pagina == $i) {
                                $class = "active";
                            } else {
                                $class = "";
                            }
                            ?>
                            <li class="<?= $class ?>"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
                        <? } ?>

                    </ul>
                </div>
            </div>

        </div>
        <style>
            .loader{
                width: 70px;
                height: 70px;
                margin: 40px auto;
            }
            .loader p{
                font-size: 16px;
                color: #777;
            }
            .loader .loader-inner{
                display: inline-block;
                width: 15px;
                border-radius: 15px;
                background: #74d2ba;
            }
            .loader .loader-inner:nth-last-child(1){
                -webkit-animation: loading 1.5s 1s infinite;
                animation: loading 1.5s 1s infinite;
            }
            .loader .loader-inner:nth-last-child(2){
                -webkit-animation: loading 1.5s .5s infinite;
                animation: loading 1.5s .5s infinite;
            }
            .loader .loader-inner:nth-last-child(3){
                -webkit-animation: loading 1.5s 0s infinite;
                animation: loading 1.5s 0s infinite;
            }
            @-webkit-keyframes loading{
                0%{
                    height: 15px;
                }
                50%{
                    height: 35px;
                }
                100%{
                    height: 15px;
                }
            }
            @keyframes loading{
                0%{
                    height: 15px;
                }
                50%{
                    height: 35px;
                }
                100%{
                    height: 15px;
                }
            }
        </style>
        <!--        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="loader">
                                <p>Loading...</p>
                                <div class="loader-inner"></div>
                                <div class="loader-inner"></div>
                                <div class="loader-inner"></div>
                            </div>
                        </div>
                    </div>
                </div>-->
        <script>
            function Filter() {
                var name = $("#SearchName").val();
                var eraseP =<?= $eraseP ?>;
                if (name != "") {
                    $("#Contenido").empty();
                    $.post("/MGTM/Views/Controllers/FilterCustomers.php", {name, eraseP}, function (data) {
                        $("#Contenido").html(data);
                    });
                }
            }

            function EraseUser(id_client, id_row_erase) {
                $.post(" './Controllers/EraseCleint.php'", {id_client}, function (data) {
                    if (data.status == "success") {
                        alertify.success(response.message);
                        $("#" + id_row_erase).remove();
                    } else {
                        alertify.error(response.message);
                    }
                });
            }

        </script>
    </body>
</html>