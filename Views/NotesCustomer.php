<?php
session_start();
$id_client = $_GET['id'];

$idpol = 0;
if (isset($_POST['idPol'])) {
    $idpol = $_POST['idPol'];
}


$companyID = $_POST['companyID'];

if (isset($_GET['id'])) {
    $id_client = $_GET['id'];
}
$sql = "SELECT * FROM `UserManagement` where id=$id_client";
$result = mysqli_query($mysqli, $sql);
$customer = mysqli_fetch_assoc($result);



$cantNotes = "SELECT * FROM `notas_mgtm` where habilitado=1 and id_cliente=$id_client ORDER BY fecha DESC";
$pagina = 0;
if (isset($_GET['page'])) {
    $pagina = $_GET['page'];
}

//Cuenta el número total de registros
$res = mysqli_query($mysqli, $cantNotes);
$total_registros = mysqli_num_rows($res);
$cantidad_resultados_por_pagina = 5;
//Obtiene el total de páginas existentes
$total_paginas = ceil($total_registros / $cantidad_resultados_por_pagina);

$empezar_desde = $pagina;

?>
<section class="content">

    <div class="col-md-12 ">
        <div class="panel panel-default">
            <div class="table-container">
                <table class="table table-filter" style="margin-bottom: 0px !important;">
                    <tbody>


                        <?php
                        $sql = "";
                        $factual = date("Y-m-d");
                        $cont = 0;

                        $sql = "SELECT * FROM `notas_mgtm` where habilitado=1 and id_cliente=$id_client ORDER BY id DESC LIMIT  $empezar_desde, $cantidad_resultados_por_pagina";

                        $result = mysqli_query($mysqli, $sql);
                        while ($row = mysqli_fetch_assoc($result)) {
                            $fecha = $row['fecha'];
                            $idUserSelected = $row['user_asigned'];
                            $fechafollow = $row['fecha_follow'];
                            $note = $row['nota'];
                            $fecha2 = date("Y-m-d H:i:s", strtotime($fecha));
                            $val = $row['status_aplication'];
                            $userCreate = $row['id_usuario'];
                            $createName = regresauser($userCreate, $mysqli);
                            ?>
                            <tr data-status="pagado" >

                                <td>
                                    <a href="#" class="btn btn-info btn-lg" onclick="UpdateNote(<?= $row['id'] ?>);">
                                        <span class="glyphicon glyphicon-save-file" ></span> Submit
                                    </a>
                                </td>
                                <td style="width: 151px !important;">
                                    <div class="col-md-12">
                                        <span class="help-block text-muted small-font" >  User Asigned</span>
                                        <select  id="user_Asigned<?= $row['id'] ?>" class="form-control selectpicker">
                                            <?php
                                            $sqlUser = "SELECT * FROM `users` where (agencia=101 or agencia=103 or agencia=102 or agencia=104 or agencia=105) and visible=1 and (nivel=5 or nivel=2 or nivel=1)";
                                            $result2 = mysqli_query($mysqli, $sqlUser);
                                            while ($row2 = mysqli_fetch_assoc($result2)) {
                                                ?>
                                                <option value="<?= $row2['id'] ?>" <? if ($idUserSelected == $row2['id']) { ?> selected <? } ?> ><?= $row2['user'] ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <span class="help-block text-muted small-font" >  Status </span>
                                        <select  id="StatusNote<?= $row['id'] ?>" class="form-control selectpicker"  >
                                            <option value="0" <? if ($val == 0) { ?>selected <? } ?>>Incomplete</option>
                                            <option value="1" <? if ($val == 1) { ?>selected <? } ?>>Complete</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="media col-12">
                                        <a href="#" class="pull-left">
                                            <img src="<?= $img ?>" style="width: 95px !important;" class="media-photo">
                                        </a>
                                        <div class="media-body" >
                                            <strong>    <span class="media-meta pull-right">Create Date<?= $fecha ?></span></strong>
                                            <h4 class="title">
                                                <div class="alert alert-success">
                                                    <strong>Note:</strong> <p><?= $note ?><p>
                                                </div>

                                            </h4>
                                            <h4 class="title">
                                                <span class="pull-right pagado">Create By: <?= $createName ?> </span>

                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <span class="help-block text-muted small-font" > Date Follow</span>
                                        <input id="DateFollow<?= $row['id'] ?>" type="date" style='border-style: none;' value="<?= $fechafollow ?>">
                                    </div>

                                </td>

                            </tr>
                        <? } ?>


                    </tbody>
                </table>

                <nav aria-label="Page navigation example">
                    <ul class="pagination">

                        <?php
                        $class = "";
                        for ($i = 1; $i <= $total_paginas; $i++) {
                            if ($pagina > 0) {
                                if ($pagina == $i) {
                                    $class = "active";
                                } else {
                                    $class = "";
                                }
                            } else {
                                $pagina = 1;
                                if ($pagina == $i) {
                                    $class = "active";
                                } else {
                                    $class = "";
                                }
                            }
                            ?>    

                            <li class="page-item <?= $class ?>"><a class="page-link" href="CustomerDetails.php?id=<?= $id_client ?>&page=<?= $i ?>"><?= $i ?></a></li>
                            <?php
                        }
                        ?>



                    </ul>
                </nav>


                <div class="row">
                    <div class="col-md-8">


                        <input type="text" id="id_userNote" value="<?= $_SESSION['id'] ?>"  hidden=>
                        <input type="text" id="id_client_Note" name="id_client" value="<?= $id_client ?>" hidden  >


                        <textarea   name="editor1" class="col-md-12 alert alert-success"  ></textarea>

                        <script>
                            CKEDITOR.replace('editor1');
                        </script>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a  class="btn btn-info btn-lg" onclick="SaveNote(<?= $row['id'] ?>);">
                            <span class="glyphicon glyphicon-save-file" ></span> Submit
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>


<script>
    function UpdateNote(Noteid) {
        var user_Asigned = $("#user_Asigned" + Noteid).val();
        var StatusNote = $("#StatusNote" + Noteid).val();
        var DateFollow = $("#DateFollow" + Noteid).val();
        var Noteid = Noteid;

        $.post("/MGTM/Views/Controllers/UpdateNoteCustomer.php", {user_Asigned, StatusNote, DateFollow, Noteid}, function (data) {
            alert(data.message);
        });
    }
    function SaveNote() {

        var user2 = JSON.parse(window.localStorage.getItem('user_mgtm'));
        console.log(user2.id);


        var id_userNote = user2.id;
        var id_client_Note = $("#id_client_Note").val();
        //var NoteCont = $("#NoteCont").val();
        var NoteCont = CKEDITOR.instances.editor1.getData();
        $.post("/MGTM/Views/Controllers/CreateNote.php", {id_userNote, id_client_Note, NoteCont}, function (data) {
            if (data.status == "success") {
                alertify.success(data.message);
                
                 location.reload();
            } else {
                alertify.error(data.message);
            }

        });
    }



</script>


<?php

function resgresanombrecompania3($val) {
    $sql = "SELECT * FROM `companyes` where id=" . $val;

    $res = mysql_query($sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['nombre'];
    }
}

function regresaclientes($val) {

    $sql = "SELECT * FROM `UserManagement` where id=" . $val;

    $res = mysql_query($sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['nombre'];
    }
}

function regresauser($val, $mysqli) {

    $sql = "SELECT * FROM `users` where id=" . $val;

    $res = mysqli_query($mysqli, $sql);

    while ($re = mysqli_fetch_assoc($res)) {
        return $re['user'];
    }
}
?>

