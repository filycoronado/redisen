<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];

$todayDate = date("m-d-Y");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>


        <title>Retention Report</title>

    </head>
    <style>
        .ui-widget-header {
            border: 1px solid #2d1c1c !important;
            color: #ffffff !important;
            font-weight: bold;
            background: #ed502e !important;
        }
        .ui-widget-content {
            color: #ed502e !important;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {

            font-weight: bold !important;;
            color: #f3f1f1 !important;;
            background: #ed502e !important;;
            border-collapse: collapse !important;
        }
        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            border: 1px solid #f7360c  !important;;

            color: #f7360c  !important;
            background: white  !important;
        }
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            border: 1px solid #aaaaaa;
            font-weight: bold  !important;
            color: #000000 !important;
            background: #ed502e7a !important;
        }
    </style>
    <body >




        <div>

            <?php
            include("./BarMenu.php");
            ?>

        </div>
        <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 container" >
            <div class="col-lg-12 col-md-12" style="margin-top: 3%;">



            </div>


            <div id="ContReport" >
                <div class="panel panel-primary" >
                    <div class="panel-heading print_row">Agencies Production</div>

                    <?
                    $idDealer = 0;
                    $nameDealer = "";

                    $fecha1 = new DateTime();
                    $fecha1->modify('first day of this month');
                    $f1 = $fecha1->format('m-d-Y');

                    $fecha = new DateTime();
                    $fecha->modify('last day of this month');
                    $f2 = $fecha->format('m-d-Y');
                    ?>
                    <div class="row print_row">
                        <h1 style="padding: 14px !important;"><?= $nameDealer ?></h1>
                    </div>
                    <div class="row print_row">
                        <div class="col-md-2 col-lg-2">
                            <span class="help-block text-muted small-font" >Agency</span>
                            <select id="Agency"  class="form-control" >
                                <option value="ALL">ALL</option>
                                <?
                                $sql = "SELECT * FROM `agency` where id_owner=1";
                                $result = mysql_query($sql);
                                while ($row = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['id'] ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <span class="help-block text-muted small-font" >Date 1</span>
                            <input id="datepicker"  type="text" class="form-control" placeholder="325511" value="<?= $f1 ?>" />
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <span class="help-block text-muted small-font" >Date 2</span>
                            <input id="datepicker2"  type="text" class="form-control" placeholder="" value="<?= $f2 ?>" />
                        </div>
                        <div class="col-md-2 col-lg-2">

                            <button id="btn_submit" class="btn btn-success " style="margin-top: 32px;" onclick="GetReport()">Submit</button>
                        </div>
                    </div>
                    <div class="row" id="Cont"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#Btn_sub').trigger('click');
            });
            function  GetReport() {
                $("#ContReport").empty();
                var date1 = $("#datepicker").val();
                var date2 = $("#datepicker2").val();
                var Agency = $("#Agency").val();
                $.post("./Controllers/GetCompanyReport.php", {Agency, date1, date2}, function (data) {
                    $("#ContReport").empty();
                    $("#ContReport").html(data);
                });



            }

        </script>
    </body>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>



    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
            $("#datepicker").datepicker();
            $("#datefinal").datepicker();
            $(".datePickerSel").datepicker({dateFormat: 'mm-dd-yy'});
    </script>
</html>