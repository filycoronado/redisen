<?php
//include("../inc/dbconnection.php");
$idClient = 0;
$flag = 0; //new customer
$iduser = $iduserloig;
$agencia_inUse = $_SESSION['agencia'];
$username = $_SESSION["username"];
if (isset($_POST['id_client'])) {
    $flag = 1;
    $idClient = $_POST['id_client'];
    $sql = "SELECT * FROM `UserManagement` where  id=$idClient";
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
}
if (isset($_GET['id'])) {
    $flag = 1;
    $idClient = $_GET['id'];
    $sql = "SELECT * FROM `UserManagement` where  id=$idClient";
    $result = mysqli_query($mysqli, $sql);
    $row = mysqli_fetch_assoc($result);
}
if ($flag == 0) {
    $iduser = $iduserloig;
}
?>

<form class="credit-card-div  col-lg-12 col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">

            <div class="row">
                <div class="col-md-4">
                    <select id="LanguajePreference" class="form-control selectpicker request_field">
                        <option value="0" <? if ($row['languajePrefernce'] == 0) { ?>selected<? } ?>>Select Languaje</option>
                        <option value="1" <? if ($row['languajePrefernce'] == 1) { ?>selected<? } ?>>English</option>
                        <option value="2" <? if ($row['languajePrefernce'] == 2) { ?>selected<? } ?>>Spanish</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <span class="help-block text-muted small-font"> Customer Name:</span>
                    <input id="nombre" type="text" class="form-control request_field" placeholder="Jhon Due" value="<?= $row['nombre']; ?>" />
                </div>
                <div class="col-md-6">
                    <label><input type="checkbox" id="check_email" <? if ($row["verifed_email"] != 0) { ?>checked<? } ?>> Verified email By <?= $row["verifed_email_user"] ?></label><br>
                    <label><input type="checkbox" id="check_addres" <? if ($row["verifed_addres"] != 0) { ?>checked<? } ?>> Verified address By <?= $row["verifed_addres_user"] ?></label><br>
                    <label><input type="checkbox" id="check_phone" <? if ($row["verified_phone"] != 0) { ?>checked<? } ?>> Verified phone By <?= $row["verified_phone_user"] ?></label><br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font"> Address:</span>
                    <input id="address" type="text" class="form-control request_field" placeholder="W Ajo way 25" value="<?= $row['address']; ?>" />
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font"> City;</span>
                    <input id="city" type="text" class="form-control request_field" placeholder="Tucson" value="<?= $row['city']; ?>" />
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font"> Zip Code:</span>
                    <input id="zipCode" type="text" class="form-control request_field" placeholder="2345" value="<?= $row['zipcode']; ?>" />
                </div>
            </div>
            <div class="row ">
                <div class="col-md-6 pad-adjust">
                    <span class="help-block text-muted small-font"> Primary email</span>
                    <input id="PrimaryEmail" type="email" class="form-control" placeholder="Primary Email" value="<?= $row['mail']; ?>" />
                </div>

                <div class="col-md-6 pad-adjust">
                    <span class="help-block text-muted small-font"> Secondary email</span>
                    <input id="SecondaryMail" type="email" class="form-control" placeholder="Secondary Email" value="<?= $row['addemail']; ?>" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 pad-adjust">
                    <span class="help-block text-muted small-font"> Primary Phone</span>
                    <input id="PrimaryPhone" type="text" class="form-control phoneMask request_field" placeholder="Primary Phone" value="<?= $row['tel']; ?>" />
                </div>
                <div class="col-md-3 pad-adjust">
                    <span class="help-block text-muted small-font"> Secondary Phone</span>
                    <input id="SecondPhone" type="text" class="form-control phoneMask" placeholder="Secondary Phone" value="<?= $row['addphone']; ?>" />
                </div>

                <div class="col-md-3 pad-adjust">
                    <span class="help-block text-muted small-font"> Home Phone</span>
                    <input id="Homep" type="text" class="form-control phoneMask" placeholder="Home Phone" value="<?= $row['homephone']; ?>" />
                </div>

                <div class="col-md-3 pad-adjust">
                    <span class="help-block text-muted small-font"> Work Phone</span>
                    <input id="Workp" type="text" class="form-control phoneMask" placeholder="Work Phone" value="<?= $row['workphone']; ?>" />
                </div>
            </div>



        </div>
        <div class="row container" style="    padding: 10px;">
            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                <input type="button" onclick="<? if ($flag == 0) { ?>NewSave()<? } else { ?>Save()<? } ?>" class="btn btn-success" value="Save" />
            </div>

        </div>

    </div>

</form>

<script>
    function Save() {
        var id_client = <?= $idClient ?>;
        var isNewCustomer = <?= $flag ?>;
        var userCreate = <?= $iduser ?>;
        var agencia = <?= $agenica_sel ?>;
      
        var workPhone = $("#Workp").val();
        var Home = $("#Homep").val();
        var SecondPhone = $("#SecondPhone").val();
        var Primary = $("#PrimaryPhone").val();
        var emailSecond = $("#SecondaryMail").val();
        var emailPrimary = $("#PrimaryEmail").val();
        var zipCode = $("#zipCode").val();
        var city = $("#city").val();
        var address = $("#address").val();
        var apellido = $("#apellido").val();
        var nombre = $("#nombre").val();
        var languaje = $("#LanguajePreference").val();

        var check_email = 0;
        var check_address = 0;
        var check_phone = 0;
        if ($('#check_email').is(":checked")) {
            // it is checked
            check_email = 1;
        }
        if ($('#check_addres').is(":checked")) {
            // it is checked
            check_address = 1;
        }

        if ($('#check_phone').is(":checked")) {
            // it is checked
            check_phone = 1;
        }
        $.post("/MGTM/Views/Controllers/UpdateCustomer.php", {
            workPhone,
            Home,
            SecondPhone,
            Primary,
            emailSecond,
            emailPrimary,
            zipCode,
            city,
            address,
            apellido,
            nombre,
            id_client,
            languaje,
            isNewCustomer,
            userCreate,
            agencia,
            check_email,
            check_address,
            check_phone


        }, function(data) {
            alert(data.message);
        });
    }

    function NewSave() {
        var id_client = <?= $idClient ?>;
        var isNewCustomer = <?= $flag ?>;
        var userCreate = <?= $iduser ?>;
        var agencia = <?= $agenica_sel ?>;
        var workPhone = $("#Workp").val();
        var Home = $("#Homep").val();
        var SecondPhone = $("#SecondPhone").val();
        var Primary = $("#PrimaryPhone").val();
        var emailSecond = $("#SecondaryMail").val();
        var emailPrimary = $("#PrimaryEmail").val();
        var zipCode = $("#zipCode").val();
        var city = $("#city").val();
        var address = $("#address").val();
        var apellido = $("#apellido").val();
        var nombre = $("#nombre").val();
        var languaje = $("#LanguajePreference").val();
        if (Primary != "" && nombre != "" && address != "" && city != "" && zipCode != "" && languaje != 0) {
            $.post("/MGTM/Views/Controllers/UpdateCustomer.php", {
                workPhone,
                Home,
                SecondPhone,
                Primary,
                emailSecond,
                emailPrimary,
                zipCode,
                city,
                address,
                apellido,
                nombre,
                id_client,
                languaje,
                isNewCustomer,
                userCreate,
                agencia
            }, function(data) {
                if (data.status == "success") {
                    CustomerCreated = true;
                    Gid_customer = data.id_customer;
                    alertify.success(data.message);
                    $(".request_field").removeClass("border-error-field");
                    $("#tagProfile").click();
                } else {
                    alertify.error(data.message);
                }
            });
        } else {
            alertify.error("The Fields Name, address, city, zip,  languaje  must are necessary");
            $(".request_field").addClass("border-error-field");

        }
    }
</script>