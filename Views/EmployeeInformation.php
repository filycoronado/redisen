<?php
//include("../inc/dbconnection.php");
$idClient = 0;
$flag = 0; //new customer
$iduser = 0;
$row;
$id = 0;
$ag;
$imageUrl = "../employee_files/default_avatar.jpg";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM `users` where id=$id";
    $result = mysqli_query($mysqli, $sql);
    $row = mysqli_fetch_assoc($result);
    $ag = $row['agencia'];
    $image = $row['photoimage'];
    if ($image != '0' && $image != "") {
        $imageUrl = "../employee_files/$image";
    }
}
?>

<style>
    .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }
    .kv-avatar {
        display: inline-block;
    }
    .kv-avatar .file-input {
        display: table-cell;
        width: 213px;
    }
    .kv-reqd {
        color: red;
        font-family: monospace;
        font-weight: normal;
    }
</style>

<form  class="credit-card-div  col-lg-12 col-md-12">
    <div class="panel panel-default" >
        <div class="panel-heading">
            <div class="row">

                <div class="col-sm-4 text-center">
                    <div class="kv-avatar">
                        <div class="file-loading">
                            <input id="avatar-1" name="avatar-1" type="file" required>
                        </div>
                    </div>
                    <div class="kv-avatar-hint"><small>Select file < 1500 KB</small></div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <select  id="agency" class="form-control selectpicker request_field" >
                        <?
                        $sql = "SELECT id,nombre FROM `agency` where id_owner=1";
                        $result = mysqli_query($mysqli, $sql);
                        while ($r = mysqli_fetch_assoc($result)) {
                            ?>

                            <option value="<?= $r['id'] ?>" <? if ($ag == $r['id']) { ?>selected<? } ?>><?= $r['nombre'] ?></option>
                        <? } ?>

                    </select>
                </div>
            </div>  

            <div class="row">
                <div class="col-md-6">
                    <span class="help-block text-muted small-font" > Employee Name:</span>
                    <input id="nombre" type="text" class="form-control request_field" placeholder="Jhon Due" value="<?= $row['name']; ?>"/>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font" > Username:</span>
                    <input id="username" type="text" class="form-control request_field" placeholder="W Ajo way 25" value="<?= $row['user']; ?>" />
                </div>
                <div class="col-md-4 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font" > Password</span>
                    <input id="pasword" type="password" class="form-control request_field" placeholder="" value="<?= $row['pass']; ?>" />
                </div>
                <div class="col-md-4 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font" >Repeat Password</span>
                    <input id="pasword2" type="password" class="form-control request_field" placeholder="" value="<?= $row['pass']; ?>" />
                </div>

            </div>
            <div class="row ">
                <div class="col-md-6 pad-adjust">
                    <span class="help-block text-muted small-font" > Primary email</span>
                    <input id="email" type="email" class="form-control request_field" placeholder="Primary Email"  value="<?= $row['email']; ?>"/>
                </div>

                <div class="col-md-6 pad-adjust">
                    <span class="help-block text-muted small-font" > Phone Number</span>
                    <input id="phone" type="text" class="form-control request_field" placeholder="(000)000-0000" value="<?= $row['phone']; ?>" />
                </div>
            </div>




        </div>
        <div class="row container" style="    padding: 10px;">
            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                <input type="button"  onclick="SaveEmployee();" class="btn btn-success btn-lg" value="Save" />
            </div>

        </div>

    </div>

</form>

<link rel="stylesheet" href="../libs/kartik-v-bootstrap-fileinput/css/fileinput.css">
<link rel="stylesheet" href="../libs/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.css">
<!--        <script src="../js/maks.js"></script>-->

<script src="../libs/jquery-ui/jquery-ui.js"></script>
<script src="../libs/kartik-v-bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="../libs/kartik-v-bootstrap-fileinput/js/fileinput.min.js"></script>

<script src="../libs/kartik-v-bootstrap-fileinput/js/fileinput.js"></script>
<script src="../libs/kartik-v-bootstrap-fileinput/themes/fa/theme.min.js"></script>
<script src="../libs/kartik-v-bootstrap-fileinput/js/plugins/piexif.js"></script>
<script>

                    var btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' +
                            'onclick="alert(\'Call your custom code here.\')">' +
                            '<i class="glyphicon glyphicon-tag"></i>' +
                            '</button>';
                    $("#avatar-1").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1500,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancel or reset changes',
                        elErrorContainer: '#kv-avatar-errors-1',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="<?= $imageUrl ?>" alt="Your Avatar" style="width: 166px;">',
                        layoutTemplates: {main2: '{preview} ' + btnCust + ' {remove} {browse}'},
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });


                    function SaveEmployee() {
                        var fd = new FormData();
                        var id =<?= $id ?>;
                        var agency = $("#agency").val();
                        var files = $('#avatar-1')[0].files[0];
                        var nombre = $("#nombre").val();
                        var pasword = $("#pasword").val();
                        var email = $("#email").val();
                        var phone = $("#phone").val();
                        var pasword2 = $("#pasword2").val();
                        if (pasword2 === pasword) {
                            if (nombre != "" && email != "") {
                                fd.append('file', files);
                                fd.append('agency', agency);
                                fd.append('password', pasword);
                                fd.append('name', nombre);
                                fd.append('email', email);
                                fd.append('phone', phone);
                                fd.append('id', id);

                                $.ajax({
                                    url: './Controllers/SaveEmployee.php',
                                    type: 'post',
                                    data: fd,
                                    contentType: false,
                                    processData: false,
                                    success: function (response) {
                                        if (response.status === "success") {
                                            alertify.success(response.message2);
                                            alertify.success(response.message);
                                        } else {
                                            alertify.success(response.message);
                                            alertify.success(response.message2);
                                        }
                                    },
                                });
                            }
                        } else {
                            alertify.error("Password Error");
                        }
                    }
</script>