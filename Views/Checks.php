<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");
//include ("./../Views/Controllers/Utils.php");
$iduser = $_SESSION['id'];
$username = $_SESSION['username'];

$todayDate = date("m-d-Y");
$agency_user = $_SESSION['agencia'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>





        <title>New Policy</title>

    </head>
    <style>
        .ui-widget-header {
            border: 1px solid #2d1c1c !important;
            color: #ffffff !important;
            font-weight: bold;
            background: #ed502e !important;
        }
        .ui-widget-content {
            color: #ed502e !important;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {

            font-weight: bold !important;;
            color: #f3f1f1 !important;;
            background: #ed502e !important;;
            border-collapse: collapse !important;
        }
        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            border: 1px solid #f7360c  !important;;

            color: #f7360c  !important;
            background: white  !important;
        }
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            border: 1px solid #aaaaaa;
            font-weight: bold  !important;
            color: #000000 !important;
            background: #ed502e7a !important;
        }

        .btn-round {
            color: #666;
            padding: 3px 9px;
            font-size: 13px;
            line-height: 1.5;
            background-color: #fff;
            border-color: #ccc;
            border-radius: 50px;
        }
        .alert-success {
            color: #3c763d;
            background-color: #dff0d8;
            border-color: #d6e9c6;
            max-height: 232px !important; 
            height: 232px !important;
        }
        .btn.active {                
            display: none;		
        }

        .btn span:nth-of-type(1)  {            	
            display: none;
        }
        .btn span:last-child  {            	
            display: block;		
        }

        .btn.active  span:nth-of-type(1)  {            	
            display: block;		
        }
        .btn.active span:last-child  {            	
            display: none;			
        }
    </style>
    <body >




        <div>

            <?php
            include("./BarMenu.php");
            ?>

        </div>
        <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 container" style="margin-top: 2%;" >
            <?
            $today = date("Y-m-d");
            $sql = "SELECT * FROM `registroclock` where iduser=$iduser and date='$today'  order by id desc";
            $result = mysqli_query($mysqli, $sql);
            $num = mysqli_num_rows($result);
            $in = "";
            $out = "";
            $checkin = "";
            $checkOut = "";

            if ($num > 0) {
                $row = mysqli_fetch_assoc($result);
                /* ¨verificas siguiente movimiento */

                if ($row['hora'] != null && is_null($row['horaout'])) {
                    $in = 0;
                    $out = 1;
                } else if ($row['horaout'] != null) {
                    $in = 1;
                    $out = 0;
                }
            } else {
                $in = 1;
                $out = 0;
            }
            ?>
            <div class="row">

                <div class="col-md-2">
                    <? if ($in == 1) { ?>
                        <button class="btn btn-success btn-lg" onclick="CheckTime(1)">Check In</button>
                    <? } else if ($out == 1) { ?>
                        <button class="btn btn-danger btn-lg"  onclick="CheckTime(2)">Check Out</button>
                    <? } ?>
                </div>

            </div>

            <?
            $sql = "SELECT * FROM `registroclock` where iduser=$iduser and date='$today'  order by id desc";
            $result = mysqli_query($mysqli, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
                ?> 
                <div class="row" style="margin-top: 1%;">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-lg" ><?= $row['hora'] ?></button>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-info btn-lg"  ><?= $row['diff']; ?></button>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-danger btn-lg"  ><?= $row['horaout'] ?></button>
                    </div>
                </div>
            <? } ?>


        </div>


    </body>






    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

</html>

<script>
                            function CheckTime(value) {
                                var id_user =<?= $iduser ?>;
                                var agency =<?= $agency_user ?>;
                                $.post("./Controllers/CheckMoves.php", {value, id_user, agency}, function (data) {
                                    if (data.status == "success") {
                                        alertify.success(data.message);
                                        setTimeout('document.location.reload()', 2000);
                                    } else {
                                        alertify.error(data.message);
                                    }
                                });
                            }
</script>