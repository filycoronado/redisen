<style>
    .colorgraph {
        height: 5px;
        border-top: 0;
        background: #c4e17f;
        border-radius: 5px;
        background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
    }
    body{
        background-color: gray !important;
    }
    .loginblok{
        background-color: white;
    }
</style>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> -->

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">


        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>


        <script src="../jquery.maskedinput.js"></script>
        <script src="../js/mask/jquery/src/jquery.mask.js"></script>


      <!--  <script src="../libs/ckeditor/ckeditor.js"></script>-->
        <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
        <meta http-equiv="x-ua-compatible" content="IE=11">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <title>Cusatomer Details</title>



    </head>

    <div class="container">

        <div class="row" style="margin-top:20px">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 loginblok">
                <form role="form">
                    <fieldset>
                        <h2>Please Sign In</h2>
                        <hr class="colorgraph">
                        <div class="form-group">
                            <input type="text"  id="agency_Code" class="form-control input-lg" placeholder="ADC101">
                        </div>
                        <div class="form-group">
                            <input type="text"  id="username" class="form-control input-lg" placeholder="User Demo">
                        </div>
                        <div class="form-group">
                            <input type="password"  id="password" class="form-control input-lg" placeholder="Password">
                        </div>
                        <span class="button-checkbox">
                            <button type="button" class="btn" data-color="info">Remember Me</button>
                            <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
                            <!--                            <a href="" class="btn btn-link pull-right">Forgot Password?</a>-->
                        </span>
                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <input type="button" class="btn btn-lg btn-success btn-block" value="Sign In" onclick="DoLogin();">
                            </div>
                            <!--                            <div class="col-xs-6 col-sm-6 col-md-6">
                                                            <a href="" class="btn btn-lg btn-primary btn-block">Register</a>
                                                        </div>-->
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

    </div>
</html>

<script>
    $(document).ready(function () {
        var agency = localStorage.getItem("agency");
        var username = localStorage.getItem("user");
        var password = localStorage.getItem("pass");
        $("#agency_Code").val(agency);
        $("#username").val(username);
        $("#password").val(password);
    });
    function DoLogin() {
        var agency_Code = $("#agency_Code").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var remember = 0;
        if ($("#remember_me").is(':checked')) {
            remember = 1;
        }


        var str2 = "ADC";
        if (agency_Code.indexOf(str2) != -1) {
            if (username != "" && password != "" && agency_Code != "") {
                var Token = localStorage.getItem("Token");
                $.post("./Controllers/DoLogin.php", {username, password, agency_Code, remember, Token}, function (data) {
                    if (data.status == "success") {
                        if (remember == 1) {
                            localStorage.setItem("agency", agency_Code);
                            localStorage.setItem("user", username);
                            localStorage.setItem("pass", password);
                        }
                        alertify.success(data.message);
                        localStorage.setItem("Token", data.token);
                       // window.open("./Transactions.php");
                        window.location.href = './Transactions.php';
                    } else {
                        alertify.error(data.message);
                    }
                });
            } else {
                alertify.error("all fields are required");
            }
        } else {
            alertify.error("Agency Code not contain 'ADC'");
        }
    }
    $(function () {
        $('.button-checkbox').each(function () {
            var $widget = $(this),
                    $button = $widget.find('button'),
                    $checkbox = $widget.find('input:checkbox'),
                    color = $button.data('color'),
                    settings = {
                        on: {
                            icon: 'glyphicon glyphicon-check'
                        },
                        off: {
                            icon: 'glyphicon glyphicon-unchecked'
                        }
                    };

            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });

            $checkbox.on('change', function () {
                updateDisplay();
            });

            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');
                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                        .removeClass()
                        .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                            .removeClass('btn-default')
                            .addClass('btn-' + color + ' active');
                } else
                {
                    $button
                            .removeClass('btn-' + color + ' active')
                            .addClass('btn-default');
                }
            }
            function init() {
                updateDisplay();
                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });
</script>