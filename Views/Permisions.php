<?php
session_start();
$nivelLoge = 0;
$iduserloig = 0;
if (isset($_SESSION['nivel'])) {
    $nivelLoge = $_SESSION['nivel'];
    $iduserloig = $_SESSION['id'];
} else {
    
}

$id = $_GET['id'];
$sql = "SELECT * FROM `permits` where id_user=$id";
$result = mysqli_query($mysqli, $sql);

$erase = "";
$edit = "";
$agency = "";
$salesRepot = "";
$dealerReport = "";
$retentionReport = "";
$wellcomeCallReport = "";
$productionReport = "";
$externalLogin = "";
$TakePayments = "";
$asing = "";
$mapReport = "";
$status_profile = "";
$credit_card = "";
$edit_policy_status = "";
$erase_users = "";
if ($result) {
    $row = mysqli_fetch_assoc($result);
    if ($row['agency'] == 1) {
        $agency = "checked";
    }

    if ($row['erase'] == 1) {
        $erase = "checked";
    }

    if ($row['edit_t'] == 1) {
        $edit = "checked";
    }

    if ($row['salesReport'] == 1) {
        $salesRepot = "checked";
    }

    if ($row['DealerReport'] == 1) {
        $dealerReport = "checked";
    }

    if ($row['wellcomeCall'] == 1) {
        $wellcomeCallReport = "checked";
    }


    if ($row['Prodcution'] == 1) {
        $productionReport = "checked";
    }

    if ($row['ReportRetention'] == 1) {
        $retentionReport = "checked";
    }
    if ($row['external_login'] == 1) {
        $externalLogin = "checked";
    }

    if ($row['TakePayments'] == 1) {
        $TakePayments = "checked";
    }

    if ($row['asing'] == 1) {
        $asing = "checked";
    }
    if ($row['mapReport'] == 1) {
        $mapReport = "checked";
    }

    if ($row['status_profile'] == 1) {
        $status_profile = "checked";
    }

    if ($row['credit_card'] == 1) {
        $credit_card = "checked";
    }
    if ($row['edit_policy_status'] == 1) {
        $edit_policy_status = "checked";
    }

    if ($row['erase_users'] == 1) {
        $erase_users = "checked";
    }
}
?>
<style>
    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
        float:right;
    }

    /* Hide default HTML checkbox */
    .switch input {display:none;}

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input.default:checked + .slider {
        background-color: #444;
    }
    input.primary:checked + .slider {
        background-color: #2196F3;
    }
    input.success:checked + .slider {
        background-color: #8bc34a;
    }
    input.info:checked + .slider {
        background-color: #3de0f5;
    }
    input.warning:checked + .slider {
        background-color: #FFC107;
    }
    input.danger:checked + .slider {
        background-color: #f44336;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .list-group-item {
        position: relative;
        display: block;
        padding: 21px 15px !important;
        margin-bottom: -1px;
        background-color: #fff;
        border: 1px solid #ddd;
    }
</style>
<div class="panel panel-default" >
    <div class="panel-heading">

        <div class="">
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item">
                            Sales Report
                            <label class="switch ">
                                <input  id="check1" type="checkbox" class="success" <?= $salesRepot ?> onclick="UpdatePermision('check1', 1);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Dealer Report
                            <label class="switch ">
                                <input id="check2" type="checkbox" class="success" <?= $dealerReport ?> onclick="UpdatePermision('check2', 2);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Retention Report
                            <label class="switch ">
                                <input id="check3"type="checkbox" class="success" <?= $retentionReport ?> onclick="UpdatePermision('check3', 3);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Wellcome Call Report
                            <label class="switch ">
                                <input id="check4" type="checkbox" class="success" <?= $wellcomeCallReport ?> onclick="UpdatePermision('check4', 4);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Production Report
                            <label class="switch ">
                                <input id="check5" type="checkbox" class="success" <?= $productionReport ?> onclick="UpdatePermision('check5', 5);">
                                <span class="slider round"></span>
                            </label>
                        </li>

                        <li class="list-group-item">
                            Map Report
                            <label class="switch ">
                                <input id="check12" type="checkbox" class="success" <?= $mapReport ?> onclick="UpdatePermision('check12', 12);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>

                </div>


                <div class="col-md-4">
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item">
                            External Login
                            <label class="switch ">
                                <input id="check6" type="checkbox" class="success" <?= $externalLogin ?> onclick="UpdatePermision('check6', 6);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Take Payments
                            <label class="switch ">
                                <input  id="check7" type="checkbox" class="success" <?= $TakePayments ?> onclick="UpdatePermision('check7', 7);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            View More Agencies
                            <label class="switch ">
                                <input id="check8" type="checkbox" class="success" <?= $agency ?> onclick="UpdatePermision('check8', 8);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Create Taks
                            <label class="switch ">
                                <input id="check9" type="checkbox" class="success" <?= $asing ?> onclick="UpdatePermision('check9', 9);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Edit
                            <label class="switch ">
                                <input  id="check10" type="checkbox" class="success " <?= $edit ?> onclick="UpdatePermision('check10', 10);">
                                <span class="slider round"></span>
                            </label>
                        </li>

                        <li class="list-group-item">
                            Erase
                            <label class="switch ">
                                <input id="check11" type="checkbox" class="success" <?= $erase ?> onclick="UpdatePermision('check11', 11);">
                                <span class="slider round"></span>
                            </label>
                        </li>



                    </ul>

                </div>
                <div class="col-md-4">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            Status Profile
                            <label class="switch ">
                                <input id="check13" type="checkbox" class="success" <?= $status_profile ?> onclick="UpdatePermision('check13', 13);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                        <li class="list-group-item">
                            Credic Card 
                            <label class="switch ">
                                <input id="check14" type="checkbox" class="success" <?= $credit_card ?> onclick="UpdatePermision('check14', 14);">
                                <span class="slider round"></span>
                            </label>
                        </li>

                        <li class="list-group-item">
                            Edit Policy Status 
                            <label class="switch ">
                                <input id="check15" type="checkbox" class="success" <?= $edit_policy_status ?> onclick="UpdatePermision('check15', 15);">
                                <span class="slider round"></span>
                            </label>
                        </li>


                        <li class="list-group-item">
                            Erase Clients
                            <label class="switch ">
                                <input id="check15" type="checkbox" class="success" <?= $erase_users ?> onclick="UpdatePermision('check15', 16);">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


    </div>


</div>



<script>

    function UpdatePermision(id, type) {
        var check = $("#" + id + ":checkbox").val();
        var option;
        var id_user =<?= $id ?>;

        if ($("#" + id + ":checkbox").is(':checked')) {

            option = 1;
        } else {

            option = 0;
        }

        $.post("./Controllers/UpdatePermission.php", {option, id_user, type}, function (data) {

            if (data.status === "success") {
                alertify.success(data.message);
            } else {
                alertify.error(data.message);
            }

        });


    }
</script>
