<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");
//include ("./../Views/Controllers/Utils.php");
$iduser = $_SESSION['id'];
$username = $_SESSION['username'];

$todayDate = date("m-d-Y");
$agency_user = $_SESSION['agencia'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>





        <title>New Policy</title>

    </head>
    <style>
        .ui-widget-header {
            border: 1px solid #2d1c1c !important;
            color: #ffffff !important;
            font-weight: bold;
            background: #ed502e !important;
        }
        .ui-widget-content {
            color: #ed502e !important;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {

            font-weight: bold !important;;
            color: #f3f1f1 !important;;
            background: #ed502e !important;;
            border-collapse: collapse !important;
        }
        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            border: 1px solid #f7360c  !important;;

            color: #f7360c  !important;
            background: white  !important;
        }
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            border: 1px solid #aaaaaa;
            font-weight: bold  !important;
            color: #000000 !important;
            background: #ed502e7a !important;
        }

        .btn-round {
            color: #666;
            padding: 3px 9px;
            font-size: 13px;
            line-height: 1.5;
            background-color: #fff;
            border-color: #ccc;
            border-radius: 50px;
        }

        .btn.active {                
            display: none;		
        }

        .btn span:nth-of-type(1)  {            	
            display: none;
        }
        .btn span:last-child  {            	
            display: block;		
        }

        .btn.active  span:nth-of-type(1)  {            	
            display: block;		
        }
        .btn.active span:last-child  {            	
            display: none;			
        }
    </style>
    <body >




        <div>

            <?php
            include("./BarMenu.php");
            ?>

        </div>
        <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 container" style="margin-top: 2%;" >
            <?
            $date = date("Y-m-d");
            $sqlPast = "SELECT `notas_mgtm`.`fecha_follow`, `notas_mgtm`.`id_cliente`, `UserManagement`.`nombre`, `users`.`user` FROM `notas_mgtm` LEFT JOIN UserManagement on notas_mgtm.id_cliente=UserManagement.id LEFT JOIN users on notas_mgtm.id_usuario=users.id where notas_mgtm.fecha_follow!='0000-00-00' and (UserManagement.agencia=101 or UserManagement.agencia=103) and notas_mgtm.user_follow_marc=0 and notas_mgtm.status_aplication=0 and `notas_mgtm`.`fecha_follow`<'$date' ORDER BY `notas_mgtm`.`fecha_follow` DESC";
            $resultPast = mysqli_query($mysqli, $sqlPast);


            $sqlPresent = "SELECT `notas_mgtm`.`fecha_follow`, `notas_mgtm`.`id_cliente`, `UserManagement`.`nombre`, `users`.`user` FROM `notas_mgtm` LEFT JOIN UserManagement on notas_mgtm.id_cliente=UserManagement.id LEFT JOIN users on notas_mgtm.id_usuario=users.id where notas_mgtm.fecha_follow!='0000-00-00' and (UserManagement.agencia=101 or UserManagement.agencia=103) and notas_mgtm.user_follow_marc=0 and notas_mgtm.status_aplication=0 and `notas_mgtm`.`fecha_follow`='$date' ORDER BY `notas_mgtm`.`fecha_follow` DESC";
            $resultPresent = mysqli_query($mysqli, $sqlPresent);


            $sqlFuture = "SELECT `notas_mgtm`.`fecha_follow`, `notas_mgtm`.`id_cliente`, `UserManagement`.`nombre`, `users`.`user` FROM `notas_mgtm` LEFT JOIN UserManagement on notas_mgtm.id_cliente=UserManagement.id LEFT JOIN users on notas_mgtm.id_usuario=users.id where notas_mgtm.fecha_follow!='0000-00-00' and (UserManagement.agencia=101 or UserManagement.agencia=103) and notas_mgtm.user_follow_marc=0 and notas_mgtm.status_aplication=0 and `notas_mgtm`.`fecha_follow`>'$date' ORDER BY `notas_mgtm`.`fecha_follow` DESC";
            $resultFuture = mysqli_query($mysqli, $sqlFuture);
            ?>

            <div class="col-md-4">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    PAST REQUESTS
                </div>
                <?
                while ($r = mysqli_fetch_assoc($resultPast)) {
                    $fecha = split(" ", $r['fecha_follow']);
                    $fecha = date("m-d-Y", strtotime($fecha[0]));

                    $name = $r['nombre'];
                    $idCLient = $r['id_cliente'];
                    $user = strtoupper($r['user']);
                    ?>
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span> <?= $fecha ?>

                        <p> <strong><?= $name ?></strong></p><br>
                        <p> <strong><?= $user ?></strong></p>
                        <hr class="message-inner-separator">

                        <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>

                    </div>
                <? } ?>
            </div>
            <div class="col-md-4">
                <div class="alert alert-warning" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    PRESENT REQUESTS
                </div>
                <?
                while ($r = mysqli_fetch_assoc($resultPresent)) {
                    $cont++;
                    $fecha = split(" ", $r['fecha_follow']);
                    $fecha = date("m-d-Y", strtotime($fecha[0]));

                    $name = $r['nombre'];
                    $idCLient = $r['id_cliente'];
                    $user = strtoupper($r['user']);
                    ?>
                    <div class="alert alert-warning" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span> <?= $fecha ?>

                        <p> <strong><?= $name ?></strong></p><br>
                        <p> <strong><?= $user ?></strong></p>
                        <hr class="message-inner-separator">

                        <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                    </div>
                <? } ?>
            </div>
            <div class="col-md-4">
                <div class="alert alert-success" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    FUTURE REQUESTS
                </div>
                <?
                while ($r = mysqli_fetch_assoc($resultFuture)) {
                    $cont++;
                    $fecha = split(" ", $r['fecha_follow']);
                    $fecha = date("m-d-Y", strtotime($fecha[0]));

                    $name = $r['nombre'];
                    $idCLient = $r['id_cliente'];
                    $user = strtoupper($r['user']);
                    ?>
                    <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span> <?= $fecha ?>

                        <p> <strong><?= $name ?></strong></p><br>
                        <p> <strong><?= $user ?></strong></p>
                        <hr class="message-inner-separator">

                        <a class="caption-helper btn btn-round" href="./CustomerDetails.php?id=<?= $idCLient ?>">Go to profile Customer</a>
                    </div>
                <? } ?>
            </div>



        </div>


    </body>






    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

</html>

<script>
    function CheckTime(value) {
        var id_user =<?= $iduser ?>;
        var agency =<?= $agency_user ?>;
        $.post("./Controllers/CheckMoves.php", {value, id_user, agency}, function (data) {
            if (data.status == "success") {
                alertify.success(data.message);
                setTimeout('document.location.reload()', 2000);
            } else {
                alertify.error(data.message);
            }
        });
    }
</script>