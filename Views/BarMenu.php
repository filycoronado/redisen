
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style>
    .nav-side-menu {
        overflow: auto;
        font-family: sans-serif;
        font-size: 12px;
        font-weight: 200;
        background-color: #ED502E;
        position: fixed;
        top: 0px;
        width: 220px;
        height: 100%;
        color: white;
    }
    .nav-side-menu .brand {
        line-height: 50px;
        display: block;
        text-align: center;
        font-size: 14px;
        height: 123px;
        border-color: ba;
        border-color: black;
        border-style: solid;
        border-width: 1px; 
        background-image: url(../img/InsGenius.png);
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
    }
    .nav-side-menu .toggle-btn {
        display: none;
    }
    .nav-side-menu ul,
    .nav-side-menu li {
        list-style: none;
        padding: 0px;
        margin: 0px;
        line-height: 35px;
        cursor: pointer;
        /*    
          .collapsed{
             .arrow:before{
                       font-family: FontAwesome;
                       content: "\f053";
                       display: inline-block;
                       padding-left:10px;
                       padding-right: 10px;
                       vertical-align: middle;
                       float:right;
                  }
           }
        */
    }
    .nav-side-menu ul :not(collapsed) .arrow:before,
    .nav-side-menu li :not(collapsed) .arrow:before {
        font-family: FontAwesome;
        content: "\f078";
        display: inline-block;
        padding-left: 10px;
        padding-right: 10px;
        vertical-align: middle;
        float: right;
    }
    .nav-side-menu ul .active,
    .nav-side-menu li .active {
        border-left: 3px solid #d19b3d;
        background-color: #4f5b69;
    }
    .nav-side-menu ul .sub-menu li.active,
    .nav-side-menu li .sub-menu li.active {
        color: #d19b3d;
    }
    .nav-side-menu ul .sub-menu li.active a,
    .nav-side-menu li .sub-menu li.active a {
        color: #d19b3d;
    }
    .nav-side-menu ul .sub-menu li,
    .nav-side-menu li .sub-menu li {
        background-color: #181c20;
        border: none;
        line-height: 28px;
        border-bottom: 1px solid #23282e;
        margin-left: 0px;
    }
    .nav-side-menu ul .sub-menu li:hover,
    .nav-side-menu li .sub-menu li:hover {
        background-color: #020203;
    }
    .nav-side-menu ul .sub-menu li:before,
    .nav-side-menu li .sub-menu li:before {
        font-family: FontAwesome;
        content: "\f105";
        display: inline-block;
        padding-left: 10px;
        padding-right: 10px;
        vertical-align: middle;
    }
    .nav-side-menu li {
        padding-left: 0px;
        border-left: 3px solid #2e353d;
        border-bottom: 1px solid #23282e;
    }
    .nav-side-menu li a {
        text-decoration: none;
        color: #e1ffff;
    }
    .nav-side-menu li a i {
        padding-left: 10px;
        width: 20px;
        padding-right: 20px;
    }
    .nav-side-menu li:hover {
        border-left: 3px solid #d19b3d;
        background-color: #4f5b69;
        -webkit-transition: all 1s ease;
        -moz-transition: all 1s ease;
        -o-transition: all 1s ease;
        -ms-transition: all 1s ease;
        transition: all 1s ease;
    }
    @media (max-width: 767px) {
        .nav-side-menu {
            position: relative;
            width: 100%;
            margin-bottom: 10px;
        }
        .nav-side-menu .toggle-btn {
            display: block;
            cursor: pointer;
            position: absolute;
            right: 10px;
            top: 10px;
            z-index: 10 !important;
            padding: 3px;
            background-color: #ffffff;
            color: #000;
            width: 40px;
            text-align: center;
        }
        .brand {
            text-align: left !important;
            font-size: 22px;
            padding-left: 20px;
            line-height: 50px !important;
        }
    }
    @media (min-width: 767px) {
        .nav-side-menu .menu-list .menu-content {
            display: block;
        }
    }
</style>

<?
$id_logeed;
if (isset($_SESSION['nivel'])) {
    $nivelLoge = $_SESSION['nivel'];
    $id_logeed = $_SESSION['id'];
} else {
    
}
//
$date = date("Y-m-d");
$totalNotificacion = 0;
$sqlPast = "SELECT `notas_mgtm`.`id_cliente` FROM `notas_mgtm` LEFT JOIN UserManagement on notas_mgtm.id_cliente=UserManagement.id LEFT JOIN users on notas_mgtm.id_usuario=users.id where notas_mgtm.fecha_follow!='0000-00-00' and (UserManagement.agencia=101 or UserManagement.agencia=103) and notas_mgtm.user_follow_marc=0 and notas_mgtm.status_aplication=0 and `notas_mgtm`.`fecha_follow`<'$date' ORDER BY `notas_mgtm`.`fecha_follow` DESC";

$resultPast = mysqli_query($mysqli, $sqlPast);
$totalNotificacion += $num1 = mysqli_num_rows($resultPast);

$sqlPresent = "SELECT `notas_mgtm`.`id_cliente`  FROM `notas_mgtm` LEFT JOIN UserManagement on notas_mgtm.id_cliente=UserManagement.id LEFT JOIN users on notas_mgtm.id_usuario=users.id where notas_mgtm.fecha_follow!='0000-00-00' and (UserManagement.agencia=101 or UserManagement.agencia=103) and notas_mgtm.user_follow_marc=0 and notas_mgtm.status_aplication=0 and `notas_mgtm`.`fecha_follow`='$date' ORDER BY `notas_mgtm`.`fecha_follow` DESC";
$resultPresent = mysqli_query($mysqli, $sqlPresent);
$totalNotificacion += $num2 = mysqli_num_rows($resultPresent);


$sqlFuture = "SELECT `notas_mgtm`.`id_cliente`  FROM `notas_mgtm` LEFT JOIN UserManagement on notas_mgtm.id_cliente=UserManagement.id LEFT JOIN users on notas_mgtm.id_usuario=users.id where notas_mgtm.fecha_follow!='0000-00-00' and (UserManagement.agencia=101 or UserManagement.agencia=103) and notas_mgtm.user_follow_marc=0 and notas_mgtm.status_aplication=0 and `notas_mgtm`.`fecha_follow`>'$date' ORDER BY `notas_mgtm`.`fecha_follow` DESC";
$resultFuture = mysqli_query($mysqli, $sqlFuture);
$totalNotificacion += $num3 = mysqli_num_rows($resultFuture);
//
$sql = "SELECT * FROM `permits` where id_user=$id_logeed";
$result = mysqli_query($mysqli, $sql);

$eraseP = 0;
$editP = "";
$agencyP = "";
$salesRepotP = "";
$dealerReportP = "";
$retentionReportP = "";
$wellcomeCallReportP = "";
$productionReportP = "";
$externalLoginP = "";
$TakePaymentsP = "";
$asingP = "";
$mapReportP = "";
$edit_policy_statusP = "";
$credit_cardP = "";
$status_profileP = "";
$erase_usersP = "";
if ($result) {
    $row = mysqli_fetch_assoc($result);
    if ($row['agency'] == 1) {
        $agencyP = 1;
    }

    if ($row['erase'] == 1) {
        $eraseP = 1;
    }

    if ($row['edit_t'] == 1) {
        $editP = 1;
    }

    if ($row['salesReport'] == 1) {
        $salesRepotP = 1;
    }

    if ($row['DealerReport'] == 1) {
        $dealerReportP = 1;
    }

    if ($row['wellcomeCall'] == 1) {
        $wellcomeCallReportP = 1;
    }


    if ($row['Prodcution'] == 1) {
        $productionReportP = 1;
    }

    if ($row['ReportRetention'] == 1) {
        $retentionReportP = 1;
    }
    if ($row['external_login'] == 1) {
        $externalLoginP = 1;
    }

    if ($row['TakePayments'] == 1) {
        $TakePaymentsP = 1;
    }

    if ($row['asing'] == 1) {
        $asingP = 1;
    }
    if ($row['mapReport'] == 1) {
        $mapReportP = 1;
    }
    if ($row['status_profile'] == 1) {
        $status_profileP = 1;
    }
    if ($row['edit_policy_status'] == 1) {
        $edit_policy_statusP = 1;
    }

    if ($row['credit_card'] == 1) {
        $credit_cardP = 1;
    }

    if ($row['erase_users'] == 1) {
        $eraseP = 1;
    }
}
$_SESSION['eraseT'] = $eraseP;
$_SESSION['deleteT'] = $editP;
?>
<div class="nav-side-menu">
    <div class="brand">
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">

            <li>
                <a href="#">
                    <i class="fa fa-user fa-lg"></i><?= $_SESSION['username'] ?>
                </a>
            </li>
            <li>
                <a href="./../../MGTM/Views/Employee_Month.php">
                    <i class="fa fa-trophy fa-lg"></i>Employee of Month
                </a>
            </li>
            <li>
                <a href="./../../MGTM/Views/Tasks.php">
                    <i class="fa fa-tags fa-lg"></i>My Tasks
                </a>
            </li>
            <li>
                <a href="./../../MGTM/Views/PolicyPendings.php">
                    <i class="fa fa-close fa-lg"></i> P Cancellations
                </a>
            </li>
            <li>
                <a href="./../../MGTM/Views/EmployeeProduction.php">
                    <i class="fa fa-database fa-lg"></i>Employee Production
                </a>
            </li>
            <li>
                <a href="./../../MGTM/Views/Transactions.php">
                    <i class="fa fa-list-alt fa-lg"></i> Transactions
                </a>
            </li>
            <li>
                <a href="./../../MGTM/Views/Customers.php">
                    <i class="fa fa-users fa-lg"></i> Clients
                </a>
            </li>
            <li  data-toggle="collapse" data-target="#products" class="collapsed">
                <a href="#"><i class="fa fa-plus-circle fa-lg"></i> Register <span class="arrow"></span></a>
            </li>
            <ul class="sub-menu collapse" id="products">
                <li class="active"><a href="./../../MGTM/Views/NewCustomer.php">New Customer</a></li>
            </ul>
            <li  data-toggle="collapse" data-target="#clock" class="collapsed">
                <a href="#"><i class="fa fa-clock-o fa-lg"></i> Clock <span class="arrow"></span></a>
            </li>
            <ul class="sub-menu collapse" id="clock">
                <li class="active"><a href="./MyTimeReport.php">My Report</a></li>
                <li class="active"><a href="./Checks.php">Check In/Check Out</a></li>
            </ul>
            <li data-toggle="collapse" data-target="#service" class="collapsed">
                <a href="#"><i class="fa fa-globe fa-lg"></i> Marketing <span class="arrow"></span></a>
            </li>  
            <ul class="sub-menu collapse" id="service">
                <? if ($mapReportP == 1) { ?>
                    <a href="./../../MGTM/Views/Map.php"> <li>Distribution Map</li></a>
                <? } ?>
            </ul>


            <li data-toggle="collapse" data-target="#new" class="collapsed">
                <a href="#"><i class="fa fa-cog fa-lg"></i> Configuration <span class="arrow"></span></a>
            </li>
            <ul class="sub-menu collapse" id="new">
                <li class="active"><a href="#">Agencies</a></li>
                <li class="active"><a href="#">Companies</a></li>
                <li class="active"><a href="#">Dealers</a></li>
                <li class="active"><a href="#">Employees</a></li>
            </ul>
            <li data-toggle="collapse" data-target="#Reports" class="collapsed">
                <a href="#"><i class="fa fa-paper-plane fa-lg"></i> Reports <span class="arrow"></span></a>
            </li>  
            <ul class="sub-menu collapse" id="Reports">
                <? if ($salesRepotP == 1) { ?><li><a href="./../../MGTM/Views/SalesReport.php">Sales Report</a></li><? } ?>
                <? if ($dealerReportP == 1) { ?><li><a href="./../../MGTM/Views/DealerReport.php">Dealers Report</a></li><? } ?>
                <? if ($retentionReportP == 1) { ?> <li><a href="./../../MGTM/Views/RetentionReport.php">Retention Report</a></li><? } ?>
                <? if ($wellcomeCallReportP == 1) { ?> <li><a href="./../../MGTM/Views/WellcomeCallReport.php">Wellcome Call Report</a></li><? } ?>
                <? if ($productionReportP == 1) { ?><li><a href="./../../MGTM/Views/ProductionReport.php">Production Report</a></li><? } ?>

            </ul>


            <li>
                <a href="#">
                    <i class="fa fa-user fa-lg"></i> Profile
                </a>
            </li>
            <? if ($id_logeed == 1 || $id_logeed == 364) { ?>
                <li>
                    <a href="./../../MGTM/Views/Employees.php">
                        <i class="fa fa-users fa-lg"></i> Users
                    </a>
                </li>
            <? } ?>
            <li>
                <a href="./../../MGTM/Views/Notifications.php">
                    <i class="fa fa-bell fa-lg"></i> <?= $totalNotificacion ?> Notifications
                </a>
            </li>
            <li data-toggle="collapse" data-target="#forms" class="collapsed">
                <a href="#"><i class="fa fa-cog fa-lg"></i> Forms <span class="arrow"></span></a>
            </li>
            <ul class="sub-menu collapse" id="forms">
                <li data-toggle="collapse" data-target="#gainsco" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> Gainsco <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="gainsco">
                    <li>
                        <a target="_blank" href="./../files/AGENT OF RECORD CHANGE GAINSCO.pdf">
                            <i class="fa fa-file fa-lg"></i> AGENT OF RECORD CHANGE GAINSCO
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/GAINSCO VEHICLE INSPECTION REPORT.pdf">
                            <i class="fa fa-file fa-lg"></i> GAINSCO VEHICLE INSPECTION REPORT
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/GAINSCO NO LOSS STATEMENT Final.pdf">
                            <i class="fa fa-file fa-lg"></i> GAINSCO NO LOSS STATEMENT
                        </a>
                    </li>
                </ul>


                <li data-toggle="collapse" data-target="#infinity" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> Infinity <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="infinity">
                    <li>  <a target="_blank" href="./../files/INFINITY AUTOPAY FORM CARD.pdf">  <i class="fa fa-file fa-lg"></i> INFINITY AUTOPAY FORM CARD </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/INFINITY AUTOPAY FORM CHECK.pdf">
                            <i class="fa fa-file fa-lg"></i> INFINITY AUTOPAY FORM CHECK
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/INFINITY DRIVER  EXCLUSION.pdf">
                            <i class="fa fa-file fa-lg"></i> INFINITY DRIVER  EXCLUSION
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/INFINITY INSPECTION REPORT.pdf">
                            <i class="fa fa-file fa-lg"></i> INFINITY INSPECTION REPORT
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/INFINITY NO LOSS.pdf">
                            <i class="fa fa-file fa-lg"></i> INFINITY NO LOSS
                        </a>
                    </li>
                </ul>


                <li data-toggle="collapse" data-target="#progressive" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> Progressive <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="progressive">
                    <li>
                        <a target="_blank" href="./../files/AGENT OF RECORD CHANGE PROGRESSIVE.pdf">
                            <i class="fa fa-file fa-lg"></i> AGENT OF RECORD CHANGE PROGRESSIVE
                        </a>
                    </li>
                </ul>


                <li data-toggle="collapse" data-target="#hallmark" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> Hallmark <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="hallmark">
                    <li>
                        <a target="_blank" href="./../files/AUTOPAY AUTORIZATION FORM HALLMARK.pdf">
                            <i class="fa fa-file fa-lg"></i> AUTOPAY AUTORIZATION FORM HALLMARK
                        </a>
                    </li>
                </ul>


                <li data-toggle="collapse" data-target="#legacy" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> Legacy <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="legacy">
                    <li>
                        <a target="_blank" href="./../files/LEGACY Additional Driver Discovery Agreement.pdf">
                            <i class="fa fa-file fa-lg"></i>LEGACY Additional Driver Discovery Agreement
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/Legacy Fax Supply Request Form.pdf">
                            <i class="fa fa-file fa-lg"></i> Legacy Fax Supply Request Form
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/LEGACY Marriage Certification Form.pdf">
                            <i class="fa fa-file fa-lg"></i>LEGACY Marriage Certification Form
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/LEGACY Vehicle Inspection Form.pdf">
                            <i class="fa fa-file fa-lg"></i>LEGACY Vehicle Inspection Form
                        </a>
                    </li>
                </ul>


                <li data-toggle="collapse" data-target="#cashier" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> Cashier Forms <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="cashier">
                    <li>
                        <a target="_blank" href="./../files/CashierFormat.pdf">
                            <i class="fa fa-file fa-lg"></i> Cashier Format 1
                        </a>
                    </li>
                </ul>

                <li data-toggle="collapse" data-target="#adc" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> America Drivers <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="adc">
                    <li>
                        <a target="_blank" href="./../files/ACD_Cancellation_Request(1).pdf">
                            <i class="fa fa-file fa-lg"></i> ADC Cancellation Request
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/FormEFTC.PDF">
                            <i class="fa fa-file fa-lg"></i>EFT Form
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="./../files/MembershipContract.PDF">
                            <i class="fa fa-file fa-lg"></i> Membership Contract
                        </a>
                    </li>
                </ul>

                <li data-toggle="collapse" data-target="#blc" class="collapsed">
                    <a href="#"><i class="fa fa-cog fa-lg"></i> Buy low Cost <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="blc">
                    <li><a target="_blank" href="./../files/Endorsement request.pdf">Endorsement request</a></li>
                    <li><a target="_blank" href="./../files/NOTICE OF CANCELLATION.pdf">NOTICE OF CANCELLATION</a></li>
                    <li><a target="_blank" href="./../files/NO LAPSE LETTER.docx">NO LAPSE LETTER</a></li>
                    <li><a target="_blank" href="./../files/WELCOME LETTER .pdf">WELCOME LETTER OLD INSURANCE</a></li>
                    <li><a target="_blank" href="./../files/AGENT OF RECORD CHANGE BUYLOWCOST.pdf">AGENT OF RECORD CHANGE BUYLOWCOST</a></li>
                    <li><a target="_blank" href="./../files/HOMEOWNER APP PRINT.pdf">HOMEOWNER APP PRINT</a></li>
                    <li><a target="_blank" href="./../files/CHECK LIST NEW BUSINESS DEFINITIVO.pdf">CHECK LIST NEW BUSINESS</a></li>
                    <li><a target="_blank" href="./../files/PAYMENT SCHEDULE ADC.xlsx">PAYMENT SCHEDULE ADC</a></li>
                    <li><a target="_blank" href="./../files/FAX COVER SHEET FINAL.pdf">FAX COVER SHEET</a></li>
                    <li><a target="_blank" href="./../files/LIENHOLDERS INFORMATION.pdf">LIENHOLDERS INFORMATION</a></li>
                    <li><a target="_blank" href="./../files/DEALERS INFO.pdf">DEALERS INFO</a></li>
                    <li><a target="_blank" href="./../files/REFERRALS.pdf">REFERRALS</a></li>
                    <li><a target="_blank" href="./../files/HOMEOWNER APPLICATION.pdf">HOMEOWNER APPLICATION</a></li>
                    <li><a target="_blank" href="./../files/MISSING ITEMS AGREEMENT.pdf">MISSING ITEMS AGREEMENT</a></li>
                    <li><a target="_blank" href="./../files/No_Loss_Statement.pdf">No_Loss_Statement</a></li>
                    <li><a target="_blank" href="./../files/Dealership Contacts Final.pdf">Dealership Contacts</a></li>

                </ul>
            </ul>
        </ul>
    </div>
</div>
