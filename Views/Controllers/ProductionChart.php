<?php
$today = date("Y-m-d");

$fecha1 = new DateTime();
$fecha1->modify('first day of this month');
$f1 = $fecha1->format('Y-m-d');

$fecha = new DateTime();
$fecha->modify('last day of this month');
$f2 = $fecha->format('Y-m-d');


$users = array();
$useresCancellations = array();

$sql = "SELECT * FROM `users` where (agencia=101 or agencia=102 or agencia=103 or  agencia=104 or agencia=105) AND (nivel=1 or nivel=2 or nivel=5) and habilitado =1";
$result = mysqli_query($mysqli, $sql);
$totalPuser = 0;
$Aux = 0;
$grandTotal = 0;
while ($r = mysqli_fetch_assoc($result)) {
    $arraPerUser = array();
    $totalPin2 = getPIN($mysqli, $r['id'], $f1, $f2);
    $totaleft2 = getDPeft($mysqli, $r['id'], $f1, $f2);
    $totaldp2 = getDP($mysqli, $r['id'], $f1, $f2);

    $totalPuser += $totalPin2 + $totaleft2 + $totaldp2;
    $grandTotal += $totalPuser;



    if ($totalPuser > 0) {
        $perocentajeDP2 = (($totaleft2 * 100) / $totalPuser);
        $porcentajeNoEFTDP2 = (($totaldp2 * 100) / $totalPuser);
        $porcentajePIN2 = (($totalPin2 * 100) / $totalPuser);
    }



    $perocentajeDP2 = number_format((float) $perocentajeDP2, 2, '.', '');
    $porcentajeNoEFTDP2 = number_format((float) $porcentajeNoEFTDP2, 2, '.', '');
    $porcentajePIN2 = number_format((float) $porcentajePIN2, 2, '.', '');

    array_push($arraPerUser, $totalPuser, $r['user'], $perocentajeDP2, $porcentajeNoEFTDP2, $porcentajePIN2, $r['id']);
    array_push($users, $arraPerUser);

    $totalPuser = 0;
    $perocentajeDP2 = 0;
    $porcentajeNoEFTDP2 = 0;
    $porcentajePIN2 = 0;
    $perocentajeDP2 = 0;
    $porcentajeNoEFTDP2 = 0;
    $porcentajePIN2 = 0;
    $arraPerUser = null;
}

foreach ($users as $key => $row) {
    $aux[$key] = $row[0];
}

array_multisort($aux, SORT_DESC, $users);
$cont = 0;



//calcular cancelacion mes anterior
//
//echo "<PRE>";
//print_r($users);
//echo "</PRE>";
?>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>New Business , Per Users , Grand Total :<?= $grandTotal ?></h3>
            </div>
            <div class="panel-body">
                <?
                foreach ($users as $key => $row) {
                    if ($row[0] > 0) {
                        $cont++;
                        $lasNb = GetNNLastMonth($mysqli, $row[5]);
                        $cancelled = CalcularCacellations($mysqli, $row[5], $lasNb);
                        ?>
                        <div class="col-md-5  col-lg-6" style="border-radius: 6px;

                             -webkit-box-shadow: -1px -2px 24px 3px rgba(217,104,43,0.56);
                             -moz-box-shadow: -1px -2px 24px 3px rgba(217,104,43,0.56);
                             box-shadow: -1px 1px 24px 3px rgba(217,104,43,0.56);">
                            <p><?= $row[1] ?>#<?= $cont ?></p>
                            <p>NB:<?= $row[0] ?></p>
                            <canvas id="myChart<?= $cont ?>" ></canvas>
                        </div>

                        <div class="col-md-6  col-lg-6" style="border-radius: 6px;

                             -webkit-box-shadow: -1px -2px 24px 3px rgba(217,104,43,0.56);
                             -moz-box-shadow: -1px -2px 24px 3px rgba(217,104,43,0.56);
                             box-shadow: -1px 1px 24px 3px rgba(217,104,43,0.56);">
                            <p><?= $row[1] ?> </p>
                            <p>Total NB in Last Month:<?=$lasNb?> / Cancellations<?= $cancelled[1] ?></p>
                           
                            <canvas id="myChartCancel<?= $cont ?>" ></canvas>
                        </div>
                        <?
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script>
<?
$cont2 = 0;
foreach ($users as $key => $row) {
    $cont2++;

    $lasNb = GetNNLastMonth($mysqli, $row[5]);
    $cancelled = CalcularCacellations($mysqli, $row[5], $lasNb);
    $tc = $cancelled[1];
    ?>
        var ctx = document.getElementById('myChart<?= $cont2 ?>');
        var ctx2 = document.getElementById('myChartCancel<?= $cont2 ?>');


        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["NB. EFT", "Pay in full", "DP"],
                datasets: [{
                        backgroundColor: [
                            "#2ecc71",
                            "#3498db",
                            "#95a5a6",
                        ],
                        data: [<?= $row[2] ?>,<?= $row[4] ?>,<?= $row[3] ?>]
                    }]
            }

        });


        var myPieChart = new Chart(ctx2, {
            type: 'pie',
            data: {
                labels: ["Cencelled","Not Cancelled"],
                datasets: [{
                        backgroundColor: [
                            "#FF0707",
                            "#2ecc71",
                        ],
                        data: [<?= $cancelled[0] ?>,<?=$cancelled[2]?>]
                    }]
            }

        });
<? } ?>
</script>
<?
/* * Methos For Progres CHarts */

function getADC($id_user) {
    
}

function getPIN($mysqli, $id_user, $f1, $f2) {
    $sqlPayinFull = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_user and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=10;";

    $result4 = mysqli_query($mysqli, $sqlPayinFull);
    $r4 = mysqli_fetch_assoc($result4);
    $toalPIN = $r4['totalDPNOEFT'];
    return $toalPIN;
}

function getDP($mysqli, $id_user, $f1, $f2) {
    $sqlDPNOEFT = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_user and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=1;";
    $result3 = mysqli_query($mysqli, $sqlDPNOEFT);
    $r3 = mysqli_fetch_assoc($result3);
    $toalDPNOEFT = $r3['totalDPNOEFT'];
    return $toalDPNOEFT;
}

function getDPeft($mysqli, $id_user, $f1, $f2) {
    $sqlDP = "SELECT COUNT(*) as totalDP FROM `cutvone` where  id_usuario=$id_user and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=5;";

    $result2 = mysqli_query($mysqli, $sqlDP);
    $r2 = mysqli_fetch_assoc($result2);
    $toalDP = $r2['totalDP'];

    return $toalDP;
}

function resgresanombrecompania3($mysqli, $val) {
    $sql = "SELECT * FROM `companyes` where id=" . $val;

    $res = mysqli_query($mysqli, $sql);

    while ($re = mysqli_fetch_assoc($res)) {
        return $re['nombre'];
    }
}

function regresauser($mysqli, $val) {


    $sql = "SELECT * FROM `users` where id=" . $val;

    $res = mysqli_query($mysqli, $sql);

    while ($re = mysqli_fetch_assoc($res)) {
        return $re['user'];
    }
}

function resgresanombrecashier($mysqli, $val) {
    if (empty($val))
        return "-";


    $sql = "SELECT * FROM cashier where id=" . $val;

    $res = mysqli_query($mysqli, $sql);
    $vr = "-";
    while ($re = mysqli_fetch_assoc($res)) {
        $vr = $re['name'];
    }
    return $vr;
}

function CalcularCacellations($mysqli, $id_usuario, $nbTotal) {


    //verificar cancelaciones del usuario en el mes actual
    $results = array();
    $fecha1 = new DateTime();
    $fecha1->modify('first day of this month');
    $f1 = $fecha1->format('Y-m-d');

    $fecha = new DateTime();
    $fecha->modify('last day of this month');
    $f2 = $fecha->format('Y-m-d');


    //
    $fecha = date("Y-m-d");
    $nuevafecha = strtotime('-1 month', strtotime($fecha));
    $today = date('Y-m-d', $nuevafecha);

    $fecha1 = new DateTime($today);
    $fecha1->modify('first day of this month');
    $f1C = $fecha1->format('Y-m-d');

    $fecha = new DateTime($today);
    $fecha->modify('last day of this month');
    $f2C = $fecha->format('Y-m-d');
    $sqloptima = "SELECT Count(Can.customer_id) as TC  FROM `Cancellations` as Can join cutvone as Cut on Cut.id_cliente=Can.customer_id where Can.date BETWEEN '$f1' and '$f2' and Cut.fecha BETWEEN '$f1C' and '$f2C' and Can.habilitado=1 and (Can.status_policy=2 or Can.status_policy=3 or Can.status_policy=6) and Cut.erase=0 and (Cut.Dpayment=10 or Cut.Dpayment=1 or Cut.Dpayment=5) and Cut.id_usuario=$id_usuario";

    //$sql="SELECT customer_id,customer_name,status_policy,date FROM `Cancellations` where  date BETWEEN '$f1'  and  '$f2' and habilitado=1 and (status_policy=2 or status_policy=3 or status_policy=6)";
    $result = mysqli_query($mysqli, $sqloptima);
    $totalCaccelations = 0;
    $cont = 0;
    $cant = 0;
    $row = mysqli_fetch_assoc($result);
    $cant = $row['TC'];
//    while($row= mysql_fetch_assoc($result)){
    //      $cont++;
//          $idCustomer=$row['customer_id'];
//         
//          
//                $fecha = date("Y-m-d");
//                $nuevafecha = strtotime('-1 month', strtotime($fecha));
//                $today = date('Y-m-d', $nuevafecha);
//
//                $fecha1 = new DateTime($today);
//                $fecha1->modify('first day of this month');
//                $f1C = $fecha1->format('Y-m-d');
//
//                $fecha = new DateTime($today);
//                $fecha->modify('last day of this month');
//                $f2C = $fecha->format('Y-m-d');
//          
//              $sqlCutvone="SELECT id FROM `cutvone` where  id_usuario=$id_usuario and fecha BETWEEN '$f1C' and '$f2C' and erase=0 and (Dpayment=10 or Dpayment=1 or Dpayment=5)  and id_cliente=$idCustomer";
//               $resulC= mysql_query($sqlCutvone);
//               $rows= mysql_num_rows($resulC);
//         
//               if($rows>0){
//                   $cant++;
//               }  
    // }
    $percentaje = 0;
    $notCancel=0;
    if ($nbTotal > 0) {
        $percentaje = ($cant * 100) / $nbTotal;
        $notCancel=100-$percentaje;
    }

    array_push($results, $percentaje, $cant,$notCancel);
    return $results;
}

function GetNNLastMonth($mysqli, $id) {
    $fecha = date("Y-m-d");
    $nuevafecha = strtotime('-1 month', strtotime($fecha));
    $today = date('Y-m-d', $nuevafecha);

    $fecha1 = new DateTime($today);
    $fecha1->modify('first day of this month');
    $f1 = $fecha1->format('Y-m-d');

    $fecha = new DateTime($today);
    $fecha->modify('last day of this month');
    $f2 = $fecha->format('Y-m-d');

    $sql = "SELECT * FROM `users` where id=$id  and habilitado =1";
    $result = mysqli_query($mysqli, $sql);
    $totalPuser = 0;
    $Aux = 0;
    $grandTotal_lastMonth = 0;

    while ($r = mysqli_fetch_assoc($result)) {
        $arraPerUser = array();
        $totalPin2 = getPIN($mysqli, $r['id'], $f1, $f2);
        $totaleft2 = getDPeft($mysqli, $r['id'], $f1, $f2);
        $totaldp2 = getDP($mysqli, $r['id'], $f1, $f2);

        $totalPuser += $totalPin2 + $totaleft2 + $totaldp2;
        $grandTotal_lastMonth += $totalPuser;
    }

    return $grandTotal_lastMonth;
}
?>