<?php
error_reporting(0);
include ("./conn.php");
include ("./Utils.php");
session_start();

$eraseP = $_SESSION['eraseT'];
$editP = $_SESSION['deleteT'];
$caseReport = $_POST['caseReport'];
if ($caseReport == 1) {

    $agencies = $_POST['agencies'];
    $usres = $_POST['usres'];
    $companies = $_POST['companies'];
    $date1 = $_POST['date1'];
    $ordate1 = $date1;
    $date2 = $_POST['date2'];
    $ordate2 = $date2;
    $TypePolicy = $_POST['TypePolicy'];
    $MethodTransaction = $_POST['MethodTransaction'];
    $TypeSell = $_POST['TypeSell'];

    $arr1 = split("-", $date1);
    $arr2 = split("-", $date2);

    $date1 = $arr1[2] . "-" . $arr1[0] . "-" . $arr1[1];
    $date2 = $arr2[2] . "-" . $arr2[0] . "-" . $arr2[1];
    $pm = "";
    $trans = "";
    $erase = "";
    $p_type = "";
    $user = "";
    $comp = "";
    $ag = "";
    if ($agencies == -1) {
        $ag = "";
    } else {
        $ag = "and cv.agency=$agencies";
    }

    if ($TypeSell == 0) {
        $erase = "and cv.erase = 1  ";
    }



    if ($TypeSell > 0) {
        $trans = "and cv.Dpayment=$TypeSell";
        $erase = "and cv.erase !=1  ";
    }
    if ($TypeSell == -1) {
        $trans = "";
        $erase = "and cv.erase !=1  ";
    }


    if ($TypePolicy != -1) {
        $p_type = "and um.policy_type=$TypePolicy";
    }

    if ($usres != -1) {
        $user = "and cv.id_usuario=$usres";
    }

    if ($companies != -1) {
        $comp = "and cv.company=$companies";
    }

    if ($MethodTransaction != -1) {
        $pm = "and cv.companytype=$MethodTransaction";
    }

    $sql = "SELECT cv.* FROM "
            . "cutvone as cv LEFT JOIN UserManagement as um on cv.id_cliente = um.id "
            . "where cv.fecha BETWEEN '$date1' AND '$date2' $ag $erase  $comp $user  $trans $pm $p_type   order by cv.id_usuario,cv.fecha,cv.horat ASC ";

    $result = mysqli_query($mysqli, $sql);
    $nameReport = GenerateSalesReport($mysqli, $sql, $ordate1, $ordate2);
    ?>
    <div class="row col-md-12 col-lg-12 custyle" style="overflow-x: auto; margin-top: 2%;">
        <table class="table table-striped custab">
            <thead>
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class='btn btn-success btn-sm' target="_blank" href="MGTM/<?= $nameReport ?>" ><span class="fa  fa-file fa-lg"></span>Download PDF</a> 
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class='btn btn-success btn-sm' target="_blank"  ><span class="fa  fa-file fa-lg"></span>Download XLS</a> 
            </div>
            <tr>
                <th>#</th>
                <th>Location</th>
                <th>Customer</th>
                <th>User</th>
                <th>Company</th>
                <th>Cashier</th>
                <th>Ins</th>
                <th>Office</th>
                <th>RSA</th>
                <th>Total</th>
                <th>Payment Method</th>

            </tr>
            </thead>
            <?
            $cont = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $cont++;

                $nameUser = getName($mysqli, $row['id_usuario']);
                if ($row['id_cashier'] != 0 || $row['id_cashier'] != null) {
                    $nameCashier = getName($mysqli, $row['id_cashier']);
                }
                $customer = $row['nameclient'];
                $feeCompany = $row['feecompany'];
                $feeoffice = $row['feeoffice'];
                $feeforaz = $row['feeforaz'];
                $total = $feeCompany + $feeoffice + $feeforaz;

                if ($row['companytype'] == 1) {
                    $feecompanycoc = "CASH";
                } else {
                    $feecompanycoc = "CARD";
                }
                if ($row['typeaz'] == 3) {
                    $feecompanycoc = "CARD";
                    $feecompanycoc = "<p style='color:#79a85a'>CARD</P>";
                }
                ?>
                <tr>
                    <td><?= $cont ?></td>
                    <td><?= $row['agency'] ?></td>
                    <td style="width:150px"><?= strtoupper($customer) ?></td>
                    <td><?= strtoupper($nameUser) ?></td>
                    <td style="width:170px"><?= getCompany($mysqli, $row['company']) ?></td>
                    <td><?= strtoupper($nameCashier) ?></td>
                    <td><?= $feeCompany ?></td>
                    <td><?= $feeoffice ?></td>
                    <td><?= $feeforaz ?></td>
                    <td><?= $total ?></td>
                    <td><?= $feecompanycoc ?></td>

                </tr>
                <tr>

                    <td colspan="13">

                        <div class="col-lg-1 col-md-1 col-xs-12 col-sm-12">
                            <a class='btn btn-success btn-sm' target="_blank" href="<?= $row['recibo'] ?>" ><span class="fa  fa-download fa-lg"></span>Ticket</a> 
                        </div>

                        <? if ($editP == 1) { ?>
                            <div class="col-lg-1 col-md-1 col-xs-12 col-sm-12">
                                <a type="button" class="btn btn-info btn-sm" href="Edit_transaction.php?transaction_number=<?= $row['id'] ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                            </div>
                        <? } ?>
                        <? if ($eraseP == 1) { ?>
                            <div class="col-lg-1 col-md-1 col-xs-12 col-sm-12">

                                <a class='btn btn-danger btn-sm'  onclick="EraseRow('<?= $idTRansaction ?>')"><span class="fa  fa-remove fa-lg"></span>Remove</a> 

                            </div>
                        <? } ?>
                    </td>

                </tr>
            <? } ?>
        </table>
    </div>    
    <?
} else if ($caseReport == 2) {

    $agencies = $_POST['agencies'];
    $usres = $_POST['usres'];
    $companies = $_POST['companies'];
    $date1 = $_POST['date1'];
    $ordate1 = $date1;
    $date2 = $_POST['date2'];
    $ordate2 = $date2;
    $Dealership = $_POST['Dealership'];


    $arr1 = split("-", $date1);
    $arr2 = split("-", $date2);

    $date1 = $arr1[2] . "-" . $arr1[0] . "-" . $arr1[1];
    $date2 = $arr2[2] . "-" . $arr2[0] . "-" . $arr2[1];

    $erase = "";
    $user = "";
    $comp = "";
    $ag = "";
    $dealersquer = "";
    if ($agencies == -1) {
        $ag = "and (cv.agency=101 or cv.agency=102 or cv.agency=103 or cv.agency=104 or cv.agency=105)";
    } else {
        $ag = "and cv.agency=$agencies";
    }


    $erase = "and cv.erase !=1  ";

    if ($usres != -1) {
        $user = "and cv.id_usuario=$usres";
    }

    if ($companies != -1) {
        $comp = "and cv.company=$companies";
    }

    if ($Dealership != -1) {
        $dealersquer = "and cv.dealership=$Dealership";
    }
    $delivery = "and (cv.nameentrega is not null or cv.nameentrega!='NA' or cv.nameentrega!='')";




    $sql = "SELECT cv.* FROM "
            . "cutvone as cv LEFT JOIN UserManagement as um on cv.id_cliente = um.id "
            . "where cv.fecha BETWEEN '$date1' AND '$date2' $ag $erase  $comp $user  $dealersquer $delivery order by cv.id_usuario,cv.fecha,cv.horat ASC ";

    $result = mysqli_query($mysqli, $sql);
    $nameReport = GetDealersReport($mysqli, $sql, $ordate1, $ordate2);
    ?>
    <div class="row col-md-12 col-lg-12 custyle" style="overflow-x: auto; margin-top: 2%;">
        <table class="table table-striped custab">
            <thead>
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class='btn btn-success btn-sm' target="_blank" href="MGTM/<?= $nameReport ?>" ><span class="fa  fa-file fa-lg"></span>Download PDF</a> 
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class='btn btn-success btn-sm' target="_blank"  ><span class="fa  fa-file fa-lg"></span>Download XLS</a> 
            </div>
            <tr>
                <th>#</th>
                <th>Location</th>
                <th>Customer</th>
                <th>User</th>
                <th>Company</th>
                <th>Ins</th>
                <th>Office</th>
                <th>RSA</th>
                <th>Total</th>
                <th>Payment Method</th>
                <th>Dealer</th>
                <th>Delivery</th>
                <th>Seller</th>

            </tr>
            </thead>
            <?
            $cont = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $cont++;
                $nameUser = getName($mysqli, $row['id_usuario']);
                $nameCashier = getName($mysqli, $row['id_cashier']);
                $customer = $row['nameclient'];
                $feeCompany = $row['feecompany'];
                $feeoffice = $row['feeoffice'];
                $feeforaz = $row['feeforaz'];
                $namevendedor = $row['namevendedor'];
                $nameentrega = $row['nameentrega'];
                $nameDelivery = GetNameDelivery($mysqli, $row['dealership']);

                $total = $feeCompany + $feeoffice + $feeforaz;

                if ($row['companytype'] == 1) {
                    $feecompanycoc = "CASH";
                } else {
                    $feecompanycoc = "CARD";
                }
                if ($row['typeaz'] == 3) {
                    $feecompanycoc = "CARD";
                    $feecompanycoc = "<p style='color:#79a85a'>CARD</P>";
                }
                $idTRansaction = $row['id'];
                ?>
                <tr>
                    <td><?= $cont ?></td>
                    <td><?= $row['agency'] ?></td>
                    <td style="width:150px"><?= strtoupper($customer) ?></td>
                    <td><?= strtoupper($nameUser) ?></td>
                    <td style="width:170px"><?= getCompany($mysqli, $row['company']) ?></td>
                    <td><?= $feeCompany ?></td>
                    <td><?= $feeoffice ?></td>
                    <td><?= $feeforaz ?></td>
                    <td><?= $total ?></td>
                    <td><?= $feecompanycoc ?></td>
                    <td style="width:150px"><?= strtoupper($nameDelivery) ?></td>
                    <td style="width:150px"><?= strtoupper($nameentrega) ?></td>
                    <td style="width:150px"><?= strtoupper($namevendedor) ?></td>

                </tr>

            <? } ?>
        </table>
    </div>    
    <?
} else if ($caseReport == 3) {

    $agencies = $_POST['agencies'];
    $usres = $_POST['usres'];
    $companies = $_POST['companies'];
    $date1 = $_POST['date1'];
    $ordate1 = $date1;
    $date2 = $_POST['date2'];
    $ordate2 = $date2;


    $arr1 = split("-", $date1);
    $arr2 = split("-", $date2);

    $date1 = $arr1[2] . "-" . $arr1[0] . "-" . $arr1[1];
    $date2 = $arr2[2] . "-" . $arr2[0] . "-" . $arr2[1];

    $erase = "";
    $user = "";
    $comp = "";
    $ag = "";

    if ($agencies == -1) {
        $ag = "and (cv.agency=101 or cv.agency=102 or cv.agency=103 or cv.agency=104 or cv.agency=105)";
    } else {
        $ag = "and cv.agency=$agencies";
    }


    $erase = "and cv.erase !=1  ";

    if ($usres != -1) {
        $user = "and cv.id_usuario=$usres";
    }

    if ($companies != -1) {
        $comp = "and cv.company=$companies";
    }
    $trans = "and (cv.Dpayment=1 or cv.Dpayment=3  or cv.Dpayment=3 or cv.Dpayment=5 or cv.Dpayment=10  )";


    $sql = "SELECT cv.*,um.aplication,um.phoneNumber,um.adcsinged,um.pictures,um.proofop,um.registration,um.driverLicense FROM"
            . " cutvone as cv LEFT JOIN UserManagement as um on cv.id_cliente = um.id "
            . "where cv.fecha BETWEEN '$date1' AND '$date2'  $ag  $erase  $comp $user  $trans    order by cv.id_usuario,cv.fecha,cv.horat ASC ";
    $result = mysqli_query($mysqli, $sql);

    $nameReport = GetWCReport($mysqli, $sql, $date1, $date2);
    ?>

    <div class="row col-md-12 col-lg-12 custyle" style="overflow-x: auto; margin-top: 2%;">
        <table class="table table-striped custab">
            <thead>
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class='btn btn-success btn-sm' target="_blank" href="MGTM/<?= $nameReport ?>" ><span class="fa  fa-file fa-lg"></span>Download PDF</a> 
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class='btn btn-success btn-sm' target="_blank"  ><span class="fa  fa-file fa-lg"></span>Download XLS</a> 
            </div>
            <tr>
                <th>#</th>
                <th>Location</th>
                <th>Customer</th>
                <th>User</th>
                <th>Company</th>
                <th>Ins</th>
                <th>Office</th>
                <th>RSA</th>
                <th>Total</th>
                <th>Payment Method</th>



            </tr>
            </thead>
            <?
            $cont = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $cont++;
                $nameUser = getName($mysqli, $row['id_usuario']);
                $nameCashier = getName($mysqli, $row['id_cashier']);
                $customer = $row['nameclient'];
                $feeCompany = $row['feecompany'];
                $feeoffice = $row['feeoffice'];
                $feeforaz = $row['feeforaz'];



                $total = $feeCompany + $feeoffice + $feeforaz;

                if ($row['companytype'] == 1) {
                    $feecompanycoc = "CASH";
                } else {
                    $feecompanycoc = "CARD";
                }
                if ($row['typeaz'] == 3) {
                    $feecompanycoc = "CARD";
                    $feecompanycoc = "<p style='color:#79a85a'>CARD</P>";
                }
                ?>
                <tr>
                    <td><?= $cont ?></td>
                    <td><?= $row['agency'] ?></td>
                    <td style="width:150px"><?= strtoupper($customer) ?></td>
                    <td><?= strtoupper($nameUser) ?></td>
                    <td style="width:170px"><?= getCompany($mysqli, $row['company']) ?></td>
                    <td><?= $feeCompany ?></td>
                    <td><?= $feeoffice ?></td>
                    <td><?= $feeforaz ?></td>
                    <td><?= $total ?></td>
                    <td><?= $feecompanycoc ?></td>
                </tr>

            <? } ?>
        </table>
    </div>    
    <?
} else if ($caseReport == 4) {

    $agencies = $_POST['agencies'];
    $usres = $_POST['usres'];
    $companies = $_POST['companies'];
    $Status = $_POST['Status'];
    $date1 = $_POST['date1'];
    $ordate1 = $date1;
    $date2 = $_POST['date2'];
    $ordate2 = $date2;

    $titlereport = "";
    if ($status == 0) {
        $titlereport = "All Filters";
    } else if ($status == 1) {
        $titlereport = "PENDING CANCELLATION";
    } else if ($status == 2) {
        $titlereport = "CANCELLED NON-PAYMENT";
    } else if ($status == 3) {
        $titlereport = "CANCELLED UW";
    } else if ($status == 4) {
        $titlereport = "RETAINED";
    } else if ($status == 5) {
        $titlereport = "REINSTATED";
    } else if ($status == 6) {
        $titlereport = "CANCELLED SERVICE";
    } else if ($status == 7) {
        $titlereport = "EXPIRED";
    } else if ($status == 8) {
        $titlereport = "RENEWED";
    } else if ($status == 9) {
        $titlereport = "PENDING RENEWAL";
    } else if ($status == 10) {
        $titlereport = "NEW BUSINESS";
    } else if ($status == 11) {
        $titlereport = "NO LONGER CLIENT";
    } else if ($status == 12) {
        $titlereport = "UNDERWRITING";
    }


    $arr1 = split("-", $date1);
    $arr2 = split("-", $date2);

    $date1 = $arr1[2] . "-" . $arr1[0] . "-" . $arr1[1];
    $date2 = $arr2[2] . "-" . $arr2[0] . "-" . $arr2[1];

    $agencia = "and (agencia=101 or agencia=102 or agencia=103 or agencia=104 or agencia=105)";
    $agencia2 = "and (UM.agencia=101 or UM.agencia=102 or UM.agencia=103 or UM.agencia=104 or UM.agencia=105)";
    $company = "";
    $company2 = "";
    $status1 = "";
    $status2 = "";


    if ($companies != -1) {
        $company = "and compania=$companies";
        $company2 = "and PMOLD.company_id=$companies";
    }

    if ($agencies != -1) {
        $agencia = "and agencia=$agencies";
        $agencia2 = "and UM.agencia=$agencies";
    }

    if ($Status != 0) {
        $status1 = "and status_client=$Status";
        $status2 = "and PMOLD.status_policy=$Status";
    }

    $sql = "SELECT * FROM UserManagement where fecha_mod_satus BETWEEN '$date1' AND '$date2' $agencia $company $status1";
    $sqlSecond = "SELECT PMOLD.*,UM.nombre as nombreClient, UM.mail as email ,UM.addemail as email2 , UM.tel as phone , UM.addphone as phone2 , UM.agencia as agencia  FROM `PoliciesForOldMGTM` AS PMOLD join  UserManagement AS UM ON UM.id= PMOLD.id_cliente "
            . "and  fech_mod_status  BETWEEN '$date1' AND '$date2' $agencia2 $company2 $status2";
    $result2 = mysqli_query($mysqli, $sqlSecond);
    $result = mysqli_query($mysqli, $sql);
    $nameReport = CreateRetentionReport($mysqli, $sql, $sqlSecond, $date1, $date2);

    ?>
    <div class = "row col-md-12 col-lg-12 custyle" style = "overflow-x: auto; margin-top: 2%;">
        <table class = "table table-striped custab">
            <thead>
            <div class = "col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class = 'btn btn-success btn-sm' target = "_blank" href = "MGTM/<?= $nameReport ?>" ><span class = "fa  fa-file fa-lg"></span>Download PDF</a>
            </div>
            <div class = "col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <a class = 'btn btn-success btn-sm' target = "_blank" ><span class = "fa  fa-file fa-lg"></span>Download XLS</a>
            </div>
            <tr>
                <th>#</th>
                <th>Location</th>
                <th>Customer</th>
                <th>Status</th>
                <th>Company</th>
                <th>Tel</th>






            </tr>
            </thead>
            <?
            $cont = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $cont++;
                $customer = $row['nombre'];
                $status = $row['status_client'];
                $titlereport = "";
                if ($status == 0) {
                    $titlereport = "All Filters";
                } else if ($status == 1) {
                    $titlereport = "PENDING CANCELLATION";
                } else if ($status == 2) {
                    $titlereport = "CANCELLED NON-PAYMENT";
                } else if ($status == 3) {
                    $titlereport = "CANCELLED UW";
                } else if ($status == 4) {
                    $titlereport = "RETAINED";
                } else if ($status == 5) {
                    $titlereport = "REINSTATED";
                } else if ($status == 6) {
                    $titlereport = "CANCELLED SERVICE";
                } else if ($status == 7) {
                    $titlereport = "EXPIRED";
                } else if ($status == 8) {
                    $titlereport = "RENEWED";
                } else if ($status == 9) {
                    $titlereport = "PENDING RENEWAL";
                } else if ($status == 10) {
                    $titlereport = "NEW BUSINESS";
                } else if ($status == 11) {
                    $titlereport = "NO LONGER CLIENT";
                } else if ($status == 12) {
                    $titlereport = "UNDERWRITING";
                }
                $companyNaname = getCompany($mysqli, $row['compania']);
                $tel = $row['tel'];
                ?>
                <tr>
                    <td><?= $cont ?></td>
                    <td style="width:80px"><?= $row['agencia'] ?></td>
                    <td ><?= strtoupper($customer) ?></td>
                    <td><?= strtoupper($titlereport) ?></td>
                    <td><?= $companyNaname ?></td>
                    <td><?= $tel ?></td>


                </tr>

            <? } ?>


            <?
            while ($row = mysqli_fetch_assoc($result2)) {
                $cont++;
                $customer = $row['nombreClient'];
                $status = $row['status_policy'];
                $titlereport = "";
                if ($status == 0) {
                    $titlereport = "All Filters";
                } else if ($status == 1) {
                    $titlereport = "PENDING CANCELLATION";
                } else if ($status == 2) {
                    $titlereport = "CANCELLED NON-PAYMENT";
                } else if ($status == 3) {
                    $titlereport = "CANCELLED UW";
                } else if ($status == 4) {
                    $titlereport = "RETAINED";
                } else if ($status == 5) {
                    $titlereport = "REINSTATED";
                } else if ($status == 6) {
                    $titlereport = "CANCELLED SERVICE";
                } else if ($status == 7) {
                    $titlereport = "EXPIRED";
                } else if ($status == 8) {
                    $titlereport = "RENEWED";
                } else if ($status == 9) {
                    $titlereport = "PENDING RENEWAL";
                } else if ($status == 10) {
                    $titlereport = "NEW BUSINESS";
                } else if ($status == 11) {
                    $titlereport = "NO LONGER CLIENT";
                } else if ($status == 12) {
                    $titlereport = "UNDERWRITING";
                }

                $companyNaname = getCompany($mysqli, $row['company_id']);
                $tel = $row['phone'];
                ?>

                <tr>
                    <td><?= $cont ?></td>
                    <td style="width:80px"><?= $row['agencia'] ?></td>
                    <td ><?= strtoupper($customer) ?></td>
                    <td><?= strtoupper($titlereport) ?></td>
                    <td><?= $companyNaname ?></td>
                    <td><?= $tel ?></td>


                </tr>
            <? } ?>
        </table>
    </div>  
    <?
}
?>
