<?php

require('../../libs/fpdf181/fpdf.php');

//require('../../fpdf.php');


function getName($mysqli, $id) {
    $sql = "SELECT user FROM `users` where id=$id";
    $res = mysqli_query($mysqli, $sql);
    $r = mysqli_fetch_assoc($res);
    return $r['user'];
}

function getCompany($mysqli, $id) {
    $sql = "SELECT nombre FROM `companyes` where id=$id";
    $res = mysqli_query($mysqli, $sql);
    $r = mysqli_fetch_assoc($res);
    return $r['nombre'];
}

function GetNameDelivery($mysqli, $id) {
    $sql = "SELECT nombre FROM `dealers` where id=$id";
    $res = mysqli_query($mysqli, $sql);
    $r = mysqli_fetch_assoc($res);
    return $r['nombre'];
}

function getimage($mysqli, $id) {
    $sql = "SELECT ImageRoute FROM `companyes` where id=$id";
    $res = mysqli_query($mysqli, $sql);
    $r = mysqli_fetch_assoc($res);
    return $r['ImageRoute'];
}

function GenerateSalesReport($mysqli, $sql, $date1, $date2) {
    $pdf = new FPDF('L', 'mm', 'A4');
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetFillColor(237, 80, 46, 1); //Fondo rgb
    $pdf->SetTextColor(255, 255, 255); //Letra color blanco
    $pdf->Image('../../img/InsGenius-01.png', 10, 1, 35, 38, 'png', '');
    $pdf->SetXY(50, 15);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Ln(25);

    $pdf->Cell(60, 3.5, "Date: " . $date1 . "/" . $date2, 1, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Agency:", 1, 0, 'C', true);
    $pdf->Ln(6);

    $pdf->Cell(30, 3.5, "User", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Company", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Customer", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Ins", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Office", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "RSA", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Total", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Pay Method", 0, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Date/Time", 0, 0, 'C', true);

    $pdf->Ln();
    $result = mysqli_query($mysqli, $sql);
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetTextColor(10, 10, 10);
    $daz = 0;
    $cardaz = 0;
    $cashaz = 0;
    $cardoffice = 0;
    $cardinsurance = 0;
    $cashInsurance = 0;
    $cashoffice = 0;
    while ($row = mysqli_fetch_assoc($result)) {

        $nameUser = getName($mysqli, $row['id_usuario']);
        $Company = getCompany($mysqli, $row['company']);
        $date = $row['fecha'];
        $time = $row['horat'];

        $type = ($row['companytype'] == 1) ? "CASH" : "CARD";
        $conttran += 1;

        if ($row['companytype'] == 1) {
            $type = "CASH";
            $cashInsurance += $row['feecompany'];
            $cashoffice += $row['feeoffice'];
        } else {
            $type = "CARD";
            $cardinsurance += $row['feecompany'];
            $cardoffice += $row['feeoffice'];
            // $cardaz+=$row['feeforaz'];
        }
        if ($row['typeaz'] == 3) {
            $type = "CARD";
            $daz += $row['feeforaz'];
        } else if ($row['typeaz'] == 2) {
            $cardaz += $row['feeforaz'];
        } else if ($row['typeaz'] == 1) {
            $cashaz += $row['feeforaz'];
        }

        $pdf->Cell(30, 5, $nameUser, 0, 0, 'C');
        $pdf->Cell(30, 5, $Company, 0, 0, 'C');
        //$pdf->MultiCell(30, 5, utf8_decode($row['nameclient']),1, 0, 'C');
        $pdf->Cell(30, 5, $row['nameclient'], 0, 0, 'C');
        $pdf->Cell(30, 5, $row['feecompany'], 0, 0, 'C');
        $pdf->Cell(30, 5, $row['feeoffice'], 0, 0, 'C');
        $pdf->Cell(30, 5, $row['feeforaz'], 0, 0, 'C');
        $pdf->Cell(30, 5, $row['feeforaz'] + $row['feedealer'] + $row['feeoffice'] + $row['feecompany'], 0, 0, 'C');
        $pdf->Cell(30, 5, $type, 0, 0, 'C');
        $pdf->Cell(30, 5, $date . " / " . $time, 0, 0, 'C');
        $pdf->Ln();
    }
    $pdf->Ln();
    $pdf->SetFillColor(237, 80, 46, 1); //Fondo rgb
    $pdf->SetTextColor(255, 255, 255); //Letra color blanco
    $pdf->Cell(30, 3.5, "Totals", 1, 0, 'C', true);
    $pdf->Ln();
    $pdf->Cell(15, 3.5, "Concept", 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Cash", 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Card", 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Direct", 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Total", 1, 0, 'C', true);
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetTextColor(10, 10, 10);
    $pdf->Cell(15, 3.5, "Insurance", 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cashInsurance, 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cardinsurance, 1, 0, 'C');
    $pdf->Cell(15, 3.5, "0", 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cashInsurance + $cardinsurance, 1, 0, 'C');
    $pdf->Ln();
    $pdf->Cell(15, 3.5, "Office", 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cashoffice, 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cardoffice, 1, 0, 'C');
    $pdf->Cell(15, 3.5, "0", 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cashoffice + $cardoffice, 1, 0, 'C');
    $pdf->Ln();
    $pdf->Cell(15, 3.5, "RSA", 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cashaz, 1, 0, 'C');
    $pdf->Cell(15, 3.5, $cardaz, 1, 0, 'C');
    $pdf->Cell(15, 3.5, $daz, 1, 0, 'C');
    $pdf->Cell(15, 3.5, $daz + $cardaz + $cashaz, 1, 0, 'C');
    $pdf->Ln();
    $pdf->SetFillColor(237, 80, 46, 1); //Fondo rgb
    $pdf->SetTextColor(255, 255, 255); //Letra color blanco


    $pdf->Cell(15, 3.5, "", 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, $cashInsurance + $cashoffice, 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, $cardaz + $cardoffice + $cardinsurance, 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, $daz, 1, 0, 'C', true);
    $pdf->Cell(15, 3.5, $cardaz + $cardoffice + $cardinsurance + $cashInsurance + $cashoffice + $cashaz + $daz, 1, 0, 'C', true);

    $pdf->Ln();

    $queryCashier = "SELECT * FROM `users` where ( agencia=101 or agencia=103 or agencia=104 or agencia=105) and habilitado=1 and isCashier=1";
    $resCashier = mysqli_query($mysqli, $queryCashier);

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetFillColor(237, 80, 46, 1); //Fondo rgb
    $pdf->SetTextColor(255, 255, 255); //Letra color blanco
    $pdf->Ln(29);
    $pdf->Cell(40, 5, "Cashier", 1, 0, 'C', true);
    $pdf->Cell(40, 5, "Total", 1, 0, 'C', true);
    $pdf->Ln();
    $pdf->SetTextColor(10, 10, 10);
    while ($r = mysqli_fetch_assoc($resCashier)) {
        $amount = getAllCashUser($r['id'], $date1, $date2, $mysqli);

        $pdf->Cell(40, 5, getName($mysqli, $r['id']), 0, 0, 'C');
        $pdf->Cell(40, 5, $amount, 0, 0, 'C');
        $pdf->Ln();
    }
    $t = time();
    $name = "../../PDF_reports/SalesReport$t.PDF";
    $pdf->Output($name, 'F');
    return $name;
}

function GetDealersReport($mysqli, $sql, $date1, $date2) {
    $pdf = new FPDF('L', 'mm', 'A4');
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetFillColor(237, 80, 46, 1); //Fondo rgb
    $pdf->SetTextColor(255, 255, 255); //Letra color blanco
    $pdf->Image('../../img/InsGenius-01.png', 10, 1, 35, 38, 'png', '');
    $pdf->SetXY(50, 15);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Ln(25);

    $pdf->Cell(60, 3.5, "Date: " . $date1 . "/" . $date2, 1, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Agency:", 1, 0, 'C', true);
    $pdf->Ln(6);

    $pdf->Cell(25, 3.5, "User", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Company", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Customer", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Ins", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Office", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "RSA", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Total", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Pay Method", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Dealer", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Delivery", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Seller", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Date/Time", 0, 0, 'C', true);

    $pdf->Ln();
    $result = mysqli_query($mysqli, $sql);
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetTextColor(10, 10, 10);
    $daz = 0;
    $cardaz = 0;
    $cashaz = 0;
    $cardoffice = 0;
    $cardinsurance = 0;
    $cashInsurance = 0;
    $cashoffice = 0;
    while ($row = mysqli_fetch_assoc($result)) {

        $nameUser = getName($mysqli, $row['id_usuario']);
        $Company = getCompany($mysqli, $row['company']);
        $date = $row['fecha'];
        $time = $row['horat'];

        $type = ($row['companytype'] == 1) ? "CASH" : "CARD";
        $conttran += 1;

        if ($row['companytype'] == 1) {
            $type = "CASH";
            $cashInsurance += $row['feecompany'];
            $cashoffice += $row['feeoffice'];
        } else {
            $type = "CARD";
            $cardinsurance += $row['feecompany'];
            $cardoffice += $row['feeoffice'];
            // $cardaz+=$row['feeforaz'];
        }
        if ($row['typeaz'] == 3) {
            $type = "CARD";
            $daz += $row['feeforaz'];
        } else if ($row['typeaz'] == 2) {
            $cardaz += $row['feeforaz'];
        } else if ($row['typeaz'] == 1) {
            $cashaz += $row['feeforaz'];
        }


        $namevendedor = $row['namevendedor'];
        $nameentrega = $row['nameentrega'];
        $nameDelivery = GetNameDelivery($mysqli, $row['dealership']);

        $pdf->Cell(25, 5, $nameUser, 0, 0, 'C');
        $pdf->Cell(25, 5, $Company, 0, 0, 'C');
        $pdf->Cell(25, 5, $row['nameclient'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feecompany'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feeoffice'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feeforaz'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feeforaz'] + $row['feedealer'] + $row['feeoffice'] + $row['feecompany'], 0, 0, 'C');
        $pdf->Cell(25, 5, $type, 0, 0, 'C');
        $pdf->Cell(25, 5, $nameDelivery, 0, 0, 'C');
        $pdf->Cell(25, 5, $nameentrega, 0, 0, 'C');
        $pdf->Cell(25, 5, $namevendedor, 0, 0, 'C');
        $pdf->Cell(25, 5, $date . " / " . $time, 0, 0, 'C');
        $pdf->Ln();
    }


    $t = time();
    $name = "../../PDF_reports/DealerReport$t.PDF";
    $pdf->Output($name, 'F');
    return $name;
}

function GetWCReport($mysqli, $sql, $date1, $date2) {
    $pdf = new FPDF('L', 'mm', 'A4');
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetFillColor(237, 80, 46, 1); //Fondo rgb
    $pdf->SetTextColor(255, 255, 255); //Letra color blanco
    $pdf->Image('../../img/InsGenius-01.png', 10, 1, 35, 38, 'png', '');
    $pdf->SetXY(50, 15);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Ln(25);

    $pdf->Cell(60, 3.5, "Date: " . $date1 . "/" . $date2, 1, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Agency:", 1, 0, 'C', true);
    $pdf->Ln(6);

    $pdf->Cell(25, 3.5, "User", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Company", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Customer", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Ins", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Office", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "RSA", 0, 0, 'C', true);
    $pdf->Cell(15, 3.5, "Total", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Pay Method", 0, 0, 'C', true);
    $pdf->Cell(25, 3.5, "Date/Time", 0, 0, 'C', true);

    $pdf->Ln();
    $result = mysqli_query($mysqli, $sql);
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetTextColor(10, 10, 10);
    $daz = 0;
    $cardaz = 0;
    $cashaz = 0;
    $cardoffice = 0;
    $cardinsurance = 0;
    $cashInsurance = 0;
    $cashoffice = 0;
    while ($row = mysqli_fetch_assoc($result)) {

        $nameUser = getName($mysqli, $row['id_usuario']);
        $Company = getCompany($mysqli, $row['company']);
        $date = $row['fecha'];
        $time = $row['horat'];

        $type = ($row['companytype'] == 1) ? "CASH" : "CARD";
        $conttran += 1;

        if ($row['companytype'] == 1) {
            $type = "CASH";
            $cashInsurance += $row['feecompany'];
            $cashoffice += $row['feeoffice'];
        } else {
            $type = "CARD";
            $cardinsurance += $row['feecompany'];
            $cardoffice += $row['feeoffice'];
            // $cardaz+=$row['feeforaz'];
        }
        if ($row['typeaz'] == 3) {
            $type = "CARD";
            $daz += $row['feeforaz'];
        } else if ($row['typeaz'] == 2) {
            $cardaz += $row['feeforaz'];
        } else if ($row['typeaz'] == 1) {
            $cashaz += $row['feeforaz'];
        }

        $pdf->Cell(25, 5, $nameUser, 0, 0, 'C');
        $pdf->Cell(25, 5, $Company, 0, 0, 'C');
        $pdf->Cell(25, 5, $row['nameclient'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feecompany'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feeoffice'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feeforaz'], 0, 0, 'C');
        $pdf->Cell(15, 5, $row['feeforaz'] + $row['feedealer'] + $row['feeoffice'] + $row['feecompany'], 0, 0, 'C');
        $pdf->Cell(25, 5, $type, 0, 0, 'C');
        $pdf->Cell(25, 5, $date . " / " . $time, 0, 0, 'C');
        $pdf->Ln();
        $variable1 = ( $row['aplication'] == 1 || $row['aplication'] == -1) ? true : false;
        $variable2 = ( $row['phoneNumber'] == 1 || $row['phoneNumber'] == -1) ? true : false;
        $variable3 = ( $row['driverLicense'] == 1 || $row['driverLicense'] == -1) ? true : false;
        $variable4 = ( $row['registration'] == 1 || $row['registration'] == -1) ? true : false;
        $variable5 = ( $row['pictures'] == 1 || $row['pictures'] == -1) ? true : false;
        $variable6 = ( $row['proofop'] == 1 || $row['proofop'] == -1) ? true : false;
        $variable7 = ( $row['adcsinged'] == 1 || $row['adcsinged'] == -1) ? true : false;

        $pdf->SetFillColor(112, 166, 50); //Fondo rgb

        $pdf->Cell(56, 5, "", 0, 0, 'C');

        if ($row['aplication'] == -1) {
            $pdf->SetFillColor(217, 237, 247);
            $pdf->Cell(25, 5, "Aplication", 0, 0, 'C', $variable1);
        } else {
            $pdf->Cell(25, 5, "Aplication", 0, 0, 'C', $variable1);
        }
        if ($row['phoneNumber'] == -1) {
            $pdf->SetFillColor(217, 237, 247);
            $pdf->Cell(25, 5, "Phone Num", 0, 0, 'C', $variable2);
        } else {
            $pdf->Cell(25, 5, "Phone Num", 0, 0, 'C', $variable2);
        }

        if ($row['driverLicense'] == -1) {
            $pdf->SetFillColor(217, 237, 247);
            $pdf->Cell(25, 5, "D License", 0, 0, 'C', $variable3);
        } else {
            $pdf->Cell(25, 5, "D License", 0, 0, 'C', $variable3);
        }

        if ($row['registration'] == -1) {
            $pdf->SetFillColor(217, 237, 247);
            $pdf->Cell(25, 5, "Resgistratiom", 0, 0, 'C', $variable4);
        } else {
            $pdf->Cell(25, 5, "Resgistratiom", 0, 0, 'C', $variable4);
        }
        if ($row['pictures'] == -1) {
            $pdf->SetFillColor(217, 237, 247);
            $pdf->Cell(25, 5, "Pictures", 0, 0, 'C', $variable5);
        } else {
            $pdf->Cell(25, 5, "Pictures", 0, 0, 'C', $variable5);
        }
        if ($row['proofop'] == -1) {
            $pdf->SetFillColor(217, 237, 247);
            $pdf->Cell(25, 5, "Proof Of Prior", 0, 0, 'C', $variable6);
        } else {
            $pdf->Cell(25, 5, "Proof Of Prior", 0, 0, 'C', $variable6);
        }
        if ($row['adcsinged'] == -1) {
            $pdf->SetFillColor(217, 237, 247);
            $pdf->Cell(25, 5, "ADC Singed", 0, 0, 'C', $variable7);
        } else {
            $pdf->Cell(25, 5, "ADC Singed", 0, 0, 'C', $variable7);
        }



        $pdf->Ln();
        $pdf->Ln();
    }


    $t = time();
    $name = "../../PDF_reports/WellcomeCallReport$t.PDF";
    $pdf->Output($name, 'F');
    return $name;
}

function CreateRetentionReport($mysqli, $sql, $sql2, $date1, $date2) {
    $pdf = new FPDF('L', 'mm', 'A4');
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetFillColor(237, 80, 46, 1); //Fondo rgb
    $pdf->SetTextColor(255, 255, 255); //Letra color blanco
    $pdf->Image('../../img/InsGenius-01.png', 10, 1, 35, 38, 'png', '');
    $pdf->SetXY(50, 15);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Ln(25);
    $stringTel = "";
    $strmails = "";
    $pdf->Cell(60, 3.5, "Date: " . $date1 . "/" . $date2, 1, 0, 'C', true);
    $pdf->Cell(30, 3.5, "Agency:", 1, 0, 'C', true);
    $pdf->Ln(6);

    $pdf->Cell(25, 3.5, "Location", 0, 0, 'C', true);
    $pdf->Cell(40, 3.5, "Customer", 0, 0, 'C', true);
    $pdf->Cell(40, 3.5, "Status", 0, 0, 'C', true);
    $pdf->Cell(35, 3.5, "Company", 0, 0, 'C', true);
    $pdf->Cell(35, 3.5, "Phone", 0, 0, 'C', true);
    $pdf->Ln();

    $result = mysqli_query($mysqli, $sql);
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->SetTextColor(10, 10, 10);
    while ($row = mysqli_fetch_assoc($result)) {
        $customer = $row['nombre'];
        $stringTel .= $row['tel'] . ",";
        $strmails .= $row['mail'] . ",";
        $status = $row['status_client'];
        $agencia = $row['agencia'];
        $titlereport = "";
        if ($status == 0) {
            $titlereport = "All Filters";
        } else if ($status == 1) {
            $titlereport = "PENDING CANCELLATION";
        } else if ($status == 2) {
            $titlereport = "CANCELLED NON-PAYMENT";
        } else if ($status == 3) {
            $titlereport = "CANCELLED UW";
        } else if ($status == 4) {
            $titlereport = "RETAINED";
        } else if ($status == 5) {
            $titlereport = "REINSTATED";
        } else if ($status == 6) {
            $titlereport = "CANCELLED SERVICE";
        } else if ($status == 7) {
            $titlereport = "EXPIRED";
        } else if ($status == 8) {
            $titlereport = "RENEWED";
        } else if ($status == 9) {
            $titlereport = "PENDING RENEWAL";
        } else if ($status == 10) {
            $titlereport = "NEW BUSINESS";
        } else if ($status == 11) {
            $titlereport = "NO LONGER CLIENT";
        } else if ($status == 12) {
            $titlereport = "UNDERWRITING";
        }

        $companyNaname = getCompany($mysqli, $row['compania']);
        $tel = $row['tel'];

        $pdf->Cell(25, 5, $agencia, 0, 0, 'C');
        $pdf->Cell(40, 5, $customer, 0, 0, 'C');
        $pdf->Cell(40, 5, $titlereport, 0, 0, 'C');
        $pdf->Cell(35, 5, $companyNaname, 0, 0, 'C');
        $pdf->Cell(35, 5, $tel, 0, 0, 'C');
        $pdf->Ln();
    }

    $result2 = mysqli_query($mysqli, $sql2);
    while ($row = mysqli_fetch_assoc($result2)) {

        $customer = $row['nombreClient'];
        $status = $row['status_policy'];
        $stringTel .= $row['phone'] . ",";
        $strmails .= $row['email'] . ",";
        $agencia = $row['agencia'];
        $companyNaname = getCompany($mysqli, $row['company_id']);
        $tel = $row['phone'];
        $titlereport = "";
        if ($status == 0) {
            $titlereport = "All Filters";
        } else if ($status == 1) {
            $titlereport = "PENDING CANCELLATION";
        } else if ($status == 2) {
            $titlereport = "CANCELLED NON-PAYMENT";
        } else if ($status == 3) {
            $titlereport = "CANCELLED UW";
        } else if ($status == 4) {
            $titlereport = "RETAINED";
        } else if ($status == 5) {
            $titlereport = "REINSTATED";
        } else if ($status == 6) {
            $titlereport = "CANCELLED SERVICE";
        } else if ($status == 7) {
            $titlereport = "EXPIRED";
        } else if ($status == 8) {
            $titlereport = "RENEWED";
        } else if ($status == 9) {
            $titlereport = "PENDING RENEWAL";
        } else if ($status == 10) {
            $titlereport = "NEW BUSINESS";
        } else if ($status == 11) {
            $titlereport = "NO LONGER CLIENT";
        } else if ($status == 12) {
            $titlereport = "UNDERWRITING";
        }
        $pdf->Cell(25, 5, $agencia, 0, 0, 'C');
        $pdf->Cell(40, 5, $customer, 0, 0, 'C');
        $pdf->Cell(40, 5, $titlereport, 0, 0, 'C');
        $pdf->Cell(35, 5, $companyNaname, 0, 0, 'C');
        $pdf->Cell(35, 5, $tel, 0, 0, 'C');
        $pdf->Ln();
    }
    $pdf->Ln();
    $pdf->Ln();
    $pdf->MultiCell(270, 15, $stringTel, 1);
    $pdf->Ln();
    $pdf->Ln();
    $pdf->MultiCell(270, 15, $strmails, 1);
    $t = time();
    $name = "../../PDF_reports/RetentionReport$t.PDF";
    $pdf->Output($name, 'F');
    return $name;
}

function getAllCashUser($id_user, $fecha1, $fecha2, $mysqli) {
    $arr1 = split("-", $fecha1); //m-d-y
    $arr2 = split("-", $fecha2);

    $date1 = $arr1[2] . "-" . $arr1[0] . "-" . $arr1[1];
    $date2 = $arr2[2] . "-" . $arr2[0] . "-" . $arr2[1];

    $sqlREs = "SELECT * FROM `cutvone` where id_cashier=$id_user and fecha BETWEEN '$date1' and  '$date2' and erase=0 and (companytype=1 or typeaz=1 )";
    $resCashier = mysqli_query($mysqli, $sqlREs);
    //echo $sqlREs;
    $totalCash = 0;
    while ($row = mysqli_fetch_assoc($resCashier)) {

        if ($row['companytype'] == 1) {
            $totalCash += $row['feecompany'] + $row['feeoffice'];
        }
        if ($row['typeaz'] == 1) {
            $totalCash += $row['feeforaz'];
        }
    }

    return $totalCash;
}
