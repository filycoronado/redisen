<?php
include ("./conn.php");
$f1 = $_POST['date1']; //mm-dd-yyyy
$f2 = $_POST['date2'];
$Agency = $_POST['Agency'];

$arr1 = split("-", $f1);
$f1 = $arr1[2] . "-" . $arr1[0] . "-" . $arr1[1];

$arr2 = split("-", $f2);
$f2 = $arr2[2] . "-" . $arr2[0] . "-" . $arr2[1];
$array_agencies = array();
$labels = "";
$sql = "";
$sqlLabelAgency = "";
if ($Agency == "ALL") {

    $sql = "SELECT * FROM `agency` where id_owner=1";
} else {
    $sql = "SELECT * FROM `agency` where id=$Agency";
}

$result = mysqli_query($mysqli, $sql);
$cont = 0;

$label_totalExpiredByuser2 = "";
$label_totalnlcByuser2 = "";
$label_totaluwByuser2 = "";
$label_np_by_user2 = "";
$$label_actives = "";
while ($row = mysqli_fetch_assoc($result)) {
    $cont++;
    $labels .= $row['id'] . ",";

    if ($cont == 1) {
        $sqlLabelAgency .= "agency=" . $row['id'];
    } else {
        $sqlLabelAgency .= " or  agency=" . $row['id'];
    }

    array_push($array_agencies, $row['id']);
}
$nbpa = "";
$grandTotalNB;
$nbdealerships;
$grandTotalDealers = 0;
$nbOffice;
for ($i = 0; $i < count($array_agencies); $i++) {
    $totalNW = "SELECT count(id) as nb FROM `cutvone` where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1  or Dpayment=5 or Dpayment=10 ) and (agency=$array_agencies[$i]) and erase!=1";

    $resultlopp = mysqli_query($mysqli, $totalNW);
    $row = mysqli_fetch_assoc($resultlopp);
    $grandTotalNB += $row['nb'];
    $nbpa .= $row['nb'] . ",";


    $sqlDelaership = "SELECT count(id) as nbd FROM `cutvone` where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1  or Dpayment=5 or Dpayment=10 ) and (agency=$array_agencies[$i]) and erase!=1 and dealership>0";
    $resultdealers = mysqli_query($mysqli, $sqlDelaership);
    $rowdealers = mysqli_fetch_assoc($resultdealers);
    $grandTotalDealers += $rowdealers['nbd'];
    $nbdealerships .= $rowdealers['nbd'] . ",";



    $sqlOffice = "SELECT count(id) as nboff FROM `cutvone` where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1  or Dpayment=5 or Dpayment=10 ) and (agency=$array_agencies[$i]) and erase!=1 and(!dealership>0 or dealership IS NULL)";

    $resultoffice = mysqli_query($mysqli, $sqlOffice);
    $rowoffice = mysqli_fetch_assoc($resultoffice);
    $totalOffice += $rowoffice['nboff'];
    $nbOffice .= $rowoffice['nboff'] . ",";
}

$namesCompanys = "";
$array_companies = array();
$sqlCompany = "SELECT * FROM `companyes` where agency=101 and active=1";
$result = mysqli_query($mysqli, $sqlCompany);
$strRGB = "";
$rgbRED = "";
while ($row = mysqli_fetch_assoc($result)) {
    $namesCompanys .= "'" . $row['nombre'] . "'" . ",";
    array_push($array_companies, $row['id']);
    $strRGB .= "'blue'" . ",";
    $rgbRED .= "'red'" . ",";
}
$totalPC = "";

for ($i = 0; $i < count($array_companies); $i++) {
    $totalNWPC = "SELECT count(id) as nbPC FROM `cutvone` where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1 or Dpayment=5 or Dpayment=10 ) and (company=$array_companies[$i]) and erase!=1";

    $result = mysqli_query($mysqli, $totalNWPC);
    $row = mysqli_fetch_assoc($result);
    $totalPC .= $row['nbPC'] . ",";




    $sqlExpiredByUser = "SELECT count(id) as expired FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=7) and compania=" . $array_companies[$i];
    $resultExpiredByUser = mysqli_query($mysqli, $sqlExpiredByUser);
    $rowExpByuser = mysqli_fetch_assoc($resultExpiredByUser);
    $totalExpByuser = $rowExpByuser['expired'];

    $sqlExpiredByuser2 = "SELECT count(*)as expired FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=7) and company_id=" . $array_companies[$i];
    $resultExpiredByuser2 = mysqli_query($mysqli, $sqlExpiredByuser2);
    $rowExpiredbyuser2 = mysqli_fetch_assoc($resultExpiredByuser2);
    $totalExpByuser += $rowExpiredbyuser2['expired'];

    $label_totalExpiredByuser2 .= $totalExpByuser . ",";


    $sqlnlcByUser = "SELECT count(id) as nlc FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=11) and compania=" . $array_companies[$i];
    $resultnlcByUser = mysqli_query($mysqli, $sqlnlcByUser);
    $rownlcByuser = mysqli_fetch_assoc($resultnlcByUser);
    $totalnlcByuser = $rownlcByuser['nlc'];

    $sqlnlcByuser2 = "SELECT count(*)as nlc FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=11) and company_id=" . $array_companies[$i];
    $resultnlcByuser2 = mysqli_query($mysqli, $sqlnlcByuser2);
    $rownlcbyuser2 = mysqli_fetch_assoc($resultnlcByuser2);
    $totalnlcByuser += $rownlcbyuser2['nlc'];

    $label_totalnlcByuser2 .= $totalnlcByuser . ",";


    $sqluwByUser = "SELECT count(id) as uw FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=3) and compania=" . $array_companies[$i];
    $resultuwByUser = mysqli_query($mysqli, $sqluwByUser);
    $rowuwByuser = mysqli_fetch_assoc($resultuwByUser);
    $totaluwByuser = $rowuwByuser['uw'];

    $sqluwByuser2 = "SELECT count(*)as uw FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=3) and company_id=" . $array_companies[$i];
    $resultuwByuser2 = mysqli_query($mysqli, $sqluwByuser2);
    $rowuwbyuser2 = mysqli_fetch_assoc($resultuwByuser2);
    $totaluwByuser += $rowuwbyuser2['uw'];


    $label_totaluwByuser2 .= $totaluwByuser . ",";


    $sqlnpByUser = "SELECT count(id) as np FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=2) and compania=" . $array_companies[$i];
    $resultnpByUser = mysqli_query($mysqli, $sqlnpByUser);
    $rownpByuser = mysqli_fetch_assoc($resultnpByUser);
    $totalnpByuser = $rownpByuser['np'];

    $sqlnpByuser2 = "SELECT count(*)as np FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=2) and company_id=" . $array_companies[$i];
    $resultnpByuser2 = mysqli_query($mysqli, $sqlnpByuser2);
    $rownpbyuser2 = mysqli_fetch_assoc($resultnpByuser2);
    $totalnpByuser += $rownpbyuser2['np'];
    $label_np_by_user2 .= $totalnpByuser . ",";



    $sql = "SELECT count(*) as totalActive FROM `UserManagement` where (status_client=4 or status_client=5 or status_client=8 or status_client=10 or status_client=11) and compania=" . $array_companies[$i];
    $result1 = mysqli_query($mysqli, $sql);
    $row = mysqli_fetch_assoc($result1);
    $totalActives1 = $row['totalActive'];

    $sqlact = "SELECT count(*) as totalActive FROM `PoliciesForOldMGTM` a where (status_policy=4 or status_policy=5 or status_policy=8 or status_policy=10 or status_policy=11) and company_id=" . $array_companies[$i];
    $result1 = mysqli_query($mysqli, $sqlact);
    $row = mysqli_fetch_assoc($result1);
    $totalActives1 += $row['totalActive'];


    $label_actives .= $totalActives1 . ",";
}

$officePercentaje = ($totalOffice * 100) / $grandTotalNB;
$dealersPercentaje = ($grandTotalDealers * 100) / $grandTotalNB;



$sqlUsers = "SELECT cv.id_usuario as id_usuario, us.user as user FROM `cutvone` as cv JOIN users as us on us.id= cv.id_usuario where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1 or Dpayment=5 or Dpayment=10 ) and ($sqlLabelAgency) and erase!=1 GROUP BY id_usuario order BY id_usuario";

$query_all_users = "SELECT id as id_usuario ,user FROM `users` where  (agencia=101 or agencia=102 or agencia=103 or agencia=104 or agencia=105) and (nivel=2 or nivel=5 or nivel=1)";
?>

<?
$resultUsers = mysqli_query($mysqli, $sqlUsers);
$labelTotal = "";
$labelTotalD = "";
$labelUsers;
$cont = 0;

$label_totalExpiredByuser = "";
$label_totalnlcByuser = "";
$label_totaluwByuser = "";
$label_np_by_user = "";

$label_actives_by_user = "";
//NB
while ($r = mysqli_fetch_assoc($resultUsers)) {
    $cont++;

    $labelUsers .= "'" . $r['user'] . "',";
    $slqOffice = "SELECT count(id) as total FROM `cutvone` where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1  or Dpayment=5 or Dpayment=10 ) and ($sqlLabelAgency) and erase!=1 and(!dealership>0 or dealership IS NULL) and id_usuario=" . $r['id_usuario'];
    $result = mysqli_query($mysqli, $slqOffice);
    $row = mysqli_fetch_assoc($result);
    $labelTotal .= $row['total'] . ",";


    $sqlDealerTotal = "SELECT count(id) as totalDealersPU FROM `cutvone` where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1  or Dpayment=5 or Dpayment=10 ) and ($sqlLabelAgency) and erase!=1 and dealership>0 and id_usuario=" . $r['id_usuario'];

    $resultD = mysqli_query($mysqli, $sqlDealerTotal);
    $rowD = mysqli_fetch_assoc($resultD);
    $labelTotalD .= $rowD['totalDealersPU'] . ",";
}

$resultAll_users = mysqli_query($mysqli, $query_all_users);
$labelAllUsers = "";
//Cancelde/actives by user
$gtotalACTives = 0;
while ($r = mysqli_fetch_assoc($resultAll_users)) {
    $labelAllUsers .= "'" . $r['user'] . "',";
    $sqlExpiredByUser = "SELECT count(id) as expired FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=7) and id_usurio=" . $r['id_usuario'];
    $resultExpiredByUser = mysqli_query($mysqli, $sqlExpiredByUser);
    $rowExpByuser = mysqli_fetch_assoc($resultExpiredByUser);
    $totalExpByuser = $rowExpByuser['expired'];

    $sqlExpiredByuser2 = "SELECT count(*)as expired FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=7) and id_user=" . $r['id_usuario'];
    $resultExpiredByuser2 = mysqli_query($mysqli, $sqlExpiredByuser2);
    $rowExpiredbyuser2 = mysqli_fetch_assoc($resultExpiredByuser2);
    $totalExpByuser += $rowExpiredbyuser2['expired'];

    $label_totalExpiredByuser .= $totalExpByuser . ",";


    $sqlnlcByUser = "SELECT count(id) as nlc FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=11) and id_usurio=" . $r['id_usuario'];
    $resultnlcByUser = mysqli_query($mysqli, $sqlnlcByUser);
    $rownlcByuser = mysqli_fetch_assoc($resultnlcByUser);
    $totalnlcByuser = $rownlcByuser['nlc'];

    $sqlnlcByuser2 = "SELECT count(*)as nlc FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=11) and id_user=" . $r['id_usuario'];
    $resultnlcByuser2 = mysqli_query($mysqli, $sqlnlcByuser2);
    $rownlcbyuser2 = mysqli_fetch_assoc($resultnlcByuser2);
    $totalnlcByuser += $rownlcbyuser2['nlc'];

    $label_totalnlcByuser .= $totalnlcByuser . ",";


    $sqluwByUser = "SELECT count(id) as uw FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=3) and id_usurio=" . $r['id_usuario'];
    $resultuwByUser = mysqli_query($mysqli, $sqluwByUser);
    $rowuwByuser = mysqli_fetch_assoc($resultuwByUser);
    $totaluwByuser = $rowuwByuser['uw'];

    $sqluwByuser2 = "SELECT count(*)as uw FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=3) and id_user=" . $r['id_usuario'];
    $resultuwByuser2 = mysqli_query($mysqli, $sqluwByuser2);
    $rowuwbyuser2 = mysqli_fetch_assoc($resultuwByuser2);
    $totaluwByuser += $rowuwbyuser2['uw'];


    $label_totaluwByuser .= $totaluwByuser . ",";


    $sqlnpByUser = "SELECT count(id) as np FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=2) and id_usurio=" . $r['id_usuario'];
    $resultnpByUser = mysqli_query($mysqli, $sqlnpByUser);
    $rownpByuser = mysqli_fetch_assoc($resultnpByUser);
    $totalnpByuser = $rownpByuser['np'];

    $sqlnpByuser2 = "SELECT count(*)as np FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN  '$f1' and '$f2'  and (status_policy=2) and id_user=" . $r['id_usuario'];
    $resultnpByuser2 = mysqli_query($mysqli, $sqlnpByuser2);
    $rownpbyuser2 = mysqli_fetch_assoc($resultnpByuser2);
    $totalnpByuser += $rownpbyuser2['np'];
    $label_np_by_user .= $totalnpByuser . ",";


    $sql = "SELECT count(*) as totalActive FROM `UserManagement` where (status_client=1 or status_client=4 or status_client=5 or status_client=8 or status_client=10 or status_client=9) and id_usurio=" . $r['id_usuario'];

    $result1 = mysqli_query($mysqli, $sql);
    if ($result1) {
        $row = mysqli_fetch_assoc($result1);
        $totalActives1 = $row['totalActive'];
    }else{
        $totalActives1=0;
    }



    $sqlact = "SELECT count(*) as totalActive FROM `PoliciesForOldMGTM` a where (status_policy=1 or status_policy=4 or status_policy=5 or status_policy=8 or status_policy=10 or status_policy=9) and id_user=" . $r['id_usuario'];
    if ($result1) {
        $result1 = mysqli_query($mysqli, $sqlact);
        $row = mysqli_fetch_assoc($result1);
        $totalActives1 += $row['totalActive'];
    }else{
        $totalActives1+=0;
    }
    
    $gtotalACTives += $totalActives1;
    $label_actives_by_user .= $totalActives1 . ",";
}
$label_Calcel = "Expired,No longer client,Underwriting";
$sqlCancellationsExpired = "SELECT count(id) as expired FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=7)";
$resultCacelationExp = mysqli_query($mysqli, $sqlCancellationsExpired);
$rowExp = mysqli_fetch_assoc($resultCacelationExp);
$totalExp = $rowExp['expired'];

$sqlexp1 = "SELECT count(*)as expired FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN '2019-04-01' and '2019-04-30' and (status_policy=7)";
$resultCacelationExp2 = mysqli_query($mysqli, $sqlexp1);
$rowExp2 = mysqli_fetch_assoc($resultCacelationExp2);
$totalExp += $rowExp2['expired'];

$sqlCancellationsNLC = "SELECT count(id) as TotalNLC FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=11)";
$resultCacelationNLC = mysqli_query($mysqli, $sqlCancellationsNLC);
$rowNLC = mysqli_fetch_assoc($resultCacelationNLC);
$totalNLC = $rowNLC['TotalNLC'];



$sqlnlc = "SELECT count(*)as nlc FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN '$f1' and '$f2' and (status_policy=11)";
$resultCacelationnlc = mysqli_query($mysqli, $sqlnlc);
$rownlc = mysqli_fetch_assoc($resultCacelationnlc);
$totalNLC += $rownlc['nlc'];



$sqlCancellationsUW = "SELECT count(id) as TotaUW FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=3)";
$resultCacelationUW = mysqli_query($mysqli, $sqlCancellationsUW);
$rowUW = mysqli_fetch_assoc($resultCacelationUW);
$totalUW = $rowUW['TotaUW'];


$sqluw = "SELECT count(*)as uw FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN '$f1' and '$f2' and (status_policy=3)";
$resultCacelationuw = mysqli_query($mysqli, $sqluw);
$rowuw = mysqli_fetch_assoc($resultCacelationuw);
$totalUW += $rowuw['uw'];



$sqlCancellationsnp = "SELECT count(id) as np FROM UserManagement where fecha_mod_satus BETWEEN '$f1' and '$f2' and (status_client=2)";
$resultCacelationnp = mysqli_query($mysqli, $sqlCancellationsnp);
$rownp = mysqli_fetch_assoc($resultCacelationnp);
$totalnp = $rownp['np'];


$sqlnp = "SELECT count(*)as np FROM `PoliciesForOldMGTM`  where  fech_mod_status BETWEEN '$f1' and '$f2' and (status_policy=2)";
$resultCacelationnp = mysqli_query($mysqli, $sqlnp);
$rownp = mysqli_fetch_assoc($resultCacelationnp);
$totalnp += $rownp['np'];





$Gct = $totalUW + $totalNLC + $totalExp + $totalnp;
?>



<div class="row">
    <div class="col-md-6 col-lg-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>New Business , All Agencies , Grand Total :<?= $grandTotalNB ?></h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart" ></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-lg-6">

        <table class="table table-bordered">
            <thead>

                <tr>
                    <th>Office</th>
                    <th>Dealers</th>
                    <th>Total</th>
                <tr>

            </thead>
            <tr>
                <td><?= $totalOffice ?></td>
                <td><?= $grandTotalDealers ?></td>
                <td><?= $grandTotalNB ?></td>
            </tr>
            <tr>
                <td><?= $officePercentaje . " %" ?></td>
                <td><?= $dealersPercentaje . " %" ?></td>
                <td></td>
            </tr>
        </table>
    </div>

</div>  
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>Cancelled , Grand Total :<?= $Gct ?></h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart5" ></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>Cancellation , Per Users , Grand Total :<?= $Gct ?></h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart6" ></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>Actives , Per Users <?= $gtotalACTives ?></h3>
            </div>
            <div class="panel-body">
                <canvas id="ActivesByUser" ></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>New Business , Per Users , Grand Total :<?= $grandTotalNB ?></h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart3" ></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>New Business , Per Company , Grand Total :<?= $grandTotalNB ?></h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart2" ></canvas>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>Actives Per Companny</h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart22" ></canvas>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>Cancellations , Per Company:</h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart12" ></canvas>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var ctx = document.getElementById('myChart');
    var ctx2 = document.getElementById('myChart2');
    var ctx12 = document.getElementById('myChart12')

    var ctx22 = document.getElementById('myChart22')
    var ctxDealers = document.getElementById('myChartDealers');
    var ctxUsers = document.getElementById('myChart3');
    var ctx5 = document.getElementById('myChart5');
    var ctx6 = document.getElementById('myChart6');

    var ActivesByUser = document.getElementById('ActivesByUser');
    var myChart = new Chart(ctx2, {
        type: 'horizontalBar',
        data: {
            labels: [<?= $namesCompanys ?>],
            datasets: [{
                    label: 'Agencies',
                    data: [<?= $totalPC ?>],
                    backgroundColor: [<?= $strRGB ?>],
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });


    var myChart = new Chart(ctx22, {
        type: 'horizontalBar',
        data: {
            labels: [<?= $namesCompanys ?>],
            datasets: [{
                    label: 'Actives',
                    data: [<?= $label_actives ?>],
                    backgroundColor: [<?= $strRGB ?>],
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });



    var myChart = new Chart(ctx12, {
        type: 'horizontalBar',
        data: {
            labels: [<?= $namesCompanys ?>],
            datasets: [
                {
                    label: 'Expired',
                    data: [<?= $label_totalExpiredByuser2 ?>],
                    backgroundColor: [<?= $rgbRED ?>],
                    borderWidth: 1
                },
                {
                    label: 'NLC',
                    data: [<?= $label_totalnlcByuser2 ?>],
                    backgroundColor: [<?= $rgbRED ?>],
                    borderWidth: 1
                },
                {
                    label: 'UW',
                    data: [<?= $label_totaluwByuser2 ?>],
                    backgroundColor: [<?= $rgbRED ?>],
                    borderWidth: 1
                },
                {
                    label: 'NP',
                    data: [<?= $label_np_by_user2 ?>],
                    backgroundColor: [<?= $rgbRED ?>],
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });

    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?= $labels ?>],
            datasets: [{
                    data: [<?= $nbpa ?>],
                    label: "Total Production",
                    backgroundColor: "#3e95cd",
                    fill: false
                },
                {
                    data: [<?= $nbdealerships ?>],
                    label: "Dealers Production",
                    backgroundColor: "#FF0000",
                    fill: false
                }, {
                    data: [<?= $nbOffice ?>],
                    label: "Office Production",
                    backgroundColor: "#1FFA61",
                    fill: false
                },
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });



    var myBarChart = new Chart(ctx5, {
        type: 'bar',
        data: {
            labels: ["Cancellations"],
            datasets: [{
                    data: [<?= $totalExp ?>],
                    label: ["Expired", ],
                    backgroundColor: "red",
                    fill: false
                },
                {
                    data: [<?= $totalNLC ?>],
                    label: ["No Longer Client", ],
                    backgroundColor: "red",
                    fill: false
                },
                {
                    data: [<?= $totalUW ?>],
                    label: ["Underwriting", ],
                    backgroundColor: "red",
                    fill: false
                },
                {
                    data: [<?= $totalnp ?>],
                    label: ["Non-payment", ],
                    backgroundColor: "red",
                    fill: false
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });


//    var myLineChart = new Chart(ctx, {
//        type: 'line',
//        data: {
//            labels: [<?= $labels ?>],
//            datasets: [{
//                    data: [<?= $nbpa ?>],
//                    label: "Total Production",
//                    borderColor: "#3e95cd",
//                    fill: false
//                },
//                {
//                    data: [<?= $nbdealerships ?>],
//                    label: "Dealers Production",
//                    borderColor: "#FF0000",
//                    fill: false
//                }, {
//                    data: [<?= $nbOffice ?>],
//                    label: "Office Production",
//                    borderColor: "#1FFA61",
//                    fill: false
//                }
//
//            ]
//        },
//        options: {
//            title: {
//                display: true,
//                text: 'New Business'
//            }
//        }
//    });

    var data = {
        labels: [<?= $labelUsers ?>],
        datasets: [
            {
                label: "Office",
                backgroundColor: "blue",
                data: [<?= $labelTotal ?>]
            },
            {
                label: "Dealers",
                backgroundColor: "red",
                data: [<?= $labelTotalD ?>]
            }

        ]
    };

    var myBarChart = new Chart(ctxUsers, {
        type: 'bar',
        data: data,
        options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                        ticks: {
                            min: 0,
                        }
                    }]
            }
        }
    });



    var data2 = {
        labels: [<?= $labelAllUsers ?>],
        datasets: [
            {
                label: "Expired",
                backgroundColor: "blue",
                data: [<?= $label_totalExpiredByuser ?>]
            },
            {
                label: "NLC",
                backgroundColor: "red",
                data: [<?= $label_totalnlcByuser ?>]
            },
            {
                label: "UW",
                backgroundColor: "red",
                data: [<?= $label_totaluwByuser ?>]
            }
            ,
            {
                label: "Non-payment",
                backgroundColor: "red",
                data: [<?= $label_np_by_user ?>]
            }

        ]
    };

    var myBarChart = new Chart(ctx6, {
        type: 'horizontalBar',
        data: data2,
        options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                        ticks: {
                            min: 0,
                        }
                    }]
            }
        }
    });



    var data_users = {
        labels: [<?= $labelAllUsers ?>],
        datasets: [
            {
                label: "Actives",
                backgroundColor: "Green",
                data: [<?= $label_actives_by_user ?>]
            }
        ]
    };

    var myBarChart = new Chart(ActivesByUser, {
        type: 'horizontalBar',
        data: data_users,
        options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                        ticks: {
                            min: 0,
                        }
                    }]
            }
        }
    });

</script>