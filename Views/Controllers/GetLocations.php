<?php

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
include ("./conn.php");
$sql = 'SELECT * FROM `dealers` WHERE agency=101 and latitud!="" and dealers.long!=""';
$result = mysqli_query($mysqli, $sql);
$array = array();
$data = array();

$fecha1 = new DateTime();
$fecha1->modify('first day of this month');
$f1 = $fecha1->format('Y-m-d');

$fecha = new DateTime();
$fecha->modify('last day of this month');
$f2 = $fecha->format('Y-m-d');


$cont = 0;
while ($row = mysqli_fetch_assoc($result)) {
    $cont++;
    $lat = $row['latitud'];
    $lon = $row['long'];
    $name = $row['nombre'];
    $id = $row['id'];

    $NB = getNBDealer($id, $mysqli, $f1, $f2);
    $name.=" ".$NB;
    $array = array('lat' => $lat, 'lon' => $lon, 'title' => $name, 'ID' => $id, "NB" => $NB);
    array_push($data, $array);
}

function getNBDealer($idDealer, $mysqli, $f1, $f2) {
    $sql = "SELECT count(id) as nbd FROM `cutvone` where fecha BETWEEN '$f1' and '$f2' and (Dpayment=1 or Dpayment=3 or Dpayment=3 or Dpayment=5 or Dpayment=10 ) and erase!=1 and dealership=$idDealer";
    $result = mysqli_query($mysqli, $sql);
    $row = mysqli_fetch_assoc($result);
    return $row['nbd'];
}

$arr = array('data' => $data);
echo json_encode($arr);
