<?php
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id_User = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="datatables/jquery.dataTables.min.js"></script>
        <script src="datatables/dataTables.bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>


        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }
        </style>


    </head>

    <body >
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();

            });

        </script>
        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>


            <div >
                <? ?>
                <div class="col-md-12 col-lg-12">
                    <div class="panel panel-primary" >
                        <div class="panel-heading">Dealers Locations</div>
                        <div id="map" class="col-md-12" style="width: 100%; height: 700px;"></div>
                    </div>



                </div>




                <div class="col-md-12 col-lg-12">
                    <div class="panel panel-primary" >
                        <div class="panel-heading">Dealers Details</div>

                        <?
                        $idDealer = 0;
                        $nameDealer = "";

                        $fecha1 = new DateTime();
                        $fecha1->modify('first day of this month');
                        $f1 = $fecha1->format('m-d-Y');

                        $fecha = new DateTime();
                        $fecha->modify('last day of this month');
                        $f2 = $fecha->format('m-d-Y');
                        if (isset($_GET['SelectedD'])) {
                            $idDealer = $_GET['SelectedD'];
                            $sql = "SELECT nombre FROM `dealers` where id=$idDealer";
                            $result = mysql_query($sql);
                            $row = mysql_fetch_assoc($result);
                            $nameDealer = $row['nombre'];
                        }
                        ?>
                        <div class="row">
                            <h1 style="padding: 14px !important;"><?= $nameDealer ?></h1>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Date 1</span>
                                <input id="datepicker"  type="text" class="form-control" placeholder="325511" value="<?= $f1 ?>" />
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Date 2</span>
                                <input id="datepicker2"  type="text" class="form-control" placeholder="" value="<?= $f2 ?>" />
                            </div>
                            <div class="col-md-2 col-lg-2">

                                <button id="btn_submit" class="btn btn-success " style="margin-top: 32px;" onclick="GetReport()">Submit</button>
                            </div>
                        </div>
                        <div class="row" id="Cont"></div>
                    </div>



                </div>
            </div>

        </div>
        <style>
            .label {
                box-sizing:border-box;
                background: #05F24C;
                box-shadow: 2px 2px 4px #333;
                border:5px solid #346FF7;
                height: 20px;
                width: 20px;
                border-radius: 10px;
                -webkit-animation: pulse 1s ease 1s 3;
                -moz-animation: pulse 1s ease 1s 3;
                animation: pulse 1s ease 1s 3;
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <script type="text/javascript">

                                    function GetReport() {

                                        var id_dealer =<?= $idDealer ?>;
                                        var date1 = $("#datepicker").val();
                                        var date2 = $("#datepicker2").val();
                                        $.post("./Views/Controllers/GetReportDealer.php", {id_dealer, date1, date2}, function (data) {
                                             $("#Cont").empty();
                                             $("#Cont").html(data);
                                        });
                                    }
                                    $("#datepicker").datepicker({dateFormat: 'mm-dd-yy'});
                                    $("#datepicker2").datepicker({dateFormat: 'mm-dd-yy'});

        </script>


        <script type="text/javascript">

// Initialize and add the map
            function initMap() {
                // The location of Uluru


                var uluru = {lat: 32.2217407, lng: -110.9264832};
                var myLatLng = {lat: 32.2217407, lng: -110.9264832};
                var buylocostajo = {lat: 32.1777513, lng: -110.9763121};
                var labels = 'name Dealer';
                var labelIndex = 0;

                // The map, centered at Uluru
                map = new google.maps.Map(
                        document.getElementById('map'), {zoom: 13, center: uluru});
                // The marker, positioned at Uluru
//                var marker = new google.maps.Marker({
//                    position: myLatLng,
//                    map: map,
//                    labelOrigin:  new google.maps.Point(50,-15),
//                    label: {
//                        text: labels,
//                        color: "#eb3a44",
//                        fontSize: "16px",
//                        fontWeight: "bold"
//                    }
//                });
//
//                marker.setMap(map);




                var markerIcon = {
                    url: './img/buylowcostTree.png',
                    scaledSize: new google.maps.Size(80, 80),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(32, 35),
                    labelOrigin: new google.maps.Point(50, -15),
                };

                var markerLabel = 'Buy Low Cost';
                var marker = new google.maps.Marker({
                    position: buylocostajo,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: markerIcon,
                    label: {
                        text: markerLabel,
                        color: "#eb3a44",
                        fontSize: "15px",
                        fontWeight: "bold",

                    }
                });



                marker.addListener('click', function () {
                    alert("click marker");
                });

                var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                var icons = {
                    parking: {
                        icon: iconBase + 'parking_lot_maps.png'
                    },
                    library: {
                        icon: iconBase + 'library_maps.png'
                    },
                    info: {
                        icon: iconBase + 'info-i_maps.png'
                    }
                };



                $.post("./Views/Controllers/GetLocations.php", {}, function (data) {

                    var markers = [];
                    var marklabels = [];
                    var ids = [];

                    for (var cont = 0; cont < data.data.length; cont++) {
                        var lat = Number.parseFloat(data.data[cont].lat);
                        var lon = Number.parseFloat(data.data[cont].lon);
                        marklabels[cont] = data.data[cont].title;
                        var idDealer = data.data[cont].ID;
                        ids[cont] = data.data[cont].ID;

                        var location = {lat: lat, lng: lon};
                        var markerIcon = {
                            url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5MUiSlJ1Sf-AnkWSCkeAxIASQleJiMXjOdq-EGffNDnw9kvqENw',
                            scaledSize: new google.maps.Size(80, 80),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(32, 35),
                            labelOrigin: new google.maps.Point(50, -15),
                        };

                        var labelmark = data.data[cont].title;
                        marklabels = data.data[cont].title;

                        markers[cont] = new google.maps.Marker({
                            position: location,
                            map: map,
                            animation: google.maps.Animation.DROP,
                            icon: markerIcon,
                            label: {
                                text: labelmark,
                                color: "#eb3a44",
                                fontSize: "11px",
                                fontWeight: "bold",

                            }
                        });

                        markers[cont].index = idDealer; //
                        google.maps.event.addListener(markers[cont], 'click', function () {

                            window.location.href = "View_Map.php?SelectedD=" + this.index;

                        });
//                          markers[cont].addListener('click', function () {
//                           alert(marklabels[cont]);
//                          window.location.href = "View_Map.php?SelectedD="+ ids[cont];
//                        });
                    }

                });



            }
        </script>
        <!--Load the API from the specified URL
        * The async attribute allows the browser to render the page while the API loads
        * The key parameter will contain your own API key (which is not needed for this tutorial)
        * The callback parameter executes the initMap() function
        -->
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4XIWXEWQeTRZC3wunJMzELHYVb83D4VI&callback=initMap">
        </script>


    </body>
</html>
<?
?>
