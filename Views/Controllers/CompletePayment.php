<?php

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
date_default_timezone_set( "America/Phoenix" );

session_start();
include ("./conn.php");
require('../../fpdf.php');

$id_client = $_POST['id_client'];
$id_user = $_POST['id_user'];
$id_policy = $_POST['id_policy'];
if (isset($_POST['id_policy'])) {
    $id_policy = $_POST['id_policy'];
    if($_POST['IsPop']==1){
      updatePolicy(2,$id_policy,$mysqli);
    }
} else {
    
    $id_policy = 0;
     if($_POST['IsPop']==1){
        updatePolicy(1,$id_client,$mysqli);
    }else{
        
    }
}
$Fee = $_POST['Fee'];
$Total = $_POST['Total'];
$TypeTransaction = $_POST['TypeTransaction'];
$InsMp = $_POST['InsMp'];
$ADCMp = $_POST['ADCMp'];
$CashierSel = $_POST['CashierSel'];
$InsAmount = $_POST['InsAmount'];
$AdcAmount = $_POST['AdcAmount'];
$nameClient = $_POST['nameClient'];
$company = $_POST['company'];
$policyN = $_POST['policyN'];
$clietnInfo = getCliente($id_client, $mysqli);
$agencia = $clietnInfo['agencia'];
$agencia = $_POST['id_agency'];
$fecha = date("Y-m-d");
$time = date("H:i:s");


 $nameClient=str_replace('undefined', '', $nameClient);
$total = $InsAmount + $AdcAmount + $Fee;
$typeTS = "";

$sql;
if ($InsMp == 1) {
    $typeTS = "Cash";
} else if($InsMp==2){
    $typeTS = "Card";
}
$DeliveryOptions = $_POST['DeliveryOptions'];

$nota = "Payment For $:" . $total . " Type Transacction:" . $typeTS;
$pass = false;
$test="";
if ($id_policy == $id_client) {

    $id_policy = 0;
   
    
}
if ($id_policy > 0) {
    $pass = true;
    
}

$IsAendrose = $_POST['IsAendrose'];
$CocneptText = $_POST['CocneptText'];
$typeDo = 2;
if ($IsAendrose == "No") {
    $typeDo = 2;
} else if ($IsAendrose == "Yes") {
    $typeDo = 4;
}



if (isset($_POST['card'])) {
    if ($_POST['card'] != "") {
        $card = $_POST['card'];
        $cvv = $_POST['cvv'];
        $expfulldate = $_POST['expfulldate'];
        $route=$_POST['route'];
        $account=$_POST['account'];
        insertCard($card, $id_client, $cvv, $expfulldate,$route,$account, $mysqli);
    }
}
if ($DeliveryOptions == "No") {
    $total = $InsAmount + $ADCMp + $Fee;
    $sql = "INSERT INTO cutvone (company, nameclient , feecompany , companytype, feeoffice ,feeforaz ,typeaz,id_usuario,typeDO,fecha,agency,Dpayment,id_cliente,`horat`,`policynumber`, `id_cashier`,`id_policy`,`total`,`comment`)"
            . " VALUES ($company ,'$nameClient' , $InsAmount ,$InsMp,$Fee,$AdcAmount,$ADCMp,$id_user,$typeDo,'$fecha',$agencia,'$TypeTransaction',$id_client,'$time','$policyN', $CashierSel,$id_policy,$total,'$CocneptText')";
} else {

    $SellerName = $_POST['NameSeller'];
    $deliveryName = $_POST['NameDelivery'];
    $DealerId = $_POST['DeliveryCompany'];
    $AmountDealer = $_POST['AmonDealer'];

    $total = $InsAmount + $ADCMp + $Fee;

    if ($idPolicy == $id_client) {
        $id_policy = 0;
    }
    $sql = "INSERT INTO cutvone (company, nameclient , feecompany , companytype, feeoffice ,feeforaz ,typeaz,id_usuario,typeDO,fecha,agency,Dpayment,id_cliente,`horat`,`policynumber`, `id_cashier`,`id_policy`,`feedealer`,`dealership`,`nameentrega`,`namevendedor`,`total`,`comment`)"
            . " VALUES ($company ,'$nameClient' , $InsAmount ,$InsMp,$Fee,$AdcAmount,$ADCMp,$id_user,$typeDo,'$fecha',$agencia,'$TypeTransaction',$id_client,'$time','$policyN', $CashierSel,$id_policy,$AmountDealer,$DealerId,'$deliveryName','$SellerName',$total,'$CocneptText')";
}

mysqli_query($mysqli, $sql);


$ultimoID = mysqli_insert_id($mysqli);


$dateRes = "Error";
$url = "";
$pos = 0;
$val = 0;
$status = "error";
$namefile_rul;
if ($ultimoID > 0 && $pass == false) {
    $val = 1;
    $namefile = crearinvoice($ultimoID, $mysqli);
    $namefile_rul = $namefile;
    actualizaCliente($id_client, $company, $InsAmount, $AdcAmount, $mysqli);

    insertNote($id_user, $nota, $id_client, $mysqli);
     $url = "../../../MGTM/principal22.php?Ticket=1&id_client=$id_client&tik=$ultimoID";
    $dateRes = "Payment Success";
    $pos = 1;
    $status = "success";
}
if ($ultimoID > 0 && $pass == true) {
    $namefile = crearinvoice($ultimoID, $mysqli);
    $namefile_rul = $namefile;
    actualizaPoliza($id_client, $id_policy, $mysqli);
    $val = 1;
    $url = "../../../MGTM/principal22.php?Ticket=1&id_client=$id_client&tik=$ultimoID";
    $dateRes = "Payment Success";
    $pos = 2;
    $status = "success";
    insertNote($id_user, $nota, $id_client, $mysqli);
}

if ($val == 1) {
    
}
$arr = array('message' => $dateRes, 'url' => $url, "log" => $pos, "status" => $status, "filepdf" => $namefile_rul, "tik" => $ultimoID,"id_client"=>$id_client ,"test"=>$test);
echo json_encode($arr);

function actualizaPoliza($idc, $idPoliza, $mysqli) {
    $sql = "SELECT * FROM `PoliciesForOldMGTM` where Id=$idPoliza";
    $result = mysqli_query($mysqli, $sql);
    $status_client = "";
    $fecha_mod_satus = "";
    while ($r = mysqli_fetch_assoc($result)) {
        $status_client = $r['status_policy'];
        $fecha_mod_satus = $r['fech_mod_status'];
    }
    if ($status_client == 1) {
        $status_client = 4;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 6) {
        $status_client = 5;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 7) {
        $status_client = 8;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 2) {
        $status_client = 5;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 3) {
        $status_client = 5;
        $fecha_mod_satus = date("Y-m-d");
    }

    if ($status_client == 9) {
        $status_client = 8;
        $fecha_mod_satus = date("Y-m-d");
    }
    $sqlUpdate = "UPDATE `PoliciesForOldMGTM` SET  status_policy=$status_client,fech_mod_status='$fecha_mod_satus' WHERE Id=$idPoliza";
    mysqli_query($mysqli, $sqlUpdate);

    actualiza_cancellation_table($idc, $status_client, $mysqli);
}

function actualizaCliente($idc, $company, $pagomenusl, $pagoadc, $mysqli) {

    $sql = "";

    $sqlq = "SELECT * FROM `UserManagement` where id=$idc";
    $result = mysqli_query($mysqli, $sqlq);
    $status_client = "";
    $fecha_mod_satus = "";
    while ($r = mysqli_fetch_assoc($result)) {
        $status_client = $r['status_client'];
        $fecha_mod_satus = $r['fecha_mod_satus'];
    }
    if ($status_client == 1) {
        $status_client = 4;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 6) {
        $status_client = 5;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 7) {
        $status_client = 8;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 2) {
        $status_client = 5;
        $fecha_mod_satus = date("Y-m-d");
    }
    if ($status_client == 3) {
        $status_client = 5;
        $fecha_mod_satus = date("Y-m-d");
    }

    if ($status_client == 9) {
        $status_client = 8;
        $fecha_mod_satus = date("Y-m-d");
    }



    $sql = "UPDATE UserManagement set compania='$company' , pagomensual='$pagomenusl' , pagomensualdrivers='$pagoadc',status_client='$status_client',fecha_mod_satus='$fecha_mod_satus',status=1 where id=" . $idc;
    mysqli_query($mysqli, $sql);
    actualiza_cancellation_table($idc, $status_client, $mysqli);
}

function actualiza_cancellation_table($customerid, $status, $mysqli) {
    // verficiar
    $fecha1 = new DateTime();
    $fecha1->modify('first day of this month');
    $f1 = $fecha1->format('Y-m-d');

    $fecha = new DateTime();
    $fecha->modify('last day of this month');
    $f2 = $fecha->format('Y-m-d');


    $sql2 = "SELECT * FROM `Cancellations` where customer_id=$customerid and date BETWEEN '$f1' and '$f2' and status_policy=1 order by id DESC LIMIT 1";
    //echo $sql2;
    $r2 = mysqli_query($mysqli, $sql2);
    $num = mysqli_num_rows($r2);
    if ($num > 0) {
        $sql = "UPDATE `Cancellations` SET  status_policy='$status' WHERE customer_id=$customerid";
        $r = mysqli_query($mysqli, $sql);
    }
}

function crearinvoice($id, $mysqli) {

    $pdf = new FPDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetTextColor(10, 10, 10);
    $sql = "SELECT `cv`.*,`us`.`user` , `us`.`name` as nameuser  FROM `cutvone` AS `cv` LEFT JOIN `users` AS `us` ON `cv`.`id_usuario` = `us`.`id` where `cv`.`id`=" . $id;
    $result = mysqli_query($mysqli, $sql);

    $company = "";
    $nameclient = "";
    $fecha = "";
    $time = "";
    $total = "";
    $typepago = "";
    $user = "";
    $id_cliente = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $company = RegresanombreC2($row['company'], $mysqli);
        $user = strtoupper($row['user']);
        $id_cliente = $row['id_cliente'];
        $nameclient = $row['nameclient'];
        $fecha = $row['fecha'];
        $time = $row['horat'];

        if ($_SESSION['agencia'] == 105) {
            $user = strtoupper($row['nameuser']);
        }
        $total = $row['feecompany'] + $row['feedealer'] + $row['feeforaz'] + $row['feeoffice'];
        if ($row['companytype'] == 1) {
            $typepago = "Cash";
        } else {
            $typepago = "Card";
        }
    }

    $lista = split("-", $fecha);
    $fecha = $lista[1] . "-" . $lista[2] . "-" . $lista[0];
    $nameAgency = regresaNa2($mysqli);
    $telAgenci = regresaTelA2($mysqli);
    $emal = Regesamail($mysqli);
    $fax = regresaFaxA2($mysqli);
    $dir = regresaDir($mysqli);
    $city = regreesacity($mysqli);
    $estado = regrestate($mysqli);
    $zip = regresazip($mysqli);
    $pdf->Cell(40, 4, $nameAgency, 0, 1, ' L');
    $pdf->Ln();
    $pdf->Cell(40, 4, $dir, 0, 1, ' L ');
    $pdf->Ln();
    $pdf->Cell(40, 4, $city . " , " . $estado . " , " . $zip, 0, 1, ' L ');
    $pdf->Ln(10);

    $pdf->Cell(90, 5, "Phone:  " . $telAgenci, 0, 0, 'C');
    $pdf->Cell(90, 5, "Fax:   " . $fax, 0, 0, 'C');
    $pdf->Ln(15);
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetTextColor(10, 10, 10);
    $pdf->Cell(70, 5, "Company:" . $company, 0, 0, 'L');
    $pdf->Ln();
    $pdf->Cell(70, 5, $nameclient, 0, 0, 'L');
    $pdf->Cell(70, 5, "Agency #" . $_SESSION['agencia'], 0, 0, 'C');
    $pdf->Ln(15);


    $pdf->Cell(190, 5, "PAYMENT RECEIPT", 1, 0, 'C');
    $pdf->Ln();
    $pdf->Cell(30, 5, "Date", 1, 0, 'C');
    $pdf->Cell(60, 5, $fecha . "Time:" . $time, 1, 0, 'L');
    $pdf->Cell(30, 5, "Amount", 1, 0, 'C');
    $pdf->Cell(70, 5, "$" . $total, 1, 0, 'L');
    $pdf->Ln();
    $pdf->Cell(30, 5, "Transaction", 1, 0, 'C');
    $pdf->Cell(60, 5, "Payment", 1, 0, 'L');
    $pdf->Cell(30, 5, "", 1, 0, 'C');
    $pdf->Cell(70, 5, "", 1, 0, 'L');
    $pdf->Ln();
    $pdf->Cell(30, 5, "Method", 1, 0, 'C');
    $pdf->Cell(60, 5, $typepago, 1, 0, 'L');
    $pdf->Cell(30, 5, "Total", 1, 0, 'C');
    $pdf->Cell(70, 5, "$" . $total, 1, 0, 'L');
    $pdf->Ln(20);

    $pdf->Cell(60, 9, "Client's Signature:", 0, 0, 'C');
    $pdf->Cell(60, 9, " ", 0, 0, 'C');
    $pdf->Cell(60, 9, "Agent's Signature:", 0, 0, 'C');
    $pdf->ln(10);
    $pdf->Cell(60, 9, "______________________", 0, 0, 'C');
    $pdf->Cell(60, 9, " ", 0, 0, 'C');
    $pdf->Cell(60, 9, $user, 'B', 0, 'C');
    $pdf->Ln(20);

    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetTextColor(10, 10, 10);
    if ($emal != "") {
        $pdf->Cell(40, 4, "                                                                                                                                  " . $emal . "        ", 0, 1, 'C');
    }



    /* CUPON: */
    $pdf->Ln(20);
    //$id_cliente == 3664
    if (true) {
        //$pdf->setFillColor(97,159,197);
        $pdf->setFillColor(184, 228, 255);
        $pdf->SetFont('Arial', 'B', 12);
        //$pdf->SetTextColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->Rect(0, 160, 250, 100, 'F');
        $file = "../../img/logoadc.png";
        $pdf->Image($file, 20, 170, 46, 0, 'png');
        $file2 = "../../img/logobluywhite.png";
        $pdf->Image($file2, 135, 170, 60, 0, 'png');
        $pdf->Ln(60);
//        $pdf->Cell(0, 9, "For all the Buy Low Cost Insurance customers Premier Auto Center is offering", 0, 0, 'C');
//        $pdf->Ln();
//        $pdf->Cell(0, 9, "$500 off towards a Nicer Newer Car!!!", 0, 0, 'C');
    } /* else {
      //$pdf->setFillColor(97,159,197);
      $pdf->setFillColor(184, 228, 255);
      $pdf->SetFont('Arial', 'B', 12);
      //$pdf->SetTextColor(255, 255, 255);
      $pdf->SetTextColor(0, 0, 0);

      $pdf->Rect(0, 160, 250, 100, 'F');
      $pdf->Image('../../logo-ftp.png', 60, 170, 90, 0, 'png');
      $pdf->Ln(60);
      $pdf->Cell(0, 9, "For all the Buy Low Cost Insurance customers Premier Auto Center is offering", 0, 0, 'C');
      $pdf->Ln();
      $pdf->Cell(0, 9, "$500 off towards a Nicer Newer Car!!!", 0, 0, 'C');
      } */
    $time = date("H:i:s");
    $date = date("Y-m-d");

    $nameTicket = $id . $date . $time . "receipt.PDF";
    $tmp_file2 = "../../../recibos/" . $id . $date . $time . "receipt.PDF";
    //$tmp_file="".$id.$date.$time."Invoicern2222.PDF";
    $pdf->Output($tmp_file2, 'F');

    $sqlupdate = "update cutvone set recibo='$tmp_file2', total='$total' where id=" . $id;
    mysqli_query($mysqli, $sqlupdate);
    return $nameTicket;
}

function Regesamail($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['website'];
    }
}

function resgresanombrecompania22($val) {
    global $mysqli;
    $sql = "SELECT * FROM users where id=" . $val;

    $res = mysqli_query($mysqli, $sql);
    $vr = "w";
    while ($re = mysqli_fetch_assoc($res)) {
        $vr = $re['user'];
    }
    return $vr;
}

function RegresanombreC2($val, $mysqli) {

    $sql = "SELECT * FROM companyes where id=" . $val;

    $res = mysqli_query($mysqli, $sql);
    $vr = "w";
    while ($re = mysqli_fetch_assoc($res)) {
        $vr = $re['nombre'];
    }
    return $vr;
}

function regresaNa2($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['nombre'];
    }
}

function regresaTelA2($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['tel'];
    }
}

function regresaFaxA2($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['fax'];
    }
}

function regresaDir($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['direccion'];
    }
}

function regreesacity($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['ciudad'];
    }
}

function regrestate($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['estado'];
    }
}

function regresazip($mysqli) {

    $sql = "SELECT * FROM agency where id=" . $_SESSION['agencia'];
    $result = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        return $row['zip'];
    }
}

function regresanombreuser($val, $mysqli) {

    $sql = "SELECT * FROM users where id=" . $val;

    $res = mysqli_query($mysqli, $sql);
    $vr = "";
    while ($re = mysqli_fetch_assoc($res)) {
        $vr = $re['user'];
    }
    return $vr;
}

function getCliente($idc, $mysqli) {
    $sqlq = "SELECT * FROM `UserManagement` where id=$idc";
    $result = mysqli_query($mysqli, $sqlq);
    $r = mysqli_fetch_assoc($result);
    return $r;
}

function insertNote($idus, $nota, $id_clie, $mysqli) {
    $fecha = date("Y-m-d");
    $sql = "INSERT INTO `notas_mgtm`(`id_usuario`, `fecha`, `nota`, `id_cliente`) VALUES ($idus,'$fecha','$nota',$id_clie)";
  //  $res = mysqli_query($mysqli, $sql);
}

function insertCard($card, $idClient, $cvv, $expDate, $route,$account,$mysqli) {
    $sql = "SELECT * FROM `BankInformation` where id_client=$idClient";
    $result = mysqli_query($mysqli, $sql);
    $rows = mysqli_num_rows($result);
    if ($rows > 0) {
        $sqlUpdate = "UPDATE `BankInformation` SET `cardNumber`='$card',`expDate`='$expDate',`cvv`='$cvv',`routenumber`='$route',`accountNumber`='$account'   WHERE id_client=$idClient";
        mysqli_query($mysqli, $sqlUpdate);
    } else {
        $sqlInsert = "INSERT INTO `BankInformation`(`cardNumber`, `expDate`, `cvv`, `id_client`) "
                . "VALUES ('$card','$expDate','$cvv',$idClient)";
        mysqli_query($mysqli, $sqlInsert);
    }
}

function  updatePolicy($value,$id,$mysqli){
    $sql="";
    if($value==1){
        $sql="UPDATE `UserManagement` SET   `proofop`=-2   WHERE id=".$id;
    }else if($value==2){
        $sql="UPDATE `PoliciesForOldMGTM` SET `proofop`=-2  WHERE id=".$id;
    }
   
       mysqli_query($mysqli, $sql);
}

?>