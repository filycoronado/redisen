<?php

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json; charset=utf-8');
include ("./conn.php");

$id = $_POST['id_client'];
$sql = "DELETE FROM UserManagement
WHERE id=$id";
mysqli_query($mysqli, $sql);
$status = "";
$message = "";
$count = mysqli_affected_rows($mysqli);
if ($count > 0) {
    $status = "success";
    $message = "Erase Success";
} else {
    $status = "error";
    $message = "Erase Error";
}
$array = array('status' => $status, 'message' => $message);
echo json_encode($array);
