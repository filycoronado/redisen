<?php
include ("./conn.php");
$f1 = $_POST['date1']; //mm-dd-yyyy
$f2 = $_POST['date2']; //mm-dd-yyyy



$slq = "SELECT id,(SELECT nombre FROM UserManagement where  id=customer)as customer_name ,(SELECT user FROM users where id=agent) as agente , date_visit, time_visit "
        . "FROM `visit_profiles` where  date_visit BETWEEN '$f1' and '$f2'";

$result = mysqli_query($mysqli, $slq);
?>
<div class="col-md-10  col-md-offset-1">
    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Customer</th>
                <th scope="col">Agent</th>
                <th scope="col">Date</th>
                <th scope="col">Time</th>
            </tr>
        </thead>
        <tbody>
            <?
            $cont = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $date = date("m-d-Y", strtotime($row['date_visit']));
                $time = date("H:i:s a", strtotime($row['time_visit']));
                ?>
                <tr>
                    <th scope="row"><?= ++$cont ?></th>
                    <td><?= $row['customer_name']; ?></td>
                    <td><?= $row['agente']; ?></td>
                    <td><?= $date ?></td>
                    <td><?= $time ?></td>
                </tr>
            <? } ?>
        </tbody>
    </table>
</div>