<?php

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
include ("./conn.php");

$id_client = $_POST['id_client'];
$policyN = $_POST['policyN'];
$MembershipN = $_POST['MembershipN'];
$SeelctCompany = $_POST['SeelctCompany'];
$SeelctType = $_POST['SeelctType'];
$status_client = $_POST['status_client'];
$InsAmount = $_POST['InsAmount'];
$ADCAmount = $_POST['ADCAmount'];
$id_user = $_POST['id_user'];
$dayForPending = $_POST['dayForPending'];
$id_poliza = $_POST['id_poliza'];
$cancel_date=$_POST['cancel_date'];
$expiration_date=$_POST['expiration_date'];

$cancel_date=date("Y-m-d", strtotime($cancel_date));
$expiration_date=date("Y-m-d", strtotime($expiration_date));
$sql = "";
$staus = "";
$message = "error";
$nameClientSQl = "SELECT nombre,apellido FROM `UserManagement` where id=$id_client";
$resultClient = mysqli_query($mysqli, $nameClientSQl);
$rowClient = mysqli_fetch_assoc($resultClient);
$nameClient = $rowClient['nombre'] . " " . $rowClient['apellido'];
$cancelDate;
$expdate;

if ($id_poliza == 0) {

    $sql = "SELECT * FROM `UserManagement` where id=$id_client";
    $result = mysqli_query($mysqli, $sql);
    $res = mysqli_fetch_assoc($result);
    $Date = date("Y-m-d");
    $fechModStatus = "";
    $lastStatus = $res['status_client'];
    $cancelDate = strtotime('+' . $dayForPending . 'day', strtotime($Date));
    $cancelDate = date("Y-m-d", $cancelDate);
    // $cancelDate = date('Y-m-d', strtotime($Date . ' + ' . $dayForPending . ' days'));

    if ($lastStatus == $status_client) {
        $fechModStatus = $res['fecha_mod_satus'];
    } else {
        $fechModStatus = $Date;
    }
    if ($status_client == 1) {
        $sqlDaysP = ", CountPendingDays=$dayForPending ,DateOfPrending='$cancelDate' ,DateModifyDaysP='$Date'";
    } else {
        $sqlDaysP = ", CountPendingDays=0";
    }

    $sql = "UPDATE UserManagement set  compania='$SeelctCompany' , policynumber='$policyN' , ADC='$MembershipN' ,status_client='$status_client', policy_type='$SeelctType', fecha_mod_satus='$fechModStatus' , DateOfPrending='$cancel_date',date_expiration='$expiration_date'  where id=" . $id_client;

    mysqli_query($mysqli, $sql);
    $r = mysqli_affected_rows($mysqli);
    if ($r > 0) {

        if ($status_client == 1) {
            InsertCancellationRow($id_client, $nameClient, $status_client, $id_user, $policyN, 0, $mysqli, $cancelDate);
        } else {
            InsertCancellationRowNopending($id_client, $nameClient, $status_client, $id_user, $policyN, $id_poliza, $mysqli);
        }
        $message = "Updated Success";
        $staus = "success";
    } else {
        $message = "Error Operation";
        $staus = "error";
    }
} else {

    $sql = "SELECT * FROM `PoliciesForOldMGTM` where Id=$id_poliza";
    $result = mysqli_query($mysqli, $sql);
    $res = mysqli_fetch_assoc($result);
    $Date = date("Y-m-d");
    $fechModStatus = "";
    $lastStatus = $res['status_policy'];
    $cancelDate = strtotime('+' . $dayForPending . 'day', strtotime($Date));
    $cancelDate = date("Y-m-d", $cancelDate);
    //   $cancelDate = date('Y-m-d', strtotime($Date . ' + ' . $dayForPending . ' days'));

    if ($lastStatus == $status_client) {
        $fechModStatus = $res['fech_mod_status'];
    } else {
        $fechModStatus = $Date;
    }
    if ($status_client == 1) {
        $sqlDaysP = ", CountPendingDays=$dayForPending ,CancelForPending='$cancelDate' ,DateModifyDaysP='$Date'";
    } else {
        $sqlDaysP = ", CountPendingDays=0";
    }

    $sql = "UPDATE `PoliciesForOldMGTM` SET `company_id`='$SeelctCompany',`pago_mensual`='$InsAmount',`policy_type`='$SeelctType',`policynumber`='$policyN',`adc_number`='$MembershipN',`status_policy`='$status_client',`fech_mod_status`='$fechModStatus',`pago_menusal_drviers`='$ADCAmount' ,CancelForPending='$cancel_date',date_expiration='$expiration_date' WHERE Id=$id_poliza";
    mysqli_query($mysqli, $sql);
    $r = mysqli_affected_rows($mysqli);
    if ($r > 0) {
        if ($status_client == 1) {
            InsertCancellationRow($id_client, $nameClient, $status_client, $id_user, $policyN, $id_poliza, $mysqli, $cancelDate);
        } else {
            InsertCancellationRowNopending($id_client, $nameClient, $status_client, $id_user, $policyN, $id_poliza, $mysqli);
        }
        $message = "Success";
        $staus = "success";
    } else {
        $message = "Error Operation";
        $staus = "error";
    }
}

function InsertCancellationRow($id_cllient, $nameClient, $statusPolicy, $id_user, $policynumber, $policyid, $mysqli, $canceldate) {
    /* <option value="2" <?php if ($status == 2) { ?>selected<?}?>>CANCELLED NON-PAYMENT</option>
      <option value="3"<?php if ($status == 3) { ?>selected<?}?>>CANCELLED UW</option>
      <option value="4"<?php if ($status == 4) { ?>selected<?}?>>RETAINED</option>
      <option value="5"<?php if ($status == 5) { ?>selected<?}?>>REINSTATED</option>
      <option value="6"<?php if ($status == 6) { ?>selected<?}?>>CANCELLED SERVICE</option>
      <option value="7"<?php if ($status == 7) { ?>selected<?}?>>EXPIRED</option> */
    $today = date("Y-m-d");
    $sql = "INSERT INTO `Cancellations`(`customer_id`, `customer_name`, `date`, `id_user`, `status_policy`, `policyNumber`, `policyid` ,`canceldate`) "
            . "VALUES ($id_cllient,'$nameClient','$today',$id_user,$statusPolicy,'$policynumber',$policyid,'$canceldate')";
    $result = mysqli_query($mysqli, $sql);
}

function InsertCancellationRowNopending($id_cllient, $nameClient, $statusPolicy, $id_user, $policynumber, $policyid, $mysqli) {
    $today = date("Y-m-d");
    $sql = "INSERT INTO `Cancellations`(`customer_id`, `customer_name`, `date`, `id_user`, `status_policy`, `policyNumber`, `policyid`) "
            . "VALUES ($id_cllient,'$nameClient','$today',$id_user,$statusPolicy,'$policynumber',$policyid)";
    $result = mysqli_query($mysqli, $sql);
}

$arr = array('message' => $message, 'status' => $staus, 'cancelDAte' => $sql);
echo json_encode($arr);
