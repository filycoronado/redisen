<?php

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
include ("./conn.php");
date_default_timezone_set("America/Phoenix");
$value = $_POST['value'];
$id_user = $_POST['id_user'];
$agency = $_POST['agency'];


$responseSql;
$today = date("Y-m-d");
$todayTime = date("Y-m-d H:i:s");
$day = date("l");
$sql = "SELECT * FROM `registroclock` where iduser=$id_user and date='$today' order by id desc";
$result = mysqli_query($mysqli, $sql);
$num = mysqli_num_rows($result);
$msj;
$status;
if ($num > 0) {
    $row = mysqli_fetch_assoc($result);
    $id_row = $row['id'];
    /* ¨verificas siguiente movimiento */
    if ($row['hora'] != null && is_null($row['horaout'])) {

        $sql = "UPDATE `registroclock` SET `typereg`='$value',`horaout`='$todayTime' WHERE id=$id_row";
        $result = mysqli_query($mysqli, $sql);
        $count = mysqli_affected_rows($mysqli);
        if ($count > 0) {
            RegresaDiff($mysqli, $id_row);
            updateCashier($id_user, 0, $mysqli);
            $status = "success";
            $msj = "Check Success";
        } else {
            $status = "error";
            $msj = "error operation";
        }
    } else if ($row['horaout'] != null) {
        $sql = "INSERT INTO `registroclock`(`iduser`, `date`, `dia`, `typereg`, `agencia`,  `hora`)"
                . " VALUES ($id_user,'$today','$day',$value,$agency,'$todayTime')";
        $result = mysqli_query($mysqli, $sql);
        $lasid = mysqli_insert_id($mysqli);

        if ($lasid > 0) {
            updateCashier($id_user, 1, $mysqli);
            $status = "success";
            $msj = "Check Success";
        } else {
            $status = "error";
            $msj = "error operation";
        }
    }
} else {


    $sql = "INSERT INTO `registroclock`(`iduser`, `date`, `dia`, `typereg`, `agencia`,  `hora`)"
            . " VALUES ($id_user,'$today','$day',$value,$agency,'$todayTime')";
    $result = mysqli_query($mysqli, $sql);
    $lasid = mysqli_insert_id($mysqli);

    if ($lasid > 0) {
        updateCashier($id_user, 1, $mysqli);
        $status = "success";
        $msj = "Check Success";
    } else {
        $status = "error";
        $msj = "error operation";
    }
    //insert new row
}

$arr = array('message' => $msj, 'status' => $status);

echo json_encode($arr);

function RegresaDiff($mysqli, $id) {
    $sql1 = "SELECT TIMESTAMPDIFF(MINUTE , hora,horaout)/60 As timework FROM registroclock WHERE id=$id";
    $result = mysqli_query($mysqli, $sql1);
    while ($row = mysqli_fetch_assoc($result)) {
        $sql = "UPDATE registroclock set diff='" . $row['timework'] . "'  where id=" . $id;

        mysqli_query($mysqli, $sql);
    }
}

function updateCashier($id_user, $move, $mysqli) {
    $sql = "UPDATE `users` SET  `isloggedClock`='$move' WHERE id=$id_user";
    $result = mysqli_query($mysqli, $sql);
}
