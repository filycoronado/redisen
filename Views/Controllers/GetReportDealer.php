<?php
include ("./conn.php");
$id_dealer = $_POST['id_dealer'];
$date1 = $_POST['date1'];
$date2 = $_POST['date2'];
$loc = $_POST['loc'];

$strDealer = "";
if ($id_dealer != 0) {
    $strDealer = " and CUT.dealership=$id_dealer";
}
$strcloc="";
if($loc=="0"){
    $strcloc="";
}else if($loc!="0"){
    $strcloc="and D.location='".$loc."'";
}

$arr1 = split("-", $date1);
$date1 = $arr1[2] . "-" . $arr1[0] . "-" . $arr1[1];

$arr2 = split("-", $date2);
$date2 = $arr2[2] . "-" . $arr2[0] . "-" . $arr2[1];

$sqlSum = "SELECT  Count(CUT.id) as totalT, SUM(CUT.total) as total FROM `cutvone` AS CUT join dealers AS D on D.id=CUT.dealership "
        . " where  CUT.fecha BETWEEN '$date1' and  '$date2' $strDealer  and CUT.erase!=1 ";

$resultSum = mysqli_query($mysqli, $sqlSum);

$sumaNorth = "SELECT  Count(CUT.id) as totalT, SUM(CUT.total) as total FROM `cutvone` AS CUT join dealers AS D on D.id=CUT.dealership "
        . " where  CUT.fecha BETWEEN '$date1' and  '$date2' $strDealer  and CUT.erase!=1 and D.location='NORTH'";

$sumaSouth = "SELECT  Count(CUT.id) as totalT, SUM(CUT.total) as total FROM `cutvone` AS CUT join dealers AS D on D.id=CUT.dealership "
        . " where  CUT.fecha BETWEEN '$date1' and  '$date2' $strDealer  and CUT.erase!=1 and D.location='SOUTH'";

$sumaEast = "SELECT  Count(CUT.id) as totalT, SUM(CUT.total) as total FROM `cutvone` AS CUT join dealers AS D on D.id=CUT.dealership "
        . " where  CUT.fecha BETWEEN '$date1' and  '$date2' $strDealer  and CUT.erase!=1 and D.location='EAST'";

$sumaNorth = mysqli_query($mysqli, $sumaNorth);
$sumaSouth = mysqli_query($mysqli, $sumaSouth);
$sumaEast = mysqli_query($mysqli, $sumaEast);

$tnorth = mysqli_fetch_assoc($sumaNorth);
$tsouth = mysqli_fetch_assoc($sumaSouth);
$teast = mysqli_fetch_assoc($sumaEast);

$rsum = mysqli_fetch_assoc($resultSum);
$sql = "SELECT CUT.nameclient as namecustomer, CUT.fecha as fecha , CUT.total as total, D.nombre as dealer  ,D.location as location, CUT.namevendedor as vendedor  FROM `cutvone` AS CUT join dealers AS D on D.id=CUT.dealership "
        . " where  CUT.fecha BETWEEN '$date1' and  '$date2' $strDealer and CUT.erase!=1 $strcloc";

?>

<style>
    body{
        background-color:#f2f2f2;
    }
    .table{
        background-color:#fff;
        box-shadow:0px 2px 2px #aaa;
        margin-top:50px;
    }

</style>

<div class="row print_row">
    <div class="col-md-2">
        <button class="btn btn-primary" onclick="DoPrint();">Print</button>
    </div>
</div>
<div class="col-md-6">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Transactions: <?= $rsum['totalT']; ?></th>
                <th>Grand Total: <?= $rsum['total']; ?></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th>Customer Name</th>
                <th>Dealer</th>
                <th>Seller</th>
                <th>Date</th>
                <th>Location</th>
                <th>Total</th>
            <tr>
        </thead>

        <?
        $result = mysqli_query($mysqli, $sql);
        while ($row = mysqli_fetch_assoc($result)) {

            $date = date('m-d-Y', strtotime($row['fecha']));
            ?>
            <tr>
                <td><?= strtoupper($row['namecustomer']) ?></td>
                <td><?= strtoupper($row['dealer']) ?></td>
                <td><?= strtoupper($row['vendedor']) ?></td>
                <td><?= $date ?></td>
                <td><?= $row['location'] ?></td>
                <td><?= $row['total'] ?></td>
            </tr>
        <? } ?>
    </table>
</div>
<div class="col-md-6">

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Transactions: <?= $rsum['totalT']; ?></th>
                <th>Grand Total: <?= $rsum['total']; ?></th>
                <th></th>


            </tr>
            <tr>
                <th>NORTH</th>
                <th>SOUTH</th>
                <th>EAST</th>

            <tr>
        </thead>


        <tr>
            <td>NB:<?= $tnorth['totalT'] ?></td>
            <td>NB:<?= $tsouth['totalT'] ?></td>
            <td>NB:<?= $teast['totalT'] ?></td>

        </tr>
        <tr>
            <td>$<?= $tnorth['total'] ?></td>
            <td>$<?= $tsouth['total'] ?></td>
            <td>$<?= $teast['total'] ?></td>

        </tr>

    </table>
</div>