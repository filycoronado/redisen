<?php
include ("./../Views/Controllers/Utils.php");
$users = array();

$sql = "SELECT * FROM `users` where (agencia=101 or agencia=102 or agencia=103 or  agencia=104 or agencia=105) AND (nivel=1 or nivel=2 or nivel=5) and habilitado =1";
$result = mysqli_query($mysqli, $sql);
$totalPuser = 0;
$Aux = 0;
$grandTotal = 0;
while ($r = mysqli_fetch_assoc($result)) {
    $arraPerUser = array();
    $totalPin2 = getPIN($mysqli,$r['id'], $f1, $f2);
    $totaleft2 = getDPeft($mysqli,$r['id'], $f1, $f2);
    $totaldp2 = getDP($mysqli,$r['id'], $f1, $f2);
    $totalPuser += $totalPin2 + $totaleft2 + $totaldp2;
    $grandTotal += $totalPuser;




    $perocentajeDP2 = (($totaleft2 * 100) / $totalPuser);
    $porcentajeNoEFTDP2 = (($totaldp2 * 100) / $totalPuser);
    $porcentajePIN2 = (($totalPin2 * 100) / $totalPuser);

    $perocentajeDP2 = number_format((float) $perocentajeDP2, 2, '.', '');
    $porcentajeNoEFTDP2 = number_format((float) $porcentajeNoEFTDP2, 2, '.', '');
    $porcentajePIN2 = number_format((float) $porcentajePIN2, 2, '.', '');

    array_push($arraPerUser, $totalPuser, $r['user'], $perocentajeDP2, $porcentajeNoEFTDP2, $porcentajePIN2, $r['id']);
    array_push($users, $arraPerUser);

    $totalPuser = 0;
    $perocentajeDP2 = 0;
    $porcentajeNoEFTDP2 = 0;
    $porcentajePIN2 = 0;
    $perocentajeDP2 = 0;
    $porcentajeNoEFTDP2 = 0;
    $porcentajePIN2 = 0;
    $arraPerUser = null;
}

foreach ($users as $key => $row) {
    $aux[$key] = $row[0];
}

array_multisort($aux, SORT_DESC, $users);
$cont = 0;

foreach ($users as $key => $row) {
    $cont++;
}
?>
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3>New Business , Per Users , Grand Total :<?= $grandTotalNB ?></h3>
            </div>
            <div class="panel-body">
                <canvas id="myChart" ></canvas>
            </div>
        </div>
    </div>
</div>
<?
?>
<script>
    var ctx = document.getElementById('myChart');
    data = {
        datasets: [{
                data: [10, 20, 30]
            }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Red',
            'Yellow',
            'Blue'
        ]
    };
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: options
    });
</script>
