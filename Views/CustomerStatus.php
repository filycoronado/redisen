<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<?php
session_start();
$nivelLoge = 0;
$iduserloig = 0;
if (isset($_SESSION['nivel'])) {
    $nivelLoge = $_SESSION['nivel'];
    $iduserloig = $_SESSION['id'];
} else {
    
}
//include("../inc/dbconnection.php");
$idClient = $_GET['id'];

$sql = "SELECT aplication,phoneNumber,registration,pictures,proofop,adcsinged,driverLicense FROM `UserManagement` where id=$idClient";
$result = mysqli_query($mysqli, $sql);
$r = mysqli_fetch_assoc($result);

$caslapication = "danger";
$caslphonenumber = "danger";
$caslregustation = "danger";
$caslpictures = "danger";
$caslproofodo = "danger";
$casladcsinged = "danger";
$casldriverLicense = "danger";

$Isaplication = "";
$isPhoneNumber = "";
$isregistration = "";
$ispictures = "";
$ispop = "";
$isadcSinged = "";
$isdriverlicense = "";
if ($r['aplication'] == 1) {
    $Isaplication = "checked";
    $caslapication = "success";
} else if ($r['aplication'] == -1) {
    $Isaplication = "checked";
    $caslapication = "info";
}

if ($r['phoneNumber'] == 1) {
    $caslphonenumber = "success";
    $isPhoneNumber = "checked";
} else if ($r['phoneNumber'] == -1) {
    $caslphonenumber = "info";
    $isPhoneNumber = "checked";
}

if ($r['registration'] == 1) {
    $caslregustation = "success";
    $isregistration = "checked";
} else if ($r['registration'] == -1) {
    $caslregustation = "info";
    $isregistration = "checked";
}

if ($r['pictures'] == 1) {
    $caslpictures = "success";
    $ispictures = "checked";
} else if ($r['pictures'] == -1) {
    $caslpictures = "info";
    $ispictures = "checked";
}

if ($r['proofop'] == 1) {
    $caslproofodo = "success";
    $ispop = "checked";
} else if ($r['proofop'] == -1) {
    $caslproofodo = "info";
    $ispop = "checked";
}

if ($r['adcsinged'] == 1) {
    $casladcsinged = "success";
    $isadcSinged = "checked";
} else if ($r['adcsinged'] == -1) {
    $casladcsinged = "info";
    $isadcSinged = "checked";
}
if ($r['driverLicense'] == 1) {
    $casldriverLicense = "success";
    $isdriverlicense = "checked";
} else if ($r['driverLicense'] == -1) {
    $casldriverLicense = "info";
    $isdriverlicense = "checked";
}


?>

<style>
    li:hover{
        cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-md-4 col-sm-3 col-xs-12">
        <ul class="list-group">
            <li class="list-group-item list-group-item-<?= $caslapication ?>" onclick="UpdateStatusGeneral(1)">1. Application</li>
            <li class="list-group-item list-group-item-<?= $caslphonenumber ?>" onclick="UpdateStatusGeneral(2)">2. Phone Number</li>
            <li class="list-group-item list-group-item-<?= $casldriverLicense ?>" onclick="UpdateStatusGeneral(3)">3. Driver License</li>
            <li class="list-group-item list-group-item-<?= $caslregustation ?>" onclick="UpdateStatusGeneral(4)">4. Registation</li>
            <li class="list-group-item list-group-item-<?= $caslpictures ?>" onclick="UpdateStatusGeneral(5)">5. Pictures</li>
            <li class="list-group-item list-group-item-<?= $caslproofodo ?>" onclick="UpdateStatusGeneral(6)">6. Proof Of Prior</li>
            <li class="list-group-item list-group-item-<?= $casladcsinged ?>" onclick="UpdateStatusGeneral(7)">7. ADC Singed</li>
        </ul>
    </div>
</div>

<? if ($status_profileP==1) { ?>
    <div class="row form-group">
        <div>
            <label>choose the status that does not require a file</label>  
        </div>

        <div class="col-md-2 col-sm-6 col-xs-12">

            <select id="StatusSelect"  class="form-control">
                <option value="1"> Aplication</option>
                <option value="2"> Phone Number</option>
                <option value="3"> Driver License</option>
                <option value="4"> Registration</option>
                <option value="5"> Pictures</option>
                <option value="6"> Proof of Prior</option>
                <option value="7"> ADC Singed</option>

            </select>

        </div>
        <div class="col-md-4 center-block"> 
            <button class="btn-primary " onclick="SaveSelectOption();" style="margin-top: 4px;">
                Submit
            </button>
        </div>

    </div>
    <div class="row" >



        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label>Application</label>
            </div>
            <input   id="chekCashier1" type="checkbox" <? echo $Isaplication ?> data-toggle="toggle" onchange="ChangeValue(1)">
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label>Phone Number</label>
            </div>
            <input   id="chekCashier2" type="checkbox" <? echo $isPhoneNumber; ?> data-toggle="toggle" onchange="ChangeValue(2)">
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label>Driver License</label>
            </div>
            <input   id="chekCashier3" type="checkbox" <? echo $isdriverlicense; ?> data-toggle="toggle" onchange="ChangeValue(3)">
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label>Registration</label>
            </div>
            <input   id="chekCashier4" type="checkbox" <? echo $isregistration; ?> data-toggle="toggle" onchange="ChangeValue(4)">
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label>Pictures</label>
            </div>
            <input   id="chekCashier5" type="checkbox" <? echo $ispictures; ?> data-toggle="toggle" onchange="ChangeValue(5)">
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label>Proof Of Prior</label>
            </div>
            <input   id="chekCashier6" type="checkbox" <? echo $ispop; ?> data-toggle="toggle" onchange="ChangeValue(6)">
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label>ADC Singed</label>
            </div>
            <input   id="chekCashier7" type="checkbox" <? echo $isadcSinged; ?> data-toggle="toggle" onchange="ChangeValue(7)">
        </div>
    </div>
<? } ?>


<script>


    function UpdateStatusGeneral2(value) {
        var idClient =<?= $idClient ?>;
        $.post("/MGTM/Views/Controllers/ModifySatusProfile.php", {idClient, value}, function (data) {
            alert(data.message);
            //  location.reload();

            $(this).removeClass("list-group-item-danger").addClass("list-group-item-success");
        });
    }

    function ChangeValue(numberCheck) {

        var value = $("#chekCashier" + numberCheck + ":checked").val();
        if (value == "on") {
            value = 1;
        } else {
            value = 0;
        }
        var idClient =<?= $idClient ?>;
        $.post("/MGTM/Views/Controllers/ModifySatusProfile.php", {idClient, value, numberCheck}, function (data) {
            if (data.status == "success") {
                

                alertify.success(data.message);

            } else {
                alertify.error(data.message);
            }
            //  location.reload();

            //$(this).removeClass("list-group-item-danger").addClass("list-group-item-success");
        });

    }

    function SaveSelectOption() {
        var statusForNA = $("#StatusSelect").val();
        var idClient =<?= $idClient ?>;
        var naOption = 1;
        $.post("/MGTM/Views/Controllers/ModifySatusProfile.php", {idClient, statusForNA, naOption}, function (data) {
            if (data.status == "success") {
            

                alertify.success(data.message);

            } else {
                alertify.error(data.message);
            }
        });
    }
</script>