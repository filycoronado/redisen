<?php
$type = 1;
$id = 0;

$data = [];
if (isset($_POST['id_client'])) {
    $type = 1;
    $id = $_POST['id_client'];

    $sql = "SELECT * FROM `UserManagement` where id=$id";
    $res = mysql_query($sql);
    $data = mysql_fetch_assoc($res);
} else {
    $type = 2;
    $id = $_POST['idPol'];

    $sql = "SELECT * FROM `PoliciesForOldMGTM` where Id=$id";
    $res = mysql_query($sql);
    $data = mysql_fetch_assoc($res);
}
?>

<div class="panel panel-default"  style="margin-right: 20px;">
    <form  class="credit-card-div formCC">
        <div class="panel-heading">
            <h2>Configure Auto Pay or Direct Biling</h2>
            <div class="row">
                <div class="col-md-5">
                    <span class="help-block text-muted small-font" >Status</span>
                    <select  class="form-control" id="selectc1">
                        <option value="0" <? if ($data['autoordirect'] == 0) { ?>selected<? } ?>>Select</option>
                        <option value="1" <? if ($data['autoordirect'] == 1) { ?>selected<? } ?>>Auto Pay</option>
                        <option value="2" <? if ($data['autoordirect'] == 2) { ?>selected<? } ?>>Direct Billing</option>
                    </select>
                </div>
                <div class="col-md-5">
                    <span class="help-block text-muted small-font" >Date</span>
                    <input type="date" class="form-control" placeholder=""  id="datec1" value="<?= $data['dateautopay'] ?>"/>
                </div>
            </div>

            <div class="row ">
                <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                    <input type="button"  class="btn btn-success" value="Save"  onclick="saveConf1()" />
                </div>
            </div>
        </div>
    </form>
</div>


<div class="panel panel-default" style="margin-right: 20px;">
    <form action="#" class="credit-card-div formCC">
        <div class="panel-heading">
            <h2>Requote Request</h2>
            <div class="row">
                <div class="col-md-6">
                    <span class="help-block text-muted small-font" >Date</span>
                    <input type="date" class="form-control" placeholder="Enter Card Number"  id="date_requote" value="<?= $data['date_requote'] ?>"/>
                </div>
            </div>

            <div class="row ">
                <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                    <input type="button"  class="btn btn-success" value="Save" onclick="saveConf2()"/>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="panel panel-default" style="margin-right: 20px;">
    <form action="#" class="credit-card-div formCC">
        <div class="panel-heading">
            <h2>Do you own a house or are you a renter?</h2>
            <div class="row">
                <div class="col-md-6">
                    <span class="help-block text-muted small-font" >Status</span>
                    <select class="form-control" id="selectc3" >
                        <option value="0" <? if ($data['status_renter'] == 0) { ?>selected<? } ?>>Select</option>
                        <option value="1" <? if ($data['status_renter'] == 1) { ?>selected<? } ?>>Renter</option>
                        <option value="2" <? if ($data['status_renter'] == 2) { ?>selected<? } ?>>House Owner</option>
                        <option value="3" <? if ($data['status_renter'] == 3) { ?>selected<? } ?>>Does not apply</option>
                    </select>
                </div>
            </div>

            <div class="row ">
                <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                    <input type="button"  class="btn btn-success" value="Save" onclick="saveConf3()" />
                </div>
            </div>
        </div>
    </form>
</div>



<script>
    function saveConf1() {
        var select = $("#selectc1").val();
        var date = $("#datec1").val();
        var id =<?= $id ?>;
        var type =<?= $type ?>;
        $.post("/MGTM/Views/Controllers/saveConf.php", {conf: 1, status: select, date: date, id: id, type: type}, function (data) {
            alert(data.message);
        });
    }
    function saveConf2() {

        var date = $("#date_requote").val();
        var id =<?= $id ?>;
        var type =<?= $type ?>;
        $.post("/MGTM/Views/Controllers/saveConf.php", {conf: 2, date: date, id: id, type: type}, function (data) {
            alert(data.message);
        });
    }

    function saveConf3() {

        var select = $("#selectc3").val();
        var id =<?= $id ?>;
        var type =<?= $type ?>;
        $.post("/MGTM/Views/Controllers/saveConf.php", {conf: 3, status: select, id: id, type: type}, function (data) {
            alert(data.message);
        });
    }
</script>
