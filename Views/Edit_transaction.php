<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];



$idCustomer = "";
if (isset($_GET['transaction_number'])) {
    $idCustomer = $_GET['transaction_number'];
    $sql = "SELECT * FROM `cutvone` where id=$idCustomer";
} else {
    $sql = "SELECT * FROM `cutvone` where id=4207";
}
$Transaction = mysqli_query($mysqli, $sql);
$rowT = mysqli_fetch_assoc($Transaction);


$todayDate = date("m-d-Y");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>
        <script src="../jquery.maskedinput.js"></script>
        <script src="../js/mask/jquery/src/jquery.mask.js"></script>



        <title>Edit Transaction</title>

    </head>
    <style>
        .ui-widget-header {
            border: 1px solid #2d1c1c !important;
            color: #ffffff !important;
            font-weight: bold;
            background: #ed502e !important;
        }
        .ui-widget-content {
            color: #ed502e !important;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {

            font-weight: bold !important;;
            color: #f3f1f1 !important;;
            background: #ed502e !important;;
            border-collapse: collapse !important;
        }
        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            border: 1px solid #f7360c  !important;;

            color: #f7360c  !important;
            background: white  !important;
        }
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            border: 1px solid #aaaaaa;
            font-weight: bold  !important;
            color: #000000 !important;
            background: #ed502e7a !important;
        }
    </style>
    <body >




        <div>

            <?php
            include("./BarMenu.php");
            ?>

        </div>
        <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 container" >



            <div class="row">
                <form  class="credit-card-div  col-lg-12 col-md-12">
                    <div class="panel panel-default" >
                        <div class="panel-heading">



                            <div class="row">
                                <div class="col-md-6">
                                    <span class="help-block text-muted small-font" > Customer Name:</span>
                                    <input id="nombre" type="text" class="form-control" placeholder="Jhon Due" value="<?= $rowT['nameclient']; ?>"/>
                                </div>
                                <div class="col-md-3" >
                                    <span class="help-block text-muted small-font" >Responsible user:</span>
                                    <select  id="User" class="form-control selectpicker" >
                                        <option value="0" <? if ($row['languajePrefernce'] == 0) { ?>selected<? } ?>>Select User</option>
                                        <?
                                        $sqlUsers = "SELECT id, user FROM `users` where (agencia=101 or agencia=103 or agencia=104 or agencia=105 or agencia=102 ) and (nivel=1 or nivel=5 or nivel=2)and habilitado=1";
                                        $result = mysqli_query($mysqli, $sqlUsers);
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            ?>
                                            <option value="<?= $row['id']; ?>" <? if ($rowT['id_usuario'] == $row['id']) { ?>selected<? } ?>><?= $row['user'] ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font" >Insurance Amount:</span>
                                    <input id="insuranceAmount" type="number" class="form-control" placeholder="100.0" value="<?= $rowT['feecompany']; ?>" />
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font" >Office Amount:</span>
                                    <input id="Feeamount" type="number" class="form-control" placeholder="W Ajo way 25" value="<?= $rowT['feeoffice']; ?>" />
                                </div>
                                <div class="col-md-3 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font" > Insurance Method Payment</span>
                                    <select  id="InsuranceMethod" class="form-control selectpicker" >
                                        <option value="0" <? if ($rowT['companytype'] == 0) { ?>selected<? } ?>>Select</option>
                                        <option value="1" <? if ($rowT['companytype'] == 1) { ?>selected<? } ?>>Cash</option>
                                        <option value="2" <? if ($rowT['companytype'] == 2) { ?>selected<? } ?>>Card</option>
                                    </select>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font" >ADC Amount:</span>
                                    <input id="ADCamount" type="number" class="form-control" placeholder="100.0" value="<?= $rowT['feeforaz']; ?>" />
                                </div>

                                <div class="col-md-3 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font" > ADC Method Payment</span>
                                    <select  id="ADCMethod" class="form-control selectpicker" >
                                        <option value="0" <? if ($rowT['languajePrefernce'] == 0) { ?>selected<? } ?>>Select Languaje</option>
                                        <option value="1" <? if ($rowT['typeaz'] == 1) { ?>selected<? } ?>>Cash</option>
                                        <option value="2" <? if ($rowT['typeaz'] == 2) { ?>selected<? } ?>>Card Chase</option>
                                        <option value="3" <? if ($rowT['typeaz'] == 3) { ?>selected<? } ?>>Card Direct Payment Drivers</option>
                                    </select>
                                </div>

                            </div>
                            <div class="row ">
                                <div class="col-md-3 pad-adjust">
                                    <span class="help-block text-muted small-font" >Cashier</span>

                                    <select  id="Cashiers" class="form-control selectpicker" >
                                        <option value="0" <? if ($row['languajePrefernce'] == 0) { ?>selected<? } ?>>Select Cashier</option>
                                        <?
                                        $sqlCashiers = "SELECT id, user FROM `users` where (agencia=101 or agencia=103 or agencia=104 or agencia=105 ) and (nivel=1 or nivel=5 or nivel=2)and habilitado=1 and isCashier=1";
                                        $result = mysqli_query($mysqli, $sqlCashiers);
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            ?>
                                            <option value="<?= $row['id']; ?>" <? if ($rowT['id_cashier'] == $row['id']) { ?>selected<? } ?>><?= $row['user'] ?></option>
                                        <? } ?>
                                    </select>

                                </div>


                            </div>



                        </div>
                        <div class="row ">
                            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                <input type="button"  onclick="Update();" class="btn btn-success" value="Update Transaction" />
                            </div>

                        </div>

                    </div>

                </form>
            </div>

        </div>

        <script type="text/javascript">

        </script>
    </body>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>



    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
            $("#datepicker").datepicker();
            $("#datefinal").datepicker();
            $(".datePickerSel").datepicker({dateFormat: 'mm-dd-yy'});
            function Update() {
                var Cashiers = $("#Cashiers").val();
                var ADCMethod = $("#ADCMethod").val();
                var InsuranceMethod = $("#InsuranceMethod").val();
                var Feeamount = $("#Feeamount").val();
                var ADCamount = $("#ADCamount").val();
                var insuranceAmount = $("#insuranceAmount").val();
                var user = $("#User").val();
                var id_transaction =<?= $idCustomer ?>;
                $.post("./Controllers/UpdateTransaction.php", {Cashiers, ADCMethod, InsuranceMethod, ADCamount, Feeamount, insuranceAmount, user, id_transaction}, function (data) {


                    if (data.status == "success") {
                        alertify.success(data.message);

                    } else {
                        alertify.error(data.message);
                    }
                });


            }
    </script>
</html>