<?php

$nivelLoge = 0;


//include("../inc/dbconnection.php");
$idClient;
$cardNumber;
$expDate;
$mm;
$yy;
$rn;
$an;
$new = 0;
$user_update = "";
$date_update = "";
if (isset($_POST['id_client'])) {
    $new = 1;
    $idClient = $_POST['id_client'];
    $sql = "SELECT * FROM `BankInformation` where id_client=$idClient";
    $res = mysql_query($sql);
    $row = mysql_fetch_assoc($res);
    $cardNumber = $row['cardNumber'];
    $expDate = $row['expDate'];
    $expDate = split('/', $expDate);
    $mm = $expDate[0];
    $yy = $expDate[1];
    $cvv = $row['cvv'];
    $rn = $row['routenumber'];
    $an = $row['accountNumber'];
    $user_update = $row['id_user'];
    $date_update = $row['date_update'];
}
if (isset($_POST['iduserloig'])) {
    $iduserloig = $_POST['iduserloig'];
}
$flag;

if ($iduserloig == 2177 ||  $iduserloig == 1925 ||  $iduserloig == 2201 ||  $iduserloig == 2198 || $iduserloig == 2194 ||  $iduserloig == 2195 || $iduserloig == 2189 || $iduserloig == 2190 || $iduserloig == 2193  ||  $iduserloig == 2185 || $iduserloig == 2186 || $iduserloig == 2180 || $iduserloig == 2179 || $iduserloig == 2127 || $iduserloig == 2175 || $iduserloig == 1 || $iduserloig == 364 || $iduserloig == 2117 || $iduserloig == 2164  || $iduserloig == 2162 || $iduserloig == 2099 || $iduserloig == 2172) {
    $disabled = "";
    $flag = 1;
} else {
    //  $disabled = "disabled";
    $flag = 0;
}


$realCadnumber = "";
$routenumberOriginal = "";
$accountNumberOriginal = "";
if ($cardNumber != "" || $rn != "" || $an != "") {
    if ($flag == 0) {
        //  $disabled = "disabled";
        if ($cardNumber != "") {
            $realCadnumber = $cardNumber;
        }
        if ($rn != "") {
            $routenumberOriginal = $rn;
        }
        if ($an != "") {
            $accountNumberOriginal = $an;
        }

        //    echo  $realCadnumber."<br>";
        //    echo  $routenumberOriginal."<br>";
        //    echo  $accountNumberOriginal."<br>";
        //    
        $cardNumberAux = substr($cardNumber, -4);
        $cardNumber = "XXXXXXXXXXXX-" . $cardNumberAux;
        $rnAux = substr($rn, -4);
        $rn = "XXXXXXXXXXXX-" . $rnAux;
        $anAux = substr($an, -4);
        $an = "XXXXXXXXXXXX-" . $anAux;
    }
} else {
    $disabled = "";
}
?>
<div class="row col-md-12">
    <div class="text-right">
        <label>Updated by:<?= $user_update ?>/<?= $date_update ?></label>
    </div>
</div>
<form action="#" class="credit-card-div col-lg-10 col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">

            <div class="row">
                <div class="col-md-6">
                    <span class="help-block text-muted small-font"> Card Number</span>

                    <input id="cardNumber" type="text" class="form-control" placeholder="Enter Card Number" value="<?= $cardNumber ?>" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font"> Expiry Month</span>
                    <input id="ExpM" type="text" class="form-control" placeholder="MM" value="<?= $mm ?>" />
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font"> Expiry Year</span>
                    <input id="ExpY" type="text" class="form-control" placeholder="YY" value="<?= $yy ?>" />
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font"> CCV</span>
                    <input id="cvv" type="text" class="form-control" placeholder="CCV" value="<?= $cvv ?>" />
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <img src="img/cc.png" class="img-rounded" />
                </div>
            </div>
            <div class="row ">
                <div class="col-md-6 pad-adjust">
                    <span class="help-block text-muted small-font"> Route Number</span>
                    <input id="RouteNum" type="text" class="form-control" placeholder="Name On The Card" value="<?= $rn ?>" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pad-adjust">
                    <span class="help-block text-muted small-font"> Account Number</span>
                    <input id="AccountNumber" type="text" class="form-control" placeholder="Name On The Card" value="<?= $an ?>" />
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                <input type="button" onclick="saveBank();" class="btn btn-success" value="Save" />
            </div>

        </div>

    </div>

</form>
<script>
    function saveBank() {
        var id_client = <?= $idClient ?>;
        var cardNumber = $("#cardNumber").val();
        var ExpM = $("#ExpM").val();
        var ExpY = $("#ExpY").val();
        var cvv = $("#cvv").val();
        var RouteNum = $("#RouteNum").val();
        var AccountNumber = $("#AccountNumber").val();


        var originalCC = "<?= $realCadnumber ?>";
        var orRn = "<?= $routenumberOriginal ?>";
        var orAn = "<?= $accountNumberOriginal ?>";



        if (cardNumber.indexOf('X') != -1) {
            cardNumber = originalCC;
        }

        if (RouteNum.indexOf('X') != -1) {
            RouteNum = orRn;
        }

        if (AccountNumber.indexOf('X') != -1) {
            AccountNumber = orAn;
        }
        $.post("/MGTM/Views/Controllers/UpdateBankInformation.php", {
            id_client,
            cardNumber,
            ExpM,
            ExpY,
            cvv,
            RouteNum,
            AccountNumber
        }, function(data) {
            alert(data.message);
        });
    }
</script>