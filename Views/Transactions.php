<?
session_start();
$idUser = 0;
$niveluser = 0;
if (isset($_SESSION['nivel'])) {
    $niveluser = $_SESSION['nivel'];
    $idUser = $_SESSION['id'];
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");

$enabled = "disabled";
if ($idUser == 1 || $idUser == 364 || $idUser == 2117 || $idUser == 2155 || $idUser == 2149 || $idUser == 2127) {
    $enabled = "";
}
$agency = $_SESSION['agencia'];
if (isset($_GET['agency'])) {
    $agency = $_GET['agency'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="./../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../libs/alertifyjs/css/alertify.css">
        <script src="../libs/alertifyjs/alertify.js"></script>
        <script src="../jquery.maskedinput.js"></script>
        <script src="../js/mask/jquery/src/jquery.mask.js"></script>
        <title> Transactions</title>



    </head>

    <body >

        <div>

            <?
            include("./BarMenu.php");
            ?>

        </div>


        <div class="col-md-10 col-lg-10 col-md-offset-4 col-lg-offset-2" >

            <?
            $sqlT = "";
            $factual = date("Y-m-d");

            $sqlT = "SELECT cv.* ,c.nombre as companyname , c.ImageRoute as image , us.user as user FROM cutvone as cv JOIN companyes as c join users as us on cv.company=c.id and us.id=cv.id_usuario  where cv.erase!=1 and  cv.fecha='$factual' and cv.agency=$agency";

            // $sqlT = "SELECT cv.* ,c.nombre as companyname , c.ImageRoute as image , us.user as user FROM cutvone as cv JOIN companyes as c join users as us on cv.company=c.id and us.id=cv.id_usuario where  cv.erase!=1 and cv.id_usuario=" . $_SESSION['id'] . " and cv.fecha='" . $factual . "'";
            ?>
            <div class="row" style="padding-bottom: 0">
                <div class="col-sm-6">
                    <div class="col-xs-4" style="padding: 0">
                        <select class="form-control input-sm" onchange="cargarT();" id="selag" <?= $enabled ?>>

                            <option value="101"<?= (101 == $agency) ? " selected" : "" ?>>101</option>
                            <option value="102"<?= (102 == $agency) ? " selected" : "" ?>>102</option>
                            <option value="103"<?= (103 == $agency) ? " selected" : "" ?>>103</option>
                            <option value="104"<?= (104 == $agency) ? " selected" : "" ?>>104</option>
                            <option value="105"<?= (105 == $agency) ? " selected" : "" ?>>105</option>
                        </select> 
                    </div>
                </div>
            </div>
            <?
            $contcompany;
            $contfee;
            $contdealer;
            $granTotal;
            $contfeeforaz;
            $totales = "SELECT cv.* ,c.nombre as companyname , c.ImageRoute as image , us.user as user FROM cutvone as cv JOIN companyes as c join users as us on cv.company=c.id and us.id=cv.id_usuario  where cv.erase!=1 and  cv.fecha='$factual' and (cv.agency=101 or cv.agency=102 or cv.agency=103 or cv.agency=104 or cv.agency=105)";
            $res = mysqli_query($mysqli, $totales);
            while ($r = mysqli_fetch_assoc($res)) {
                $contcompany += $r['feecompany'];
                $contfee += $r['feeoffice'];
                $contdealer += $r['feedealer'];
                $contfeeforaz += $r['feeforaz'];
                $total = $r['feedealer'] + $r['feeoffice'] + $r['feecompany'] + $r['feeforaz'];
                $granTotal += $total;
            }
            ?>
            <div id="tabcont">
                <div class="cars-horizon">
                    <div class="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row col-md-12 custyle" style="overflow-x: auto;">
                                    <table class="table table-striped custab">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Company</th>
                                                <th>Customer</th>
                                                <th>User</th>
                                                <th>Cashier</th>
                                                <th>Delivery</th>
                                                <th>Ins</th>
                                                <th>Office</th>
                                                <th>RSA</th>
                                                <th>Total</th>
                                                <th>Mp</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>

                                        <?
                                        $result_t = mysqli_query($mysqli, $sqlT);
                                        $cont = 0;

                                        while ($row_t = mysqli_fetch_assoc($result_t)) {
                                            $image = $row_t['image'];
                                            $customer = strtoupper($row_t['nameclient']);
                                            $user = strtoupper($row_t['user']);
                                            $delivery = $row_t['nameentrega'];


                                            $total = $row_t['feedealer'] + $row_t['feeoffice'] + $row_t['feecompany'] + $row_t['feeforaz'];


                                            if ($row_t['companytype'] == 1) {
                                                $feecompanycoc = "CASH";
                                            } else {
                                                $feecompanycoc = "CARD";
                                            }
                                            if ($row_t['typeaz'] == 3) {
                                                $feecompanycoc = "CARD";
                                                $feecompanycoc = "<p style='color:#79a85a'>CARD</P>";
                                            }
                                            $cont++;
                                            $idTRansaction = $row_t['id'];
                                            ?>
                                            <tr id="row_t1<?= $row_t['id'] ?>">
                                                <td style="width: 1px; text-align: center;"><?= $cont ?></td>
                                                <td><image style="    max-height: 73px;  max-width: 80px" src="<?= $image ?>"/></td>
                                                <td class="fix_ancho"> 
                                                    <a href="CustomerDetails.php?id=<?= $row_t['id_cliente'] ?>">
                                                        <?= $customer ?>
                                                    </a>
                                                </td>
                                                <td><?= $user ?></td>
                                                <td><?= regresauser($mysqli, $row_t['id_cashier']); ?></td>
                                                <td><?= $delivery ?></td>
                                                <td><?= $row_t['feecompany'] ?></td>
                                                <td><?= $row_t['feeoffice'] ?></td>
                                                <td><?= $row_t['feeforaz'] ?></td>
                                                <td><?= $total ?></td>
                                                <td><?= $feecompanycoc ?></td>



                                            </tr>
                                            <tr id="row_t2<?= $row_t['id'] ?>">


                                                <td colspan="13">

                                                    <div class="col-lg-1 col-md-1 col-xs-12 col-sm-12">
                                                        <a class='btn btn-success btn-sm' target="_blank" href="<?= $row['recibo'] ?>" ><span class="fa  fa-download fa-lg"></span>Ticket</a> 
                                                    </div>
                                                    <?if($editP==1){?>
                                                        <div class="col-lg-1 col-md-1 col-xs-1 col-sm-1">
                                                            <a type="button" class="btn btn-info btn-sm" href="Edit_transaction.php?transaction_number=<?= $row_t['id'] ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                                        </div>
                                                    <?}?>
                                                    <?if($eraseP==1){?>
                                                        <div class="col-lg-1 col-md-1 col-xs-1 col-sm-1">

                                                            <a class='btn btn-danger btn-sm'  onclick="EraseRow('<?= $idTRansaction ?>')"><span class="fa  fa-remove fa-lg"></span>Remove</a> 

                                                        </div>
                                                    <?}?>
                                                </td>

                                            </tr>


                                        <? } ?>


                                    </table>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <?
            if ($_SESSION['id'] == 1) {
                ?>
                <div class="row">
                    <div class="row col-md-12 custyle">
                        <table class="table table-striped custab">
                            <thead>
                                <tr>
                                    <th>Insurance</th>
                                    <th>Office</th>
                                    <th>Dealers</th>
                                    <th>Rsa</th>  
                                    <th>Total</th>

                                </tr>
                            </thead>
                            <tr>
                                <td>$<?= $contcompany ?></td>
                                <td>$<?= $contfee ?></td>
                                <td>$<?= $contdealer ?></td>
                                <td>$<?= $contfeeforaz ?></td>
                                <td>$<?= $granTotal ?></td>

                            </tr>
                        </table>
                    </div>
                </div>   
            <? } ?>
        </div>
    </div>





    <script type="text/javascript">


        function EraseRow(id) {
            var id_transaction = id;
            var action = "delete";
            alertify.confirm('Please Confim This Ation', 'Delete Transaction?', function () {


                $.post("./Controllers/UpdateTransaction.php", {id_transaction, action}, function (data) {
                    if (data.status == "success") {
                        $("#row_t2" + id).remove();
                        $("#row_t1" + id).remove();
                        alertify.success(data.message);
                    } else {
                        alertify.error(data.message);
                    }
                });

            }
            , function () {
                alertify.error('Cancel');
            });


        }



        function cargarT() {

            var agencia = $("#selag").val();
            location.href = "Transactions.php?agency=" + agencia;
        }
    </script>
</body>
</html>
<?

function resgresanombrecompania3($mysqli, $val) {
    $sql = "SELECT * FROM `companyes` where id=" . $val;

    $res = mysqli_query($mysqli, $sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['nombre'];
    }
}

function regresauser($mysqli, $val) {


    $sql = "SELECT * FROM `users` where id=" . $val;

    $res = mysqli_query($mysqli, $sql);

    while ($re = mysqli_fetch_assoc($res)) {
        return $re['user'];
    }
}

function resgresanombrecashier($mysqli, $val) {
    if (empty($val))
        return "-";


    $sql = "SELECT * FROM cashier where id=" . $val;

    $res = mysqli_query($mysqli, $sql);
    $vr = "-";
    while ($re = mysqli_fetch_assoc($res)) {
        $vr = $re['name'];
    }
    return $vr;
}
?>
