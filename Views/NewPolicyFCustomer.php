<?php
//include("../inc/dbconnection.php");
$idClient = 0;
if (isset($_GET['id'])) {
    $idClient = $_GET['id'];
}
$iduserloig = $_SESSION['id'];

$QueryPolicy = "SELECT * FROM `policy_type`";
$resultTypes = mysqli_query($mysqli, $QueryPolicy);

$queryCompany = "SELECT * FROM `companyes` where agency=101";
$resultCompany = mysqli_query($mysqli, $queryCompany);
$nameCustomer = $_GET['name'];

$id_policy = 0;
$policyType;
$company_id;
$policynumber;
$adc_number;
$pago_mensual;
$pago_menusal_drviers;
$status_policy;
$CancelForPending;
$flag = 0;
if (isset($_GET['id_policy'])) {
    $flag = 1;
    $id_policy = $_GET['id_policy'];
    $idClient = $_GET['id_client'];
    if ($id_policy == $idClient) {
        $id_policy = 0;
        $sql = "SELECT * FROM `UserManagement` where id=$idClient";

        $res = mysqli_query($mysqli, $sql);
        $row = mysqli_fetch_assoc($res);

        $policynumber = $row['policynumber'];
        $adc_number = $row['ADC'];
        $company_id = $row['compania'];
        $policyType = $row['policy_type'];
        $status_policy = $row['status_client'];
        $pago_menusal_drviers = $row['pagomensualdrivers'];
        $pago_mensual = $row['pagomensual'];
        //$CancelForPending = $row['DateOfPrending'];

        $CancelForPending = date("m-d-Y", strtotime($row['DateOfPrending']));
    } else {
        $sql = "SELECT * FROM `PoliciesForOldMGTM` where id=$id_policy";
        $result = mysqli_query($mysqli, $sql);
        $row = mysqli_fetch_assoc($result);

        $policyType = $row['policy_type'];
        $company_id = $row['company_id'];
        $policynumber = $row['policynumber'];
        $adc_number = $row['adc_number'];
        $pago_mensual = $row['pago_mensual'];
        $pago_menusal_drviers = $row['pago_menusal_drviers'];
        $status_policy = $row['status_policy'];
        $CancelForPending = date("m-d-Y", strtotime($row['CancelForPending']));
    }
}
$update = 0;
if (isset($_GET['update'])) {
    $update = $_GET['update'];
}
$disabled = "";
if ($edit_policy_statusP == 0 && $flag == 1) {
    $disabled = "disabled";
}
?>
<style>
    .border-error-field{
        border-color: red;
    }
</style>
<form  class="credit-card-div  col-lg-10 col-md-6" style="margin-top: 5%;">
    <div class="panel panel-default" >
        <div class="panel-heading">

            <div class="row">
                <div class="col-md-4">
                    <span class="help-block text-muted small-font" > Policy Number:</span>
                    <input id="policyN" type="text" class="form-control request_field" placeholder="Policy Number" value="<?= $policynumber ?>"/>
                </div>
                <div class="col-md-4">
                    <span class="help-block text-muted small-font" > Membership Number:</span>
                    <input id="MembershipN" type="text" class="form-control" placeholder="Membership Number" value="<?= $adc_number ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <span class="help-block text-muted small-font" > 
                        Insurance Company :

                    </span>
                    <select  id="SeelctCompany" class="form-control selectpicker" >
                        <?php
                        while ($row = mysqli_fetch_assoc($resultCompany)) {
                            ?>
                            <option  value="<?= $row['id']; ?>" <? if ($company_id == $row['id']) { ?>selected<? } ?>>  <?= $row['nombre']; ?></option>
                        <?php } ?>

                    </select>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3"> 
                    <span class="help-block text-muted small-font" >
                        Policy Type:        
                    </span>

                    <select  id="SeelctType" class="form-control selectpicker">
                        <?php
                        while ($row = mysqli_fetch_assoc($resultTypes)) {
                            ?>
                            <option  value="<?= $row['id']; ?>"  <? if ($policyType == $row['id']) { ?>selected<? } ?>>  <?= $row['name']; ?></option>
                        <?php } ?>

                    </select>
                </div>

            </div>
            <div class="row ">
                <div class="col-md-4 pad-adjust">
                    <span class="help-block text-muted small-font" > Status Policy :</span>
                    <select type="text" class="form-control request_field" id="status_client" onchange="ShowDays();" <?= $disabled ?>>
                        <option value="0">SELECT</option>
                        <option value="1" <? if ($status_policy == 1) { ?>selected<? } ?>>PENDING CANCELLATION</option>
                        <option value="2" <? if ($status_policy == 2) { ?>selected<? } ?> >CANCELLED NON-PAYMENT</option>
                        <option value="3" <? if ($status_policy == 3) { ?>selected<? } ?> >CANCELLED UW</option>
                        <option value="4" <? if ($status_policy == 4) { ?>selected<? } ?> >RETAINED</option>
                        <option value="5" <? if ($status_policy == 5) { ?>selected<? } ?> >REINSTATED</option>
                        <option value="6" <? if ($status_policy == 6) { ?>selected<? } ?> >CANCELLED SERVICE</option>
                        <option value="7" <? if ($status_policy == 7) { ?>selected<? } ?> >EXPIRED</option>
                        <option value="8" <? if ($status_policy == 8) { ?>selected<? } ?>>RENEWED</option>
                        <option value="9" <? if ($status_policy == 9) { ?>selected<? } ?>>PENDING RENEWAL</option>
                        <option value="10" <? if ($status_policy == 10) { ?>selected<? } ?> >NEW BUSINESS</option>
                        <option value="11" <? if ($status_policy == 11) { ?>selected<? } ?> >NO LONGER CLIENT</option> 
                        <option value="11" <? if ($status_policy == 12) { ?>selected<? } ?> >UNDERWRITING</option>
                        <option value="13" <? if ($status_policy == 13) { ?>selected<? } ?>>FLAT CANCELLATION</option>
                        <option value="13" <? if ($status_policy == 14) { ?>selected<? } ?>>REWRITE</option>
                    </select>
                </div>

                <div class="col-md-2 pad-adjust daysInfo" style="display:none;">
                    <span class="help-block text-muted small-font" > Days:</span>
                    <input id="ManyDays" type="number" class="form-control" placeholder="0" value="0"/>
                </div>
            </div>
            <?php if ($status == 1) { ?>
                <div class="row ">
                    <div class="col-md-4 pad-adjust">
                        <span class="help-block text-muted small-font" >Date For Cancel:<?= $CancelForPending ?></span>
                    </div>
                </div>
            <? } ?>

            <div class="row ">
                <div class="col-md-3">
                    <span class="help-block text-muted small-font" > Amount Insurance:</span>
                    <input id="InsuranceAmount" type="number" class="form-control " placeholder="Amount" value="<?= $pago_mensual ?>"/>
                </div>
                <div class="col-md-3">
                    <span class="help-block text-muted small-font" > Amount ADC:</span>
                    <input id="ADCAmount" type="number" class="form-control " placeholder="Amount" value="<?= $pago_menusal_drviers ?>"/>
                </div>
            </div>


        </div>
        <div class="row ">
            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust" style="padding: 27px;">
                <input type="button"  <? if ($update > 0) { ?>onclick="UpdatePolicy();" <? } else { ?>  onclick="SavePolicy();"<? } ?>class="btn btn-success btn-default" value="Save" />
            </div>

        </div>

    </div>
</form>
<script>
    console.log(<?= $iduserloig ?>);
    function ChangeIcons1() {
        var TypePolicy = $("#SeelctType").val();
    }

    function ShowDays() {
        var value = $("#status_client").val();

        if (value == 1) {
            $(".daysInfo").show();
        } else {
            $(".daysInfo").hide();
        }
    }
    function UpdatePolicy() {
        var id_client =<?= $idClient ?>;
        var id_user =<?= $iduserloig ?>;
        var id_poliza =<?= $id_policy ?>;
        var policyN = $("#policyN").val();
        var MembershipN = $("#MembershipN").val();
        var SeelctCompany = $("#SeelctCompany").val();
        var SeelctType = $("#SeelctType").val();

        var status_client = $("#status_client").val();
        var InsAmount = $("#InsuranceAmount").val();
        var ADCAmount = $("#ADCAmount").val();
        var dayForPending = $("#ManyDays").val();

        $.post("/MGTM/Views/Controllers/UpdatePolicy.php", {id_client, policyN, MembershipN, SeelctCompany, SeelctType, status_client, InsAmount, ADCAmount, id_user, dayForPending, id_poliza}, function (data) {
            if (data.status == "success") {
                alertify.success(data.message);
            } else {
                alertify.error(data.message);
            }
        });

    }

    function SavePolicy() {
        var id_client =<?= $idClient ?>;
        var id_user =<?= $iduserloig ?>;
        var policyN = $("#policyN").val();
        var MembershipN = $("#MembershipN").val();
        var SeelctCompany = $("#SeelctCompany").val();
        var SeelctType = $("#SeelctType").val();
        var selLocation = $("#selLocation").val();
        var status_client = $("#status_client").val();
        var InsAmount = $("#InsuranceAmount").val();
        var ADCAmount = $("#ADCAmount").val();

        if (policyN != "" && status_client != 0) {

            $.post("/MGTM/Views/Controllers/CreatePolicy.php", {id_client, policyN, MembershipN, SeelctCompany, SeelctType, selLocation, status_client, InsAmount, ADCAmount, id_user}, function (data) {
                if (data.status == "success") {
                    alertify.success(data.message);
                    var companyid = SeelctCompany;
                    var customer = "<?= $nameCustomer ?>";
                    var id_customer = id_client;
                    var id_policy = data.id_policy;
                    $(".request_field").removeClass("border-error-field");

                    //id_client=3664&id_policy=2&Customer=ALEXIS%20ANGULO%20SIERRA&Company=1
                    window.location.href = 'MakeAPaymentS.php?id_client=' + id_customer + '&id_policy=' + id_policy + '&Customer=' + customer + '&Company=' + companyid;
                } else {

                    alertify.error(data.message);
                }


            });
        } else {
            $(".request_field").addClass("border-error-field");
            alertify.error("Policy number and amount are necesary");
        }

    }


</script>