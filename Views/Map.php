<?php
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include ("./../Views/Controllers/conn.php");
$id_User = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="print.css" media="print" />

        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
       <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  
        <title>Marketing Map </title>



    </head>

    <body >

        <div style="height:100%">
            <div>
                <? include("./BarMenu.php"); ?>
            </div>
            <div class="col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2" >
                <div class="col-md-12 col-lg-12" id="MAP_CONTAINER">
                    <div class="panel panel-primary" >
                        <div class="panel-heading">Dealers Locations</div>
                        <div id="map" class="col-md-12" style="width: 100%; height: 700px;"></div>
                    </div>
                </div>




                <div class="col-md-12 col-lg-12 "  >
                    <div class="panel panel-primary" >
                        <div class="panel-heading print_row">Dealers Details</div>

                        <?
                        $idDealer = 0;
                        $nameDealer = "";

                        $fecha1 = new DateTime();
                        $fecha1->modify('first day of this month');
                        $f1 = $fecha1->format('m-d-Y');

                        $fecha = new DateTime();
                        $fecha->modify('last day of this month');
                        $f2 = $fecha->format('m-d-Y');
                        if (isset($_GET['SelectedD'])) {
                            $idDealer = $_GET['SelectedD'];
                            $sql = "SELECT nombre FROM `dealers` where id=$idDealer";
                            $result = mysqli_query($mysqli,$sql);
                            $row = mysqli_fetch_assoc($result);
                            $nameDealer = $row['nombre'];
                        }
                        ?>
                        <div class="row print_row">
                            <h1 style="padding: 14px !important;"><?= $nameDealer ?></h1>
                        </div>
                        <div class="row print_row">
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Date 1</span>
                                <input id="datepicker"  type="text" class="form-control" placeholder="325511" value="<?= $f1 ?>" />
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Date 2</span>
                                <input id="datepicker2"  type="text" class="form-control" placeholder="" value="<?= $f2 ?>" />
                            </div>
                            <div class="col-md-2 col-lg-2">

                                <button id="btn_submit" class="btn btn-success " style="margin-top: 32px;" onclick="GetReport()">Submit</button>
                            </div>
                        </div>
                        <div class="row" id="Cont"></div>
                    </div>



                </div>
            </div>
        </div>


        <style>
            .label {
                box-sizing:border-box;
                background: #05F24C;
                box-shadow: 2px 2px 4px #333;
                border:5px solid #346FF7;
                height: 20px;
                width: 20px;
                border-radius: 10px;
                -webkit-animation: pulse 1s ease 1s 3;
                -moz-animation: pulse 1s ease 1s 3;
                animation: pulse 1s ease 1s 3;
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <script type="text/javascript">
                                    function DoPrint() {
                                        window.print();
                                    }


                                    function GetReport() {

                                        var id_dealer =<?= $idDealer ?>;
                                        var date1 = $("#datepicker").val();
                                        var date2 = $("#datepicker2").val();
                                        $.post("./Controllers/GetReportDealer.php", {id_dealer, date1, date2}, function (data) {
                                            $("#Cont").empty();
                                            $("#Cont").html(data);
                                        });
                                    }
                                    $("#datepicker").datepicker({dateFormat: 'mm-dd-yy'});
                                    $("#datepicker2").datepicker({dateFormat: 'mm-dd-yy'});

        </script>


        <script type="text/javascript">

            // Initialize and add the map
            function initMap() {
                // The location of Uluru


                var uluru = {lat: 32.2217407, lng: -110.9264832};
                var myLatLng = {lat: 32.2217407, lng: -110.9264832};
                var buylocostajo = {lat: 32.1777513, lng: -110.9763121};
                var labels = 'name Dealer';
                var labelIndex = 0;

                // The map, centered at Uluru
                map = new google.maps.Map(
                        document.getElementById('map'), {zoom: 13, center: uluru});
                // The marker, positioned at Uluru
                //                var marker = new google.maps.Marker({
                //                    position: myLatLng,
                //                    map: map,
                //                    labelOrigin:  new google.maps.Point(50,-15),
                //                    label: {
                //                        text: labels,
                //                        color: "#eb3a44",
                //                        fontSize: "16px",
                //                        fontWeight: "bold"
                //                    }
                //                });
                //
                //                marker.setMap(map);




                var markerIcon = {
                    url: '../img/buylowcostTree.png',
                    scaledSize: new google.maps.Size(30, 30),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(32, 35),
                    labelOrigin: new google.maps.Point(50, -15),
                };

                var markerLabel = 'Buy Low Cost';
                var marker = new google.maps.Marker({
                    position: buylocostajo,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: markerIcon,
                    label: {
                        text: markerLabel,
                        color: "#eb3a44",
                        fontSize: "10px",
                        fontWeight: "bold",

                    }
                });



                marker.addListener('click', function () {
                    alert("click marker");
                });

                var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                var icons = {
                    parking: {
                        icon: iconBase + 'parking_lot_maps.png'
                    },
                    library: {
                        icon: iconBase + 'library_maps.png'
                    },
                    info: {
                        icon: iconBase + 'info-i_maps.png'
                    }
                };



                $.post("./Controllers/GetLocations.php", {}, function (data) {

                    var markers = [];
                    var marklabels = [];
                    var ids = [];

                    for (var cont = 0; cont < data.data.length; cont++) {
                        var lat = Number.parseFloat(data.data[cont].lat);
                        var lon = Number.parseFloat(data.data[cont].lon);
                        marklabels[cont] = data.data[cont].title;
                        var idDealer = data.data[cont].ID;
                        ids[cont] = data.data[cont].ID;

                        var location = {lat: lat, lng: lon};
                        var markerIcon = {
                            url: './../img/IcoAuto.png',
                            scaledSize: new google.maps.Size(25, 25),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(32, 35),
                            labelOrigin: new google.maps.Point(50, -15),
                        };

                        var labelmark = data.data[cont].title;
                        marklabels = data.data[cont].title;

                        markers[cont] = new google.maps.Marker({
                            position: location,
                            map: map,
                            animation: google.maps.Animation.DROP,
                            icon: markerIcon,
                            label: {
                                text: labelmark,
                                color: "#eb3a44",
                                fontSize: "13px",
                                fontWeight: "bold",

                            }
                        });

                        markers[cont].index = idDealer; //
                        google.maps.event.addListener(markers[cont], 'click', function () {

                            window.location.href = "Map.php?SelectedD=" + this.index;

                        });
                        //                          markers[cont].addListener('click', function () {
                        //                           alert(marklabels[cont]);
                        //                          window.location.href = "View_Map.php?SelectedD="+ ids[cont];
                        //                        });
                    }

                });



            }
        </script>

        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4XIWXEWQeTRZC3wunJMzELHYVb83D4VI&callback=initMap">
        </script>


    </body>
</html>
<?
?>
