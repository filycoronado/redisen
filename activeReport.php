<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
        <title> Report</title>

        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }
        </style>
    </head>

    <body >
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();
                $("#datepicker").datepicker();
            });
        </script>


        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>

<?
                   $sql = "SELECT Count(*) as total FROM `UserManagement` where (status_client=1 or status_client=4 or status_client=5 or status_client=8 or status_client=10 or status_client=9)";

                    $result = mysql_query($sql);
                    $r= mysql_fetch_assoc($result);

                   $sql2 = "SELECT Count(*) as total FROM `PoliciesForOldMGTM` where (status_policy=1 or status_policy=4 or status_policy=5 or status_policy=8 or status_policy=10 or status_policy=9)";

                    $result2 = mysql_query($sql2);
                    $r2= mysql_fetch_assoc($result2);
                    $total=$r['total'] + $r2['total'];
?>
            <div class="container" style="max-height: 86% !important; height: 86%; overflow:auto;">
                <div style="text-align: center;">

                    <h3>Actives: <?=$total?></h3>

                </div>
                <div style="height: 5rem;background-color: #1c335c;color: white; text-align: center;     border-radius: 6px 6px 0px 0px;"> <h3 style="padding: 11px;">  Information </h3></div>     




                <div class="table-responsive container" id="contenido-tabla">
                    <?
                    
                 $sql="SELECT nombre,policynumber,agencia, (select user from users where id=id_usurio) as agent   FROM `UserManagement` where (status_client=1 or status_client=4 or status_client=5 or status_client=8 or status_client=10 or status_client=9)";   
                   /*$sql = "SELECT *,us.user as agent, com.nombre as nombrecompany, UserManagement.nombre as nombre FROM `UserManagement`"
        . " join companyes as com on com.id=UserManagement.compania  join  users as us on  UserManagement.id_usurio=us.id "
        . " where (status_client=1 or status_client=4 or status_client=5 or status_client=8 or status_client=10 or status_client=9)";
*/
                    $result = mysql_query($sql);
$sql2="SELECT policynumber, (select nombre from UserManagement where id=id_cliente) as nombre,  (select agencia from UserManagement where id=id_cliente) as agencia ,(select user from users where id_user=id) as agent  FROM `PoliciesForOldMGTM` where (status_policy=1 or status_policy=4 or status_policy=5 or status_policy=8 or status_policy=10 or status_policy=9)";
                  /* $sql2 = "SELECT us.user as agent,com.nombre as companyname, um.nombre as nombre, um.tel as phone , um.mail as mail  ,um.agencia as ag, policies.policynumber as pn  FROM `PoliciesForOldMGTM` as policies join UserManagement as um on policies.id_cliente=um.id "
        . " join companyes as com on com.id=policies.company_id  join users as us on um.id_usurio=us.id  where (policies.status_policy=1 or policies.status_policy=4 or policies.status_policy=5 or policies.status_policy=8 or policies.status_policy=10 or policies.status_policy=9)";
*/
                    $result2 = mysql_query($sql2);
                    ?>
                    <a  href="./ActivesRecordsExcel.php" target="_blank" class="btn btn-primary">Excel</a>
                    <table class="table table-striped" style="border-width: 1px; border-style: solid; background-color: #9c9c9c;">
                        <tbody>
                            <!-- Aplicadas en las filas -->
                            <tr >
                                <th class="cabecera"  COLSPAN=13>Transactions</th> 

                            </tr>
                            <tr>
                                <th >Number</th>   
                                <th>Agent</th>
                                <th >Customer </th>
                                <th >policy Number</th>
                                <th >Agency</th>



                            </tr>

                            <?
                            $cont = 0;
                            while ($r = mysql_fetch_assoc($result)) {
                                $cont++;
                                ?>

                                <tr>
                                    <td><?= $cont ?></td>
                                    <td><?=$r['agent']?></td>
                                    <td><?= $r['nombre']; ?></td>
                                    <td><?= $r['policynumber']; ?></td>
                                    <td><?= $r['agencia']; ?></td>
                                </tr>
                                <?
                            }
                            ?>


                            <?
                            while ($r2 = mysql_fetch_assoc($result2)) {
                                $cont++;
                                ?>

                                <tr>
                                    <td><?= $cont ?></td>
                                    <td><?=$r2['agent']?></td>
                                    <td><?= $r2['nombre']; ?></td>
                                    <td><?= $r2['policynumber']; ?></td>
                                    <td><?= $r2['agencia']; ?></td>
                                </tr>
                                <?
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">Panel Footer</div>
        </div>


    </body>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>



    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
            $("#datepicker").datepicker();
            $("#datefinal").datepicker();
    </script>
</html>