<?php
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id_User = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="datatables/jquery.dataTables.min.js"></script>
        <script src="datatables/dataTables.bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="print.css" media="print" />

        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <script src="./dist/Chart.js-master/dist/Chart.js" type="text/javascript"></script>


        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }
        </style>


    </head>

    <body >
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();

            });

        </script>
        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>


            <div >
                <? ?>

                <div class="col-md-12 col-lg-12 "  >
                    <div class="panel panel-primary" >
                        <div class="panel-heading print_row">Agencies Production</div>

                        <?
                        $idDealer = 0;
                        $nameDealer = "";

                        $fecha1 = new DateTime();
                        $fecha1->modify('first day of this month');
                        $f1 = $fecha1->format('m-d-Y');

                        $fecha = new DateTime();
                        $fecha->modify('last day of this month');
                        $f2 = $fecha->format('m-d-Y');
                        ?>
                        <div class="row print_row">
                            <h1 style="padding: 14px !important;"><?= $nameDealer ?></h1>
                        </div>
                        <div class="row print_row">
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Agency</span>
                                <select id="Agency"  class="form-control" >
                                    <option value="ALL">ALL</option>
                                    <?
                                    $sql = "SELECT * FROM `agency` where id_owner=1";
                                    $result = mysql_query($sql);
                                    while ($row = mysql_fetch_assoc($result)) {
                                        ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['id'] ?></option>
<? } ?>
                                </select>
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Date 1</span>
                                <input id="datepicker"  type="text" class="form-control" placeholder="325511" value="<?= $f1 ?>" />
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Date 2</span>
                                <input id="datepicker2"  type="text" class="form-control" placeholder="" value="<?= $f2 ?>" />
                            </div>
                            <div class="col-md-2 col-lg-2">

                                <button id="btn_submit" class="btn btn-success " style="margin-top: 32px;" onclick="GetReport()">Submit</button>
                                <button id="btn_submit" class="btn btn-success " style="margin-top: 32px;" onclick="DoPrint()">Print</button>
                                
                            </div>
                        </div>
                        <div class="row" id="Cont"></div>
                    </div>



                </div>
            </div>

        </div>
        <style>
            .label {
                box-sizing:border-box;
                background: #05F24C;
                box-shadow: 2px 2px 4px #333;
                border:5px solid #346FF7;
                height: 20px;
                width: 20px;
                border-radius: 10px;
                -webkit-animation: pulse 1s ease 1s 3;
                -moz-animation: pulse 1s ease 1s 3;
                animation: pulse 1s ease 1s 3;
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <script type="text/javascript">
                                    function DoPrint() {
                                        window.print();
                                    }


                                    function GetReport() {

                                        var id_dealer =<?= $idDealer ?>;
                                        var date1 = $("#datepicker").val();
                                        var date2 = $("#datepicker2").val();
                                        var Agency = $("#Agency").val();
                                        $.post("./Views/Controllers/GetCompanyReport.php", {Agency,date1, date2}, function (data) {
                                            $("#Cont").empty();
                                            $("#Cont").html(data);
                                        });
                                    }
                                    $("#datepicker").datepicker({dateFormat: 'mm-dd-yy'});
                                    $("#datepicker2").datepicker({dateFormat: 'mm-dd-yy'});

        </script>






    </body>
</html>
<? ?>
