<?php
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id_User = $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="datatables/jquery.dataTables.min.js"></script>
        <script src="datatables/dataTables.bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="print.css" media="print" />

        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }

            /*stlye card*/
            .square, .btn {
                border-radius: 0px!important;
            }

            /* -- color classes -- */
            .coralbg {
                background-color: #FA396F;
            } 

            .coral {
                color: #FA396f;
            }

            .turqbg {
                background-color: #46D8D2;
            }

            .turq {
                color: #46D8D2;
            }

            .white {
                color: #fff!important;
            }

            /* -- The "User's Menu Container" specific elements. Custom container for the snippet -- */
            div.user-menu-container {
                z-index: 10;
                background-color: #fff;
                margin-top: 20px;
                background-clip: padding-box;
                opacity: 0.97;
                filter: alpha(opacity=97);
                -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
                box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
            }

            div.user-menu-container .btn-lg {
                padding: 0px 12px;
            }

            div.user-menu-container h4 {
                font-weight: 300;
                color: #8b8b8b;
            }

            div.user-menu-container a, div.user-menu-container .btn  {
                transition: 1s ease;
            }

            div.user-menu-container .thumbnail {
                width:100%;
                min-height:200px;
                border: 0px!important;
                padding: 0px;
                border-radius: 0;
                border: 0px!important;
            }

            /* -- Vertical Button Group -- */
            div.user-menu-container .btn-group-vertical {
                display: block;
            }

            div.user-menu-container .btn-group-vertical>a {
                padding: 20px 25px;
                background-color: #46D8D2;
                color: white;
                border-color: #fff;
            }

            div.btn-group-vertical>a:hover {
                color: white;
                border-color: white;
            }

            div.btn-group-vertical>a.active {
                background: #FA396F;
                box-shadow: none;
                color: white;
            }
            /* -- Individual button styles of vertical btn group -- */
            div.user-menu-btns {
                padding-right: 0;
                padding-left: 0;
                padding-bottom: 0;
            }

            div.user-menu-btns div.btn-group-vertical>a.active:after{
                content: '';
                position: absolute;
                left: 100%;
                top: 50%;
                margin-top: -13px;
                border-left: 0;
                border-bottom: 13px solid transparent;
                border-top: 13px solid transparent;
                border-left: 10px solid #46D8D2;
            }
            /* -- The main tab & content styling of the vertical buttons info-- */
            div.user-menu-content {
                color: #323232;
            }

            ul.user-menu-list {
                list-style: none;
                margin-top: 20px;
                margin-bottom: 0;
                padding: 10px;
                border: 1px solid #eee;
            }
            ul.user-menu-list>li {
                padding-bottom: 8px;
                text-align: center;
            }

            div.user-menu div.user-menu-content:not(.active){
                display: none;
            }

            /* -- The btn stylings for the btn icons -- */
            .btn-label {position: relative;left: -12px;display: inline-block;padding: 6px 12px;background: rgba(0,0,0,0.15);border-radius: 3px 0 0 3px;}
            .btn-labeled {padding-top: 0;padding-bottom: 0;}

            /* -- Custom classes for the snippet, won't effect any existing bootstrap classes of your site, but can be reused. -- */

            .user-pad {
                padding: 20px 25px;
            }

            .no-pad {
                padding-right: 0;
                padding-left: 0;
                padding-bottom: 0;
            }

            .user-details {
                background: #eee;
                min-height: 333px;
            }

            .user-image {
                max-height:200px;
                overflow:hidden;
            }

            .overview h3 {
                font-weight: 300;
                margin-top: 15px;
                margin: 10px 0 0 0;
            }

            .overview h4 {
                font-weight: bold!important;
                font-size: 40px;
                margin-top: 0;
            }

            .view {
                position:relative;
                overflow:hidden;
                margin-top: 10px;
            }

            .view p {
                margin-top: 20px;
                margin-bottom: 0;
            }

            .caption {
                position:absolute;
                top:0;
                right:0;
                background: rgba(70, 216, 210, 0.44);
                width:100%;
                height:100%;
                padding:2%;
                display: none;
                text-align:center;
                color:#fff !important;
                z-index:2;
            }

            .caption a {
                padding-right: 10px;
                color: #fff;
            }

            .info {
                display: block;
                padding: 10px;
                background: #eee;
                text-transform: uppercase;
                font-weight: 300;
                text-align: right;
            }

            .info p, .stats p {
                margin-bottom: 0;
            }

            .stats {
                display: block;
                padding: 10px;
                color: white;
            }

            .share-links {
                border: 1px solid #eee;
                padding: 15px;
                margin-top: 15px;
            }

            .square, .btn {
                border-radius: 0px!important;
            }

            /* -- media query for user profile image -- */
            @media (max-width: 767px) {
                .user-image {
                    max-height: 400px;
                }
            }
            .imagelogo{
                height: 371px;
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                background-image: url(./img/logoblc.png); 
            }
        </style>


    </head>

    <body >
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();

            });

        </script>
        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <? include("menu_mgtm_boostrap.php"); ?>
            </nav>


            <div >
                <? ?>
                <div class="col-md-12 col-lg-12" id="Employee">
                    <div class="panel panel-primary" >
                        <div class="panel-heading">Employee Configuration</div>
                        <div class="row print_row">
                            <div class="col-md-2 col-lg-2">
                                <span class="help-block text-muted small-font" >Select Employee</span>
                                <select id="empployeesel" class="form-control"  >
                                    <?
                                    $sql = "SELECT id,user FROM `users` where habilitado=1 and (nivel=1 or nivel=2 or nivel=5) and (agencia =101 or agencia=103 or agencia=104 or agencia=105)";
                                    $result = mysql_query($sql);
                                    while ($row = mysql_fetch_assoc($result)) {
                                        ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['user'] ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <span class="help-block text-muted small-font" >Total (NB)</span>
                                <input id="NB"  type="number" class="form-control" placeholder="" value="" />
                            </div>
                            <div class="col-md-1 col-lg-1">
                                <span class="help-block text-muted small-font" >Date</span>
                                <input id="datepicker2"  type="text" class="form-control" placeholder="mm-dd-YYYY" value="" />
                            </div>
                            <div class="col-md-2 col-lg-2">

                                <button id="btn_submit" class="btn btn-success " style="margin-top: 32px;" onclick="AsignarEmployee()">Submit</button>
                            </div>
                        </div>
                    </div>



                </div>




            </div>

        </div>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script>

                                    $("#datepicker2").datepicker({dateFormat: 'mm-dd-yy'});
                                    function AsignarEmployee() {
                                        var date = $("#datepicker2").val();
                                        var NB = $("#NB").val();
                                        var empployeesel = $("#empployeesel").val();
                                        $.post("./Views/Controllers/AsignarEmployee.php", {date, NB, empployeesel}, function (data) {
                                            alert(data.message);
                                        });
                                    }
        </script> 
    </body>
</html>
<?
?>
