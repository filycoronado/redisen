app.controller('ClientSearchController', ['$rootScope', '$scope', '$filter', '$routeParams', '$http', '$timeout', 'UserService', 'PageService', function ($rootScope, $scope, $filter, $routeParams, $http, $timeout, UserService, PageService) {


    $scope.vm = this;

    $scope.vm.dummyItems = []; // dummy array of items to be paged
    $scope.vm.pager = {};
    $scope.vm.setPage = setPage;



    $scope.User = JSON.parse(localStorage.getItem('user_mgtm'));

    $scope.Show = false;
    $scope.show_action = false;




    if ($scope.User.id == 1) {
        $scope.Show = !$scope.Show;
        $scope.show_action = !$scope.show_action;
    }



    $scope.customers = [];
  

    $scope.agencies = [
        {
            "number": 101,
        },
        {
            "number": 102,
        },
        {
            "number": 103,
        },
        {
            "number": 104,
        },
        {
            "number": 105,
        }
    ];

    $scope.transaction = {
        customer: "weew",
        user: "",
        agency: ""
    }
    $scope.filters = {
        phone: "",
        name_client: ""
    };
  
    $scope.filter = false;



    $scope.getCustomers = function () {
        $http.post(UserService.url + '/GetClients.php', {  filters: $scope.filters }).then(function success(response) {
            if (response.data.status == "success") {
                alert(response.data.msj);
                $scope.customers = response.data.list;
                $scope.vm.dummyItems = response.data.list;
                initController();
            } else {
                alert(response.data.msj);
            }
        });
    };











    $scope.applyFilter = function () {
        $scope.filter = true;
        $scope.getCustomers();
    };
    $scope.getCustomers();







    function setPage(page) {
        if (page < 1 || page > $scope.vm.pager.totalPages) {
            return;
        }
        // get pager object from service
        $scope.vm.pager = PageService.GetPager($scope.vm.dummyItems.length, page,150);
      

        // get current page of items
        $scope.vm.items = $scope.vm.dummyItems.slice($scope.vm.pager.startIndex, $scope.vm.pager.endIndex + 1);
    }
    function initController() {
        // initialize to page 1
        $scope.vm.setPage(1);
    }


}]);
