app.controller('LoginController', ['$rootScope', '$scope', '$filter', '$routeParams', '$http', '$timeout', 'UserService', function ($rootScope, $scope, $filter, $routeParams, $http, $timeout, UserService) {

    $scope.loging = {
        agency: "101"
    };
    $scope.agencies = [
        {
            "number": 101,
        },
        {
            "number": 102,
        },
        {
            "number": 103,
        },
        {
            "number": 104,
        },
        {
            "number": 105,
        }
    ];

    $scope.Login = function () {
        $http.post(UserService.url + '/Login.php', { user: $scope.loging }).then(function success(response) {
            if (response.data.status == "success") {
                alert(response.data.msj);
                var loc = response.data.location;
                window.localStorage.setItem('user_mgtm', JSON.stringify(response.data.user));
                window.localStorage.setItem('mgtm_permits', JSON.stringify(response.data.permits));
                //console.log(response.data);
                document.location.replace('./App/Patrials/' + loc + ".php");
            } else {
                alert(response.data.msj);
            }
        });
    };


}]);
