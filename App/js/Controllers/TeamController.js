app.controller('TeamController', ['$rootScope', '$scope', '$filter', '$routeParams', '$http', '$timeout', 'UserService', function ($rootScope, $scope, $filter, $routeParams, $http, $timeout, UserService) {
    $scope.CurrentMonth = {
        total: 150,
        list: [
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 }
        ]
    };
    $scope.last_month = {
        total: 150,
        list: [
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 },
            { agent: "ALBERTAGENT", goals: 100, currentData: 50, percentaje: 50 }
        ]
    };

    $scope.get_data = function () {
        $http.get(UserService.url + '/Get_progress_team.php', {}).then(function success(response) {
            if (response.data.status == "success") {
                $scope.CurrentMonth.list = response.data.current;
                $scope.last_month.list = response.data.last;
                $scope.last_month.total = response.data.totalLast;
                $scope.CurrentMonth.total = response.data.totalCurrent;
            }
        });
    };
    $scope.get_data();

}]);
