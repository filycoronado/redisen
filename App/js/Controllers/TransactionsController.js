app.controller('TransactionsController', ['$rootScope', '$scope', '$filter', '$routeParams', '$http', '$timeout', 'UserService', 'PageService', function ($rootScope, $scope, $filter, $routeParams, $http, $timeout, UserService, PageService) {


    $scope.vm = this;

    $scope.vm.dummyItems = []; // dummy array of items to be paged
    $scope.vm.pager = {};
    $scope.vm.setPage = setPage;



    $scope.User = JSON.parse(localStorage.getItem('user_mgtm'));

    $scope.Show = false;
    $scope.show_action = false;




    if ($scope.User.id == 1) {
        $scope.Show = !$scope.Show;
        $scope.show_action = !$scope.show_action;
    }



    $scope.Transactions = [];
    $scope.cashiers = [];
    $scope.users = [];
    $scope.dealers = [];

    $scope.agencies = [
        {
            "number": 101,
        },
        {
            "number": 102,
        },
        {
            "number": 103,
        },
        {
            "number": 104,
        },
        {
            "number": 105,
        }
    ];

    $scope.transaction = {
        customer: "weew",
        user: "",
        agency: ""
    }
    $scope.filters = {
        agency: "101",
        currentDate: ""
    };
    $scope.filters.agency = $scope.User.agencia;
    $scope.filter = false;



    $scope.getTransactions = function () {
        $http.post(UserService.url + '/Get_Transactions.php', { id: $scope.User.id, filter: $scope.filter, filters: $scope.filters }).then(function success(response) {
            if (response.data.status == "success") {
                alert(response.data.msj);
                $scope.Transactions = response.data.list;
                $scope.vm.dummyItems = response.data.list;
                initController();
            } else {
                alert(response.data.msj);
            }
        });
    };

    $scope.getCashiers = function () {
        $http.post(UserService.url + '/get_user_aviable.php', { type: "cashier" }).then(function success(response) {
            if (response.data.status == "success") {

                $scope.cashiers = response.data.list;

            } else {
                //   alert(response.data.msj);
            }

        });
    };

    $scope.getUsers = function () {
        $http.post(UserService.url + '/get_user_aviable.php', { type: "users" }).then(function success(response) {
            if (response.data.status == "success") {

                $scope.users = response.data.list;
            } else {
                // alert(response.data.msj);
            }
        });
    };
    $scope.getDealership = function () {
        $http.post(UserService.url + '/Get_Dealership.php', { type: "users" }).then(function success(response) {
            if (response.data.status == "success") {

                $scope.dealers = response.data.list;
            } else {
                //   alert(response.data.msj);
            }
        });
    };

    $scope.get_pay_methd = function (item) {
        if (item == 1) {
            return "CASH";
        } else if (item == 2) {
            return "CARD";
        } else if (item == 3) {
            return "CARD DIRECT COMPANY";
        }
    };





    $scope.applyFilter = function () {
        $scope.filter = true;
        $scope.getTransactions();
    };


    $scope.getCashierFilter = function (id) {
        let cashierid = parseInt(id);
        let Cashier = "No Cashier";
        angular.forEach($scope.users, function (value, key) {
            if (cashierid == parseInt(value.id)) {
                Cashier = value.user;
            }
        });
        return Cashier;
    };


    $scope.getCashiers();
    $scope.getUsers();
    $scope.getTransactions();







    function setPage(page) {
        if (page < 1 || page > $scope.vm.pager.totalPages) {
            return;
        }
        // get pager object from service
        $scope.vm.pager = PageService.GetPager($scope.vm.dummyItems.length, page);
        console.log($scope.vm.pager);

        // get current page of items
        $scope.vm.items = $scope.vm.dummyItems.slice($scope.vm.pager.startIndex, $scope.vm.pager.endIndex + 1);
    }
    function initController() {
        // initialize to page 1
        $scope.vm.setPage(1);
    }


}]);
