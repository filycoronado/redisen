<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Transactions</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./../../css/Redisen.css">


    <script src="./../js/angular1.7.2.js"></script>
    <script data-require="angular-translate@*" data-semver="2.5.0" src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.5.0/angular-translate.js"></script>
    <script src="https://code.angularjs.org/1.6.0/angular-route.min.js"></script>
    <script src="https://code.angularjs.org/1.6.9/angular-cookies.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.6.3/mask.js"></script>
    <script src="./../../bower_components/lodash/dist/lodash.js"></script>

</head>

<body>
    <? include("./components/NavBar.php"); ?>
    <div class="containder-fuild" ng-controller="TransactionsController">

        <div class="container">
            <div class="row p-5">
                <div class="col-md-12">

                    <form ng-submit="applyFilter()" class="p-2" ng-if="Show">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group">

                                    <select class="form-control" ng-model="filters.agency">
                                        <option ng-repeat="a in agencies" value="{{a.number}}">{{a.number}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input model-view-value="true" name="date" type="text" class="form-control" placeholder="MM-DD-YYYY" ng-model="filters.currentDate" ui-mask="99-99-9999" ui-mask-placeholder-char="-">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-danger mx-auto " type="submit">Apply</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-11 mx-auto col-12">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table  table-borderless table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Company</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">User</th>
                                        <th scope="col">Cashier</th>
                                        <th scope="col">Delivery</th>
                                        <th scope="col">Insurance</th>
                                        <th scope="col">Office</th>
                                        <th scope="col">RoadSide</th>
                                        <th scope="col">Method Payment</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in vm.items">
                                        <th scope="row">{{$index+1}}</th>
                                        <td>{{item.companyname | uppercase}}</td>
                                        <td> <i class="fa fa-id-card-o ico_actions" aria-hidden="true"></i> {{item.nameclient | uppercase}}</td>
                                        <td>{{item.user | uppercase}}</td>
                                        <td>{{getCashierFilter(item.id_cashier) | uppercase }}</td>
                                        <td>{{item.nameentrega | uppercase}}</td>
                                        <td>${{item.feeoffice}}</td>
                                        <td>${{item.feeforaz}}</td>
                                        <td>${{item.feecompany}}</td>
                                        <td>{{get_pay_methd(item.typeaz)}} </td>
                                        <td>
                                            <i ng-if="show_action" class="fa fa-file-pdf-o m-1 ico_actions" aria-hidden="true"></i>

                                            <i ng-if="show_action" class="fa fa-pencil-square m-1 ico_actions " aria-hidden="true" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"></i>
                                            <i ng-if="show_action" class="fa fa-minus-square m-1 ico_actions" aria-hidden="true"></i>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <nav>
                        <!-- pager -->
                        <ul class="pagination" ng-if="vm.pager.pages.length" class="pagination">
                            <li class="page-item" ng-class="{disabled:vm.pager.currentPage === 1}">
                                <a class="page-link" ng-click="vm.setPage(1)">First</a>
                            </li>
                            <li class="page-item" ng-class="{disabled:vm.pager.currentPage === 1}">
                                <a class="page-link" ng-click="vm.setPage(vm.pager.currentPage - 1)">Previous</a>
                            </li>
                            <li class="page-item" ng-repeat="page in vm.pager.pages" ng-class="{active:vm.pager.currentPage === page}">
                                <a class="page-link" ng-click="vm.setPage(page)">{{page}}</a>
                            </li>
                            <li class="page-item" ng-class="{disabled:vm.pager.currentPage === vm.pager.totalPages}">
                                <a class="page-link" ng-click="vm.setPage(vm.pager.currentPage + 1)">Next</a>
                            </li>
                            <li class="page-item" ng-class="{disabled:vm.pager.currentPage === vm.pager.totalPages}">
                                <a class="page-link" ng-click="vm.setPage(vm.pager.totalPages)">Last</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Transaction</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row p-2">
                            <div class="col-md-12">

                                <form ng-submit="updateTransaction()" class="p-2">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Customer Name</label>
                                            <div class="input-group">

                                                <input name="customer" type="text" class="form-control" placeholder="Full Name" ng-model="transaction.customer">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Responsible User</label>
                                            <div class="input-group">
                                                <select class="form-control" ng-model="transaction.user">
                                                    <option ng-repeat="user in users" value="{{user.id}}">{{user.name}} {{user.apellido}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Date</label>
                                            <div class="input-group">
                                                <input model-view-value="true" name="date" type="text" class="form-control" placeholder="MM-DD-YYYY" ng-model="filters.currentDate" ui-mask="99-99-9999" ui-mask-placeholder-char="-">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Insurance </label>
                                            <div class="input-group">
                                                <input name="Insurance" type="number" class="form-control" placeholder="0.00" ng-model="transaction.ins">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Roadside </label>
                                            <div class="input-group">

                                                <input name="Roadside" type="number" class="form-control" placeholder="0.00" ng-model="transaction.roadside">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Dealer </label>
                                            <div class="input-group">
                                                <input name="Dealer" type="number" class="form-control" placeholder="0.00" ng-model="transaction.dealer">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Cashier</label>
                                            <div class="input-group">
                                                <select class="form-control" ng-model="transaction.agency">
                                                    <option ng-repeat="a in cashiers" value="{{a.id}}">{{a.name}} {{a.apellido}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Agency</label>
                                            <div class="input-group">
                                                <select class="form-control" ng-model="transaction.agency">
                                                    <option ng-repeat="a in agencies" value="{{a.number}}">Agency-{{a.number}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Dealership</label>
                                            <div class="input-group">
                                                <select class="form-control" ng-model="transaction.agency">
                                                    <option ng-repeat="a in dealers" value="{{a.id}}">{{a.name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 p-2 text-center ">
                                            <button class="btn btn-danger mx-auto " type="submit">Apply</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>




    </div>
    <? include("./components/Footer.php"); ?>
</body>


<script src="./../js/App.js"></script>
<script src="./../js/Controllers/LoginController.js"></script>
<script src="./../js/Controllers/TransactionsController.js"></script>