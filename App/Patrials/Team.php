<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Team</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./../../css/Redisen.css">


    <script src="./../js/angular1.7.2.js"></script>
    <script data-require="angular-translate@*" data-semver="2.5.0" src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.5.0/angular-translate.js"></script>
    <script src="https://code.angularjs.org/1.6.0/angular-route.min.js"></script>
    <script src="https://code.angularjs.org/1.6.9/angular-cookies.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.6.3/mask.js"></script>

</head>

<body>
    <? include("./components/NavBar.php"); ?>
    <div class="containder-fuild" ng-controller="TeamController">
        <div class="row delte_maring">
            <div class="col-md-9 mx-auto col-12 p-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row p-1">
                            <div class="col-md-12 col-12 title_team">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <label> CURENT MONTH</label>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <label> TOTAL SALES:{{CurrentMonth.total}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-12 container_item">
                                <div class="row" ng-repeat="item in CurrentMonth.list">
                                    <div class="col-md-12 col-12">
                                        <label class="item_agent">{{item.user}} </label><label class="item_nb">- New Business({{item.nb}}) </label>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width:{{item.nb}}%;" aria-valuenow="{{item.nb}}" aria-valuemin="0" aria-valuemax="100">{{item.nb}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

           
                    <div class="col-md-6">
                        <div class="row p-1">
                            <div class="col-md-12 col-12 title_team">
                                <div class="row">
                                    <div class="col-md-6 col-6">
                                        <label> LAST MONTH</label>
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <label> TOTAL SALES:{{last_month.total}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-12 container_item">
                                <div class="row" ng-repeat="item in last_month.list">
                                    <div class="col-md-12 col-12">
                                        <label class="item_agent">{{item.user}} </label><label class="item_nb">- New Business({{item.nb}}) </label>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width:{{item.nb}}%;" aria-valuenow="{{item.nb}}" aria-valuemin="0" aria-valuemax="100">{{item.nb}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <? include("./components/Footer.php"); ?>
</body>
<script src="./../js/App.js"></script>
<script src="./../js/Controllers/TeamController.js"></script>

</html>