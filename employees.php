<?
session_start();
if(isset($_SESSION['nivel'])){
  if($_SESSION['nivel']>2){header('Location: principal22.php');}
} else{
 header('Location: index.php');
}
include("inc/dbconnection.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="datatables/jquery.dataTables.min.js"></script>
  <script src="datatables/dataTables.bootstrap.min.js"></script>
  <script src="sweetalert2/sweetalert2.min.js"></script>
  <script src="js/moment.min.js"></script>
  <script src="js/datetime-moment.js"></script>

  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
  <link rel="stylesheet" href="css/toogle-switch.css">
  <title> </title>
  <style type="text/css">
    html,
    body {
      height: 100%;
    }

    .navbar-default {
      background-color: black !important;
      border-color: white !important;
    }

    .navbar-default .navbar-nav>li>a:focus,
    .navbar-default .navbar-nav>li>a:hover {
      color: white !important;
      background-color: #555555 !important;
    }

    .fondo {
      background-image: url(img/logo.svg);
      background-size: contain;
      height: 50px;
      width: 133px;
      background-repeat: no-repeat;
    }

    .cabecera {
      text-align: center !important;
    }

    #paginacion {
      text-align: center;
    }

    table.dataTable td {
      padding: 3px 10px;
      width: 1px;
      white-space: nowrap;
    }

    .dataTables_wrapper>.row {
      margin-right: -15px;
      margin-left: -15px;
      padding: 0px !important;
    }

    .loader {
      position: absolute;
      left: 50%;
      top: 50%;
      border: 16px solid #f3f3f3;
      /* Light grey */
      border-top: 16px solid #3498db;
      /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      margin: -60px 0 0 -60px;
      animation: spin 2s linear infinite;
    }

    @keyframes spin {
      0% {
        transform: rotate(0deg);
      }

      100% {
        transform: rotate(360deg);
      }
    }
  </style>
</head>

<body>
  <div style="height:100%">
    <nav class="navbar navbar-default" role="navigation">
      <?include("menu_mgtm_boostrap.php");?>
    </nav>

    <div class="container" style="max-height: 87% !important; height: 87%; overflow-y: auto;">

      <h3 style="margin-top: 0;">Employees</h3> <a class="btn btn-success" href="employees_edit.php?id=0"> Add</a>
      <div class="loader"></div>
      <div class="table-responsive" id="contenido-tabla" style="overflow-x: hidden; height: 0;">
        <table id="example1" class="table table-striped" cellspacing="0" width="100%" style="background-color: #9c9c9c;">
          <thead>
            <tr>
              <th></th>
              <th>ID</th>
              <th>User</th>
              <th>Name</th>
              <th>Agency</th>
              <th>Level</th>
              <!--<th>Active</th>-->
            </tr>
          </thead>
          <tbody>
            <?php
            $Sql = "SELECT `users`.* FROM `users` WHERE `users`.`id` != 0 AND (`users`.`nivel`=1 or `users`.`nivel`=2 or `users`.`nivel`=5) AND `users`.`habilitado` = 1 AND (`users`.`agencia`=101 or `users`.`agencia`=103 or `users`.`agencia`=104 OR `users`.`agencia`=105 )   ORDER BY `users`.`id` ASC";
            $result = mysql_query($Sql);
            while ($row = mysql_fetch_assoc($result)) {
              $fecha = date("m/d/Y", strtotime($row['fechaalta']));
              $nivel = "-";
              switch ($row['nivel']) {
                case 2:
                  $nivel = "Admin";
                  break;
                case 5:
                  $nivel = "Agent";
                  break;
              }
            ?>
              <tr data-id="<?= $row['id'] ?>">
                <td class="actions">
                  <i <?if($row["visible"]==1){?> class="glyphicon glyphicon-eye-open"
                    <?}else{?>class="glyphicon glyphicon-eye-close"
                    <?}?> style="font-size: 16px;cursor:pointer;" title="visible" onclick="ChangeVisible(<?= $row['visible'] ?>,<?= $row['id'] ?>);"></i>
                  <?if($_SESSION['id']==1 || $_SESSION['id']==2127){?> <a href="employees_goals.php?id=<?= $row['id'] ?>"><i class="glyphicon glyphicon-flag" style="font-size: 16px;cursor:pointer;" title="Goals"></i></a>
                  <?}?>
                  <a href="employees_notes.php?id=<?= $row['id'] ?>"><i class="glyphicon glyphicon-book" style="font-size: 16px;cursor:pointer;" title="Notes"></i></a>
                  <a href="employees_edit.php?id=<?= $row['id'] ?>"><i class="glyphicon glyphicon-edit edit" style="font-size: 16px;cursor:pointer;" title="Edit"></i></a>
                  <? if( ($_SESSION['nivel']==1) && $_SESSION['can_delete']==1){ ?>
                  <i class="glyphicon glyphicon-trash delete" style="font-size: 16px;cursor:pointer;" title="Delete"></i>
                  <?}?>
                </td>
                <td><?= $row['id'] ?></td>
                <td><?= strtoupper($row['user']) ?></td>
                <td><?= $row['name'] . ' ' . $row['apellido'] ?></td>
                <td><?= $row['agencia'] ?></td>
                <td><?= $nivel ?></td>
                <!--<td style="text-align: center;">
                <label class="switch">
                  <input type="checkbox"<?= $row['habilitado'] == 1 ? ' checked' : '' ?>>
                  <span class="slider round"></span>
                </label>
              </td>-->
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer" style="bottom: 0; position: absolute; width: 100%;">Panel Footer<br /></div>
  </div>

  <script type="text/javascript">
    var data = <?= json_encode($rows) ?>;

    $.fn.dataTable.moment('MM/DD/YYYY');
    var table = $('#example1').DataTable({
      "responsive": true,
      "order": [
        [1, "asc"]
      ],
      "columnDefs": [{
          "sortable": false,
          "width": "5%",
          "targets": 0
        },
        {
          "width": "5%",
          "targets": 1
        },
        {
          "sortable": false,
          "width": "5%",
          "targets": -1
        },
      ]
    });
    $('.loader').hide();
    $('#contenido-tabla').css('height', 'auto');


    $('#example1 tbody').on('click', 'i.delete', function() {
      var id = $(this).parents('tr').attr("data-id");
      var row = table.row($(this).parents('tr'));
      swal({
        title: '¿Borrar registro?',
        text: "El registro se borrara permanentemente!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        cancelButtonText: 'Cancelar',
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
      }).then(function() {
        $.ajax({
          url: "employees_delete.php",
          type: "POST",
          data: {
            id: id
          },
          success: function(data) {
            var resp = jQuery.parseJSON(data);
            if (resp.status == "success") {
              $("#number_leads").text(resp.contleads);
              row.remove().draw();
              swal(
                'Deleted!', 'El registro ha sido borrado.', 'success');
            } else {
              swal('Error!', 'Ocurrio un problema al borrar el registro.', 'error');
            }

          },
          error: function(xhr, ajaxOptions, thrownError) {
            swal('Error!', 'Problema de conexion.', 'error');
          }
        });
      });
    });

    $('#example1 tbody').on('click', 'i.view', function() {
      var row = $(this).parents('tr');
      var id = row.attr("data-id");
      $.ajax({
        url: "Lead_viewed.php",
        type: "POST",
        data: {
          id: id
        },
        success: function(data) {
          var resp = jQuery.parseJSON(data);
          if (resp.status == "success") {
            $("#number_leads").text(resp.contleads);
            $("#view-" + id).remove();
          } else {
            swal('Error!', 'Ocurrio un problema.', 'error');
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          swal('Error!', 'Problema de conexion.', 'error');
        }
      });
    });
    $('[data-toggle="tooltip"]').tooltip();

    function ChangeVisible(status, id) {
      $.post("./Views/Controllers/UpdateStatusCustomer.php", {
        status,
        id
      }, function(data) {
        if (data.status == "success") {
          alert(data.message);

        } else {
          alert(data.message);
        }
      });
    }
  </script>
  <script>
    $(document).ready(function() {

    });
  </script>
</body>

</html>