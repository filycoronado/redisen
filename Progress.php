<?php
session_start();
if (isset($_SESSION['nivel'])) {
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");

$id_User = $_SESSION['id'];

$today = date("Y-m-d");

$fecha1 = new DateTime();
$fecha1->modify('first day of this month');
$f1 = $fecha1->format('Y-m-d');

$fecha = new DateTime();
$fecha->modify('last day of this month');
$f2 = $fecha->format('Y-m-d');

$sqlADCS = "SELECT count(*) as total  FROM `ADC_NB` where id_user=$id_User and date BETWEEN '$f1' and '$f2' and habilitado=1";
$result = mysql_query($sqlADCS);
$r = mysql_fetch_assoc($result);
$totalADCS = $r['total'];
$perocentajeADC = (($totalADCS * 100) / 24);


//DP EFT
$sqlDP = "SELECT COUNT(*) as totalDP FROM `cutvone` where  id_usuario=$id_User and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=5;";
$result2 = mysql_query($sqlDP);
$r2 = mysql_fetch_assoc($result2);
$toalDP = $r2['totalDP'];



//DP
$sqlDPNOEFT = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_User and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=1;";
$result3 = mysql_query($sqlDPNOEFT);
$r3 = mysql_fetch_assoc($result3);
$toalDPNOEFT = $r3['totalDPNOEFT'];


//Pay inFulñl
$sqlPayinFull = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_User and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=10;";
$result4 = mysql_query($sqlPayinFull);
$r4 = mysql_fetch_assoc($result4);
$toalPIN = $r4['totalDPNOEFT'];



$realTotal = $toalDP + $toalDPNOEFT + $toalPIN;


$perocentajeDP = (($toalDP * 100) / $realTotal);
$porcentajeNoEFTDP = (($toalDPNOEFT * 100) / $realTotal);
$porcentajePIN = (($toalPIN * 100) / $realTotal);

$perocentajeDP = number_format((float) $perocentajeDP, 2, '.', '');
$porcentajeNoEFTDP = number_format((float) $porcentajeNoEFTDP, 2, '.', '');
$porcentajePIN = number_format((float) $porcentajePIN, 2, '.', '');

$classPIN = "";
if ($porcentajePIN > 80) {
    $classPIN = "success";
} else if ($porcentajePIN > 50 && $porcentajePIN <= 80) {
    $classPIN = "warning";
} else if ($porcentajePIN <= 20) {

    $classPIN = "danger";
}

$classDP = "";
if ($porcentajeNoEFTDP > 80) {
    $classDP = "success";
} else if ($porcentajeNoEFTDP > 50 && $porcentajeNoEFTDP <= 80) {
    $classDP = "warning";
} else if ($porcentajeNoEFTDP <= 20) {

    $classDP = "danger";
}

$classDPEFT = "";
if ($perocentajeDP > 80) {
    $classDPEFT = "success";
} else if ($perocentajeDP > 50 && $perocentajeDP <= 80) {
    $classDPEFT = "warning";
} else if ($perocentajeDP <= 20) {

    $classDPEFT = "danger";
}


$classADC = "";
if ($perocentajeADC > 80) {
    $classADC = "success";
} else if ($perocentajeADC > 20 && $perocentajeADC <= 50) {
    $classADC = "warning";
} else if ($perocentajeADC <= 20) {

    $classADC = "danger";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="datatables/jquery.dataTables.min.js"></script>
    <script src="datatables/dataTables.bootstrap.min.js"></script>
    <script src="sweetalert2/sweetalert2.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="./css/Redisen.css">


    <title>Team </title>



</head>

<body>
    <? include("./Patrials/components/NavBar.php"); ?>
    <div style="height:100%">


        <?
            $userEM = "";
            $total_NB = "";
            $sqlEmployee = "SELECT EM.total_NB, us.name, us.apellido, us.photoimage FROM `Employee_of_month` as EM JOIN users as us on us.id=EM.id_user where EM.fecha BETWEEN '$f1' and '$f2' and us.visible=1 ";

            $resultEM = mysql_query($sqlEmployee);

            $rowEM = mysql_fetch_assoc($resultEM);
            $name = strtoupper($rowEM['name']);

            $show = true;
            if ($name == "") {
                $show = false;
            }
            ?>
        <? if ($show == true) { ?>


        <?
                                $flag = true;
                                if (isset($_GET['employe_id']) || $flag == true) {
                                  //  include './Employee_Month.php';
                                }
                                ?>

        <? } ?>
        <div>
            <? ?>
            <div class="col-md-6">
                Dates: <?= $f1 ?>/<?= $f2 ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">Global</div>
                    <div class="panel-body">

                        <?
                            $users = array();

                            $sql = "SELECT * FROM `users` where (agencia=101 or  agencia=102   or agencia=103 or  agencia=104 or agencia=105) AND (nivel=1 or nivel=2 or nivel=5) and habilitado =1 and visible=1";
                            $result = mysql_query($sql);
                            $totalPuser = 0;
                            $Aux = 0;
                            $grandTotal = 0;
                            while ($r = mysql_fetch_assoc($result)) {
                                $arraPerUser = array();
                                $totalPin2 = getPIN($r['id'], $f1, $f2);
                                $totaleft2 = getDPeft($r['id'], $f1, $f2);
                                $totaldp2 = getDP($r['id'], $f1, $f2);
                                $totalPuser += $totalPin2 + $totaleft2 + $totaldp2;
                                $grandTotal += $totalPuser;




                                $perocentajeDP2 = (($totaleft2 * 100) / $totalPuser);
                                $porcentajeNoEFTDP2 = (($totaldp2 * 100) / $totalPuser);
                                $porcentajePIN2 = (($totalPin2 * 100) / $totalPuser);

                                $perocentajeDP2 = number_format((float) $perocentajeDP2, 2, '.', '');
                                $porcentajeNoEFTDP2 = number_format((float) $porcentajeNoEFTDP2, 2, '.', '');
                                $porcentajePIN2 = number_format((float) $porcentajePIN2, 2, '.', '');

                                array_push($arraPerUser, $totalPuser, $r['user'], $perocentajeDP2, $porcentajeNoEFTDP2, $porcentajePIN2);
                                array_push($users, $arraPerUser)
                                ?>

                        <?
                                $totalPuser = 0;
                                $perocentajeDP2 = 0;
                                $porcentajeNoEFTDP2 = 0;
                                $porcentajePIN2 = 0;
                                $perocentajeDP2 = 0;
                                $porcentajeNoEFTDP2 = 0;
                                $porcentajePIN2 = 0;
                                $arraPerUser = null;
                            }


                            foreach ($users as $key => $row) {
                                $aux[$key] = $row[0];
                            }
                            array_multisort($aux, SORT_DESC, $users);
                            $cont = 0;
                            ?>
                        <h1>Grand Total (<?= $grandTotal ?>)</h1>
                        <?
                            foreach ($users as $key => $row) {
                                $cont++;
                                ?>

                        <h3> #<?= $cont ?> <?= $row[1] ?></h3>
                        <h4>Total New Business:(<?= $row[0] ?>)</h4>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?= $row[2] ?>%">
                                <?= $row[2] ?> % NB EFT
                            </div>
                            <div class="progress-bar progress-bar-warning" role="progressbar" style="width:<?= $row[3] ?>%">
                                <?= $row[3] ?> % Down Payment
                            </div>
                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:<?= $row[4] ?>%">
                                <?= $row[4] ?> % Pay In Full
                            </div>
                        </div>
                        <?
                            }
                            ?>




                    </div>

                </div>



            </div>

            <?
//last month calc here

                $fecha = date("Y-m-d");
                $nuevafecha = strtotime('-1 month', strtotime($fecha));
                $today = date('Y-m-d', $nuevafecha);

                $fecha1 = new DateTime($today);
                $fecha1->modify('first day of this month');
                $f1 = $fecha1->format('Y-m-d');

                $fecha = new DateTime($today);
                $fecha->modify('last day of this month');
                $f2 = $fecha->format('Y-m-d');

                $sqlADCS = "SELECT count(*) as total  FROM `ADC_NB` where id_user=$id_User and date BETWEEN '$f1' and '$f2' and habilitado=1";
                $result = mysql_query($sqlADCS);
                $r = mysql_fetch_assoc($result);
                $totalADCS = $r['total'];
                $perocentajeADC = (($totalADCS * 100) / 24);


//DP EFT
                $sqlDP = "SELECT COUNT(*) as totalDP FROM `cutvone` where  id_usuario=$id_User and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=5;";
                $result2 = mysql_query($sqlDP);
                $r2 = mysql_fetch_assoc($result2);
                $toalDP = $r2['totalDP'];



//DP
                $sqlDPNOEFT = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_User and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=1;";
                $result3 = mysql_query($sqlDPNOEFT);
                $r3 = mysql_fetch_assoc($result3);
                $toalDPNOEFT = $r3['totalDPNOEFT'];


//Pay inFulñl
                $sqlPayinFull = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_User and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=10;";
                $result4 = mysql_query($sqlPayinFull);
                $r4 = mysql_fetch_assoc($result4);
                $toalPIN = $r4['totalDPNOEFT'];



                $realTotal = $toalDP + $toalDPNOEFT + $toalPIN;


                $perocentajeDP = (($toalDP * 100) / $realTotal);
                $porcentajeNoEFTDP = (($toalDPNOEFT * 100) / $realTotal);
                $porcentajePIN = (($toalPIN * 100) / $realTotal);

                $perocentajeDP = number_format((float) $perocentajeDP, 2, '.', '');
                $porcentajeNoEFTDP = number_format((float) $porcentajeNoEFTDP, 2, '.', '');
                $porcentajePIN = number_format((float) $porcentajePIN, 2, '.', '');

                $classPIN = "";
                if ($porcentajePIN > 80) {
                    $classPIN = "success";
                } else if ($porcentajePIN > 50 && $porcentajePIN <= 80) {
                    $classPIN = "warning";
                } else if ($porcentajePIN <= 20) {

                    $classPIN = "danger";
                }

                $classDP = "";
                if ($porcentajeNoEFTDP > 80) {
                    $classDP = "success";
                } else if ($porcentajeNoEFTDP > 50 && $porcentajeNoEFTDP <= 80) {
                    $classDP = "warning";
                } else if ($porcentajeNoEFTDP <= 20) {

                    $classDP = "danger";
                }

                $classDPEFT = "";
                if ($perocentajeDP > 80) {
                    $classDPEFT = "success";
                } else if ($perocentajeDP > 50 && $perocentajeDP <= 80) {
                    $classDPEFT = "warning";
                } else if ($perocentajeDP <= 20) {

                    $classDPEFT = "danger";
                }


                $classADC = "";
                if ($perocentajeADC > 80) {
                    $classADC = "success";
                } else if ($perocentajeADC > 20 && $perocentajeADC <= 50) {
                    $classADC = "warning";
                } else if ($perocentajeADC <= 20) {

                    $classADC = "danger";
                }
                ?>
            <div class="col-md-6">
                Dates: <?= $f1 ?>/<?= $f2 ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">Last Month</div>
                    <div class="panel-body">

                        <?
                            $users = array();

                            $sql = "SELECT * FROM `users` where (agencia=101 or agencia=103 or  agencia=104 or agencia=105) AND (nivel=1 or nivel=2 or nivel=5) and habilitado =1 and visible=1";
                            $result = mysql_query($sql);
                            $totalPuser = 0;
                            $Aux = 0;
                            $grandTotal = 0;
                            while ($r = mysql_fetch_assoc($result)) {
                                $arraPerUser = array();
                                $totalPin2 = getPIN($r['id'], $f1, $f2);
                                $totaleft2 = getDPeft($r['id'], $f1, $f2);
                                $totaldp2 = getDP($r['id'], $f1, $f2);
                                $totalPuser += $totalPin2 + $totaleft2 + $totaldp2;
                                $grandTotal += $totalPuser;




                                $perocentajeDP2 = (($totaleft2 * 100) / $totalPuser);
                                $porcentajeNoEFTDP2 = (($totaldp2 * 100) / $totalPuser);
                                $porcentajePIN2 = (($totalPin2 * 100) / $totalPuser);

                                $perocentajeDP2 = number_format((float) $perocentajeDP2, 2, '.', '');
                                $porcentajeNoEFTDP2 = number_format((float) $porcentajeNoEFTDP2, 2, '.', '');
                                $porcentajePIN2 = number_format((float) $porcentajePIN2, 2, '.', '');

                                array_push($arraPerUser, $totalPuser, $r['user'], $perocentajeDP2, $porcentajeNoEFTDP2, $porcentajePIN2, $r['id']);
                                array_push($users, $arraPerUser)
                                ?>

                        <?
                                $totalPuser = 0;
                                $perocentajeDP2 = 0;
                                $porcentajeNoEFTDP2 = 0;
                                $porcentajePIN2 = 0;
                                $perocentajeDP2 = 0;
                                $porcentajeNoEFTDP2 = 0;
                                $porcentajePIN2 = 0;
                                $arraPerUser = null;
                            }


                            foreach ($users as $key => $row) {
                                $aux[$key] = $row[0];
                            }
                            array_multisort($aux, SORT_DESC, $users);
                            $cont = 0;

                            /* echo "<PRE>";
                              print_r($users);
                              echo "</PRE>"; */
                            ?>




                        <h1>Grand Total (<?= $grandTotal ?>)</h1>
                        <?
                            foreach ($users as $key => $row) {
                                $cont++;
                                ?>

                        <h3> #<?= $cont ?> <?= $row[1] ?></h3>
                        <h4>Total New Business:(<?= $row[0] ?>)</h4>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?= $row[2] ?>%">
                                <?= $row[2] ?> % NB EFT
                            </div>
                            <div class="progress-bar progress-bar-warning" role="progressbar" style="width:<?= $row[3] ?>%">
                                <?= $row[3] ?> % Down Payment
                            </div>
                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:<?= $row[4] ?>%">
                                <?= $row[4] ?> % Pay In Full
                            </div>
                        </div>


                        <? $cant = CalcularCacellations($row[5], $row[0]); ?>
                        <h4>Total Cancellations:(<?= $cant[1] ?>)</h4>
                        <div class="progress">

                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width:<?= $cant[0] ?>%">
                                <?= $cant[0] ?> % Loss Ratin
                            </div>
                        </div>
                        <?
                            }
                            ?>




                    </div>

                </div>



            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">My Progress</div>
                    <div class="panel-body">
                        <div class="row">
                            <h4>NB ADC:<?= $totalADCS ?></h4>
                            <div class="progress">
                                <? if ($perocentajeADC > 0) { ?>
                                <div class="col-lg-6 progress-bar progress-bar-<?= $classADC ?>" role="progressbar" style="width: <?= $perocentajeADC ?>%" aria-valuenow="<?= $perocentajeADC ?>" aria-valuemin="0" aria-valuemax="100"><?= $perocentajeADC ?>% Complete</div>
                                <? } ?>
                            </div>
                        </div>
                        <div class="row">
                            <h4>Down Payment EFT #<?= $toalDP ?></h4>
                            <div class="progress">
                                <? if ($perocentajeDP > 0) { ?>
                                <div class="col-lg-6 progress-bar progress-bar-<?= $classDPEFT ?>" role="progressbar" style="width: <?= $perocentajeDP ?>%" aria-valuenow="<?= $perocentajeDP ?>" aria-valuemin="0" aria-valuemax="100"><?= $perocentajeDP ?>% Complete</div>
                                <? } ?>
                            </div>
                        </div>

                        <div class="row">
                            <h4>Down Payment #<?= $toalDPNOEFT ?></h4>
                            <div class="progress">
                                <? if ($porcentajeNoEFTDP > 0) { ?>
                                <div class="col-lg-6 progress-bar progress-bar-<?= $classDP ?>" role="progressbar" style="width: <?= $porcentajeNoEFTDP ?>%" aria-valuenow="<?= $porcentajeNoEFTDP ?>" aria-valuemin="0" aria-valuemax="100"><?= $porcentajeNoEFTDP ?>% Complete</div>
                                <? } ?>
                            </div>
                        </div>


                        <div class="row">
                            <h4>Pay In Full #<?= $toalPIN ?></h4>
                            <div class="progress">
                                <? if ($porcentajePIN > 0) { ?>
                                <div class="col-lg-6  progress-bar progress-bar-<?= $classPIN ?>" role="progressbar" style="width: <?= $porcentajePIN ?>%" aria-valuenow="<?= $porcentajePIN ?>" aria-valuemin="0" aria-valuemax="100"><?= $porcentajePIN ?>% Complete</div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                    </div>
                    <div class="row">

                    </div>
                </div>

            </div>

        </div>

    </div>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $("#datepicker").datepicker({
            dateFormat: 'mm-dd-yy'
        });
        $("#datepicker2").datepicker({
            dateFormat: 'mm-dd-yy'
        });
    </script>


    <script type="text/javascript">
        function SearchHours() {
            var f1 = $("#datepicker").datepicker('getDate').formatDate('yy-mm-dd');
            var f2 = $("#datepicker2").datepicker('getDate');
            var fecha1 = $.datepicker.formatDate('yy-mm-dd', f1);
            var fecha2 = $.datepicker.formatDate('yy-mm-dd', f2);

            $.post("CheckTime.php", {
                fecha1,
                fecha2
            }, function(data) {

            });
        }
        $('li').click(function() {
            $('.active').removeClass('active');
            $(this).addClass('active');
        });
        //class="active"
        function ChangeValue(number) {


            var value = $("#chekCashier" + number + ":checked").val();

            if (value == "on") {
                value = 1;
            } else {
                value = 0;
            }
            $.post("UpdateCashier.php", {
                number,
                value
            }, function(data) {
                if (data.status == "success") {
                    alert("Complete");
                }
            });
        }

        function ChangeValue2(number) {

            var value = $("#cheklog" + number + ":checked").val();

            if (value == "on") {
                value = 1;
            } else {
                value = 0;
            }
            $.post("UpdateCashier2.php", {
                number,
                value
            }, function(data) {
                if (data.status == "success") {
                    alert("Complete");
                }
            });


        }

        function borrar(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function() {
                console.log("#form-" + id);
                $("#form-" + id).submit();
            });
        }


        function viewholder(id) {
            window.open("details.php?n=" + id);
        }

        function cargarT() {

            var agencia = $("#selag").val();
            $("#dt_searchbox").val("");
            $("#tabcont").empty();
            $.post("buscaT22.php", {
                    ag: agencia
                },
                function(data) {
                    $('#tabcont').html(data);
                });
        }
    </script>
</body>

</html>
<?

function getADC($id_user) {

}

function getPIN($id_user, $f1, $f2) {
    $sqlPayinFull = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_user and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=10;";
    $result4 = mysql_query($sqlPayinFull);
    $r4 = mysql_fetch_assoc($result4);
    $toalPIN = $r4['totalDPNOEFT'];
    return $toalPIN;
}

function getDP($id_user, $f1, $f2) {
    $sqlDPNOEFT = "SELECT COUNT(*) as totalDPNOEFT FROM `cutvone` where  id_usuario=$id_user and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=1;";
    $result3 = mysql_query($sqlDPNOEFT);
    $r3 = mysql_fetch_assoc($result3);
    $toalDPNOEFT = $r3['totalDPNOEFT'];
    return $toalDPNOEFT;
}

function getDPeft($id_user, $f1, $f2) {
    $sqlDP = "SELECT COUNT(*) as totalDP FROM `cutvone` where  id_usuario=$id_user and fecha BETWEEN '$f1' and '$f2' and erase=0 and Dpayment=5;";

    $result2 = mysql_query($sqlDP);
    $r2 = mysql_fetch_assoc($result2);
    $toalDP = $r2['totalDP'];

    return $toalDP;
}

function resgresanombrecompania3($val) {
    $sql = "SELECT * FROM `companyes` where id=" . $val;

    $res = mysql_query($sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['nombre'];
    }
}

function regresauser($val) {


    $sql = "SELECT * FROM `users` where id=" . $val;

    $res = mysql_query($sql);

    while ($re = mysql_fetch_assoc($res)) {
        return $re['user'];
    }
}

function resgresanombrecashier($val) {
    if (empty($val))
        return "-";


    $sql = "SELECT * FROM cashier where id=" . $val;

    $res = mysql_query($sql);
    $vr = "-";
    while ($re = mysql_fetch_assoc($res)) {
        $vr = $re['name'];
    }
    return $vr;
}

function CalcularCacellations($id_usuario, $nbTotal) {


    //verificar cancelaciones del usuario en el mes actual
    $results = array();
    $fecha1 = new DateTime();
    $fecha1->modify('first day of this month');
    $f1 = $fecha1->format('Y-m-d');

    $fecha = new DateTime();
    $fecha->modify('last day of this month');
    $f2 = $fecha->format('Y-m-d');


    //
    $fecha = date("Y-m-d");
    $nuevafecha = strtotime('-1 month', strtotime($fecha));
    $today = date('Y-m-d', $nuevafecha);

    $fecha1 = new DateTime($today);
    $fecha1->modify('first day of this month');
    $f1C = $fecha1->format('Y-m-d');

    $fecha = new DateTime($today);
    $fecha->modify('last day of this month');
    $f2C = $fecha->format('Y-m-d');
    $sqloptima = "SELECT Count(Can.customer_id) as TC  FROM `Cancellations` as Can join cutvone as Cut on Cut.id_cliente=Can.customer_id where Can.date BETWEEN '$f1' and '$f2' and Cut.fecha BETWEEN '$f1C' and '$f2C' and Can.habilitado=1 and (Can.status_policy=2 or Can.status_policy=3 or Can.status_policy=6) and Cut.erase=0 and (Cut.Dpayment=10 or Cut.Dpayment=1 or Cut.Dpayment=5) and Cut.id_usuario=$id_usuario";

    //$sql="SELECT customer_id,customer_name,status_policy,date FROM `Cancellations` where  date BETWEEN '$f1'  and  '$f2' and habilitado=1 and (status_policy=2 or status_policy=3 or status_policy=6)";
    $result = mysql_query($sqloptima);
    $totalCaccelations = 0;
    $cont = 0;
    $cant = 0;
    $row = mysql_fetch_assoc($result);
    $cant = $row['TC'];
//    while($row= mysql_fetch_assoc($result)){
    //      $cont++;
//          $idCustomer=$row['customer_id'];
//
//
//                $fecha = date("Y-m-d");
//                $nuevafecha = strtotime('-1 month', strtotime($fecha));
//                $today = date('Y-m-d', $nuevafecha);
//
//                $fecha1 = new DateTime($today);
//                $fecha1->modify('first day of this month');
//                $f1C = $fecha1->format('Y-m-d');
//
//                $fecha = new DateTime($today);
//                $fecha->modify('last day of this month');
//                $f2C = $fecha->format('Y-m-d');
//
//              $sqlCutvone="SELECT id FROM `cutvone` where  id_usuario=$id_usuario and fecha BETWEEN '$f1C' and '$f2C' and erase=0 and (Dpayment=10 or Dpayment=1 or Dpayment=5)  and id_cliente=$idCustomer";
//               $resulC= mysql_query($sqlCutvone);
//               $rows= mysql_num_rows($resulC);
//
//               if($rows>0){
//                   $cant++;
//               }
    // }
    $percentaje = ($cant * 100) / $nbTotal;
    array_push($results, $percentaje, $cant);
    return $results;
}
?>