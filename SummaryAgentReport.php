<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <title> Report</title>

        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }
            .fondo{
                background-image: url(img/logo.svg);
                background-size: contain;
                height: 50px;
                width: 133px;
                background-repeat: no-repeat;
            }

            .cabecera{
                text-align: center !important;
            }
            #datepicker,#datefinal {
                height: 34px!important;
                width: 100%!important;
                border-radius: 4px!important;
                border-style: solid!important;
                padding: 6px 12px!important;

            }
        </style>


    </head>

    <body >
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cont-input").remove();
                $("#btn-go").remove();
                $("#datepicker").datepicker();
            });

        </script>


        <div style="height:100%">
            <nav class="navbar navbar-default" role="navigation">
                <?
                include("menu_mgtm_boostrap.php");
                ?>
            </nav>



            <?php
            $fecha1 = date("Y-m-d");

            $slq = "SELECT `cv`.`id_usuario`, `us`.`user`, COUNT(`cv`.`id`) as `total` FROM `cutvone` AS `cv` LEFT JOIN `users` AS `us` ON `cv`.`id_usuario`=`us`.`id` WHERE (`cv`.`fecha` BETWEEN '$fecha1' AND '$fecha1') AND `cv`.`erase`!=1 GROUP BY `cv`.`id_usuario` ORDER BY `cv`.`id_usuario` ASC";
            $resultU = mysql_query($slq);
            ?>
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Daily Report</h2>
                </div>
                <div class="col-md-10 mx-auto">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Agent</th>
                                <th scope="col">Transactions</th>
                                <th scope="col">NB</th>
                                <th scope="col">Renewals</th>
                                <th scope="col">Reinstatement</th>
                                <th scope="col">Cancellations</th>
                                <th scope="col">AutoPay</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                            while ($user = mysql_fetch_assoc($resultU)) {
                                $sql = "SELECT 
                SUM(CASE WHEN (`Dpayment` = 1 or `Dpayment` = 3  or `Dpayment` = 5) THEN 1 ELSE 0 END) as `nb`, 
                SUM(CASE WHEN `Dpayment` = 2 THEN 1 ELSE 0 END) as `pay`, 
                SUM(CASE WHEN `Dpayment` = 7 THEN 1 ELSE 0 END) as `reins`, 
                SUM(CASE WHEN `Dpayment` = 8 THEN 1 ELSE 0 END) as `renew`       
                FROM `cutvone` WHERE  (`fecha` BETWEEN '$fecha1' AND '$fecha1') AND `erase`!=1 AND `id_usuario`='$user[id_usuario]'";
                                $resultSUM = mysql_query($sql);
                                $sumas = mysql_fetch_assoc($resultSUM);


                                $slq1 = "SELECT             
                SUM(CASE WHEN (`status_client` =2  or `status_client` =3 or `status_client` =6 )  THEN 1 ELSE 0 END) as `cancelled`                  
                From  UserManagement  where id_usurio='$user[id_usuario]'  and fecha_mod_satus='$fecha1'";
                                $resultSUM1 = mysql_query($slq1);
                                $sumas1 = mysql_fetch_assoc($resultSUM1);

                                $sql2 = "SELECT             
                SUM(CASE WHEN (`status_policy` =2  or `status_policy` =3 or `status_policy` =6 )  THEN 1 ELSE 0 END) as `cancelled`                  
                From  PoliciesForOldMGTM  where id_user='$user[id_usuario]' and fech_mod_status='$fecha1'";
                                $resultSUM2 = mysql_query($sql2);
                                $sumas2 = mysql_fetch_assoc($resultSUM2);

                                $sql3 = "SELECT         
                SUM(CASE WHEN (`status_policy`=10 and autoordirect=1 )  THEN 1 ELSE 0 END) as `autopays`                  
                From  PoliciesForOldMGTM  where id_user='$user[id_usuario]' and dateautopay='$fecha1'";

                                $resultSUM3 = mysql_query($sql3);
                                $sumas3 = mysql_fetch_assoc($resultSUM3);



                                $sql4 = "SELECT             
                SUM(CASE WHEN (`status_client`=10 and autoordirect=1 )  THEN 1 ELSE 0 END) as `autopays`                  
                From  UserManagement  where id_usurio='$user[id_usuario]' and dateautopay='$fecha1'";

                                $resultSUM4 = mysql_query($sql4);
                                $sumas4 = mysql_fetch_assoc($resultSUM4);
                                ?>
                                <tr>
                                    <th scope="row"><?= $user['user'] ?></th>
                                    <td><?= ($sumas['nb'] + $sumas['pay'] + $sumas['reins'] + $sumas['renew']) ?></td>
                                    <td><?= $sumas['nb'] ?></td>
                                    <td><?= $sumas['renew'] ?></td>
                                    <td><?= $sumas['reins'] ?></td>
                                    <td><?= ($sumas1['cancelled'] + $sumas2['cancelled']) ?></td>
                                    <td><?= ($sumas3['autopays']+ $sumas4['autopays']) ?></td>
                                </tr>

                            <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">Panel Footer</div>
        </div>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>



</html>
