<?
session_start();
if(isset($_SESSION['nivel'])){
} else{
 header('Location: index.php');
}
include("inc/dbconnection.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
       <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="datatables/jquery.dataTables.min.js"></script>
    <script src="datatables/dataTables.bootstrap.min.js"></script>
    <script src="sweetalert2/sweetalert2.min.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/datetime-moment.js"></script>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
  <title> </title>
  <style type="text/css">
     html,body{
      height: 100%;
     }
     .navbar-default {
          background-color: black !important;
          border-color: white !important;
      }

      .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
          color: white !important;
          background-color: #555555 !important;
      }
      .fondo{
          background-image: url(img/logo.svg);
          background-size: contain;
          height: 50px;
          width: 133px;
          background-repeat: no-repeat;
      }

      .cabecera{
      text-align: center !important;
      }
      #paginacion{
        text-align: center;
      }
      table.dataTable td {
		padding: 3px 10px;
		width: 1px;
		white-space: nowrap;
	  }
	  .dataTables_wrapper>.row {
	    margin-right: -15px;
	    margin-left: -15px;
	    padding: 0px!important;
	  }
  </style>
</head>

<body >
  <div style="height:100%">
    <nav class="navbar navbar-default" role="navigation">
      <?include("menu_mgtm_boostrap.php");?>
    </nav>
    <div class="table-responsive container" style="max-height: 86% !important; height: 86%;" id="contenido-tabla">
      <table id="example1" class="table table-bordered table-striped" width="100%" style="border-width: 1px; border-style: solid; background-color: #9c9c9c;">
        <thead>
        <tr>
          <th></th>
          <th>Date</th>
          <th>User</th>
          <th>Agency</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Note</th>
          <!--
          <th>D.O.B.</th>
          <th>Address</th>
          <th>City</th>
          -->
          
        </tr>
        </thead>
        <tbody>
        <?php
          $Sql = "SELECT `leads`.*, `users`.`user` FROM `leads` LEFT JOIN `users` ON `leads`.`id_usuario` = `users`.`id` WHERE `leads`.`status_lead` < 2 ORDER BY `leads`.`id` DESC";
          $result = mysql_query($Sql);
          //$rows = array();
          while($row = mysql_fetch_assoc($result)) {
            //$rows[] = $row;
            $fecha = date("m/d/Y",strtotime($row['fechaalta']));
        ?>
          <tr data-id="<?= $row['id'] ?>">
            <td class="actions">
              <a href="Elead22.php?id=<?= $row['id'] ?>"><i class="glyphicon glyphicon-edit edit" style="font-size: 16px;cursor:pointer;" title="Edit"></i></a>
          <?php if($_SESSION['id']==1 || $_SESSION['id']==2162){?> <i class="glyphicon glyphicon-trash delete" style="font-size: 16px;cursor:pointer;" title="Delete"></i><?php }?>
              <?php if($row['status_lead']==0){ ?>
                <i class="glyphicon glyphicon-eye-close view" id="view-<?= $row['id'] ?>" style="font-size: 16px;cursor:pointer;" title="Mark as viewed"></i>
              <?php } ?>
            </td>
            <td><?= $fecha ?></td>
            <td><?= $row['user'] ?></td>
            <td><?= $row['agencia'] ?></td>
            <td><?= $row['nombre'] ?></td>
            <td><?= $row['mail'] ?></td>
            <td><?= $row['tel'] ?></td>
            <td>
              <?php if( strlen($row['nota'])>30 ){ ?>
                <span data-toggle="tooltip" data-placement="left" title="<?= $row['nota'] ?>"><?= substr($row['nota'], 0, 30)."..." ?></span>
              <?php }else{ ?>
                <?= $row['nota'] ?>
              <?php } ?>
            </td>
            
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="panel-footer">Panel Footer<br/></div> 
  </div>
  
  <script type="text/javascript">
    var data=<?= json_encode( $rows ) ?>;

    $.fn.dataTable.moment( 'MM/DD/YYYY' );
    var table = $('#example1').DataTable( {
        "responsive": true,
        "scrollX": true,
        "order": [[ 1, "desc" ]],
        "columnDefs": [{
          "targets": 0,
          "sortable": false,
        }]
    } );

    $('#example1 tbody').on( 'click', 'i.delete', function () {
        var id = $(this).parents('tr').attr("data-id");
        var row = table.row( $(this).parents('tr') );
        swal({
          title: '¿Borrar registro?',
          text: "El registro se borrara permanentemente!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Borrar',
          cancelButtonText: 'Cancelar',
          showLoaderOnConfirm: true,
          allowOutsideClick: false,
        }).then(function () {
          $.ajax({
            url: "Lead_delete.php",
            type: "POST",
            data: {id:id},
            success: function(data){
              var resp = jQuery.parseJSON( data );
              if(resp.status=="success"){
                $("#number_leads").text(resp.contleads);
                row.remove().draw();
                swal(
                  'Deleted!','El registro ha sido borrado.','success');
              }else{
                swal('Error!','Ocurrio un problema al borrar el registro.','error');
              }
              
            },
            error: function(xhr, ajaxOptions, thrownError){
              swal('Error!','Problema de conexion.','error');
            }
          });
        });
    });

    $('#example1 tbody').on( 'click', 'i.view', function () {
        var row = $(this).parents('tr');
        var id = row.attr("data-id");
        $.ajax({
          url: "Lead_viewed.php",
          type: "POST",
          data: {id:id},
          success: function(data){
            var resp = jQuery.parseJSON( data );
            if(resp.status=="success"){
              $("#number_leads").text(resp.contleads);
              $("#view-"+id).remove();
            }else{
              swal('Error!','Ocurrio un problema.','error');
            }
          },
          error: function(xhr, ajaxOptions, thrownError){
            swal('Error!','Problema de conexion.','error');
          }
        });
    } );
    $('[data-toggle="tooltip"]').tooltip();
  </script>
</body>
</html>
