<?
session_start();
if(isset($_SESSION['nivel'])){

}else{
  
 header('Location: index.php');
}
include("inc/dbconnection.php");

   $iduser=$_SESSION['id'];
  $username=$_SESSION['username'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="sweetalert2/sweetalert2.min.js"></script>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
  <title> Report</title>

  <style type="text/css">
     html,body{
      height: 100%;
     }
     .navbar-default {
    background-color: black !important;
    border-color: white !important;
}

.navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
    color: white !important;
    background-color: #555555 !important;
}
.fondo{
    background-image: url(img/logo.svg);
    background-size: contain;
    height: 50px;
    width: 133px;
    background-repeat: no-repeat;
}

.cabecera{
text-align: center !important;
}
#datepicker,#datefinal {
    height: 34px!important;
    width: 100%!important;
    border-radius: 4px!important;
    border-style: solid!important;
    padding: 6px 12px!important;
    
}
  </style>


</head>

<body >
<script type="text/javascript">
$( document ).ready(function() {
  $("#cont-input").remove();
  $("#btn-go").remove();
$( "#datepicker" ).datepicker();
});

</script>


<div style="height:100%">
  <nav class="navbar navbar-default" role="navigation">
<?
include("menu_mgtm_boostrap.php");

?>
</nav>


        <div class="container" style="max-height: 86% !important; height: 86%; overflow:auto;">
        <div style="text-align: center;">

          <h3>ADC Endorse Report</h3>

        </div>
<div style="height: 5rem;background-color: #1c335c;color: white; text-align: center;     border-radius: 6px 6px 0px 0px;"> <h3 style="padding: 11px;">  Information </h3></div>     
   


<div class="row">
  <div class="col-md-4">
    <label>Start Date</label>
    <input  id="datepicker"  name="caja" class="form-control"  type="text"  placeholder="MM/DD/YYYY" readonly>
  </div>
  <div class="col-md-4">
    <label>End Date</label>
    <input id="datefinal" name="caja2" class="form-control"  type="text" onchange="turnDownForWhat2();" placeholder="MM/DD/YYYY" readonly>
  </div>
</div>
<div class="row table-responsive" id="tablas" style="background-color: #FFFFFF;padding: 0;text-align: center;">
  <div style="width: 100%; height: 150px;"></div>
  <div style="clear: both;"></div>
</div>
         
        </div>
       <div class="panel-footer">Panel Footer</div>
    </div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
  //$("#datepicker").datepicker();
  //$("#datefinal").datepicker();
</script>
<script type="text/javascript">
  function despliega(){   //funcion del body
    $('#datepicker').focus(),
    $('#datefinal').focus();
  }
 
  $( "#datepicker" ).datepicker({ //despliega el calendario pero no muestra resultados
    dateFormat: 'mm-dd-yy',
    beforeShow: function() {
      setTimeout(function(){
        $('.ui-datepicker').css('z-index', 99999999999999);
      }, 0);
    }

  });

  $( "#datefinal" ).datepicker({ //despliega el calendario pero no muestra resultados
    dateFormat: 'mm-dd-yy',
    beforeShow: function() {
      setTimeout(function(){
        $('.ui-datepicker').css('z-index', 99999999999999);
      }, 0);
    }

  });
  llenarcompanyes();

  function turnDownForWhat(){  //despliega el calendario con los resultados
    $.post("ConsultaReportes.php",{
      caja: $('input[name="caja"]').val(),
      caja2: $('input[name="caja2"]').val()
    },

    function(data){
      $('#tablas').html(data);
    });

  }
  function guardavaloresagente(){
  var select=$('#agents').val();
   
  }
  function guardavaloresplan(){
    var plan=$('#plan').val();
   
  }
  function  guardavaloresservice(){
    var status=$('#service').val();
  }

  function createReporte(){
     var agency=$('#agencyas').val();
    var fecha1= $('input[name="caja"]').val();
    var fecha2= $('input[name="caja2"]').val();
    var agent=$('#agents').val();
    var plan=$('#plan').val();
    var ser=$('#service').val();
    var pay=$('#pay').val();
    var payment=$('#payment').val();
  $('#tablas2').html('<div><img src="img/loading.gif"/></div>');

        var page = $(this).attr('data');        
        var dataString = 'page='+page;
      $.post("generaReporte.php",{payment:payment, caja: fecha1,  caja2: fecha2,agente:agent,plans:plan,serv:ser,pay:pay,agency:agency });
   
  }
function turnDownForWhat2(){ 
   //despliega el calendario con los resultados
    
 var f1=$("#datepicker").val();
 var f2=$("#datefinal").val();

    $.post("GetADCReport.php",{f1,f2}, 
      function(data){
      $('#tablas').html(data);
    });

  }
function recargarS2(val){
  $('#agents').html('<option value="">Cargando...aguarde</option>'); 
   $.ajax({ 
      url: 'procesar.php',
     data: 'id='+val, 
     success:  function(resp){ $('#agents').html(resp) } });
}
$('#xlsfile').submit(function(e) {
 
    
       var a=$('#agents').val();
     var p=$('#plan').val();
     var s=$('#service').val();
     var fecha1=$('input[name="caja"]').val();
     var fecha2=$('input[name="caja2"]').val();
     var pay=$('#pay').val();
    var payment=$('#payment').val();

     $('#fecha').val(fecha1);
     $('#fecha2').val(fecha2);
     $('#plan2').val(p);
     $('#status').val(s);
      $('#agente').val(a);
      $('#pay').val(pay);
      $('#payment').val(payment);
       //$.post("ExcelReportes.php",{agente:a,plan:p,service:s,fe1:fecha1,fe2:fecha2 });  
      //e.preventDefault();
   });
$('#xlsfile2').submit(function(e) {
  
     var a=$('#agents').val();
     var p=$('#plan').val();
     var s=$('#service').val();
     var fecha1=$('input[name="caja"]').val();
     var fecha2=$('input[name="caja2"]').val();
     var pay=$('#pay').val();
    var payment=$('#payment').val();

     $('#fechainicio').val(fecha1);
     $('#fecha22').val(fecha2);
     $('#plan22').val(p);
     $('#status2').val(s);
      $('#agente2').val(a);
      $('#pay2').val(pay);
      $('#payment2').val(payment);
       //$.post("ExcelReportes.php",{agente:a,plan:p,service:s,fe1:fecha1,fe2:fecha2 });  
      //e.preventDefault();
   });


function llenaruser(){
llenarcompanyes();
       var agencia=$("#selag").val();
        var page = $(this).attr('data');        
        var dataString = 'page='+page;
    $.post("llenarusers.php",{ag:agencia }, 
      function(data){
      $('#user').html(data);
    });

}



function llenarcompanyes(){

       var agencia=$("#selag").val();
        var page = $(this).attr('data');        
        var dataString = 'page='+page;
    $.post("llenarcomp.php",{ag:agencia }, 
      function(data){
      $('#company').html(data);
    });
}
</script>
</body>

</html>