-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-06-2020 a las 17:40:13
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `az_base`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adc_endorse`
--

CREATE TABLE `adc_endorse` (
  `id` int(11) NOT NULL,
  `id_user` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `id_client` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `Amount` int(11) NOT NULL,
  `ADC_Number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Concept` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Agency` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adc_nb`
--

CREATE TABLE `adc_nb` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date` date NOT NULL,
  `adcNumber` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `Amount` int(11) NOT NULL,
  `NameClient` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci,
  `direccion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DocumentoReporte` text COLLATE utf8_unicode_ci,
  `ReporteClaims` text COLLATE utf8_unicode_ci,
  `su` text COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `Mactive` int(11) NOT NULL DEFAULT '0',
  `Cactive` int(11) NOT NULL DEFAULT '0',
  `Dactive` int(11) NOT NULL DEFAULT '0',
  `Comision` double NOT NULL DEFAULT '0.25',
  `CommisionReport` text COLLATE utf8_unicode_ci,
  `mgtmreport` text COLLATE utf8_unicode_ci,
  `reportereloj` text COLLATE utf8_unicode_ci,
  `tel` text COLLATE utf8_unicode_ci NOT NULL,
  `fax` text COLLATE utf8_unicode_ci,
  `website` text COLLATE utf8_unicode_ci,
  `tipopago` int(11) NOT NULL,
  `reportdealer` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_owner` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `agency`
--

INSERT INTO `agency` (`id`, `nombre`, `direccion`, `email`, `ciudad`, `estado`, `zip`, `DocumentoReporte`, `ReporteClaims`, `su`, `password`, `Mactive`, `Cactive`, `Dactive`, `Comision`, `CommisionReport`, `mgtmreport`, `reportereloj`, `tel`, `fax`, `website`, `tipopago`, `reportdealer`, `id_owner`) VALUES
(101, 'BUY LOW COST INSURANCE (South Side Location)', '505 W AJO WAY', 'OLDTOWNINS@OUTLOOK.COM', 'TUCSON', 'ARIZONA', '85713', '101Report.PDF', '', 'ADMIN101', '123', 0, 0, 0, 0.25, '101ReportCom.PDF', ' Report.PDF', '', '(520)807-0619', '(520)620-5513', 'www.buylowins.com', 0, ' ReportDealer.PDF', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `api`
--

CREATE TABLE `api` (
  `id_api` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bankinformation`
--

CREATE TABLE `bankinformation` (
  `id` int(11) NOT NULL,
  `cardNumber` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `expDate` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `cvv` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `routenumber` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `accountNumber` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `date_update` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `book`
--

CREATE TABLE `book` (
  `id_book` int(11) NOT NULL,
  `book_name` varchar(150) NOT NULL,
  `rating` enum('1','2','3','4','5') NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cancellations`
--

CREATE TABLE `cancellations` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `status_policy` int(10) NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT '1',
  `policyNumber` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `policyid` int(11) NOT NULL,
  `agency` int(11) NOT NULL,
  `canceldate` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cashier`
--

CREATE TABLE `cashier` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories_blogs`
--

CREATE TABLE `categories_blogs` (
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci,
  `estado` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `claims`
--

CREATE TABLE `claims` (
  `id` int(10) NOT NULL,
  `id_poliza` int(10) DEFAULT NULL,
  `claim` text COLLATE utf8_unicode_ci NOT NULL,
  `tipoclaim` int(10) DEFAULT NULL,
  `servicio` int(10) DEFAULT NULL,
  `nom` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `autoestado` int(10) DEFAULT NULL,
  `color` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  `dirorigen` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dirdestino` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantuso` int(10) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `documento` text COLLATE utf8_unicode_ci,
  `vin` text COLLATE utf8_unicode_ci,
  `make` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` text COLLATE utf8_unicode_ci,
  `placas` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT '1',
  `pagado` int(10) DEFAULT '2',
  `id_agencia` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `idprovedor` int(10) DEFAULT NULL,
  `mtv` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lm` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tc` int(11) DEFAULT '0',
  `lat` text COLLATE utf8_unicode_ci,
  `lon` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `id_users` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `exp` date NOT NULL DEFAULT '1920-01-01',
  `user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pass` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellido` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `date_login` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `fecAlta` date DEFAULT NULL,
  `plan` int(11) NOT NULL,
  `pay` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `paymode` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `subscriptionID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pass_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `nivel` int(11) NOT NULL DEFAULT '0',
  `active` int(11) DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_birth` date NOT NULL DEFAULT '1920-01-01',
  `effective` date NOT NULL DEFAULT '1920-01-01',
  `dln` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` double DEFAULT NULL,
  `paid` double DEFAULT NULL,
  `pending` double DEFAULT NULL,
  `ticket_cars` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticket_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ticket_invoice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` int(11) DEFAULT NULL,
  `bankstatus` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total3` double NOT NULL,
  `total2` double NOT NULL,
  `total4` double NOT NULL,
  `expiration` date DEFAULT NULL,
  `total5` double NOT NULL,
  `fechaMed` date NOT NULL,
  `compania` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CardNumber` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Activebank` int(10) NOT NULL,
  `Plan_client` int(10) DEFAULT NULL,
  `Status_client` int(10) DEFAULT NULL,
  `Dpayment` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `habilitado` int(10) DEFAULT '1',
  `id_agencia` int(10) DEFAULT NULL,
  `docbaja` text COLLATE utf8_unicode_ci,
  `contrato` text COLLATE utf8_unicode_ci,
  `premium` double DEFAULT NULL,
  `dateexpfinal` date NOT NULL,
  `agcode` int(10) NOT NULL,
  `diff` double NOT NULL DEFAULT '0',
  `estservicio` int(11) DEFAULT NULL,
  `lat` text COLLATE utf8_unicode_ci,
  `lon` text COLLATE utf8_unicode_ci,
  `fechabaja` date DEFAULT NULL,
  `MF` int(11) DEFAULT '0',
  `idioma` int(11) NOT NULL,
  `notas` text COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `credit_value` double NOT NULL DEFAULT '0',
  `payment_date_credit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients_changelog`
--

CREATE TABLE `clients_changelog` (
  `Id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `total_before` double NOT NULL,
  `total_after` double NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients_visits`
--

CREATE TABLE `clients_visits` (
  `id` int(11) NOT NULL,
  `id_userM` int(11) NOT NULL,
  `id_agent` int(11) NOT NULL,
  `reason` int(11) NOT NULL,
  `home` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients_visits_reasons`
--

CREATE TABLE `clients_visits_reasons` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `_order` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clockrecords`
--

CREATE TABLE `clockrecords` (
  `Id` int(11) NOT NULL,
  `DateIn` datetime DEFAULT NULL,
  `DateOut` datetime DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `DateRecord` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companyclaims`
--

CREATE TABLE `companyclaims` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `username` text COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `agency` int(11) NOT NULL,
  `nivel` int(10) NOT NULL DEFAULT '900',
  `id_ciudad` int(11) DEFAULT NULL,
  `sort_order` int(2) NOT NULL DEFAULT '100'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companyes`
--

CREATE TABLE `companyes` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci,
  `agency` int(11) DEFAULT NULL,
  `comision` double NOT NULL,
  `agcode` int(11) NOT NULL,
  `loss` double NOT NULL,
  `ImageRoute` text COLLATE utf8_unicode_ci NOT NULL,
  `NextPaymentPeriod` int(11) NOT NULL,
  `SecondaryNextPaymentPeriod` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `creditos`
--

CREATE TABLE `creditos` (
  `id` int(11) NOT NULL,
  `id_transaccion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `agencya` int(11) NOT NULL,
  `dealer` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `fechapago` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cutvone`
--

CREATE TABLE `cutvone` (
  `id` int(11) NOT NULL,
  `agency` int(10) DEFAULT NULL,
  `amountpoc` double DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci,
  `policynumber` text COLLATE utf8_unicode_ci,
  `company` int(10) DEFAULT NULL,
  `companytype` int(10) DEFAULT NULL,
  `dealership` int(10) DEFAULT NULL,
  `dealertype` int(10) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `feecompany` double DEFAULT '0',
  `feedealer` double DEFAULT '0',
  `feeforaz` double DEFAULT '0',
  `feeoffice` double DEFAULT '0',
  `id_usuario` int(10) DEFAULT NULL,
  `id_cashier` int(11) DEFAULT NULL,
  `nameclient` text COLLATE utf8_unicode_ci,
  `nameentrega` text COLLATE utf8_unicode_ci,
  `namevendedor` text COLLATE utf8_unicode_ci,
  `officetype` int(10) DEFAULT NULL,
  `poc` int(10) DEFAULT NULL,
  `typeaz` int(10) DEFAULT NULL,
  `typeDO` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `valponchon` double DEFAULT '0',
  `recibo` text COLLATE utf8_unicode_ci,
  `Dpayment` int(10) NOT NULL,
  `DeliveryT` int(10) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `horat` text COLLATE utf8_unicode_ci NOT NULL,
  `agcode` int(11) NOT NULL,
  `credit` tinyint(1) NOT NULL DEFAULT '0',
  `erase` int(11) NOT NULL,
  `id_policy` bigint(20) DEFAULT '0',
  `recivedcashConfirmation` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dalyreport`
--

CREATE TABLE `dalyreport` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name_user` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `type` tinyint(4) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `Agecny_Code` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dayshorarioclock`
--

CREATE TABLE `dayshorarioclock` (
  `Id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TimeIn` time NOT NULL,
  `TimeOut` time NOT NULL,
  `Lunch` time DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dealerinformationmanagement`
--

CREATE TABLE `dealerinformationmanagement` (
  `id` int(11) NOT NULL,
  `NameSeller` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `NameDelivery` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ID_Dealer` int(11) NOT NULL,
  `Amount` float NOT NULL,
  `active` int(11) NOT NULL,
  `inProgress` int(11) NOT NULL,
  `id_policy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dealers`
--

CREATE TABLE `dealers` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci,
  `agency` int(11) DEFAULT NULL,
  `password` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1234@',
  `latitud` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `habilitado` tinyint(4) NOT NULL DEFAULT '1',
  `location` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deducsiones`
--

CREATE TABLE `deducsiones` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `agency` int(11) DEFAULT NULL,
  `montivo` text COLLATE utf8_unicode_ci,
  `monto` double DEFAULT NULL,
  `fecha` date NOT NULL,
  `agcode` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deductionsdash`
--

CREATE TABLE `deductionsdash` (
  `id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `Concept` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `AgencyCode` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `token` text COLLATE utf8_unicode_ci NOT NULL,
  `habilitado` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employeegoals`
--

CREATE TABLE `employeegoals` (
  `id` int(11) NOT NULL,
  `WorkHours` int(11) NOT NULL DEFAULT '40',
  `NewB` int(11) NOT NULL,
  `ADC` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee_of_month`
--

CREATE TABLE `employee_of_month` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `total_NB` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `status` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extracitys`
--

CREATE TABLE `extracitys` (
  `id` int(11) NOT NULL,
  `city` text COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `zip` text COLLATE utf8_unicode_ci NOT NULL,
  `satateprovicia` text COLLATE utf8_unicode_ci NOT NULL,
  `id_solicitud` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `poliza` int(10) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `serviceorder` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `origen` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `destino` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `grua` int(11) NOT NULL,
  `vin` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `tax` double NOT NULL,
  `amount` double NOT NULL,
  `idprovedor` int(10) DEFAULT NULL,
  `Total` double DEFAULT NULL,
  `millas` double NOT NULL,
  `millasload` double NOT NULL,
  `operador` text COLLATE utf8_unicode_ci NOT NULL,
  `documento` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'file',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fileshistory`
--

CREATE TABLE `fileshistory` (
  `id_client` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `file` text COLLATE utf8_unicode_ci NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT '1',
  `id` int(11) NOT NULL,
  `alta` date DEFAULT NULL,
  `id_user` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files_profile`
--

CREATE TABLE `files_profile` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `file` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filtradostest`
--

CREATE TABLE `filtradostest` (
  `id` int(10) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `follo_ups_dash`
--

CREATE TABLE `follo_ups_dash` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `createdDate` date NOT NULL,
  `Type` int(11) NOT NULL,
  `Close_by` int(11) NOT NULL,
  `open` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `agency` int(11) NOT NULL,
  `id_poliza` int(11) NOT NULL,
  `follow_date` date NOT NULL,
  `infoText` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_customer` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `goalsemployee`
--

CREATE TABLE `goalsemployee` (
  `id` int(11) NOT NULL,
  `user` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `NB_ADC` int(11) NOT NULL DEFAULT '0',
  `NB_Policy` int(11) NOT NULL DEFAULT '0',
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups_menu`
--

CREATE TABLE `groups_menu` (
  `id_groups` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarioclock`
--

CREATE TABLE `horarioclock` (
  `Id` int(11) NOT NULL,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `Id_Agency` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fechaalta` date NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `apellido` text COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `mail` text COLLATE utf8_unicode_ci NOT NULL,
  `addemail` text COLLATE utf8_unicode_ci NOT NULL,
  `tel` text COLLATE utf8_unicode_ci NOT NULL,
  `addphone` text COLLATE utf8_unicode_ci NOT NULL,
  `homephone` tinytext COLLATE utf8_unicode_ci,
  `workphone` tinytext COLLATE utf8_unicode_ci,
  `address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agencia` int(11) NOT NULL,
  `policynumber` text COLLATE utf8_unicode_ci NOT NULL,
  `ADC` text COLLATE utf8_unicode_ci NOT NULL,
  `policy_type` int(11) NOT NULL,
  `dealer` int(11) NOT NULL,
  `compania` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `nota` text COLLATE utf8_unicode_ci NOT NULL,
  `descr` text COLLATE utf8_unicode_ci,
  `status_lead` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `id` int(30) NOT NULL,
  `loginId` varchar(255) NOT NULL,
  `loginType` int(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `profilePic` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `action` varchar(125) NOT NULL,
  `logs` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `ext` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  `uploaded_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '99',
  `level` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(125) NOT NULL,
  `label` varchar(25) NOT NULL,
  `link` varchar(125) NOT NULL,
  `id` varchar(25) NOT NULL DEFAULT '#',
  `id_menu_type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_type`
--

CREATE TABLE `menu_type` (
  `id_menu_type` int(11) NOT NULL,
  `type` varchar(125) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodospagopadc`
--

CREATE TABLE `metodospagopadc` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `module` varchar(20) NOT NULL,
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movementslog`
--

CREATE TABLE `movementslog` (
  `id` int(11) NOT NULL,
  `id_policy` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'status for payment',
  `date` date NOT NULL,
  `active` int(11) DEFAULT '0',
  `statuspolicy` int(11) NOT NULL DEFAULT '1' COMMENT 'estado poliza'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas_azdriver`
--

CREATE TABLE `notas_azdriver` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas_employee`
--

CREATE TABLE `notas_employee` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nota` text COLLATE utf8_unicode_ci NOT NULL,
  `habilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas_mgtm`
--

CREATE TABLE `notas_mgtm` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nota` text COLLATE utf8_unicode_ci NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT '1',
  `id_cliente` int(11) NOT NULL,
  `followtype` int(11) NOT NULL,
  `fecha_follow` date DEFAULT '0000-00-00',
  `user_follow_marc` int(11) NOT NULL,
  `status_aplication` int(11) NOT NULL,
  `user_asigned` int(11) NOT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notes_lead`
--

CREATE TABLE `notes_lead` (
  `id` int(11) NOT NULL,
  `id_lead` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `owner`
--

CREATE TABLE `owner` (
  `id` int(11) NOT NULL,
  `Name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `createdDate` date NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `pasword` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `Active` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page`
--

CREATE TABLE `page` (
  `id_page` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `featured_image` varchar(255) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `template` varchar(125) NOT NULL,
  `breadcrumb` text NOT NULL,
  `content` text NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `view` varchar(150) NOT NULL DEFAULT 'default'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payhistory`
--

CREATE TABLE `payhistory` (
  `id` int(10) NOT NULL,
  `id_client` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `pay` double NOT NULL,
  `paymode` int(11) NOT NULL DEFAULT '0',
  `meses` text COLLATE utf8_unicode_ci NOT NULL,
  `totalanio` double NOT NULL,
  `id_poliza` int(11) NOT NULL,
  `id_agencia` int(10) DEFAULT NULL,
  `agcode` int(10) NOT NULL,
  `idticket` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment`
--

CREATE TABLE `payment` (
  `id` int(10) NOT NULL,
  `dln` varchar(50) NOT NULL,
  `meses` int(10) NOT NULL,
  `total` double NOT NULL,
  `occurreneces` int(10) NOT NULL,
  `label` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `billingName` varchar(50) NOT NULL,
  `billingLastName` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zip` varchar(50) NOT NULL,
  `cardNumber` varchar(50) NOT NULL,
  `cardExpiry` varchar(50) NOT NULL,
  `subscriptionID` varchar(255) NOT NULL,
  `bankstatus` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permits`
--

CREATE TABLE `permits` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `agency` int(11) NOT NULL,
  `erase` tinyint(4) NOT NULL,
  `editpolicy` tinyint(4) NOT NULL,
  `ReportRetention` tinyint(4) NOT NULL,
  `ReportEFT` tinyint(4) NOT NULL,
  `ReportPolicy` tinyint(4) NOT NULL,
  `DealerReport` tinyint(4) NOT NULL,
  `external_login` tinyint(4) NOT NULL,
  `shedule` tinyint(4) NOT NULL,
  `Goals` tinyint(4) NOT NULL,
  `TimeReport` tinyint(4) NOT NULL,
  `EditTime` tinyint(4) NOT NULL,
  `TakePayments` tinyint(4) NOT NULL,
  `Atachment` tinyint(4) NOT NULL,
  `CreateUsers` tinyint(4) NOT NULL,
  `salesReport` tinyint(4) NOT NULL,
  `permissions` int(11) NOT NULL,
  `asing` tinyint(4) NOT NULL,
  `wellcomeCall` tinyint(4) NOT NULL DEFAULT '0',
  `Prodcution` tinyint(4) NOT NULL DEFAULT '0',
  `edit_t` tinyint(4) NOT NULL DEFAULT '0',
  `mapReport` tinyint(4) NOT NULL DEFAULT '0',
  `status_profile` tinyint(4) NOT NULL DEFAULT '0',
  `credit_card` tinyint(4) NOT NULL DEFAULT '0',
  `edit_policy_status` tinyint(4) NOT NULL DEFAULT '0',
  `erase_users` int(11) NOT NULL DEFAULT '0',
  `auto_pay_adc` tinyint(4) NOT NULL DEFAULT '0',
  `edit_transaction_adc` tinyint(4) NOT NULL DEFAULT '0',
  `sales_report_adc` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan`
--

CREATE TABLE `plan` (
  `basic` float NOT NULL,
  `popu` float NOT NULL,
  `prem` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plocy_status`
--

CREATE TABLE `plocy_status` (
  `id` int(11) NOT NULL,
  `Name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policiesforoldmgtm`
--

CREATE TABLE `policiesforoldmgtm` (
  `Id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `date_alta` date NOT NULL,
  `pago_mensual` int(11) NOT NULL,
  `policy_type` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `policynumber` text COLLATE utf8_unicode_ci NOT NULL,
  `adc_number` text COLLATE utf8_unicode_ci,
  `status_policy` int(11) NOT NULL,
  `fech_mod_status` date NOT NULL,
  `date_cancellation` date NOT NULL,
  `date_expiration` date NOT NULL,
  `date_effective` date NOT NULL,
  `pago_menusal_drviers` int(11) NOT NULL,
  `location` tinyint(4) NOT NULL,
  `id_user` int(11) NOT NULL,
  `CancelForPending` date NOT NULL,
  `DateModifyDaysP` date NOT NULL,
  `CountPendingDays` int(11) NOT NULL,
  `date_exp_adc` date DEFAULT NULL,
  `aplication` tinyint(4) NOT NULL DEFAULT '0',
  `phoneNumber` tinyint(4) NOT NULL DEFAULT '0',
  `driverLicense` tinyint(4) NOT NULL,
  `registration` tinyint(4) NOT NULL DEFAULT '0',
  `pictures` tinyint(4) NOT NULL DEFAULT '0',
  `proofop` tinyint(4) NOT NULL DEFAULT '0',
  `adcsinged` tinyint(4) NOT NULL DEFAULT '0',
  `languajePrefernce` tinyint(4) NOT NULL DEFAULT '0',
  `bank_info` tinyint(4) NOT NULL DEFAULT '0',
  `email_info` tinyint(4) NOT NULL DEFAULT '0',
  `id_user_update` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `date_update` date NOT NULL,
  `autoordirect` int(11) NOT NULL,
  `dateautopay` date NOT NULL,
  `date_requote` date NOT NULL,
  `status_renter` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policymanagement`
--

CREATE TABLE `policymanagement` (
  `id` int(11) NOT NULL,
  `policyNumber` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `created` date NOT NULL,
  `id_company` int(11) NOT NULL,
  `effectivedate` date NOT NULL,
  `downpayemntDate` date NOT NULL,
  `cancelDate` date NOT NULL,
  `expirationDate` date NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `isLead` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `office` int(11) NOT NULL,
  `inprogres` int(11) NOT NULL,
  `MonthlyPayment` float NOT NULL,
  `Premium` float NOT NULL,
  `AgencyCode` int(11) NOT NULL,
  `Dealer_ID` int(11) NOT NULL,
  `Status_policy` int(11) NOT NULL DEFAULT '1',
  `paymentM` int(11) NOT NULL,
  `EFTDate` date NOT NULL,
  `APActive` tinyint(20) NOT NULL,
  `fecha_mod_satus` date NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policy_dealer`
--

CREATE TABLE `policy_dealer` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policy_dealer_company`
--

CREATE TABLE `policy_dealer_company` (
  `id` int(11) NOT NULL,
  `dealer` int(11) NOT NULL,
  `company` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policy_type`
--

CREATE TABLE `policy_type` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ico_class` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `poliza`
--

CREATE TABLE `poliza` (
  `id` int(10) NOT NULL,
  `id_client` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `pay` double NOT NULL,
  `meses` text COLLATE utf8_unicode_ci NOT NULL,
  `totalanio` double NOT NULL,
  `id_agencia` int(10) DEFAULT NULL,
  `premium` double DEFAULT NULL,
  `agcode` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precios`
--

CREATE TABLE `precios` (
  `id` int(11) NOT NULL,
  `p1` double DEFAULT NULL,
  `p2` double DEFAULT NULL,
  `p3` double DEFAULT NULL,
  `p4` double DEFAULT NULL,
  `p5` double DEFAULT NULL,
  `id_ag` int(11) DEFAULT NULL,
  `service` int(11) DEFAULT NULL,
  `policyn` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profileemployee`
--

CREATE TABLE `profileemployee` (
  `Id` int(11) NOT NULL,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `zipCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `civilStatus` tinyint(4) NOT NULL,
  `personalPhone` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `contactPhone` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `Photo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `education` int(11) NOT NULL,
  `altaEmployee` date NOT NULL,
  `bajaEmployee` date NOT NULL,
  `PayRollType` tinyint(4) NOT NULL DEFAULT '1',
  `paymentPerHour` float NOT NULL DEFAULT '10.5',
  `SalaryPayment` float NOT NULL DEFAULT '1900',
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profileinfo`
--

CREATE TABLE `profileinfo` (
  `id` int(10) NOT NULL,
  `idus` int(10) NOT NULL,
  `agency` int(10) NOT NULL,
  `agcode` int(10) NOT NULL,
  `jobtitle` text COLLATE utf8_unicode_ci NOT NULL,
  `birth` date NOT NULL,
  `genero` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  `mail` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `country` text COLLATE utf8_unicode_ci NOT NULL,
  `city` text COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `state` text COLLATE utf8_unicode_ci NOT NULL,
  `tpay` int(11) NOT NULL,
  `extime` int(11) NOT NULL,
  `rate` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `taxid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `cp` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `alta` date NOT NULL,
  `telefono` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `referenciados`
--

CREATE TABLE `referenciados` (
  `id` int(11) NOT NULL,
  `policynumber` text COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `lastname` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `reff` text COLLATE utf8_unicode_ci NOT NULL,
  `pay` int(11) NOT NULL,
  `altafecha` date NOT NULL,
  `nota` text COLLATE utf8_unicode_ci NOT NULL,
  `eneable` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registroclock`
--

CREATE TABLE `registroclock` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `date` date NOT NULL,
  `dia` text COLLATE utf8_unicode_ci NOT NULL,
  `typereg` int(11) NOT NULL,
  `agencia` int(11) NOT NULL,
  `diff` double NOT NULL,
  `hora` timestamp NULL DEFAULT NULL,
  `horaout` timestamp NULL DEFAULT NULL,
  `id_user_update` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `date_update` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resourcesforpolicy`
--

CREATE TABLE `resourcesforpolicy` (
  `id` int(11) NOT NULL,
  `Type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `file` text COLLATE utf8_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `dateUpload` datetime NOT NULL,
  `id_policy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolclocktime`
--

CREATE TABLE `rolclocktime` (
  `Id` int(11) NOT NULL,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudp`
--

CREATE TABLE `solicitudp` (
  `id` int(11) NOT NULL,
  `legalname` text COLLATE utf8_unicode_ci NOT NULL,
  `DBA` text COLLATE utf8_unicode_ci NOT NULL,
  `addressB` text COLLATE utf8_unicode_ci NOT NULL,
  `cityB` text COLLATE utf8_unicode_ci NOT NULL,
  `zipB` text COLLATE utf8_unicode_ci NOT NULL,
  `phonB` text COLLATE utf8_unicode_ci NOT NULL,
  `emailB` text COLLATE utf8_unicode_ci NOT NULL,
  `faxB` text COLLATE utf8_unicode_ci NOT NULL,
  `webB` text COLLATE utf8_unicode_ci NOT NULL,
  `stateprovB` text COLLATE utf8_unicode_ci NOT NULL,
  `ownname` text COLLATE utf8_unicode_ci NOT NULL,
  `ownphone` text COLLATE utf8_unicode_ci NOT NULL,
  `owncell` text COLLATE utf8_unicode_ci NOT NULL,
  `managername` text COLLATE utf8_unicode_ci NOT NULL,
  `managerphone` text COLLATE utf8_unicode_ci NOT NULL,
  `managercell` text COLLATE utf8_unicode_ci NOT NULL,
  `lisence` text COLLATE utf8_unicode_ci NOT NULL,
  `taxid` text COLLATE utf8_unicode_ci NOT NULL,
  `yrsinbusisnes` text COLLATE utf8_unicode_ci NOT NULL,
  `rbtn1` int(10) NOT NULL,
  `respuestarbtn1` text COLLATE utf8_unicode_ci NOT NULL,
  `rbtn2` int(10) NOT NULL,
  `rdbtn3` int(10) NOT NULL,
  `rbdnt4` int(10) NOT NULL,
  `rbtn5` int(10) NOT NULL,
  `rbtn6` int(10) NOT NULL,
  `respuestarbtn6` text COLLATE utf8_unicode_ci NOT NULL,
  `rbtn7` int(10) NOT NULL,
  `respuestarbtn7` text COLLATE utf8_unicode_ci NOT NULL,
  `rbtn8` int(10) NOT NULL,
  `btnr9` int(10) NOT NULL,
  `subrbtn9` int(10) NOT NULL,
  `chasiswork1` text COLLATE utf8_unicode_ci NOT NULL,
  `electricalwork1` text COLLATE utf8_unicode_ci NOT NULL,
  `timeworks1` text COLLATE utf8_unicode_ci NOT NULL,
  `rbtn10` int(10) NOT NULL,
  `tirespesify1` text COLLATE utf8_unicode_ci NOT NULL,
  `rbtn11` int(10) NOT NULL,
  `sellbatery1` text COLLATE utf8_unicode_ci NOT NULL,
  `rbtn12` int(10) NOT NULL,
  `rbtnsub12` int(10) NOT NULL,
  `rbtn13` int(10) NOT NULL,
  `rbtn14` int(10) NOT NULL,
  `rbtn15` int(10) NOT NULL,
  `rbtn16` int(10) NOT NULL,
  `timer16` text COLLATE utf8_unicode_ci,
  `metoodspago` text COLLATE utf8_unicode_ci NOT NULL,
  `sdutysel1` text COLLATE utf8_unicode_ci NOT NULL,
  `roadsideservices` text COLLATE utf8_unicode_ci NOT NULL,
  `horariocompleto` int(10) NOT NULL,
  `phonmef` text COLLATE utf8_unicode_ci NOT NULL,
  `fexf` text COLLATE utf8_unicode_ci NOT NULL,
  `timefs` text COLLATE utf8_unicode_ci,
  `allterrain` text COLLATE utf8_unicode_ci NOT NULL,
  `ballhitich` text COLLATE utf8_unicode_ci NOT NULL,
  `electricvehicle` text COLLATE utf8_unicode_ci NOT NULL,
  `enclosedcarrier` text COLLATE utf8_unicode_ci NOT NULL,
  `fifthwheel` text COLLATE utf8_unicode_ci NOT NULL,
  `flatbed` text COLLATE utf8_unicode_ci NOT NULL,
  `goosneck` text COLLATE utf8_unicode_ci NOT NULL,
  `heaveduty` text COLLATE utf8_unicode_ci NOT NULL,
  `lowboy` text COLLATE utf8_unicode_ci NOT NULL,
  `ligthduty` text COLLATE utf8_unicode_ci NOT NULL,
  `ligthserivceunit` text COLLATE utf8_unicode_ci NOT NULL,
  `mediumduty` text COLLATE utf8_unicode_ci NOT NULL,
  `motocyclecarrier` text COLLATE utf8_unicode_ci NOT NULL,
  `superduty` text COLLATE utf8_unicode_ci NOT NULL,
  `trailer` text COLLATE utf8_unicode_ci NOT NULL,
  `vessel` text COLLATE utf8_unicode_ci NOT NULL,
  `wheellift` text COLLATE utf8_unicode_ci NOT NULL,
  `macmiles` text COLLATE utf8_unicode_ci NOT NULL,
  `zipcodesser` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `mailingaddress` text COLLATE utf8_unicode_ci NOT NULL,
  `mailingcity` text COLLATE utf8_unicode_ci NOT NULL,
  `mailingzip` text COLLATE utf8_unicode_ci NOT NULL,
  `mailingstate` text COLLATE utf8_unicode_ci NOT NULL,
  `doc` text COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tablaprovedoresservicio`
--

CREATE TABLE `tablaprovedoresservicio` (
  `id` int(11) NOT NULL,
  `legalname` text COLLATE utf8_unicode_ci,
  `dba` text COLLATE utf8_unicode_ci,
  `badress` text COLLATE utf8_unicode_ci,
  `state` text COLLATE utf8_unicode_ci,
  `city` text COLLATE utf8_unicode_ci,
  `zip` text COLLATE utf8_unicode_ci,
  `bphone` text COLLATE utf8_unicode_ci,
  `bfax` text COLLATE utf8_unicode_ci,
  `maddress` text COLLATE utf8_unicode_ci,
  `mstate` text COLLATE utf8_unicode_ci,
  `mcity` text COLLATE utf8_unicode_ci,
  `mzip` text COLLATE utf8_unicode_ci,
  `oname` text COLLATE utf8_unicode_ci,
  `ophone` text COLLATE utf8_unicode_ci,
  `ocell` text COLLATE utf8_unicode_ci,
  `mname` text COLLATE utf8_unicode_ci,
  `mphone` text COLLATE utf8_unicode_ci,
  `mcell` text COLLATE utf8_unicode_ci,
  `blicense` text COLLATE utf8_unicode_ci,
  `gtsnumber` text COLLATE utf8_unicode_ci,
  `yeasinb` text COLLATE utf8_unicode_ci,
  `ae` int(11) NOT NULL,
  `visa` int(11) NOT NULL,
  `mc` int(11) NOT NULL,
  `atm` int(11) NOT NULL,
  `pc` int(11) NOT NULL,
  `fleetcard` int(11) NOT NULL,
  `discover` int(11) NOT NULL,
  `cash` int(11) NOT NULL,
  `ligthduty` int(11) NOT NULL,
  `mediumduty` int(11) NOT NULL,
  `heavyduty` int(11) NOT NULL,
  `superduty` int(11) NOT NULL,
  `motorcycle` int(11) NOT NULL,
  `jumpstart` int(11) NOT NULL,
  `lockout` int(11) NOT NULL,
  `tirecharge` int(11) NOT NULL,
  `theinflation` int(11) NOT NULL,
  `ldufueldelivery` int(11) NOT NULL,
  `lddfueldelivery` int(11) NOT NULL,
  `flatbedtowing` int(11) NOT NULL,
  `vehicleunloading` int(11) NOT NULL,
  `lowboytrailer` int(11) NOT NULL,
  `winching` int(11) NOT NULL,
  `locksmith` int(11) NOT NULL,
  `mobilebatterysale` int(11) NOT NULL,
  `mobiletiresale` int(11) NOT NULL,
  `winshieldrepair` int(11) NOT NULL,
  `motrocyclejs` int(11) NOT NULL,
  `mediumdutytc` int(11) NOT NULL,
  `mediumdutyw` int(11) NOT NULL,
  `mediumdutyjs` int(11) NOT NULL,
  `mediumdutyl` int(11) NOT NULL,
  `heavydutytc` int(11) NOT NULL,
  `ballhitch` int(11) NOT NULL,
  `fifthwhelltowing` int(11) NOT NULL,
  `goosenecktowing` int(11) NOT NULL,
  `mastercrv` int(11) NOT NULL,
  `heavydutyjs` int(11) NOT NULL,
  `heavydutyl` int(11) NOT NULL,
  `superdutytc` int(11) NOT NULL,
  `superdutywin` int(11) NOT NULL,
  `superdutyjs` int(11) NOT NULL,
  `superdutyl` int(11) NOT NULL,
  `lowclerancetow` int(11) NOT NULL,
  `mddfueldelivery` int(11) NOT NULL,
  `hddfueldelivery` int(11) NOT NULL,
  `sddfueldelivery` int(11) NOT NULL,
  `mdufueldelivery` int(11) NOT NULL,
  `hdufueldelivery` int(11) NOT NULL,
  `mobilemechanic` int(11) NOT NULL,
  `sdufueldelivery` int(11) NOT NULL,
  `luxurytow` int(11) NOT NULL,
  `electricvehicle` int(11) NOT NULL,
  `allterraintow` int(11) NOT NULL,
  `exotictow` int(11) NOT NULL,
  `allterrain` int(11) NOT NULL,
  `ballhitchs` int(11) NOT NULL,
  `electricvehicles` int(11) NOT NULL,
  `enclosedcarriers` int(11) NOT NULL,
  `fifthwhells` int(11) NOT NULL,
  `flatbeds` int(11) NOT NULL,
  `heavydutys` int(11) NOT NULL,
  `goosnecks` int(11) NOT NULL,
  `lowboys` int(11) NOT NULL,
  `ligthdutys` int(11) NOT NULL,
  `ligthserviceunits` int(11) NOT NULL,
  `mediumdutys` int(11) NOT NULL,
  `motorcyclecarriers` int(11) NOT NULL,
  `superdutys` int(11) NOT NULL,
  `thrailers` int(11) NOT NULL,
  `whelllifts` int(11) NOT NULL,
  `q1` int(11) NOT NULL,
  `q2` int(11) NOT NULL,
  `q3` int(11) NOT NULL,
  `q4` int(11) NOT NULL,
  `q5` int(11) NOT NULL,
  `q6` int(11) NOT NULL,
  `q7` int(11) NOT NULL,
  `q8` int(11) NOT NULL,
  `q9` int(11) NOT NULL,
  `q10` int(11) NOT NULL,
  `q11` int(11) NOT NULL,
  `q12` int(11) NOT NULL,
  `q13` int(11) NOT NULL,
  `q14` int(11) NOT NULL,
  `q15` int(11) NOT NULL,
  `q16` int(11) NOT NULL,
  `totalmiles` text COLLATE utf8_unicode_ci,
  `zips` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `table`
--

CREATE TABLE `table` (
  `id_table` int(11) NOT NULL,
  `table_name` varchar(150) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `required` text NOT NULL,
  `columns` text NOT NULL,
  `field` text NOT NULL,
  `uploads` text NOT NULL,
  `relation_1` text NOT NULL,
  `action` text NOT NULL,
  `breadcrumb` text NOT NULL,
  `table_config` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testdate`
--

CREATE TABLE `testdate` (
  `inicio` date NOT NULL,
  `fin` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `file` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `agente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total` double NOT NULL,
  `payment` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tpaymode` int(11) NOT NULL DEFAULT '0',
  `id_agencia` int(10) DEFAULT NULL,
  `agcode` int(10) NOT NULL,
  `addcover` int(11) NOT NULL,
  `free` tinyint(1) NOT NULL DEFAULT '0',
  `label_status` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticketsmanagemet`
--

CREATE TABLE `ticketsmanagemet` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_cashier` int(11) NOT NULL,
  `total_due` double NOT NULL,
  `methodPaymentIns` tinyint(4) NOT NULL,
  `ccIns` double NOT NULL,
  `cashIns` double NOT NULL,
  `TotalccIns` double NOT NULL,
  `methodPaymentOffice` tinyint(4) NOT NULL,
  `ccOffice` double NOT NULL,
  `CashOffice` double NOT NULL,
  `TotalOffice` double NOT NULL,
  `Received` double NOT NULL,
  `ChangeForReceip` double NOT NULL,
  `DateTransacction` datetime NOT NULL,
  `Type` int(11) NOT NULL COMMENT 'office or phone',
  `active` int(11) NOT NULL,
  `inProgress` int(11) NOT NULL,
  `Location` int(11) NOT NULL COMMENT 'agency',
  `LocationTicket` text COLLATE utf8_unicode_ci NOT NULL,
  `dealerDataID` int(11) NOT NULL,
  `id_policy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Tpayment` int(11) NOT NULL,
  `reason` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SecondaryTicket` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposervicios`
--

CREATE TABLE `tiposervicios` (
  `id` int(11) NOT NULL,
  `Nombre` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipovehiculo`
--

CREATE TABLE `tipovehiculo` (
  `id` int(11) NOT NULL,
  `Nombre` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipovehiculo2`
--

CREATE TABLE `tipovehiculo2` (
  `id` int(11) NOT NULL,
  `Nombre` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipovehiculo3`
--

CREATE TABLE `tipovehiculo3` (
  `id` int(11) NOT NULL,
  `Nombre` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usergroup`
--

CREATE TABLE `usergroup` (
  `id` int(11) NOT NULL,
  `nombre` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `fecha_creado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usermanagement`
--

CREATE TABLE `usermanagement` (
  `id` int(11) NOT NULL,
  `id_usurio` int(11) NOT NULL,
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `apellido` text COLLATE utf8_unicode_ci NOT NULL,
  `fechaalta` date NOT NULL,
  `pagomensual` double NOT NULL,
  `pagomensualdrivers` double NOT NULL,
  `policy_type` int(11) NOT NULL,
  `dealer` int(11) NOT NULL,
  `compania` int(11) NOT NULL,
  `total` double NOT NULL,
  `agencia` int(11) NOT NULL,
  `agcode` int(10) NOT NULL,
  `tel` text COLLATE utf8_unicode_ci NOT NULL,
  `mail` text COLLATE utf8_unicode_ci NOT NULL,
  `policynumber` text COLLATE utf8_unicode_ci NOT NULL,
  `ADC` text COLLATE utf8_unicode_ci NOT NULL,
  `addemail` text COLLATE utf8_unicode_ci NOT NULL,
  `addphone` text COLLATE utf8_unicode_ci NOT NULL,
  `homephone` tinytext COLLATE utf8_unicode_ci,
  `workphone` tinytext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nota` text COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nextpaydate` date NOT NULL,
  `dob` date NOT NULL,
  `status_client` int(11) NOT NULL,
  `fecha_mod_satus` date NOT NULL,
  `user_group` int(11) DEFAULT NULL,
  `credit` tinyint(1) NOT NULL DEFAULT '0',
  `CountPendingDays` int(11) NOT NULL,
  `DateOfPrending` date NOT NULL,
  `DateModifyDaysP` date NOT NULL,
  `aplication` tinyint(4) NOT NULL DEFAULT '0',
  `phoneNumber` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `driverLicense` tinyint(4) NOT NULL DEFAULT '0',
  `registration` tinyint(4) NOT NULL DEFAULT '0',
  `pictures` tinyint(4) DEFAULT '0',
  `proofop` tinyint(4) NOT NULL DEFAULT '0',
  `adcsinged` tinyint(4) NOT NULL DEFAULT '0',
  `languajePrefernce` int(11) NOT NULL DEFAULT '0',
  `bank_info` tinyint(4) NOT NULL DEFAULT '0',
  `email_info` tinyint(4) NOT NULL DEFAULT '0',
  `date_exp_adc` date DEFAULT NULL,
  `id_user_update` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `date_update` date NOT NULL,
  `autoordirect` int(11) NOT NULL,
  `dateautopay` date NOT NULL,
  `date_expiration` date NOT NULL,
  `date_requote` date NOT NULL,
  `status_renter` int(11) NOT NULL,
  `verifed_email` tinyint(4) NOT NULL DEFAULT '0',
  `verifed_addres` tinyint(4) NOT NULL DEFAULT '0',
  `verified_phone` tinyint(4) NOT NULL DEFAULT '0',
  `verifed_email_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verifed_addres_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified_phone_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usermanagementdash`
--

CREATE TABLE `usermanagementdash` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `createdDate` date NOT NULL,
  `FirstName` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `Pemail` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `Semail` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `Pphone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `Sphone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `Address` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `DateBirth` date NOT NULL,
  `isLead` tinyint(4) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `inprogres` int(11) NOT NULL,
  `AgencyCode` int(11) NOT NULL,
  `MatrialStatus` tinyint(4) NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ocupation` tinyint(4) NOT NULL,
  `state` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pass` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `apellido` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `dob` date DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `license_number` tinytext COLLATE utf8_unicode_ci,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `general_profile` text COLLATE utf8_unicode_ci,
  `date_login` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fecAlta` date NOT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `email_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pass_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `admin_users` int(11) DEFAULT '0',
  `finance` int(11) DEFAULT NULL,
  `policy` int(11) DEFAULT NULL,
  `isReseller` tinyint(1) NOT NULL DEFAULT '0',
  `nivel` int(11) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) DEFAULT '1',
  `active` int(11) DEFAULT NULL,
  `agent_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date DEFAULT NULL,
  `habilitado` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `id_agente` int(10) DEFAULT NULL,
  `agencia` int(11) DEFAULT NULL,
  `NameAgency` text COLLATE utf8_unicode_ci,
  `agcode` int(10) NOT NULL,
  `id_horario` int(11) NOT NULL DEFAULT '0',
  `isCashier` tinyint(4) DEFAULT '0',
  `isloggedClock` tinyint(4) NOT NULL DEFAULT '0',
  `acesstoekn` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photoimage` text COLLATE utf8_unicode_ci,
  `note_profile` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `state` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `user`, `pass`, `auth_key`, `name`, `apellido`, `dob`, `address`, `license_number`, `phone`, `email`, `general_profile`, `date_login`, `fecAlta`, `ip_address`, `email_date`, `pass_status`, `admin_users`, `finance`, `policy`, `isReseller`, `nivel`, `can_delete`, `active`, `agent_number`, `date_birth`, `habilitado`, `deleted`, `id_agente`, `agencia`, `NameAgency`, `agcode`, `id_horario`, `isCashier`, `isloggedClock`, `acesstoekn`, `photoimage`, `note_profile`, `created_at`, `updated_at`, `state`, `city`, `zipcode`, `visible`) VALUES
(1, 'admin', 'pepe1234', 'kjwJHNLFmPPkz731jzDbfwl6s4W15c_b', 'Omar ', 'Castro', '2020-06-05', NULL, NULL, '', 'AZAGENT505@GMAIL.COM', NULL, '0', '0000-00-00', '177.230.69.165', '0', '0', 0, NULL, NULL, 0, 1, 1, NULL, '', NULL, 1, 0, NULL, 101, NULL, 0, 0, 0, 0, 'ca86303f2ce8631ad21c381121d9c9c8', 'buylowcostTree.png', '', NULL, 1539989374, '', '', '', 1);

--
-- Disparadores `users`
--
DELIMITER $$
CREATE TRIGGER `users_after_update` AFTER UPDATE ON `users` FOR EACH ROW if @updating is null then
        set @updating = 1;
        update clients set pass = new.pass where user = new.user;
        set @updating = null;
    end if
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_admin`
--

CREATE TABLE `users_admin` (
  `id_table` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_phpr`
--

CREATE TABLE `users_phpr` (
  `ID` int(11) NOT NULL,
  `username` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `fullname` varchar(300) DEFAULT NULL,
  `groupid` varchar(300) DEFAULT NULL,
  `active` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_temp`
--

CREATE TABLE `users_temp` (
  `id_usersTemp` smallint(4) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `usersTemp` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fecAlta` date NOT NULL,
  `txt_Activ` varchar(50) NOT NULL,
  `agente_id` int(10) DEFAULT NULL,
  `agencia` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `id_client` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `vin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `year` year(4) DEFAULT NULL,
  `make` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `glass` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `placas` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_agencia` int(10) DEFAULT NULL,
  `imagen1` text COLLATE utf8_unicode_ci,
  `imagen2` text COLLATE utf8_unicode_ci,
  `imagen3` text COLLATE utf8_unicode_ci,
  `imagen4` text COLLATE utf8_unicode_ci,
  `premium` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visit_profiles`
--

CREATE TABLE `visit_profiles` (
  `id` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `agent` int(11) NOT NULL,
  `date_visit` date NOT NULL,
  `time_visit` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adc_endorse`
--
ALTER TABLE `adc_endorse`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adc_nb`
--
ALTER TABLE `adc_nb`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `api`
--
ALTER TABLE `api`
  ADD PRIMARY KEY (`id_api`);

--
-- Indices de la tabla `bankinformation`
--
ALTER TABLE `bankinformation`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id_book`);

--
-- Indices de la tabla `cancellations`
--
ALTER TABLE `cancellations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cashier`
--
ALTER TABLE `cashier`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories_blogs`
--
ALTER TABLE `categories_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `claims`
--
ALTER TABLE `claims`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clients_changelog`
--
ALTER TABLE `clients_changelog`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `clients_visits`
--
ALTER TABLE `clients_visits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clients_visits_reasons`
--
ALTER TABLE `clients_visits_reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clockrecords`
--
ALTER TABLE `clockrecords`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `companyclaims`
--
ALTER TABLE `companyclaims`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `companyes`
--
ALTER TABLE `companyes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `creditos`
--
ALTER TABLE `creditos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cutvone`
--
ALTER TABLE `cutvone`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dalyreport`
--
ALTER TABLE `dalyreport`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dayshorarioclock`
--
ALTER TABLE `dayshorarioclock`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `dealerinformationmanagement`
--
ALTER TABLE `dealerinformationmanagement`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dealers`
--
ALTER TABLE `dealers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `deducsiones`
--
ALTER TABLE `deducsiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `deductionsdash`
--
ALTER TABLE `deductionsdash`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `employeegoals`
--
ALTER TABLE `employeegoals`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `employee_of_month`
--
ALTER TABLE `employee_of_month`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `extracitys`
--
ALTER TABLE `extracitys`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fileshistory`
--
ALTER TABLE `fileshistory`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `files_profile`
--
ALTER TABLE `files_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `follo_ups_dash`
--
ALTER TABLE `follo_ups_dash`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `goalsemployee`
--
ALTER TABLE `goalsemployee`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `horarioclock`
--
ALTER TABLE `horarioclock`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `menu_type`
--
ALTER TABLE `menu_type`
  ADD PRIMARY KEY (`id_menu_type`);

--
-- Indices de la tabla `metodospagopadc`
--
ALTER TABLE `metodospagopadc`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movementslog`
--
ALTER TABLE `movementslog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notas_azdriver`
--
ALTER TABLE `notas_azdriver`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notas_employee`
--
ALTER TABLE `notas_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notas_mgtm`
--
ALTER TABLE `notas_mgtm`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notes_lead`
--
ALTER TABLE `notes_lead`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id_page`);

--
-- Indices de la tabla `payhistory`
--
ALTER TABLE `payhistory`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permits`
--
ALTER TABLE `permits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plocy_status`
--
ALTER TABLE `plocy_status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `policiesforoldmgtm`
--
ALTER TABLE `policiesforoldmgtm`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `policymanagement`
--
ALTER TABLE `policymanagement`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `policy_dealer`
--
ALTER TABLE `policy_dealer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `policy_dealer_company`
--
ALTER TABLE `policy_dealer_company`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `policy_type`
--
ALTER TABLE `policy_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `poliza`
--
ALTER TABLE `poliza`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `precios`
--
ALTER TABLE `precios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `profileemployee`
--
ALTER TABLE `profileemployee`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `profileinfo`
--
ALTER TABLE `profileinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `referenciados`
--
ALTER TABLE `referenciados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `registroclock`
--
ALTER TABLE `registroclock`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `resourcesforpolicy`
--
ALTER TABLE `resourcesforpolicy`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rolclocktime`
--
ALTER TABLE `rolclocktime`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `solicitudp`
--
ALTER TABLE `solicitudp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tablaprovedoresservicio`
--
ALTER TABLE `tablaprovedoresservicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `table`
--
ALTER TABLE `table`
  ADD PRIMARY KEY (`id_table`);

--
-- Indices de la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `ticketsmanagemet`
--
ALTER TABLE `ticketsmanagemet`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiposervicios`
--
ALTER TABLE `tiposervicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipovehiculo`
--
ALTER TABLE `tipovehiculo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipovehiculo2`
--
ALTER TABLE `tipovehiculo2`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `tipovehiculo3`
--
ALTER TABLE `tipovehiculo3`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usergroup`
--
ALTER TABLE `usergroup`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usermanagement`
--
ALTER TABLE `usermanagement`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usermanagementdash`
--
ALTER TABLE `usermanagementdash`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `agent_number` (`agent_number`);

--
-- Indices de la tabla `users_admin`
--
ALTER TABLE `users_admin`
  ADD PRIMARY KEY (`id_table`);

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_phpr`
--
ALTER TABLE `users_phpr`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `users_temp`
--
ALTER TABLE `users_temp`
  ADD PRIMARY KEY (`id_usersTemp`),
  ADD UNIQUE KEY `usuario` (`usersTemp`,`email`);

--
-- Indices de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_client` (`id_client`);

--
-- Indices de la tabla `visit_profiles`
--
ALTER TABLE `visit_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adc_endorse`
--
ALTER TABLE `adc_endorse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `adc_nb`
--
ALTER TABLE `adc_nb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT de la tabla `api`
--
ALTER TABLE `api`
  MODIFY `id_api` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bankinformation`
--
ALTER TABLE `bankinformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `book`
--
ALTER TABLE `book`
  MODIFY `id_book` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cancellations`
--
ALTER TABLE `cancellations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cashier`
--
ALTER TABLE `cashier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categories_blogs`
--
ALTER TABLE `categories_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `claims`
--
ALTER TABLE `claims`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clients_changelog`
--
ALTER TABLE `clients_changelog`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clients_visits`
--
ALTER TABLE `clients_visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clients_visits_reasons`
--
ALTER TABLE `clients_visits_reasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clockrecords`
--
ALTER TABLE `clockrecords`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `companyclaims`
--
ALTER TABLE `companyclaims`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `companyes`
--
ALTER TABLE `companyes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `creditos`
--
ALTER TABLE `creditos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cutvone`
--
ALTER TABLE `cutvone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dalyreport`
--
ALTER TABLE `dalyreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dayshorarioclock`
--
ALTER TABLE `dayshorarioclock`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dealerinformationmanagement`
--
ALTER TABLE `dealerinformationmanagement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dealers`
--
ALTER TABLE `dealers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `deducsiones`
--
ALTER TABLE `deducsiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `deductionsdash`
--
ALTER TABLE `deductionsdash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `employeegoals`
--
ALTER TABLE `employeegoals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `employee_of_month`
--
ALTER TABLE `employee_of_month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `extracitys`
--
ALTER TABLE `extracitys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fileshistory`
--
ALTER TABLE `fileshistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `files_profile`
--
ALTER TABLE `files_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `follo_ups_dash`
--
ALTER TABLE `follo_ups_dash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `goalsemployee`
--
ALTER TABLE `goalsemployee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `horarioclock`
--
ALTER TABLE `horarioclock`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menu_type`
--
ALTER TABLE `menu_type`
  MODIFY `id_menu_type` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `metodospagopadc`
--
ALTER TABLE `metodospagopadc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `movementslog`
--
ALTER TABLE `movementslog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `notas_azdriver`
--
ALTER TABLE `notas_azdriver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `notas_employee`
--
ALTER TABLE `notas_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `notas_mgtm`
--
ALTER TABLE `notas_mgtm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `notes_lead`
--
ALTER TABLE `notes_lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `owner`
--
ALTER TABLE `owner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `page`
--
ALTER TABLE `page`
  MODIFY `id_page` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payhistory`
--
ALTER TABLE `payhistory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permits`
--
ALTER TABLE `permits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plocy_status`
--
ALTER TABLE `plocy_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `policiesforoldmgtm`
--
ALTER TABLE `policiesforoldmgtm`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `policymanagement`
--
ALTER TABLE `policymanagement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `policy_dealer`
--
ALTER TABLE `policy_dealer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `policy_dealer_company`
--
ALTER TABLE `policy_dealer_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `policy_type`
--
ALTER TABLE `policy_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `poliza`
--
ALTER TABLE `poliza`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `precios`
--
ALTER TABLE `precios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `profileemployee`
--
ALTER TABLE `profileemployee`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `profileinfo`
--
ALTER TABLE `profileinfo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `referenciados`
--
ALTER TABLE `referenciados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `registroclock`
--
ALTER TABLE `registroclock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `resourcesforpolicy`
--
ALTER TABLE `resourcesforpolicy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rolclocktime`
--
ALTER TABLE `rolclocktime`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `solicitudp`
--
ALTER TABLE `solicitudp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tablaprovedoresservicio`
--
ALTER TABLE `tablaprovedoresservicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `table`
--
ALTER TABLE `table`
  MODIFY `id_table` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ticketsmanagemet`
--
ALTER TABLE `ticketsmanagemet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tiposervicios`
--
ALTER TABLE `tiposervicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipovehiculo`
--
ALTER TABLE `tipovehiculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipovehiculo2`
--
ALTER TABLE `tipovehiculo2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipovehiculo3`
--
ALTER TABLE `tipovehiculo3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usergroup`
--
ALTER TABLE `usergroup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usermanagement`
--
ALTER TABLE `usermanagement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usermanagementdash`
--
ALTER TABLE `usermanagementdash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users_admin`
--
ALTER TABLE `users_admin`
  MODIFY `id_table` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_phpr`
--
ALTER TABLE `users_phpr`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_temp`
--
ALTER TABLE `users_temp`
  MODIFY `id_usersTemp` smallint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `visit_profiles`
--
ALTER TABLE `visit_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
