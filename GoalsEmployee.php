<?
session_start();
if (isset($_SESSION['nivel'])) {

} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");
$id = $_SESSION['id'];
$sql = "SELECT * FROM `users` WHERE `id` = $id AND `deleted` = 0";
$result = mysql_query($sql);
$employee = mysql_fetch_assoc($result);
if ($employee) {
    $user = $employee['user'];
    $name = $employee['name'];
    $apellido = $employee['apellido'];
    $email = $employee['email'];
    $nivel = $employee['nivel'];
    $dob = $employee['dob'];
}

function _data_last_month_day() {
    $month = date('m');
    $year = date('Y');
    $day = date("d", mktime(0, 0, 0, $month + 1, 0, $year));

    return date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
}

/** Actual month first day * */
function _data_first_month_day() {
    $month = date('m');
    $year = date('Y');
    return date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
}

$date1 = _data_first_month_day();
$date2 = _data_last_month_day();

$sql = "SELECT * FROM `GoalsEmployee` where user ='$user' and StartDate='$date1' and EndDate='$date2' ";

$result = mysql_query($sql);
$data = mysql_fetch_assoc($result);
$NB_ADC = $data['NB_ADC'];
$NB_Policy = $data['NB_Policy'];

$getADC = "SELECT count(*) as adcs FROM `tickets` JOIN clients on clients.id=tickets.id_client where agente='$user' and date BETWEEN '$date1' and '$date2' and clients.plan>1 and label_status='New_Business'";
$resultadc = mysql_query($getADC);
$dataadc = mysql_fetch_assoc($resultadc);
$adcs = $dataadc['adcs'];


$getNBP = "SELECT count(id) as bnpolicies FROM `cutvone`  as cv where ( cv.Dpayment=1 or cv.Dpayment=3  or cv.Dpayment=3 or cv.Dpayment=5 or cv.Dpayment=10 ) and id_usuario=$id and fecha BETWEEN '$date1' and '$date2' and cv.erase=0";
$resultNBP = mysql_query($getNBP);
$dataNBP = mysql_fetch_assoc($resultNBP);
$bnpolicies = $dataNBP['bnpolicies'];

$totalsNB = $NB_ADC + $NB_Policy;

$ADCINPERCENT = ($NB_ADC * 100) / $totalsNB;
$NBINPERCENT = ($NB_Policy * 100) / $totalsNB;

$currentP_policies = ($bnpolicies * $NBINPERCENT) / $NB_Policy;
$currentP_adc = ($adcs * $ADCINPERCENT) / $NB_ADC;


$ADCVal = ($adcs * 100) / $NB_ADC;
$NBVal = ($bnpolicies * 100) / $NB_Policy;
$colorADC = "";
$colorNBP = "";

if ($ADCVal >= 80) {
    $colorADC = "#00FF00";
} else if ($ADCVal >= 30 && $ADCVal < 80) {
    $colorADC = "#FFFF00";
} else if ($ADCVal < 30) {
    $colorADC = "#FF0000";
}


if ($NBVal >= 80) {
    $colorNBP = "#00FF00";
} else if ($NBVal >= 30 && $NBVal < 80) {
    $colorNBP = "#FFFF00";
} else if ($NBVal < 30) {
    $colorNBP = "#FF0000";
}
if(!$ADCVal>0){
  $ADCVal=0;
}

if(!$NBVal>0){
  $NBVal=0;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="sweetalert2/sweetalert2.min.js"></script>
        <script src="js/jquery-validation/jquery.validate.js"></script>
        <script src="js/tooltipster/js/tooltipster.bundle.min.js"></script>
        <script src="js/kartik-v-bootstrap-fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
        <script src="js/kartik-v-bootstrap-fileinput/js/fileinput.min.js"></script>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="js/tooltipster/css/tooltipster.bundle.min.css" />
        <link href="js/kartik-v-bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <title> </title>
        <style type="text/css">
            html,body{
                height: 100%;
            }
            .navbar-default {
                background-color: black !important;
                border-color: white !important;
            }

            .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
                color: white !important;
                background-color: #555555 !important;
            }

            .cabecera{
                text-align: center !important;
            }

            form .show-on-dealer{
                display: none;
            }
            form.dealer .show-on-dealer{
                display: block;
            }
            .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
                margin: 0;
                padding: 0;
                border: none;
                box-shadow: none;
                text-align: center;
            }
            .kv-avatar {
                display: inline-block;
            }
            .kv-avatar .file-input {
                display: table-cell;
                width: 213px;
            }
            .kv-reqd {
                color: red;
                font-family: monospace;
                font-weight: normal;
            }
        </style>
    </head>
    <body >
        
           
                <? include("menu_mgtm_boostrap.php"); ?>
       
            <div class="row">
                <div class="col-md-2">
                    <canvas id="myChart3" ></canvas>
                </div>
                <div class="col-md-2">
                    <canvas id="myChart4" ></canvas>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped">
                        <thead>
                            <tr>

                                <th scope="col">ADC Required</th>
                                <th scope="col">NB Policies Required</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>


                                <td><?= $NB_ADC ?></td>
                                <td><?= $NB_Policy ?></td>
                            </tr>
                        <thead>
                            <tr>

                                <th scope="col">current progress</th>
                                <th scope="col">current progress</th>

                            </tr>
                        </thead>

                        <tr>


                            <td><?= $adcs ?></td>
                            <td><?= $bnpolicies ?></td>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>



       

        <script type="text/javascript">
            var adc =<?= $ADCVal?>;
            var policies =<?= $NBVal ?>;
            var pendingaDC = pendingaDC = 100 - adc;
            var pendingpolicies = 100 - policies;
            data = {
                datasets: [{
                        data: [adc,pendingaDC],
                        label: "Total Production",
                        backgroundColor: [
                            '<?= $colorADC ?>'
                        ],
                        fill: false
                    },
                ],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [

                    'ADC',
                    'Pending'
                ]
            };


            data2 = {
                datasets: [{
                        data: [policies,pendingpolicies],
                        label: "Total Production",
                        backgroundColor: [
                            '<?= $colorNBP ?>'
                        ],
                        fill: false
                    },
                ],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    'Policies',
                    'Pending'

                ]
            };

            var ctx = document.getElementById('myChart3');
            var myDoughnutChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,

            });


            var ctx2 = document.getElementById('myChart4');
            var myDoughnutChart = new Chart(ctx2, {
                type: 'doughnut',
                data: data2,

            });
        </script>
    </body>
</html>
