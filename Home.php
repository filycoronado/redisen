<?
include("inc/dbconnection.php");
error_reporting(1);
session_start();
$idUser = 0;
$niveluser = 0;
if (isset($_SESSION['nivel'])) {
    $niveluser = $_SESSION['nivel'];
    $idUser = $_SESSION['id'];
} else {

    header('Location: index.php');
}
//include_once("./App/Back/Conection.php");
$enabled = "disabled";
if ($idUser == 2195 || $idUser == 2193 || $idUser == 1 || $idUser == 2117 || $idUser == 2155 || $idUser == 2149 || $idUser == 2127 || $idUser == 2162 || $idUser == 2166   || $idUser == 2189) {
    $enabled = "";
}
?>
<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Home</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./../css/Redisen.css">


    <script src="App/js/angular1.7.2.js"></script>
    <script data-require="angular-translate@*" data-semver="2.5.0" src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.5.0/angular-translate.js"></script>
    <script src="https://code.angularjs.org/1.6.0/angular-route.min.js"></script>
    <script src="https://code.angularjs.org/1.6.9/angular-cookies.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.6.3/mask.js"></script>

</head>

<body>
    <div class="containder-fuild">
        <? include("./../components/NavBar.php"); ?>
        <div class="container">
            <div class="row p-5" ng-controller="TransactionsController">
                <div class="col-md-12">

                    <form ng-submit="applyFilter()" class="p-2">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group">

                                    <select class="form-control" ng-model="filters.agency">
                                        <option ng-repeat="a in agencies" value="{{a.number}}">{{a.number}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input model-view-value="true" name="date" type="text" class="form-control" placeholder="MM-DD-YYYY" ng-model="filters.currentDate" ui-mask="99-99-9999" ui-mask-placeholder-char="-">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-danger mx-auto " type="submit">Apply</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-11 mx-auto col-12">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table  table-borderless table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Company</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">User</th>
                                        <th scope="col">Cashier</th>
                                        <th scope="col">Delivery</th>
                                        <th scope="col">Insurance</th>
                                        <th scope="col">Office</th>
                                        <th scope="col">RoadSide</th>
                                        <th scope="col">Method Payment</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Company name</td>
                                        <td>  <i class="fa fa-id-card-o ico_actions" aria-hidden="true"></i>  Customer name</td>
                                        <td>Username</td>
                                        <td>Cashier name</td>
                                        <td>Delivery Name</td>
                                        <td>$250</td>
                                        <td>$250</td>
                                        <td>$250</td>
                                        <td>CASH</td>
                                        <td>
                                            <i class="fa fa-file-pdf-o m-1 ico_actions" aria-hidden="true"></i>
                                            <i class="fa fa-pencil-square m-1 ico_actions " aria-hidden="true"></i>
                                            <i class="fa fa-minus-square m-1 ico_actions" aria-hidden="true"></i>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <? include("./../components/footer.php"); ?>

    </div>
</body>
<script src="App/js/App.js"></script>
<script src="App/js/Controllers/LoginController.js"></script>
<script src="App/js/Controllers/TransactionsController.js"></script>