<?
session_start();
if (isset($_SESSION['nivel'])) {
    
} else {

    header('Location: index.php');
}
include("inc/dbconnection.php");

$iduser = $_SESSION['id'];
$username = $_SESSION['username'];
$idCustomer = "";
if (isset($_GET['transaction_number'])) {
    $idCustomer = $_GET['transaction_number'];
    $sql = "SELECT * FROM `cutvone` where id=$idCustomer";
} else {
    $sql = "SELECT * FROM `cutvone` where id=4207";
}
$Transaction = mysql_query($sql);
$rowT = mysql_fetch_assoc($Transaction);
$dealershipTransaction=$rowT['dealership'];
if(!$dealershipTransaction>0){
    $dealershipTransaction=-1;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="sweetalert2/sweetalert2.min.js"></script>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
    <title> Report</title>

    <style type="text/css">
        html,
        body {
            height: 100%;
        }

        .navbar-default {
            background-color: black !important;
            border-color: white !important;
        }

        .navbar-default .navbar-nav>li>a:focus,
        .navbar-default .navbar-nav>li>a:hover {
            color: white !important;
            background-color: #555555 !important;
        }

        .fondo {
            background-image: url(img/logo.svg);
            background-size: contain;
            height: 50px;
            width: 133px;
            background-repeat: no-repeat;
        }

        .cabecera {
            text-align: center !important;
        }
    </style>
</head>

<body>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#cont-input").remove();
            $("#btn-go").remove();
            $("#datepicker").datepicker();
        });
    </script>


    <div style="height:100%">
        <nav class="navbar navbar-default" role="navigation">
            <? include("menu_mgtm_boostrap.php"); ?>
        </nav>


        <div class="container" style="max-height: 86% !important; height: 86%; overflow:auto;">
            <div style="text-align: center;">

                <h3></h3>

            </div>
            <div style="height: 5rem;background-color: #1c335c;color: white; text-align: center;     border-radius: 6px 6px 0px 0px;">
                <h3 style="padding: 11px;"> Edit Transaction </h3>
            </div>


            <div class="row">
                <form class="credit-card-div  col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">



                            <div class="row">
                                <div class="col-md-3">
                                    <span class="help-block text-muted small-font"> Customer Name:</span>
                                    <input id="nombre" type="text" class="form-control" placeholder="Jhon Due" value="<?= $rowT['nameclient']; ?>" />
                                </div>
                                <div class="col-md-3">
                                    <span class="help-block text-muted small-font">Responsible user:</span>
                                    <select id="User" class="form-control selectpicker">
                                        <option value="0" <? if ($row['languajePrefernce']==0) { ?>selected
                                            <? } ?>>Select User</option>
                                        <?
                                            $sqlUsers = "SELECT id, user FROM `users` where (agencia=101 or agencia=103 or agencia=104 or agencia=105 or agencia=102 ) and (nivel=1 or nivel=5 or nivel=2)and habilitado=1 and visible=1";
                                            $result = mysql_query($sqlUsers);
                                            while ($row = mysql_fetch_assoc($result)) {
                                                ?>
                                        <option value="<?= $row['id']; ?>" <? if ($rowT['id_usuario']==$row['id']) { ?>selected
                                            <? } ?>><?= $row['user'] ?></option>
                                        <? } ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <span class="help-block text-muted small-font">Date</span>
                                    <input id="datepicker" type="text" class="form-control" placeholder="" value="<?= $rowT['fecha']; ?>" />
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font">Insurance Amount:</span>
                                    <input id="insuranceAmount" type="number" class="form-control" placeholder="100.0" value="<?= $rowT['feecompany']; ?>" />
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font">Office Amount:</span>
                                    <input id="Feeamount" type="number" class="form-control" placeholder="W Ajo way 25" value="<?= $rowT['feeoffice']; ?>" />
                                </div>
                                <div class="col-md-3 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font"> Insurance Method Payment</span>
                                    <select id="InsuranceMethod" class="form-control selectpicker">
                                        <option value="0" <? if ($rowT['companytype']==0) { ?>selected
                                            <? } ?>>Select</option>
                                        <option value="1" <? if ($rowT['companytype']==1) { ?>selected
                                            <? } ?>>Cash pmt</option>
                                        <option value="2" <? if ($rowT['companytype']==2) { ?>selected
                                            <? } ?>>Card Chase ptm </option>
                                        <option value="3" <? if ($rowT['companytype']==3) { ?>selected
                                            <? } ?>>Card ptm Direct Company</option>
                                    </select>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font">ADC Amount:</span>
                                    <input id="ADCamount" type="number" class="form-control" placeholder="100.0" value="<?= $rowT['feeforaz']; ?>" />
                                </div>

                                <div class="col-md-3 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font"> ADC Method Payment</span>
                                    <select id="ADCMethod" class="form-control selectpicker">
                                        <option value="0" <? if ($rowT['languajePrefernce']==0) { ?>selected
                                            <? } ?>>Select </option>
                                        <option value="1" <? if ($rowT['typeaz']==1) { ?>selected
                                            <? } ?>>Cash</option>
                                        <option value="2" <? if ($rowT['typeaz']==2) { ?>selected
                                            <? } ?>>Card Chase</option>
                                        <option value="3" <? if ($rowT['typeaz']==3) { ?>selected
                                            <? } ?>>Card Direct Payment Drivers</option>
                                    </select>
                                </div>

                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font">Deealer:</span>
                                    <input id="feeDealer" type="number" class="form-control" placeholder="100.0" value="<?= $rowT['feedealer']; ?>" />
                                </div>


                                <div class="col-md-3 col-sm-2 col-xs-12">
                                    <span class="help-block text-muted small-font"> Status Payment</span>

                                    <select id="status_payment" class="form-control selectpicker">
                                        <option value="0" <? if ($rowT['Dpayment']==0) { ?>selected
                                            <? } ?>>Erase</option>
                                        <option value="1" <? if ($rowT['Dpayment']==1) { ?>selected
                                            <? } ?>>New Busisness</option>
                                        <option value="2" <? if ($rowT['Dpayment']==2) { ?>selected
                                            <? } ?>>Payment</option>
                                        <option value="3" <? if ($rowT['Dpayment']==3) { ?>selected
                                            <? } ?>>Payment EFT</option>
                                        <option value="4" <? if ($rowT['Dpayment']==4) { ?>selected
                                            <? } ?>>Endorse</option>
                                        <option value="5" <? if ($rowT['Dpayment']==5) { ?>selected
                                            <? } ?>>DownPayment EFT</option>
                                        <option value="6" <? if ($rowT['Dpayment']==6) { ?>selected
                                            <? } ?>>Cancellation</option>
                                        <option value="7" <? if ($rowT['Dpayment']==7) { ?>selected
                                            <? } ?>>REINSTATEMENT </option>
                                        <option value="8" <? if ($rowT['Dpayment']==8) { ?>selected
                                            <? } ?>>RENEWAL </option>
                                        <option value="9" <? if ($rowT['Dpayment']==9) { ?>selected
                                            <? } ?>>NEW BUSINESS TRANSFER </option>
                                        <!--<option value="10" <? if ($rowT['Dpayment'] == 10) { ?>selected<? } ?>>PAY IN FULL </option>-->
                                        <option value="11" <? if ($rowT['Dpayment']==11) { ?>selected
                                            <? } ?>>AGENT OF RECORD CHANGE </option>
                                        <option value="12" <? if ($rowT['Dpayment']==12) { ?>selected
                                            <? } ?>>NEW BUSINESS TRANSFER EFT </option>

                                    </select>
                                </div>

                            </div>
                            <div class="row ">
                                <div class="col-md-3 pad-adjust">
                                    <span class="help-block text-muted small-font">Cashier</span>

                                    <select id="Cashiers" class="form-control selectpicker">
                                        <option value="0" <? if ($row['languajePrefernce']==0) { ?>selected
                                            <? } ?>>Select Cashier</option>
                                        <?
                                            $sqlCashiers = "SELECT id, user FROM `users` where (agencia=101 or agencia=103 or agencia=104 or agencia=105 ) and (nivel=1 or nivel=5 or nivel=2)and habilitado=1 and isCashier=1";
                                            $result = mysql_query($sqlCashiers);
                                            while ($row = mysql_fetch_assoc($result)) {
                                                ?>
                                        <option value="<?= $row['id']; ?>" <? if ($rowT['id_cashier']==$row['id']) { ?>selected
                                            <? } ?>><?= $row['user'] ?></option>
                                        <? } ?>
                                    </select>

                                </div>

                                <div class="col-md-3 pad-adjust">
                                    <span class="help-block text-muted small-font">agency</span>

                                    <input id="Agencyid" type="number" class="form-control" value="<?= $rowT['agency']; ?>" />

                                </div>


                            </div>
                            <div class="row ">
                                <div class="col-md-3 pad-adjust">
                                    <span class="help-block text-muted small-font">Dealeship</span>
                                    <select id="Dealership" class="form-control selectpicker">
                                        <option value="-1" <?if($dealershipTransaction==-1){?>selected
                                            <?}?>>None</option>
                                        <?
                                       
                                      
                                        $slqDEaler="SELECT id,nombre FROM `dealers` where agency=101 and habilitado=1";
                                        $resultd=mysql_query($slqDEaler);
                                        while($row= mysql_fetch_assoc($resultd)){
                                       ?>

                                        <option value="<?= $row['id']; ?>" <? if ($dealershipTransaction==$row['id']) { ?>selected
                                            <? } ?>><?= $row['nombre'] ?></option>
                                        <?}?>
                                    </select>

                                </div>
                            </div>



                        </div>
                        <div class="row ">
                            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                <input type="button" onclick="Update();" class="btn btn-success" value="Update Transaction" />
                            </div>

                        </div>

                    </div>

                </form>
            </div>



        </div>
        <div class="panel-footer">Panel Footer</div>
    </div>


</body>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>



<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
    $("#datepicker").datepicker();
    $("#datefinal").datepicker();


    function Update() {
        var feeDealer = $("#feeDealer").val();
        var Cashiers = $("#Cashiers").val();
        var ADCMethod = $("#ADCMethod").val();
        var Agencyid = $("#Agencyid").val();
        var InsuranceMethod = $("#InsuranceMethod").val();
        var Feeamount = $("#Feeamount").val();
        var ADCamount = $("#ADCamount").val();
        var insuranceAmount = $("#insuranceAmount").val();
        var status_payment = $("#status_payment").val();
        var user = $("#User").val();
        var id_transaction = <?= $idCustomer ?>;
        var dealership = $("#Dealership").val();
        var date = $("#datepicker").val();

       
        $.post("UpdateTransaction.php", {
            Agencyid,
            Cashiers,
            ADCMethod,
            InsuranceMethod,
            ADCamount,
            Feeamount,
            insuranceAmount,
            user,
            id_transaction,
            status_payment,
            dealership,
            feeDealer,
            date
        }, function(data) {
            alert(data.message);
        });


    }
</script>

</html>